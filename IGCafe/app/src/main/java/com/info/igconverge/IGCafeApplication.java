package com.info.igconverge;

import com.info.igconverge.carpool.CarpoolClass;
import com.instabug.library.Instabug;
import com.instabug.library.InstabugFeedbackActivity;
import com.instabug.library.Instabug.IBGInvocationEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.util.LruCache;
import android.widget.Toast;

import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.emergency.IGCEmergencyActivity;
import com.info.igconverge.emergency.ShakeDetector;
import com.info.igconverge.emergency.ShakeDetector.OnShakeListener;
import com.info.igconverge.login.IGCLoginScreenActivity;
import com.info.igconverge.model.IGCEvent;
import com.info.igconverge.model.IGCEventItem;
import com.info.igconverge.model.IGCMenu;
import com.info.igconverge.model.IGCRatingMenu;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.weather.WeatherService;

public class IGCafeApplication extends MultiDexApplication {
	static ArrayList<IGCEvent> igcEventList;
	static ArrayList<CarpoolClass> igcRideList;
	static ArrayList<CarpoolClass> igAllNotifyList;
	static ArrayList<IGCMenu> igcMenuList;

	static ArrayList<IGCRatingMenu> igcRatingList;
	public static SensorManager mSensorManager;
	public static Sensor mAccelerometer;
	public static ShakeDetector mShakeDetector;
	private static Application applicationObj;
	public static LruCache<String, Bitmap> mMemoryCache;
	MediaPlayer mp;
	private static boolean activityVisible, notificationVisible, eventVisible,carPoolVisible;

	public static boolean isActivityVisible() {
		return activityVisible;
	}

	public static void activityResumed() {
		activityVisible = true;
	}

	public static void activityPaused() {
		activityVisible = false;
	}

	public static boolean isNotVisible() {
		return notificationVisible;
	}

	public static void NotctivityResumed() {
		notificationVisible = true;
	}

	public static void NotActivityPaused() {
		notificationVisible = false;
	}

	public static boolean isEventActivityVisible() {
		return eventVisible;
	}

	public static void eventActivityResumed() {
		eventVisible = true;
	}

	public static void eventActivityPaused() {
		eventVisible = false;
	}

	public static boolean isCarPoolNotVisible() {
		return carPoolVisible;
	}

	public static void carPoolNotctivityResumed() {
		carPoolVisible = true;
	}

	public static void carPoolNotActivityPaused() {
		carPoolVisible = false;
	}

	public void onCreate() {
		applicationObj = this;
		callInstabug();
		setMemoryCache();
		registerShakeFeature();

		super.onCreate();

	}

	public static Application newInstance() {
		return applicationObj;

	}

	public void registerShakeFeature() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mShakeDetector = new ShakeDetector();
		mShakeDetector.setOnShakeListener(new OnShakeListener() {

			@Override
			public void onShake(int count) {

				Intent i = new Intent(getApplicationContext(),
						IGCEmergencyActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
			}
		});

		mSensorManager.registerListener(mShakeDetector, mAccelerometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	public void setMemoryCache() {
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

		// Use 1/8th of the available memory for this memory cache.
		final int cacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				return bitmap.getByteCount() / 1024;
			}
		};
	}

	/**
	 * Google Analytics
	 */

	// change the following line
	private static final String PROPERTY_ID = "UA-64586141-3";

	public static int GENERAL_TRACKER = 0;

	public enum TrackerName {
		APP_TRACKER, GLOBAL_TRACKER, ECOMMERCE_TRACKER,
	}

	public static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public IGCafeApplication() {
		super();
	}

	public synchronized Tracker getTracker(TrackerName appTracker) {
		if (!mTrackers.containsKey(appTracker)) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = (appTracker == TrackerName.APP_TRACKER) ? analytics
					.newTracker(PROPERTY_ID)
					: (appTracker == TrackerName.GLOBAL_TRACKER) ? analytics
							.newTracker(R.xml.global_tracker) : analytics

					.newTracker(R.xml.ecommerce_tracker);
			mTrackers.put(appTracker, t);
		}
		return mTrackers.get(appTracker);
	}

	public static ArrayList<CarpoolClass> getIgAllNotifyList() {
		return igAllNotifyList;
	}

	public static void setIgAllNotifyList(ArrayList<CarpoolClass> igAllNotifyList) {
		IGCafeApplication.igAllNotifyList = igAllNotifyList;
	}

	public static ArrayList<IGCEvent> getIgcEventList() {
		return igcEventList;
	}

	public static void setIgcEventList(ArrayList<IGCEvent> igcEventList) {
		IGCafeApplication.igcEventList = igcEventList;
	}
	public static ArrayList<CarpoolClass> getIgcRideList() {
		return igcRideList;
	}

	public static void setIgcRideList(ArrayList<CarpoolClass> igcRideList) {
		IGCafeApplication.igcRideList = igcRideList;
	}

	public static ArrayList<IGCMenu> getIgcMenuList() {
		return igcMenuList;
	}

	public static void setIgcMenuList(ArrayList<IGCMenu> igcMenuList) {
		IGCafeApplication.igcMenuList = igcMenuList;
	}


	public static ArrayList<IGCRatingMenu> getIgcRatingList() {
		return igcRatingList;
	}

	public static void setIgcRatingList(ArrayList<IGCRatingMenu> igcRatingList) {
		IGCafeApplication.igcRatingList = igcRatingList;
	}

	private void callInstabug() {
		int bgColor = getResources().getColor(R.color.whitecolor);
		int orangeColor = getResources().getColor(R.color.orange);
		int transparentColor = getResources().getColor(R.color.transparent);
		Drawable headerBg = getResources().getDrawable(R.drawable.header_bg);
		// Drawable headerBg=getResources().getDrawable(R.drawable.);
		String email = IGCSharedPreferences.getEmail(this);
		Instabug.initialize(this, IGCConstants.instabugId)
				.setInvocationEvent(IBGInvocationEvent.IBGInvocationEventNone)
				.setAnnotationActivityClass(InstabugFeedbackActivity.class)
				.setPreReportRunnable(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!IGCCheckNetworkConnectivity
								.checkConnection(getApplicationContext())) {
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.no_internet_body),
									Toast.LENGTH_SHORT).show();
						}
					}
				})

				.setPreSendingRunnable(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						if (!IGCCheckNetworkConnectivity
								.checkConnection(getApplicationContext())) {
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.no_internet_body),
									Toast.LENGTH_SHORT).show();

						}
					}
				})
				.setPostBugReportMessage(
						getResources().getString(R.string.feedback_success))
				.setShowTutorial(true)
				.setShowIntroDialog(true, IGCDashboardActivity.class)
				.setTrackUserSteps(true)
				.setWillShowStartAlert(true, IGCDashboardActivity.class)
				// .setDefaultEmail(null)
				// .setShakingThreshold(1f)
				.enableEmailField(false, false)
				.setInvalidCommentAlertText(
						getResources().getString(R.string.blank_feedback_msg))
				.setCommentIsRequired(true)
				// .setWillShowEmailField(false)
				.setIsTrackingUserSteps(true)
				.setPromptColors(
						this.getResources().getColor(R.color.whitecolor),
						this.getResources().getColor(R.color.orange))
				.setSdkStyle(null, headerBg, bgColor, headerBg, bgColor)
				.setEnableOverflowMenuItem(true).setDebugEnabled(false);
		// VolleyLog.DEBUG = false;

	}
	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

}
