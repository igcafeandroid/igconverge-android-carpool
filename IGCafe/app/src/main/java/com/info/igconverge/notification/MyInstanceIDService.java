package com.info.igconverge.notification;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;
import com.info.igconverge.utility.IGCConstants;

public class MyInstanceIDService extends InstanceIDListenerService {
	public void onTokenRefresh() {
		// refreshAllTokens();

		Intent intent = new Intent(this, RegistrationIntentService.class);
		intent.putExtra("recreate", "recreate");
		startService(intent);
	}

	// private void refreshAllTokens() {
	// // assuming you have defined TokenList as
	// // some generalized store for your tokens
	// ArrayList<TokenList> tokenList = TokensList.get();
	// InstanceID iid = InstanceID.getInstance(this);
	// for(tokenItem : tokenList) {
	// tokenItem.token =
	// iid.getToken(tokenItem.authorizedEntity,tokenItem.scope,tokenItem.options);
	// // send this tokenItem.token to your server
	// }
	// }
};
