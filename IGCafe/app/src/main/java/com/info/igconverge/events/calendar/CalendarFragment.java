package com.info.igconverge.events.calendar;


import java.util.ArrayList;
import java.util.Calendar;

import com.info.igconverge.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class CalendarFragment extends DialogFragment implements
		OnDayClickListener {
	ArrayList<String> eventDates;
	ArrayList<String> eventDays = new ArrayList<String>();
	Dialog dialog;
	String selected;
	TextView dayView;
	DialogFragment dialogFragment;
	Button resetButton;
	MonthView calendarView;
	String[] monthName = { "January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October", "November",
			"December" };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		dialog = getDialog();
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogFragment = this;
		getExtraDataFromArguments();
		MonthView.setEventDates(eventDates, dialogFragment, selected);
		View view = inflater.inflate(R.layout.calendar, container, false);
		initialiseViews(view);
		setData();

		return view;
	}

	public void reset() {
		Intent data = new Intent();
		data.putExtra("date", "");
		getTargetFragment().onActivityResult(getTargetRequestCode(), 2, data);
		dialog.dismiss();
	}

	@Override
	public void onClick(String date) {
		Intent data = new Intent();
		data.putExtra("date", date);
		selected = date;
		getTargetFragment().onActivityResult(getTargetRequestCode(), 2, data);
		dialog.dismiss();

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		MonthView.setActivity(activity);
	}

	private void initialiseViews(View view) {
		calendarView = (MonthView) view.findViewById(R.id.monthView);
		resetButton = (Button) view.findViewById(R.id.reset);
		dayView = (TextView) view.findViewById(R.id.date);
		resetButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				reset();

			}
		});

	}

	private void setData() {
		Typeface myTypefaceRegular = Typeface.createFromAsset(getActivity()
				.getAssets(), "font/Roboto-Medium.ttf");
		dayView.setTypeface(myTypefaceRegular);
		setDayView();

	}

	private void setDayView() {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		//int month = c.get(Calendar.MONTH);

		String mon = monthName[c.get(Calendar.MONTH)];
		int day = c.get(Calendar.DATE);

		dayView.setText(mon + " " + String.valueOf(day) + ", "
				+ String.valueOf(year));
	}

	private void getExtraDataFromArguments() {
		if (getArguments() != null) {
			eventDates = getArguments().getStringArrayList("eventDates");
			selected = getArguments().getString("selecetdDate");

		}
	
	}
}