package com.info.igconverge.carpool;

/**
 * Created by lab2 on 05/01/16.
 */
public class EditMessageClass {
    String message;
    Boolean status;

    public EditMessageClass() {
    }

    public EditMessageClass(String message, Boolean status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }


}
