package com.info.igconverge.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.info.igconverge.R;
import com.info.igconverge.model.IGCMenuItem;

public class IGCMenuItemsAdapter extends BaseAdapter {
	Context context;
	int layoutResourceId;
	ArrayList<IGCMenuItem> menuItemList = new ArrayList<IGCMenuItem>();

	static class ViewholderItem {
		TextView itemTextView;
		TextView priceTextView;
		TextView priceRsView;
	}

	public IGCMenuItemsAdapter(Activity context,
			ArrayList<IGCMenuItem> menuItemList) {
		this.context = context;
		this.menuItemList = menuItemList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewholderItem holder;
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(R.layout.menu_items, parent, false);
			holder = new ViewholderItem();
			holder.itemTextView = (TextView) row.findViewById(R.id.item_name);
			holder.priceTextView = (TextView) row.findViewById(R.id.item_price);
			holder.priceRsView = (TextView) row.findViewById(R.id.item_Rs);
			row.setTag(holder);

		} else {
			holder = (ViewholderItem) row.getTag();
		}

		IGCMenuItem item = menuItemList.get(position);
	
		holder.itemTextView.setText(item.getItem());
		if (item.getPrice() == null || item.getPrice().equals("0")) {
			holder.priceTextView.setVisibility(View.GONE);
			holder.priceRsView.setVisibility(View.GONE);

		} else {
			holder.priceTextView.setText(item.getPrice());

		}

	
		return row;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return menuItemList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return menuItemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
