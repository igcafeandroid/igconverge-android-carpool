package com.info.igconverge.model;

public class IGCNavDrawerItem {
	
	private String title;
	int resourceDrawable;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getResourceDrawable() {
		return resourceDrawable;
	}
	public void setResourceDrawable(int resourceDrawable) {
		this.resourceDrawable = resourceDrawable;
	}
	public IGCNavDrawerItem(String title, int resourceDrawable) {
		super();
		this.title = title;
		this.resourceDrawable = resourceDrawable;
	}
	public IGCNavDrawerItem() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	

	

}
