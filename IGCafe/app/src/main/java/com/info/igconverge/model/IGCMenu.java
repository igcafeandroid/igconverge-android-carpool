package com.info.igconverge.model;

import java.util.ArrayList;
import java.util.List;

public class IGCMenu {
	String menuId;
	String menuName;
	String menuFeedback;
	String menuDate;
	int bellCount;

	List<IGCMenuItem> menuItems = new ArrayList<IGCMenuItem>();
	List<IGCMenuItem> menuPaidItems = new ArrayList<IGCMenuItem>();

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuFeedback() {
		return menuFeedback;
	}

	public void setMenuFeedback(String menuFeedback) {
		this.menuFeedback = menuFeedback;
	}

	public String getMenuDate() {
		return menuDate;
	}

	public void setMenuDate(String menuDate) {
		this.menuDate = menuDate;
	}

	public List<IGCMenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<IGCMenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public List<IGCMenuItem> getMenuPaidItems() {
		return menuPaidItems;
	}

	public void setMenuPaidItems(List<IGCMenuItem> menuPaidItems) {
		this.menuPaidItems = menuPaidItems;
	}

	public int getBellCount() {
		return bellCount;
	}

	public void setBellCount(int bellCount) {
		this.bellCount = bellCount;
	}

}
