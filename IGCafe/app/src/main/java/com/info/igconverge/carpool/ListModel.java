package com.info.igconverge.carpool;

import java.util.ArrayList;

/**
 * Created by lab1 on 28/12/15.
 */

public class ListModel implements SectionInterface{


    String time;
    String name;
    int cost;
    String carNo;
    String Location;
    boolean value;
    String status;

    String postId;
    ArrayList<String> days=new ArrayList<>();
    public ListModel(boolean value,String time, String name, int cost, String carNo,
                     String location,String postId,String status,ArrayList<String> days) {
        super();
        this.value=value;
        this.time = time;
        this.name = name;
        this.cost = cost;
        this.carNo = carNo;
        Location = location;
        this.status=status;
        this.postId=postId;
        this.days=days;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    @Override
    public boolean isSection() {
        return false;
    }

    public String getStatus() {
        return status;
    }

    public String getPostId() {
        return postId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }


    public ArrayList<String> getDays() {
        return days;
    }

    public void setDays(ArrayList<String> days) {
        this.days = days;
    }
}
