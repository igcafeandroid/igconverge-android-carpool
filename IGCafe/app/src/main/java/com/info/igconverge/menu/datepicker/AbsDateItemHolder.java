
package com.info.igconverge.menu.datepicker;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Date;

public abstract class AbsDateItemHolder<T extends AbsDateAdapter> extends RecyclerView.ViewHolder implements View.OnClickListener {

    private T dateAdapter;

    public AbsDateItemHolder(View itemView, T dateAdapter) {
        super(itemView);

        this.dateAdapter = dateAdapter;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        dateAdapter.onDateItemHolderClick(this);
    }

    public void updateDateItemView(boolean isSelected) {
        changeTextColor(isSelected);
        changeDateIndicatorColor(isSelected);

        if(dateAdapter.hasCurrentViewAnimation()) {
            if (isSelected) {
                getCurrentViewToAnimate().startAnimation(dateAdapter.getCurrentViewAnimation());
            } else {
                getCurrentViewToAnimate().clearAnimation();
            }
        }
    }

    public abstract void setDay(Date date);

    public abstract void setMonthName(Date date);

    public abstract  void setDayName(Date date);

    protected abstract void changeTextColor(boolean isSelected);

    protected abstract void changeDateIndicatorColor(boolean isSelected);

    protected abstract View getCurrentViewToAnimate();

}
