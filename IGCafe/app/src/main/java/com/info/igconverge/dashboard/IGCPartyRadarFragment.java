package com.info.igconverge.dashboard;

import java.util.Collections;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.info.igconverge.R;
import com.info.igconverge.Services.GooglePlaceParser;
import com.info.igconverge.Services.IGCCurrentLocation;
import com.info.igconverge.Services.ServiceHandler;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.emergency.GPSTracker;
import com.info.igconverge.emergency.IGCEmergencyActivity;
import com.info.igconverge.model.IGCShopDetails;
import com.info.igconverge.partyradar.IGCPartyRadarPlacesList;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;

public class IGCPartyRadarFragment extends Fragment implements
View.OnClickListener {
	IGCDashboardActivity activity;
	ImageView imgRestaurents, imgCafes, imgBars, imgClubs, imgEvents,
	imgHotels, imgMalls, imgMusic, imgMovies;
	ProgressCustomDialog dialog;
	boolean textSearch = false;
	GPSTracker gps ;
	View rootView;
	String searchString;
	Location location;
	String latitude, longitude;
	ErrorDialog errorDialog;
	GetPlaceNearbyData data = null;
	public boolean isRetry = false;
	public int retryCount = 0;
String url,jsonResult;
	String searchStringTitle;
	boolean serviceCallPlacesPending=false;
	// private InterstitialAd mInterstitialAd;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_partyradar, container,
				false);
		initializeActionBar();
		initializeViews();

		// initAd();
		// startAdds();
		return rootView;
	}

	private void initializeViews() {
		// TODO Auto-generated method stub
		imgRestaurents = (ImageView) rootView.findViewById(R.id.imgRestaurents);
		imgRestaurents.setOnClickListener(this);
		imgCafes = (ImageView) rootView.findViewById(R.id.imgCafes);
		imgCafes.setOnClickListener(this);
		imgBars = (ImageView) rootView.findViewById(R.id.imgBars);
		imgBars.setOnClickListener(this);
		imgClubs = (ImageView) rootView.findViewById(R.id.imgClubs);
		imgClubs.setOnClickListener(this);
		imgEvents = (ImageView) rootView.findViewById(R.id.imgEvents);
		imgEvents.setOnClickListener(this);
		imgHotels = (ImageView) rootView.findViewById(R.id.imgHotels);
		imgHotels.setOnClickListener(this);
		imgMalls = (ImageView) rootView.findViewById(R.id.imgMalls);
		imgMalls.setOnClickListener(this);
		imgMusic = (ImageView) rootView.findViewById(R.id.imgMusic);
		imgMusic.setOnClickListener(this);
		imgMovies = (ImageView) rootView.findViewById(R.id.imgMovies);
		imgMovies.setOnClickListener(this);
	}

	private void initializeActionBar() {
		// TODO Auto-generated method stub
		activity.actionBar.setTitle("Search");
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.header_bg));

		activity.unlockNavigationDrawerSwipeGesture();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.party_menu, menu);

		SearchManager searchManager = (SearchManager) activity
				.getSystemService(Context.SEARCH_SERVICE);

		MenuItem searchItem = menu.findItem(R.id.search);
		SearchView searchView = (SearchView) searchItem.getActionView();
		if (searchView != null) {
			searchView.setSearchableInfo(searchManager
					.getSearchableInfo(activity.getComponentName()));
			searchView.setQueryHint(getResources().getString(R.string.search_hint));
			searchView.setOnQueryTextListener(new OnQueryTextListener() {

				@Override
				public boolean onQueryTextSubmit(String searchString) {

					if (!TextUtils.isEmpty(searchString)) {
						textSearch = true;
						searchPlacesFromGoogle(searchString);
						hideKeyboard();
						return true;
					} else
						return false;
				}

				@Override
				public boolean onQueryTextChange(String searchString) {
					return false;
					// TODO Auto-generated method stub

				}
			});
		}
		super.onCreateOptionsMenu(menu, inflater);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.imgRestaurents:
			searchPlacesFromGoogle("Restaurant");

			break;
		case R.id.imgBars:
			searchPlacesFromGoogle("Bar");
			break;
		case R.id.imgCafes:
			searchPlacesFromGoogle("Cafe");
			break;
		case R.id.imgClubs:
			searchPlacesFromGoogle("Club");
			break;
		case R.id.imgMovies:
			searchPlacesFromGoogle("Movie");
			break;
		case R.id.imgMalls:
			searchPlacesFromGoogle("Mall");
			break;
		case R.id.imgMusic:
			searchPlacesFromGoogle("Medical");
			break;
		case R.id.imgEvents:
			searchPlacesFromGoogle("Park");
			break;
		case R.id.imgHotels:
			searchPlacesFromGoogle("Hotel");
			break;
		default:
			break;
		}

	}

	private void searchPlacesFromGoogle(String string) {
		searchString = string;

		hideKeyboard();
		gps=new GPSTracker(activity);
		if(gps.canGetLocation()){
			
			//location=gps.getLocation();
			if(gps.getLatitude()!=0 && gps.getLongitude()!=0)
			{
				latitude = Double.toString(gps.getLatitude());
				longitude = Double.toString(gps.getLongitude());
				callAysncForGoogleData();
			}
			else
			{
				Toast.makeText(activity, activity.getResources().getString(R.string.location_issue), Toast.LENGTH_LONG).show();
				showConfirmationOnLocationDialog();
			}
		
		}
		else{
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			isRetry=true;
			serviceCallPlacesPending=true;
			gps.showSettingsAlert();
		}
		

	}
	public void callAysncForGoogleData()
	{
		if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
			if (data == null) {
				data = new GetPlaceNearbyData();

				data.execute(searchString);
			}
		} else {
			data = null;
			ErrorDialog dialog = new ErrorDialog(getActivity(),
					"NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();
		}   
	}
	private void replaceFragment() {

		data = null;
		Fragment fragment = new IGCPartyRadarPlacesList();

		if (fragment != null) {

			Bundle bundle = new Bundle();
			bundle.putString("searchValue", searchStringTitle);
			fragment.setArguments(bundle);
			FragmentManager fragmentManager = activity
					.getSupportFragmentManager();
			fragmentManager
			.beginTransaction()
			.replace(R.id.frame_container, fragment,
					IGCConstants.PartyListFragmentName)
					.addToBackStack(null).commit();

			if ((activity.mDrawerToggle) != null)
				activity.mDrawerToggle.setDrawerIndicatorEnabled(false);
		}
	}

	private class GetPlaceNearbyData extends AsyncTask<Object, Object, String> {
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			dialog = new ProgressCustomDialog(activity);
			if (activity.mDrawerToggle != null) {

			}

			dialog.setOnKeyListener(new Dialog.OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface arg0, int keyCode,
						KeyEvent event) {

					// TODO Auto-generated method stub

					if (keyCode == KeyEvent.KEYCODE_BACK) {
						data = null;
				
						GetPlaceNearbyData.this.cancel(true);
						if (dialog!=null && dialog.isShowing()) {
							dialog.dismiss();
						}

					}
					return true;

				}
			});
			if (dialog!=null && !dialog.isShowing()) {
				dialog.show();
			}

		}

		@Override
		protected String doInBackground(Object... params) {

			if (latitude != null && longitude!=null) {


				// String jsonResult=null;
				if (!textSearch) {
					searchStringTitle = searchString + "s";
					  url = "https://maps.googleapis.com/maps/api/place/textsearch/json?location="
							+ latitude
							+ ","
							+ longitude
							+ "&radius="
							+ IGCConstants.nearByRadius
							+ "&query="
							+ searchString.toLowerCase(Locale.ENGLISH)
							+ "&sensor=true&key="
							+ IGCConstants.GOOGLE_PLACE_API_KEY;
					jsonResult = ServiceHandler.makeGetCallService(activity,url);
					
					
				} else {
					searchStringTitle = searchString;
					String textString = searchString.replace(" ", "%20");
					 url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
							+ latitude
							+ ","
							+ longitude
							+ "&radius="
							+ IGCConstants.nearByRadius
							+ "&keyword="
							+ textString.toLowerCase(Locale.ENGLISH)
							+ "&sensor=true&key="
							+ IGCConstants.GOOGLE_PLACE_API_KEY;
					 jsonResult = ServiceHandler.makeGetCallService(activity,url);
					
					
				}
				GooglePlaceParser googlePlaceParser = new GooglePlaceParser(
						textSearch);
				if (IGCConstants.placesList != null)
					IGCConstants.placesList.clear();
				location=new Location("");
				location.setLatitude(Double
						.parseDouble(latitude));
				location.setLongitude(Double
						.parseDouble(longitude));
				IGCConstants.placesList = googlePlaceParser.parseData(jsonResult, location);
				if (IGCConstants.placesList == null) {

					return null;

				} else {
					Collections.sort(IGCConstants.placesList,
							IGCShopDetails.distanceComparator);

				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			super.onPostExecute(result);
			data = null;

			if (IGCConstants.placesList == null) {

				errorDialog = new ErrorDialog(activity, "LIMIT_EXCEEDED");
				errorDialog.show();
			} else if (IGCConstants.placesList.size() > 0) {

				replaceFragment();
			}
			if (dialog!=null && dialog.isShowing()) {
				dialog.dismiss();
			}

		}
		// TODO add data in contact image adapter
	}

	public void showConfirmationOnLocationDialog() {
		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(activity);
		dialogBuilder.setTitleViewGone(true).withTitle("My Dialog")
		// .withTitle(null) no title
		.withTitleColor("#FFFFFF")
		// def
		.withDividerColor("#11000000")
		// def
		.withMessage("This is a modal Dialog.")
		// .withMessage(null) no Msg
		.setMessageViewGone(true).withMessageColor("#FFFFFFFF")
		// def | withMessageColor(int resid)
		.withDialogColor(Color.TRANSPARENT)
		// def | withDialogColor(int resid) //def
		.isCancelableOnTouchOutside(false)
		// def | isCancelable(true)
		.withDuration(300)
		// def
		.withEffect(Effectstype.Slidetop)
		.setCustomView(R.layout.delete_image_dialog, activity).show();
		TextView deleteMessage = (TextView) dialogBuilder
				.findViewById(R.id.deleteMessage);
		TextView titleMessage = (TextView) dialogBuilder
				.findViewById(R.id.titleMessage);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okDeletePhoto);
		TextView cancelDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.cancelDeletePhoto);
		okDeletePhoto.setText(getResources().getString(R.string.settings));
		deleteMessage.setText(getResources().getString(R.string.high_accuracy_tip));
		titleMessage.setText(R.string.improve_location);
		cancelDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
			}
		});
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(
						android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

				dialogBuilder.dismiss();

			}
		});

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		if (isRetry) {
			isRetry = true;
			retryCount = 1;
		}
	}

	private void hideKeyboard() {

		View view = activity.getCurrentFocus();
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);

		imm.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	public void onResume() {
		super.onResume();
		// displayAd();
		if(retryCount==1)
		{
			retryCount = 0;
			isRetry = false;
			gps=new GPSTracker(activity);
			if (gps.canGetLocation()) {
				if(gps.getLatitude()!=0 && gps.getLongitude()!=0)
				{
				latitude = Double.toString(gps.getLatitude());
				longitude = Double.toString(gps.getLongitude());
				}
				
	
			} else {
				isRetry = true;
				gps.showSettingsAlert();
			}
		}
	}

	/**
	 * Google Admob Advertisments
	 */
	/*
	 * private void initAd() { // Create the InterstitialAd and set the
	 * adUnitId. mInterstitialAd = new InterstitialAd(activity); // Defined in
	 * values/strings.xml
	 * mInterstitialAd.setAdUnitId(getString(R.string.google_addunit)); }
	 * 
	 * private void displayAd() { // Show the ad if it's ready. Otherwise toast
	 * and restart the game. if (mInterstitialAd != null &&
	 * mInterstitialAd.isLoaded()) { mInterstitialAd.show(); } else { //
	 * Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
	 * startAdds(); } }
	 * 
	 * private void startAdds() { // Hide the retry button, load the ad, and
	 * start the timer.
	 * 
	 * AdRequest adRequest = new AdRequest.Builder().build();
	 * mInterstitialAd.loadAd(adRequest);
	 * 
	 * }
	 */

}
