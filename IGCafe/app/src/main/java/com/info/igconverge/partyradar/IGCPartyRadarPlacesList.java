package com.info.igconverge.partyradar;


import android.app.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.MarginDecoration;



public class IGCPartyRadarPlacesList extends Fragment{

	 RecyclerView mRecyclerView;

	 private MyRecyclerAdapter adapter;
	
	IGCDashboardActivity activity;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.partyplace_recycler_main,
				container, false);
		activity.actionBar.setTitle(getArguments().getString("searchValue"));
		activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		 //Initialize recycler view 
	        mRecyclerView = (RecyclerView)rootView. findViewById(R.id.recycler_view);

	        mRecyclerView.addItemDecoration(new MarginDecoration(activity));
	        mRecyclerView.setHasFixedSize(true);
	        mRecyclerView.setLayoutManager(new GridLayoutManager(activity, 2));

	        adapter = new MyRecyclerAdapter(activity,activity, IGCConstants.placesList);
	        mRecyclerView.setAdapter(adapter);
    
		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		 activity.lockNavigationDrawerSwipeGesture();
	}

	

}

