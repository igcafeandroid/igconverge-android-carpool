package com.info.igconverge.weather;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.model.IGCWeatherDetails;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.weatherlibrary.YahooWeather;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class IGCWeather extends Fragment {
	private static final String TAG = "Weather";
	final Handler mHandler = new Handler();
	private LinearLayout mWeatherInfosLayout;
	private MediaPlayer mMediaPlayer;
	IGCWeatherDetails weatherDetails;
	List<String> weatherImgList = new ArrayList<String>();
	List<String> weekTempList = new ArrayList<String>();
	IGCDashboardActivity activity;
	private YahooWeather mYahooWeather = YahooWeather.getInstance(5000, 5000,
			true);
	Typeface myTypefaceMedium, myTypefaceRegular, myTypefaceLight;
	com.info.igconverge.UIControls.TextViewClimaconsRegular weather_image_1,
			weather_image_2, weather_image_3, weather_image_4, weather_image_5;
	private ProgressDialog mProgressDialog;

	TextView txt_cityname, txt_date, temperatureValue, temp_min, temp_max,
			temp_sun, temp_mon, temp_tue, temp_wed, temp_thu, temp_fri,
			temp_sat, climatecondition;
	com.info.igconverge.UIControls.TextViewClimaconsRegular weatherTextLogo;
	@SuppressWarnings("unused")
	private EditText mEtAreaOfCity;
	private Button mBtSearch;
	TextView txt_day1, txt_day2, txt_day3, txt_day4, txt_day5;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.unlockNavigationDrawerSwipeGesture();
		activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.header_bg));

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.weather, container, false);

		myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Medium.ttf");

		myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");
		myTypefaceLight = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Light.ttf");

		weatherDetails = getArguments().getParcelable("weather");
		
		initialiseViews(rootView);
		initialiseValues();
		return rootView;
	
	}

	public void initialiseViews(View view) {
		txt_cityname = (TextView) view.findViewById(R.id.txt_cityname);
		txt_date = (TextView) view.findViewById(R.id.txt_date);
		temperatureValue = (TextView) view.findViewById(R.id.temperatureValue);
		temp_min = (TextView) view.findViewById(R.id.temp_min);
		temp_max = (TextView) view.findViewById(R.id.temp_max);
		temp_sun = (TextView) view.findViewById(R.id.temp_sun);
		temp_mon = (TextView) view.findViewById(R.id.temp_mon);
		temp_tue = (TextView) view.findViewById(R.id.temp_tue);
		temp_wed = (TextView) view.findViewById(R.id.temp_wed);
		temp_thu = (TextView) view.findViewById(R.id.temp_thu);
		temp_fri = (TextView) view.findViewById(R.id.temp_fri);
		temp_sat = (TextView) view.findViewById(R.id.temp_sat);
		txt_day1 = (TextView) view.findViewById(R.id.txt_day1);
		txt_day2 = (TextView) view.findViewById(R.id.txt_day2);
		txt_day3 = (TextView) view.findViewById(R.id.txt_day3);
		txt_day4 = (TextView) view.findViewById(R.id.txt_day4);
		txt_day5 = (TextView) view.findViewById(R.id.txt_day5);
		climatecondition = (TextView) view.findViewById(R.id.climatecondition);

		txt_cityname.setTypeface(myTypefaceLight);
		txt_date.setTypeface(myTypefaceRegular);
		temperatureValue.setTypeface(myTypefaceLight);
		temp_min.setTypeface(myTypefaceLight);
		temp_max.setTypeface(myTypefaceLight);
		temp_fri.setTypeface(myTypefaceRegular);
		temp_sat.setTypeface(myTypefaceRegular);
		temp_sun.setTypeface(myTypefaceRegular);
		temp_mon.setTypeface(myTypefaceRegular);
		temp_tue.setTypeface(myTypefaceRegular);
		temp_wed.setTypeface(myTypefaceRegular);

		temp_thu.setTypeface(myTypefaceRegular);
		climatecondition.setTypeface(myTypefaceLight);
		txt_day1.setTypeface(myTypefaceRegular);
		txt_day2.setTypeface(myTypefaceRegular);
		txt_day3.setTypeface(myTypefaceRegular);
		txt_day4.setTypeface(myTypefaceRegular);
		txt_day5.setTypeface(myTypefaceRegular);

	

		weatherTextLogo = (com.info.igconverge.UIControls.TextViewClimaconsRegular) view
				.findViewById(R.id.weatherLogo);
		weatherTextLogo.setTextSize(140);
		weather_image_1 = (com.info.igconverge.UIControls.TextViewClimaconsRegular) view
				.findViewById(R.id.weather_image_1);
		weather_image_2 = (com.info.igconverge.UIControls.TextViewClimaconsRegular) view
				.findViewById(R.id.weather_image_2);
		weather_image_3 = (com.info.igconverge.UIControls.TextViewClimaconsRegular) view
				.findViewById(R.id.weather_image_3);
		weather_image_4 = (com.info.igconverge.UIControls.TextViewClimaconsRegular) view
				.findViewById(R.id.weather_image_4);
		weather_image_5 = (com.info.igconverge.UIControls.TextViewClimaconsRegular) view
				.findViewById(R.id.weather_image_5);
	}

	public void initialiseValues() {

		temp_max.setText(weatherDetails.getMaxTemp());
		temp_min.setText(weatherDetails.getMinTemp());
		txt_cityname.setText(weatherDetails.getCurrentCity());
		txt_date.setText(weatherDetails.getCurrentDate());
		temperatureValue.setText(weatherDetails.getCurrentTemp());
		climatecondition.setText(weatherDetails.getClimateCondition());
		weatherTextLogo.setText(weatherDetails.getWeatherImageText());

		weekTempList = weatherDetails.getWeekTempList();

		temp_sun.setText(weekTempList.get(0));
		temp_mon.setText(weekTempList.get(1));

		temp_tue.setText(weekTempList.get(2));

		temp_wed.setText(weekTempList.get(3));

		temp_thu.setText(weekTempList.get(4));


		weatherImgList = weatherDetails.getWeatherImgList();
	
		weather_image_1.setText(weatherImgList.get(0));
		weather_image_2.setText(weatherImgList.get(1));
		weather_image_3.setText(weatherImgList.get(2));
		weather_image_4.setText(weatherImgList.get(3));
		weather_image_5.setText(weatherImgList.get(4));
		setDayOfWeek();

		
	}

	

	private void hideProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.cancel();
		}
	}

	public void stop() {
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	public void play(Context c, int rid) {
		stop();

		mMediaPlayer = MediaPlayer.create(c, rid);
		mMediaPlayer
				.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer mediaPlayer) {
						stop();
					}
				});

		mMediaPlayer.start();
	}

	public void setDayOfWeek() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		txt_day1.setText("Today");

		// SAT
		if (day == 7) {
		
			txt_day2.setText(returnDayString(day - 6));
			txt_day3.setText(returnDayString(day - 5));
			txt_day4.setText(returnDayString(day - 4));
			txt_day5.setText(returnDayString(day - 3));
		}

		// FRI
		if (day == 6) {
			
			txt_day2.setText(returnDayString(day + 1));
			txt_day3.setText(returnDayString(day - 5));
			txt_day4.setText(returnDayString(day - 4));
			txt_day5.setText(returnDayString(day - 3));
		}

		// THRUSDAY
		if (day == 5) {
		
			txt_day2.setText(returnDayString(day + 1));
			txt_day3.setText(returnDayString(day + 2));
			txt_day4.setText(returnDayString(day - 4));
			txt_day5.setText(returnDayString(day - 3));
		}

		// WED
		if (day == 4) {
		
			txt_day2.setText(returnDayString(day + 1));
			txt_day3.setText(returnDayString(day + 2));
			txt_day4.setText(returnDayString(day + 3));
			txt_day5.setText(returnDayString(day - 3));
		}

		// TUE
		if (day == 3 || day == 2 || day == 1) {
			
			txt_day2.setText(returnDayString(day + 1));
			txt_day3.setText(returnDayString(day + 2));
			txt_day4.setText(returnDayString(day + 3));
			txt_day5.setText(returnDayString(day + 4));
		}

	}

	public String returnDayString(int day) {
	
		String dayOfWeek = "";
		switch (day) {
		case 1:
			dayOfWeek = "SUN";
			break;

		case 2:
			dayOfWeek = "MON";
			break;
		case 3:
			dayOfWeek = "TUE";
			break;

		case 4:
			dayOfWeek = "WED";
			break;

		case 5:
			dayOfWeek = "THU";
			break;

		case 6:
			dayOfWeek = "FRI";
			break;

		case 7:
			dayOfWeek = "SAT";
			break;
		}
	
		return dayOfWeek;
	}
}
