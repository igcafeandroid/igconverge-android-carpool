package com.info.igconverge.UIControls;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewClimaconsRegular extends TextView {

	public TextViewClimaconsRegular(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public TextViewClimaconsRegular(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public TextViewClimaconsRegular(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"font/Climacons.ttf");
		setTypeface(face);
		setTextSize(40);
		setTextColor(Color.WHITE);
	}
}
