package com.info.igconverge.events;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.events.calendar.CalendarFragment;

import com.info.igconverge.model.IGCEvent;
import com.info.igconverge.model.IGCEventItem;
import com.info.igconverge.parser.EventsParser;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCUtils;

import android.app.Activity;
import android.app.Dialog;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;

import android.graphics.Bitmap;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.LruCache;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class IGCEventsFragment extends Fragment implements
		com.info.igconverge.events.PullToRefreshView.OnRefreshListener,
		com.info.igconverge.Services.FragmentCallback {

	private ListView mListView;
	IGCEventsListAdapter eventsListAdapter;
	// SwipeRefreshLayout swipeRefreshLayout;
	PullToRefreshView swipeRefreshLayout;
	IGCDashboardActivity activity;
	OnRefreshListener onRefreshListener;
	LinearLayout calendarLayout;
	ArrayList<IGCEventItem> IGCEventItemsList;
	ProgressCustomDialog progressCustomDialog;
	ArrayList<String> eventDates;
	ArrayList<IGCEvent> igcEventList;
	TextView emptyText;
	IGCEvent igcEvent;
	IGCEventItem igcEventItem;
	int count = 0;
	int MY_REQUEST_CODE = 100;
	List<String> datesList;
	String dateTime, endDateTime, startDate, endDate;
	static ArrayList<IGCEvent> igcEventSelectedList;
	private LruCache<String, Bitmap> mMemoryCache;
	String selecetdDate;
	// FetchEventsTask task;
	FragmentCallback frag;

	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			swipeRefreshLayout.setRefreshing(true);

			onRefresh();

		}
	};

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
		// setHasOptionsMenu(true);
		mMemoryCache = IGCafeApplication.mMemoryCache;
		frag = this;
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter("notifyEvent"));
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.unlockNavigationDrawerSwipeGesture();
		setActionBar(IGCConstants.EventsFragmentName);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_events, container,
				false);
		initialiseViews(rootView);

		swipeRefreshLayout.setOnRefreshListener(this);
		// checkNetworkStatus();
		swipeRefreshLayout.setRefreshing(true);
		onRefresh();
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		inflater.inflate(R.menu.event_menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		DialogFragment newDialogFragment = new CalendarFragment();
		newDialogFragment.setTargetFragment(this, MY_REQUEST_CODE);
		Bundle bundle = new Bundle();
		bundle.putString("selecetdDate", selecetdDate);
		bundle.putStringArrayList("eventDates", eventDates);
		newDialogFragment.setArguments(bundle);
		if (!newDialogFragment.isVisible())
			newDialogFragment.show(activity.getSupportFragmentManager(),
					"dialog");
		return true;
	}

	@Override
	public void onRefresh() {
		setActionBar(IGCConstants.EventsFragmentName);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				count = count + 1;
				// setActionBar(IGCConstants.EventsFragmentName);
				if (eventsListAdapter != null) {
					eventsListAdapter.notifyDataSetChanged();
				}
				checkNetworkStatus();
			}

		}, 2000);
	}

	private void checkNetworkStatus() {
		if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
			setHasOptionsMenu(false);

			// params = getParams();
			IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(
					getActivity(), "GET", frag);
			getRequest.execute(IGCConstants.fetchEventsURL);

		} else {
			emptyText.setVisibility(View.VISIBLE);
			setAdapter(null, null);
			swipeRefreshLayout.setRefreshing(false);
			ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		igcEventSelectedList = new ArrayList<IGCEvent>();
		igcEventList = IGCafeApplication.getIgcEventList();
		if (requestCode == MY_REQUEST_CODE) {
			// Make sure the request was successful
			if (resultCode == 2) {
				String date = data.getStringExtra("date");
				selecetdDate = date;
				String displayDate = IGCUtils.getCurrentDateToDisplayDate(date);

				if (displayDate != null && displayDate.isEmpty()) {
					setActionBar(IGCConstants.EventsFragmentName);
				} else
					setActionBar(displayDate);
				if (date.isEmpty()) {
					setActionBar(IGCConstants.EventsFragmentName);

					igcEventSelectedList = igcEventList;

				} else {

					if (igcEventList != null) {

						for (int i = 0; i < igcEventList.size(); i++) {

							igcEventItem = new IGCEventItem();
							igcEvent = new IGCEvent();

							igcEvent = igcEventList.get(i);
							igcEventItem = igcEvent.getEventItem();
							dateTime = igcEventItem.getEventDate();

							endDateTime = igcEventItem.getEventEndDate();
							startDate = dateTime.split(" ")[0];
							endDate = endDateTime.split(" ")[0];

							IGCUtils.datesBetween(startDate, endDate);
							List<String> dates = IGCUtils.datesBetween(
									startDate, endDate);
							for (int k = 0; k < dates.size(); k++) {
								String dt = dates.get(k);
								if (date.equals(dt)) {
									igcEventSelectedList.add(igcEventList
											.get(i));
								}

							}

							if (startDate.equalsIgnoreCase(date)) {
								igcEventSelectedList.add(igcEventList.get(i));
							}

						}
					}
				}
				if (igcEventSelectedList != null) {
					if (igcEventSelectedList.size() == 0) {
						if (emptyText != null) {
							emptyText.setVisibility(View.VISIBLE);
						}
						setAdapter(null, null);
					} else {
						if (emptyText != null)
							emptyText.setVisibility(View.GONE);
						setAdapter(igcEventSelectedList, mMemoryCache);
					}
				} else {
					if (emptyText != null)
						emptyText.setVisibility(View.GONE);
					setAdapter(null, null);
				}

			}
		}
	}

	private void setActionBar(String title) {
		activity.actionBar.setTitle(title);
	}

	private void initialiseViews(View rootView) {
		mListView = (ListView) rootView.findViewById(R.id.eventsList);
		activity.actionBar.setBackgroundDrawable(activity.getResources()
				.getDrawable(R.drawable.header_bg));

		swipeRefreshLayout = (PullToRefreshView) rootView.findViewById(R.id.swiperefresh);
		emptyText = (TextView) rootView.findViewById(R.id.emptyText);
		setAdapter(null, mMemoryCache);

	}

	private void setAdapter(ArrayList<IGCEvent> igcEventList,
			LruCache<String, Bitmap> mMemoryCache) {
		eventsListAdapter = new IGCEventsListAdapter(activity, igcEventList,
				mMemoryCache);
		mListView.setAdapter(eventsListAdapter);
		eventsListAdapter.notifyDataSetChanged();
	}

	@Override
	public void fragMultipleResponse(String response, String extra) {
		// TODO Auto-generated method stub
		if (response != null && response.length() > 0) {
			try {

				if (response != null) {
					fetchEvents(response);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
			dialog.setCancelable(false);
			dialog.show();

			emptyText.setVisibility(View.VISIBLE);
			setAdapter(null, null);
			swipeRefreshLayout.setRefreshing(false);
		}

	}

	private void fetchEvents(String response) {
		EventsParser eventsParser = new EventsParser();
		igcEventList = eventsParser.parseFetchedEvents(response);
		IGCafeApplication.setIgcEventList(igcEventList);

		eventDates = new ArrayList<String>();
		if (igcEventList.size() > 0) {
			emptyText.setVisibility(View.GONE);

			setAdapter(igcEventList, mMemoryCache);

			for (int i = 0; i < igcEventList.size(); i++) {
				igcEventItem = new IGCEventItem();
				igcEvent = new IGCEvent();

				igcEvent = igcEventList.get(i);
				igcEventItem = igcEvent.getEventItem();
				dateTime = igcEventItem.getEventDate();
				endDateTime = igcEventItem.getEventEndDate();
				startDate = dateTime.split(" ")[0];
				endDate = endDateTime.split(" ")[0];
				if (i == 0) {
					eventDates.add(startDate);
				}
				IGCUtils.datesBetween(startDate, endDate);
				List<String> dates = IGCUtils.datesBetween(startDate, endDate);
				for (int k = 0; k < dates.size(); k++) {
					if (!eventDates.contains(dates.get(k))) {
						eventDates.add(dates.get(k));

					}
				}
				if (!eventDates.contains(startDate)) {
					eventDates.add(startDate);
				}

			}
			if (swipeRefreshLayout != null)
				swipeRefreshLayout.setRefreshing(false);
			setHasOptionsMenu(true);

		} else {
			if (swipeRefreshLayout != null)
				swipeRefreshLayout.setRefreshing(false);
			setHasOptionsMenu(false);
			emptyText.setVisibility(View.VISIBLE);
			setAdapter(null, null);

		}
	}

	@Override
	public void onResume() {
		IGCafeApplication.eventActivityResumed();
		super.onResume();

	}

	@Override
	public void onPause() {
		super.onPause();
		IGCafeApplication.eventActivityPaused();
	}

	@Override
	public void onStop() {
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter("notifyEvent"));
		super.onStop();
	}

}
