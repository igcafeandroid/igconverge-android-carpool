package com.info.igconverge.dashboard;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.events.IGCEventsListAdapter;
import com.info.igconverge.menu.datepicker.IGCShowMenuByDatePickFragment;

import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCUtils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import android.widget.TextView;

public class IGCMenuFragment extends Fragment implements View.OnClickListener {
	// ProgressDialog progressDialog;
	TextView txtBreakfast, txtLunch, txtEveningSnack, txtDinner;
	FragmentManager fragmentManager;
	IGCDashboardActivity activity;
	ProgressCustomDialog progressCustomDialog;
	Typeface myTypefaceMedium;
	Typeface myTypefaceRegular;
	String mealType;
	View rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_call_extension, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.callExtension:
			showAppInfoDialog();
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_menu, container, false);
		initializeActionBar();
		initializeViews();
		return rootView;
	}

	private void initializeViews() {
		// TODO Auto-generated method stub
		txtBreakfast = (TextView) rootView.findViewById(R.id.txtBreakfast);
		txtBreakfast.setOnClickListener(this);
		txtBreakfast.setOnTouchListener(null);
		txtLunch = (TextView) rootView.findViewById(R.id.txtLunch);
		txtLunch.setOnClickListener(this);
		txtEveningSnack = (TextView) rootView
				.findViewById(R.id.txtEveningSnack);
		txtEveningSnack.setOnClickListener(this);

		txtDinner = (TextView) rootView.findViewById(R.id.txtDinner);
		txtDinner.setOnClickListener(this);
		
		myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Medium.ttf");
		myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");
		txtBreakfast.setTypeface(myTypefaceMedium);
		txtLunch.setTypeface(myTypefaceMedium);
		txtEveningSnack.setTypeface(myTypefaceMedium);
		txtDinner.setTypeface(myTypefaceMedium);

	}

	private void initializeActionBar() {
		// TODO Auto-generated method stub
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.actionBar.setHomeButtonEnabled(true);
		activity.actionBar.setDisplayShowHomeEnabled(true);
		activity.actionBar.setTitle(IGCConstants.MenuFragmentName);
		activity.unlockNavigationDrawerSwipeGesture();
		activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.header_bg));
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();

		switch (v.getId()) {
		case R.id.txtBreakfast:
			fragment = new IGCShowMenuByDatePickFragment();
			mealType = "breakfast";
			bundle.putString("Type", "BreakFast Menu");
			bundle.putString("mealType", mealType);
			fragment.setArguments(bundle);
			replaceFragment(fragment, IGCConstants.MenuFragmentName);

			break;
		case R.id.txtLunch:
			// progressDialog.show();
			fragment = new IGCShowMenuByDatePickFragment();
			bundle.putString("Type", "Lunch Menu");
			mealType = "lunch";
			bundle.putString("mealType", mealType);
			fragment.setArguments(bundle);
			replaceFragment(fragment, IGCConstants.MenuFragmentName);
			break;
		case R.id.txtEveningSnack:
			fragment = new IGCShowMenuByDatePickFragment();
			mealType = "snacks";
			bundle.putString("Type", "Snacks Menu");
			bundle.putString("mealType", mealType);
			fragment.setArguments(bundle);
			replaceFragment(fragment, IGCConstants.MenuFragmentName);
			break;
		case R.id.txtDinner:
			fragment = new IGCShowMenuByDatePickFragment();
			mealType = "dinner";
			bundle.putString("mealType", mealType);
			bundle.putString("Type", "Dinner Menu");
			fragment.setArguments(bundle);
			replaceFragment(fragment, IGCConstants.MenuFragmentName);
			break;
		default:
			break;

		}

	}

	public void showAppInfoDialog() {

		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(activity);
		dialogBuilder
		.setTitleViewGone(true)
		.withTitle("My Dialog")
		// .withTitle(null) no title
		.withTitleColor("#FFFFFF")
		// def
		.withDividerColor("#11000000")
		// def
		.withMessage("This is a modal Dialog.")
		// .withMessage(null) no Msg
		.setMessageViewGone(true)
		.withMessageColor("#FFFFFFFF")
		// def | withMessageColor(int resid)
		.withDialogColor(Color.TRANSPARENT)
		// def | withDialogColor(int resid) //def
		.isCancelableOnTouchOutside(false)
		// def | isCancelable(true)
		.withDuration(300)
		// def
		.withEffect(Effectstype.Slidetop)
		.setCustomView(R.layout.call_extension_custom_view, activity)
		.show();
		TextView headerTextExtensionDialog = (TextView) dialogBuilder
				.findViewById(R.id.headerTextExtensionDialog);
		TextView textNumberExtensionDialog = (TextView) dialogBuilder
				.findViewById(R.id.textNumberExtensionDialog);
		headerTextExtensionDialog.setTypeface(myTypefaceMedium);
		textNumberExtensionDialog.setTypeface(myTypefaceRegular);
		Button callButtonExtensionDialog = (Button) dialogBuilder
				.findViewById(R.id.callButtonExtensionDialog);
		Button cancelButtonExtensionDialog = (Button) dialogBuilder
				.findViewById(R.id.cancelButtonExtensionDialog);
		cancelButtonExtensionDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
			}
		});
		callButtonExtensionDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialCall(IGCConstants.cafeteria_no);
				dialogBuilder.dismiss();

			}
		});
	}

	private void replaceFragment(Fragment fragment, String fragmentName) {
		// TODO Auto-generated method stub
		if (fragment != null) {
			fragmentManager = activity.getSupportFragmentManager();

			fragmentManager.beginTransaction()
			.replace(R.id.frame_container, fragment, fragmentName)
			.addToBackStack(null).commit();
			if ((activity.mDrawerToggle) != null)
				activity.mDrawerToggle.setDrawerIndicatorEnabled(false);

		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	public void dialCall(String phoneNo) {
		try {

			if(IGCUtils.isTelephonyEnabled(activity))
			{
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				if (phoneNo != null)
					callIntent.setData(Uri.parse("tel:" + phoneNo));
				activity.startActivity(callIntent);
			}
			else
			{
				Toast.makeText(activity, activity.getResources().getString(R.string.no_call), Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}
}
