package com.info.igconverge.carpool;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**

 * Created by lab2 on 24/12/15.
 */
public class RideParser{

ArrayList<CarpoolClass> rideList=new ArrayList<CarpoolClass>();

    public RideParser()
    {

    }

    public ArrayList<CarpoolClass> parseRideData(String jsonData) {
        if (jsonData != "")
            try {

                JSONArray rootArray = new JSONArray(jsonData);

              //  JSONObject jObject=new JSONObject(jsonData);

                for (int index = 0; index < rootArray.length(); index++) {
                    JSONObject jObject = rootArray.getJSONObject(index);
                    CarpoolClass carpoolObject = new CarpoolClass();

                    if(!jObject.isNull("id"))
                    {
                        carpoolObject.setPostId(jObject.getString("id"));
                    }

                  //  carpoolObject.setUser(jObject.getJSONObject("user"));
                    if(!jObject.isNull("startingLocation")) {
                        carpoolObject.setStartLocation(jObject.getString("startingLocation"));
                    }
                    if(!jObject.isNull("numberOfSeats")) {
                        carpoolObject.setNoOfSeats(jObject.getInt("numberOfSeats"));
                    }
                    if(!jObject.isNull("carNumber")) {
                        carpoolObject.setCarNo(jObject.getString("carNumber"));
                    }
                    if(!jObject.isNull("fare")) {
                        carpoolObject.setFare(jObject.getInt("fare"));
                    }
                    if(!jObject.isNull("status")) {
                        carpoolObject.setStatus(jObject.getBoolean("status"));
                    }
                    if(!jObject.isNull("departureTime")) {
                        carpoolObject.setDepTime(jObject.getString("departureTime"));
                    }
                    if(!jObject.isNull("postedOn")) {
                        carpoolObject.setPostedOn(jObject.getString("postedOn"));
                    }
                    if(!jObject.isNull("description")) {
                        carpoolObject.setComments(jObject.getString("description"));
                    }
                    if(!jObject.isNull("days")){
                        JSONArray dayJson =jObject.getJSONArray("days");
                        ArrayList<String> daysData=new ArrayList<>();
                        for (int i=0;i<dayJson.length();i++){

                            daysData.add(dayJson.getString(i));
                        }
                        carpoolObject.setDays(daysData);
                    }

                    if(!jObject.isNull("user"))
                    {
                        JSONObject userJsonObj=jObject.getJSONObject("user");
                        String userid=null,userName=null,userEmail=null,userPhone=null;
                        if(!userJsonObj.isNull("id"))
                        {
                            userid=userJsonObj.getString("id");
                        }
                        if(!userJsonObj.isNull("name"))
                        {
                            userName=userJsonObj.getString("name");
                        }
                        if(!userJsonObj.isNull("email"))
                        {
                            userEmail=userJsonObj.getString("email");
                        }
                        if(!userJsonObj.isNull("phoneNumber")) {
                            userPhone=userJsonObj.getString("phoneNumber");
                        }
                        carpoolObject.setUser(new User(userid, userName, userEmail, userPhone));
                    }
                    if(!jObject.isNull("occupants")) {
                        JSONArray occupantJsonArray = jObject.getJSONArray("occupants");

                        ArrayList<OccupantsClass> occupantsList=new ArrayList<>();
                        for (int i=0;i<occupantJsonArray.length();i++){
                            OccupantsClass occu =new OccupantsClass();
                            JSONObject occupantJsonObject = occupantJsonArray.getJSONObject(i);

                            if(!occupantJsonObject.isNull("id") && !occupantJsonObject.isNull("name") && !occupantJsonObject.isNull("email") && !occupantJsonObject.isNull("status")){
                           occu.setUserId(occupantJsonObject.getString("id"));
                                occu.setName(occupantJsonObject.getString("name"));
                                occu.setEmail(occupantJsonObject.getString("email"));
                                occu.setStatus(occupantJsonObject.getString("status"));
                                occupantsList.add(occu);
                            }

                        }
                        carpoolObject.setOccupantsList(occupantsList);
                    }

                     rideList.add(carpoolObject);

            } }catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return  rideList;
            }
        return rideList;
    }

}
