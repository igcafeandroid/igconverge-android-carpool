package com.info.igconverge.utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.location.Location;
import android.os.Bundle;

import com.info.igconverge.model.IGCAnnouncement;
import com.info.igconverge.model.IGCEmployeeDetails;
import com.info.igconverge.model.IGCEventItem;
import com.info.igconverge.model.IGCShopDetails;

public class IGCConstants {

    // browser key by infogain account= AIzaSyCZOAoaapJcYIGabZzTVODtPVgW6HwniOw
    public static String GOOGLE_PLACE_API_KEY = "AIzaSyCZOAoaapJcYIGabZzTVODtPVgW6HwniOw";

    public static final String PLACES_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/";
    public static final String PLACES_DETAIL_URL = "https://maps.googleapis.com/maps/api/place/details/";

    // AIzaSyCZOAoaapJcYIGabZzTVODtPVgW6HwniOw
    // AIzaSyAwCk7vmEN7wbymZdgcHwwORp4OqWRg1GE
    // AIzaSyDRtumzeaWP6YRFywJ7Kt8HQiWgk_0NN7E
    // AIzaSyDD75bBveoTypKw-eMq-Jk6rgT0uK3VCvw

    public static int nearByRadius = 5000;
    public static Location currentLocation;
    public static int columnWidth;
    public static int columnHeight;
    public static String MenuFragmentName = "Menu ";
    public static String PartyRadarFragmentName = "Party Radar ";
    public static String ContactUsFragmentName = "Contact ";
    public static String EventsFragmentName = "Events ";
    public static String ContactFragmentName = "Contact Us ";

    public static String FeedbackFragmentName = "Feedback ";
    public static String PartyListFragmentName = "Party List ";
    //public static String CarpoolFragmentName = "Carpool ";
    public static String WalkthroughFragmentName = "igConverge ";
    public static String CarpoolFragmentName = "Carpool";
    public static String HolidaysFragmentName = "Holidays List ";
    public static String WeatherFragmentName = "Weather Forecast ";
    public static final String AnnouncementsFragment = "Notifications";
    public static final String CarPoolNotificationFragment = "Car Pool Requests";
    public static final String CarPoolFragment = "Carpool";
    public static final String ShareRide = "Share a Ride";
    public static final String EditRide = "Edit Ride";
    public static Date serverDate;
    public static int displayCount = 0;


    public static String IPAddressPublic = "http://115.112.147.11:8080/igConverge";
    public static String adminContactNumber = "+919818040332";

//	 public static String IPAddressPublic = "http://172.18.84.93:8080/";

    public static String loginURL = IPAddressPublic + "/mobile/login";
    public static String sendFeedbackURL = IPAddressPublic
            + "/mobile/savefeedback";
    public static String fetchEventsURL = IPAddressPublic
            + "/mobile/fetchevents";

    public static String fetchMealsURL = IPAddressPublic
            + "/mobile/fetchmeals";
    public static String ringBellURL = IPAddressPublic
            + "/user/ringbell";
    public static String fetchAverageFeedbackURL = IPAddressPublic
            + "/mobile/fetchfeedback";
    public static String fetchEventsImageURL = IPAddressPublic
            + "/mobile/fetcheventsimage";
    public static String checkupdate = IPAddressPublic
            + "/mobile/checkupdate";
    public static String logoutURL = IPAddressPublic
            + "/mobile/logout";
    public static String fetchAnnouncementsURL = IPAddressPublic
            + "/mobile/fetchnotification/0";
    public static String registerDevice = IPAddressPublic
            + "/mobile/registerdevice";
    public static String emergencyServiceURL = IPAddressPublic
            + "/user/emergency";


    public static String carPoolURL = "http://115.112.147.11:8080/igConverge";

    //Production URL :http://115.112.147.11:8080/igConverge
    //Testing URL : http://115.112.147.11:8080/igConvergeTest
//Local Testing URL : http://172.18.87.169:8080/igConverge

    public static String fetchRidesURL = carPoolURL + "/user/ride/fetchAllRides";
    public static String fetchRidesNotificationURL = carPoolURL + "/user/ride/fetchAllRidesByEmployeeId";
    public static String acceptRidesNotificationURL = carPoolURL + "/user/ride/acceptRequest";
    public static String rejectRidesNotificationURL = carPoolURL + "/user/ride/rejectRequest";

    public static String sendRideRequestURL = carPoolURL + "/user/ride/addOccupant";
    public static String saveRideRequestURL = carPoolURL + "/user/ride/save";
    public static String updateRideRequestURL = carPoolURL + "/user/ride/update";


    public static String RideDeleteRequestURL = carPoolURL + "/user/ride/softDelete";
    public static String LeaveCarpoolRequestURL = carPoolURL + "/user/ride/removeOccupant";


    public static IGCEmployeeDetails employeeDetailObject = new IGCEmployeeDetails();
    public static String instabugId = "1f3c14729de0d16785f85372477039f9";

    public static String PartyListItemDetailFragmentName = "Party List Item Detail Fragment";

    public static boolean userLoggedIn = false;
    public static String username;
    public static String profileName;
    public static ArrayList<IGCShopDetails> placesList = new ArrayList<IGCShopDetails>();
    public static List<IGCAnnouncement> announcementsList = new ArrayList<IGCAnnouncement>();

    public static ArrayList<IGCEventItem> eventsList = new ArrayList<IGCEventItem>();
    public static String currrentdateFormat = "MM/dd/yyyy";
    public static String displayDateFormat = "dd MMM, yyyy";

    public static String sender_id = "680583261286";
    public static String deviceType = "android";

    public static String bellCount;
    public static String cafeteria_no = "+919891350755";
    public static List<String> notificationMessageList = new ArrayList<String>();
    public static List<String> carPoolNotificationMessageList = new ArrayList<String>();


    public static List<String> eventMessageList = new ArrayList<String>();
    public static int notificationCount = 0, eventCount = 0, carPoolCount = 0;
    public static int notificationId = 001, eventId = 1001, carPoolNotificationId = 10001;

    public static List<String> getNotificationMessageList() {
        return notificationMessageList;
    }

    public static void setNotificationMessageList(
            List<String> notificationMessageList) {
        IGCConstants.notificationMessageList = notificationMessageList;
    }

    public static List<String> getEventMessageList() {
        return eventMessageList;
    }

    public static void setEventMessageList(List<String> eventMessageList) {
        IGCConstants.eventMessageList = eventMessageList;
    }

    public static int getNotificationCount() {
        return notificationCount;
    }

    public static void setNotificationCount(int notificationCount) {
        IGCConstants.notificationCount = notificationCount;
    }

    public static int getEventCount() {
        return eventCount;
    }

    public static void setEventCount(int eventCount) {
        IGCConstants.eventCount = eventCount;
    }

    public static List<IGCAnnouncement> getAnnouncementsList() {
        return announcementsList;
    }

    public static void setAnnouncementsList(
            List<IGCAnnouncement> announcementsList) {
        IGCConstants.announcementsList = announcementsList;
    }

    public static List<String> getCarPoolNotificationMessageList() {
        return carPoolNotificationMessageList;
    }

    public static void setCarPoolNotificationMessageList(List<String> carPoolNotificationMessageList) {
        IGCConstants.carPoolNotificationMessageList = carPoolNotificationMessageList;
    }

    public static int getNotificationId() {
        return notificationId;
    }

    public static void setNotificationId(int notificationId) {
        IGCConstants.notificationId = notificationId;
    }

    public static Bundle editeddata = null;

    public static int getEventId() {
        return eventId;
    }

    public static void setEventId(int eventId) {
        IGCConstants.eventId = eventId;
    }

    public static String getBellCount() {
        return bellCount;
    }

    public static void setBellCount(String bellCount) {
        IGCConstants.bellCount = bellCount;
    }

    public static int getCarPoolNotificationId() {
        return carPoolNotificationId;
    }

    public static void setCarPoolNotificationId(int carPoolNotificationId) {
        IGCConstants.carPoolNotificationId = carPoolNotificationId;
    }

    public static int getCarPoolCount() {
        return carPoolCount;
    }

    public static void setCarPoolCount(int carPoolCount) {
        IGCConstants.carPoolCount = carPoolCount;
    }
}
