package com.info.igconverge.Services;

import java.util.ArrayList;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.info.igconverge.model.IGCPhotoAttachement;
import com.info.igconverge.model.IGCShopDetails;
import com.info.igconverge.utility.IGCUtils;

import android.location.Location;
import android.util.Log;

public class GooglePlaceParser {
	boolean textSearch;
	ArrayList<IGCShopDetails> list = new ArrayList<IGCShopDetails>();
	String latitude;
	String longitude;

	public GooglePlaceParser(boolean textSearch) {
		this.textSearch = textSearch;
	}

	public ArrayList<IGCShopDetails> parseData(String result, Location location)

	{

		String errorString=null;
	
		if (result != null) {
			try {
				JSONObject placesJsonObject = new JSONObject(result);

				// Getting JSON Array node
				JSONArray placesJsonArray = placesJsonObject
						.getJSONArray("results");
		

				if(placesJsonObject.has("error_message")){
					errorString = placesJsonObject.getString("error_message");
	
				}

	
				// looping through All Contacts
				if (placesJsonArray.length() == 0 || errorString != null) {
		
					return null;
				} else {

					for (int i = 0; i < placesJsonArray.length(); i++) {
						JSONObject placeDetailsObject = placesJsonArray
								.getJSONObject(i);

						IGCShopDetails details = new IGCShopDetails();
						details.setShopName(placeDetailsObject
								.optString("name"));
						details.setReferenceString(placeDetailsObject
								.optString("reference"));
						if (!textSearch) {
							details.setShopAddress(placeDetailsObject
									.optString("formatted_address"));
						} else {
							details.setShopAddress(placeDetailsObject
									.optString("vicinity"));
						}
		
						JSONObject geometry = placeDetailsObject
								.getJSONObject("geometry");
						JSONObject locationObject = geometry
								.getJSONObject("location");
						latitude = locationObject.getString("lat");
						longitude = locationObject.getString("lng");
						details.setShopLatitude(latitude);
						details.setShopLongitude(longitude);
						Location destinationLocation = new Location("");
						destinationLocation.setLatitude(Double
								.parseDouble(latitude));
						destinationLocation.setLongitude(Double
								.parseDouble(longitude));
						double destination = IGCUtils.distanceBetweenPoints(
								location, destinationLocation);
						details.setShopDistance(String.format("%.2f km",
								destination));
		if(!placeDetailsObject.isNull("rating"))
		{
						details.setShopRating(placeDetailsObject.getString("rating"));
		}
					
						if (!placeDetailsObject.isNull("photos")) {

							JSONArray attachments = placeDetailsObject.getJSONArray("photos");
							
							 ArrayList<IGCPhotoAttachement>	photoAttachement = new ArrayList<IGCPhotoAttachement>(
									attachments.length());
							for (int j= 0; j < attachments.length(); j++) {
								
								int mWidth = ((JSONObject) attachments.get(j))
										.getInt("width");
								int mHeight = ((JSONObject) attachments.get(j))
										.getInt("height");
								String mPhotoReference = ((JSONObject) attachments.get(j)).getString("photo_reference");
								
								IGCPhotoAttachement photo = new IGCPhotoAttachement( String.valueOf(mWidth), String.valueOf(mHeight),mPhotoReference);
								photoAttachement.add(photo);
			
							}
							details.setPhotoAttachements(photoAttachement);
						} else {
							 ArrayList<IGCPhotoAttachement>	photoAttachement = new ArrayList<IGCPhotoAttachement>();
					
							 details.setPhotoAttachements(photoAttachement);
						}
						list.add(details);

	

					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
		}
		return list;
	}
}
