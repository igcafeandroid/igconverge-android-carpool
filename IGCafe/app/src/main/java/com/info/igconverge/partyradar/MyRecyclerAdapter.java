package com.info.igconverge.partyradar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dashboard.IGCPartyPlaceDetailFragment;
import com.info.igconverge.model.IGCShopDetails;
import com.info.igconverge.utility.IGCConstants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class MyRecyclerAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

	private List<IGCShopDetails> feedItemList;
	private Context mContext;
	IGCDashboardActivity activity;


	public MyRecyclerAdapter(IGCDashboardActivity activity, Context context, List<IGCShopDetails> feedItemList) {
		this.feedItemList = feedItemList;
		this.mContext = context;
		this.activity=activity;

	}


	@Override
	public FeedListRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row_partyplaces, null);
		FeedListRowHolder mh = new FeedListRowHolder(v,activity);
		return mh;
	}

	@Override
	public void onBindViewHolder(final FeedListRowHolder feedListRowHolder, int position) {


		final IGCShopDetails shopDetail = feedItemList.get(position);

		feedListRowHolder.title.setText(shopDetail.getShopName());
		feedListRowHolder.distanceShop.setText(shopDetail.getShopDistance());
		if(shopDetail.getShopRating()!=null && shopDetail.getShopRating().length()>0)
		{
			feedListRowHolder.ratingBar.setRating(Float.parseFloat(shopDetail.getShopRating()));
		}
		else
		{
			feedListRowHolder.ratingBar.setRating(Float.parseFloat("0"));
		}



		feedListRowHolder.title.setText(shopDetail.getShopName());
		feedListRowHolder.main_layout_grid.setBackgroundResource(R.drawable.placeholder);
		if (shopDetail.getPhotoAttachements() != null
				&& shopDetail.getPhotoAttachements().size() > 0) {
			String url = "https://maps.googleapis.com/maps/api/place/photo?";
			String key = "key=" + IGCConstants.GOOGLE_PLACE_API_KEY;
			String sensor = "sensor=true";
			String maxWidth = "maxwidth=" + activity.screenWidth;
			String maxHeight = "maxheight=" + activity.screenWidth;
			url = url + "&" + key + "&" + sensor + "&" + maxWidth + "&"
					+ maxHeight;

			String photoReference = url
					+ "&"
					+ "photoreference="
					+ shopDetail.getPhotoAttachements().get(0)
					.getPhoto_reference();


			Picasso.with(activity).load(photoReference).into(new Target(){

				@Override
				public void onBitmapFailed(Drawable arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onBitmapLoaded(Bitmap bitmap, LoadedFrom arg1) {
					// TODO Auto-generated method stub
					feedListRowHolder.main_layout_grid.setBackgroundDrawable(new BitmapDrawable(activity.getResources(), bitmap));
				}

				@Override
				public void onPrepareLoad(Drawable arg0) {
					// TODO Auto-generated method stub

				} 

			});

		} 
		feedListRowHolder.main_layout_grid.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Fragment fragment = new IGCPartyPlaceDetailFragment();
				if (fragment != null) {

					FragmentManager fragmentManager = (activity)
							.getSupportFragmentManager();
					Bundle bundle = new Bundle();
					bundle.putParcelable("shopDetails", shopDetail);
					fragment.setArguments(bundle);
					fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment)
					.addToBackStack(null).commit();
					if ((activity.mDrawerToggle) != null)
						activity.mDrawerToggle.setDrawerIndicatorEnabled(false);

				}
			}
		});
	}

	@Override
	public int getItemCount() {
		return (null != feedItemList ? feedItemList.size() : 0);
	}


}