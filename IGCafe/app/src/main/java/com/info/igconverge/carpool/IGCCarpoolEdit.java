package com.info.igconverge.carpool;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.emergency.GPSTracker;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class IGCCarpoolEdit extends Fragment implements FragmentCallback{
    IGCDashboardActivity activity;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<OccupantsClass> occupantList;
    LinearLayout locationLayout,monLayout,tueLayout,wedLayout,thuLayout,friLayout,satLayout,sunLayout;
    TextView monday,tuesday,wednesday,thursday,friday,saturday,sunday;
    ImageView imageMon,imageTue,imageWed,imageThu,imageFri,imageSat,imageSun;
    int countMon=1,countTue=1,countWed=1,countThu=1,countFri=1,countSat=1,countSun=1;
    LinearLayout timeLayout,commentLayout,fareLayout,seatsLayout,carNoLayout,phoneLayout;
    GPSTracker gps;
    private GoogleMap gm;
    Location l;
    protected LocationManager locManager;
    LatLng loc;
    String msg="demo",postId;
    TextView textSeats,textComment,textFare,textCarNo,time,phoneNo;
    Button editButton;
    int SET_RIDE_REQUEST=111;
    double  endlatitude,endlongitude;
    private OnFragmentInteractionListener mListener;
    ArrayList<String> rideDays=new ArrayList();
    ArrayList<String> totalDays=new ArrayList();
    FragmentCallback frag;
    String setTime, setFare, setSeats, setCarNo,setPhone,rideDescription,setTime1;

    EditText editPhone,editComment,editFare,editSeats,editCarNo;
    TextView userNameText,setDestinationLocationValue;
    String destinationLocation;
    Bundle editedData;
    public boolean isRetry = false;
    public int retryCount = 0;
    boolean mapshowPlacePending=false;
    public IGCCarpoolEdit() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        frag=this;
    }
    @Override
    public void onResume() {
        super.onResume();
        initialseActionBar();
        if(retryCount==1)
        {
            retryCount = 0;
            isRetry = false;
            gps=new GPSTracker(activity);
            if (gps.canGetLocation()) {

                setMapMarker(gps.getLatitude(),gps.getLongitude());



            } else {
                isRetry = true;
                gps.showSettingsAlert();
            }
        }
    }
    private void setMapMarker(double latitude,double longitude)
    {
        if(gps.getLatitude()!=0 && gps.getLongitude()!=0) {
            loc = new LatLng(latitude, longitude);
            Marker m1 = gm.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
            gm.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (gm != null)
                        gm.animateCamera(CameraUpdateFactory.scrollBy(0, 500));
                }

            }, 3000);
        }

    }
    private void initialseActionBar() {
        // TODO Auto-generated method stub

        if (activity != null) {

            activity.actionBar.setDisplayHomeAsUpEnabled(true);
            activity.lockNavigationDrawerSwipeGesture();
            activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
            activity.actionBar.setTitle(IGCConstants.EditRide);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_igccarpool_edit, container, false);
        IGCConstants.editeddata=null;
        occupantList=new ArrayList<OccupantsClass>();
        userNameText=(TextView)v.findViewById(R.id.text_user);
        setDestinationLocationValue=(TextView)v.findViewById(R.id.setLoctionValue);
        String userName=IGCSharedPreferences.getProfileName(getContext());
        userNameText.setText(userName);

        totalDays.add(0, "Monday");
        totalDays.add(1, "Tuesday");
        totalDays.add(2, "Wednesday");
        totalDays.add(3, "Thursday");
        totalDays.add(4, "Friday");
        totalDays.add(5, "Saturday");
        totalDays.add(6, "Sunday");

        monLayout=(LinearLayout)v.findViewById(R.id.carpoolmon);
        tueLayout=(LinearLayout)v.findViewById(R.id.carpooltues);
        wedLayout=(LinearLayout)v.findViewById(R.id.carpoolwed);
        thuLayout=(LinearLayout)v.findViewById(R.id.carpoolthu);
        friLayout=(LinearLayout)v.findViewById(R.id.carpoolfri);
        satLayout=(LinearLayout)v.findViewById(R.id.carpoolsat);
        sunLayout=(LinearLayout)v.findViewById(R.id.carpoolsun);
        phoneLayout=(LinearLayout)v.findViewById(R.id.phoneLayout);

        monday=(TextView)v.findViewById(R.id.text_monday);
        imageMon=(ImageView)v.findViewById(R.id.image_monday);
        tuesday=(TextView)v.findViewById(R.id.text_tuesday);
        imageTue=(ImageView)v.findViewById(R.id.image_tuesday);
        wednesday=(TextView)v.findViewById(R.id.text_wednesday);
        imageWed=(ImageView)v.findViewById(R.id.image_wednesday);
        thursday=(TextView)v.findViewById(R.id.text_thursday);
        imageThu=(ImageView)v.findViewById(R.id.image_thursday);
        friday=(TextView)v.findViewById(R.id.text_friday);
        imageFri=(ImageView)v.findViewById(R.id.image_friday);
        saturday=(TextView)v.findViewById(R.id.text_saturday);
        imageSat=(ImageView)v.findViewById(R.id.image_saturday);
        sunday=(TextView)v.findViewById(R.id.text_sunday);
        imageSun=(ImageView)v.findViewById(R.id.image_sunday);


        textSeats = (TextView)v.findViewById(R.id.text_seats);
        textComment = (TextView) v.findViewById(R.id.text_comment);
        textFare = (TextView)v.findViewById(R.id.text_fare);
        textCarNo = (TextView)v.findViewById(R.id.text_car_no);
        time=(TextView)v.findViewById(R.id.setTime);
        phoneNo=(TextView)v.findViewById(R.id.textPhone);

        Bundle bundl = getArguments();

        setTime=bundl.getString("time");

        textFare.setText(String.valueOf(bundl.getInt("fare")));
        textSeats.setText(String.valueOf(bundl.getInt("seats")));
        //location.setText(bundl.getString("location"));
        time.setText(setTime);
        textCarNo.setText(bundl.getString("carNo"));
        textComment.setText(bundl.getString("comments"));
        phoneNo.setText(bundl.getString("phoneNo"));
        setDestinationLocationValue.setText(bundl.getString("location"));
        postId = bundl.getString("postid");
        rideDays =bundl.getStringArrayList("days");
        occupantList=bundl.getParcelableArrayList("occupantsList");

        setCarNo=textCarNo.getText().toString();
        setFare=textFare.getText().toString();
        setPhone=phoneNo.getText().toString();
        setSeats=textSeats.getText().toString();


        rideDescription=textComment.getText().toString();
        locationLayout = (LinearLayout) v.findViewById(R.id.locationlayoutEdit);
        locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, IGCPickLocationActivity.class);
                i.putExtra("requestCode", SET_RIDE_REQUEST);
                startActivityForResult(i, SET_RIDE_REQUEST);

            }
        });

        for(int i=0;i<rideDays.size();i++)
        {
            if(rideDays.get(i).equalsIgnoreCase("Monday"))
            {
                monday.setTextColor(Color.WHITE);
                imageMon.setVisibility(View.VISIBLE);
                monLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countMon++;

            }
            else if (rideDays.get(i).equalsIgnoreCase("Tuesday"))
            {
                tuesday.setTextColor(Color.WHITE);
                imageTue.setVisibility(View.VISIBLE);
                tueLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countTue++;
            }
            else if (rideDays.get(i).equalsIgnoreCase("Wednesday"))
            {
                wednesday.setTextColor(Color.WHITE);
                imageWed.setVisibility(View.VISIBLE);
                wedLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countWed++;
            }
            else if (rideDays.get(i).equalsIgnoreCase("Thursday"))
            {
                thursday.setTextColor(Color.WHITE);
                imageThu.setVisibility(View.VISIBLE);
                thuLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countThu++;
            }
            else if (rideDays.get(i).equalsIgnoreCase("Friday"))
            {
                friday.setTextColor(Color.WHITE);
                imageFri.setVisibility(View.VISIBLE);
                friLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countFri++;
            }
            else if (rideDays.get(i).equalsIgnoreCase("Saturday"))
            {
                saturday.setTextColor(Color.WHITE);
                imageSat.setVisibility(View.VISIBLE);
                satLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countSat++;
            }
            else if (rideDays.get(i).equalsIgnoreCase("Sunday"))
            {
                sunday.setTextColor(Color.WHITE);
                imageSun.setVisibility(View.VISIBLE);
                sunLayout.setBackgroundColor(getResources().getColor(R.color.green));
                countSun++;
            }
        }

        phoneLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCarpoolPhoneDialog();
            }
        });


        try {
            if (gm == null) {
                gm = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
                Log.d(msg, "Before map");
            }
            gm.setMapType(gm.MAP_TYPE_TERRAIN);
            gm.getUiSettings().setMyLocationButtonEnabled(true);
            gm.setOnMarkerClickListener(
                    new GoogleMap.OnMarkerClickListener() {
                        boolean doNotMoveCameraToCenterMarker = true;

                        public boolean onMarkerClick(Marker marker) {
                            //Do whatever you need to do here ....
                            return doNotMoveCameraToCenterMarker;
                        }
                    });
            gps = new GPSTracker(getActivity());
            if (gps.canGetLocation()) {

                setMapMarker(gps.getLatitude(), gps.getLongitude());

            }
            else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                isRetry=true;
                mapshowPlacePending=true;
                gps.showSettingsAlert();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }






        monLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (countMon%2!=0) {
                    rideDays.add("Monday");
                    monday.setTextColor(Color.WHITE);
                    imageMon.setVisibility(View.VISIBLE);
                    monLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countMon++;
                }else {
                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Monday"))
                        {
                            rideDays.remove(i);
                        }
                    }
                    monday.setTextColor(Color.BLACK);
                    imageMon.setVisibility(View.INVISIBLE);
                    monLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countMon++;

                }
            }
        });


        tueLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (countTue%2!=0) {
                    rideDays.add("Tuesday");
                    tuesday.setTextColor(Color.WHITE);
                    imageTue.setVisibility(View.VISIBLE);
                    tueLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countTue++;
                }
                else
                {
                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Tuesday"))
                        {
                            rideDays.remove(i);
                        }
                    }tuesday.setTextColor(Color.BLACK);
                    imageTue.setVisibility(View.INVISIBLE);
                    tueLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countTue++;

                }
            }
        });



        wedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(countWed%2!=0) {

                    rideDays.add("Wednesday");
                    wednesday.setTextColor(Color.WHITE);
                    imageWed.setVisibility(View.VISIBLE);
                    wedLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countWed++;
                }else{

                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Wednesday"))
                        {
                            rideDays.remove(i);
                        }
                    }
                    wednesday.setTextColor(Color.BLACK);
                    imageWed.setVisibility(View.INVISIBLE);
                    wedLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countWed++;

                }
            }
        });



        thuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(countThu%2!=0) {
                    rideDays.add("Thursday");
                    thursday.setTextColor(Color.WHITE);
                    imageThu.setVisibility(View.VISIBLE);
                    thuLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countThu++;
                }else {
                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Thursday"))
                        {
                            rideDays.remove(i);
                        }
                    }
                    thursday.setTextColor(Color.BLACK);
                    imageThu.setVisibility(View.INVISIBLE);
                    thuLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countThu++;
                }
            }
        });



        friLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(countFri%2!=0) {
                    rideDays.add("Friday");
                    friday.setTextColor(Color.WHITE);
                    imageFri.setVisibility(View.VISIBLE);
                    friLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countFri++;
                }else
                {
                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Friday"))
                        {
                            rideDays.remove(i);
                        }
                    }
                    friday.setTextColor(Color.BLACK);
                    imageFri.setVisibility(View.INVISIBLE);
                    friLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countFri++;
                }
            }
        });



        satLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(countSat%2!=0) {
                    rideDays.add("Saturday");
                    saturday.setTextColor(Color.WHITE);
                    imageSat.setVisibility(View.VISIBLE);
                    satLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countSat++;
                }else
                {
                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Saturday"))
                        {
                            rideDays.remove(i);
                        }
                    }
                    saturday.setTextColor(Color.BLACK);
                    imageSat.setVisibility(View.INVISIBLE);
                    satLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countSat++;
                }
            }
        });


        sunLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(countSun%2!=0) {
                    rideDays.add("Sunday");
                    sunday.setTextColor(Color.WHITE);
                    imageSun.setVisibility(View.VISIBLE);
                    sunLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    countSun++;
                }else
                {
                    for(int i=0;i<rideDays.size();i++)
                    {
                        if(rideDays.get(i).equalsIgnoreCase("Sunday"))
                        {
                            rideDays.remove(i);
                        }
                    }
                    sunday.setTextColor(Color.BLACK);
                    imageSun.setVisibility(View.INVISIBLE);
                    sunLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                    countSun++;
                }
            }
        });

        timeLayout=(LinearLayout)v.findViewById(R.id.timelayout);
        timeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new MyTimePicker();
                newFragment.show(getActivity().getFragmentManager(),"timePicker");


            }
        });

        commentLayout=(LinearLayout)v.findViewById(R.id.commentlayout);
        commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolCommentDialog();
            }
        });

        fareLayout=(LinearLayout)v.findViewById(R.id.farelayout);
        fareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolFareDialog();
            }
        });
        seatsLayout=(LinearLayout)v.findViewById(R.id.seatslayout);
        seatsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolSeatsDialog();
            }
        });
        carNoLayout=(LinearLayout)v.findViewById(R.id.carnolayout);
        carNoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolCarNoDialog();
            }
        });


        editButton=(Button)v.findViewById(R.id.editpostButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!time.getText().toString().equals("")) {
                    setTime = time.getText().toString();

                    if (!textFare.getText().toString() .equals("")) {
                        setFare = textFare.getText().toString();


                        if (!textSeats.getText().toString().equals("")) {
                            setSeats = textSeats.getText().toString();

                            if (!textCarNo.getText().toString().equals("")) {
                                setCarNo = textCarNo.getText().toString();
                                if(!phoneNo.getText().toString().equals("")) {
                                    setPhone = phoneNo.getText().toString();
                                    if (!setDestinationLocationValue.getText().toString().equals("")) {
                                        destinationLocation=setDestinationLocationValue.getText().toString();

                                        try {
                                            if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
                                                setTime = time.getText().toString();
                                                setCarNo = textCarNo.getText().toString();
                                                setFare = textFare.getText().toString();
                                                setSeats = textSeats.getText().toString();
                                                rideDescription = textComment.getText().toString();
                                                setPhone = phoneNo.getText().toString();
                                                destinationLocation=setDestinationLocationValue.getText().toString();
                                                JSONObject jsonObject = createJsonObjectForService(setCarNo, setTime, setFare, setSeats, rideDescription, setPhone, destinationLocation);


                                                IGCAsyncServiceRequest getPost = new IGCAsyncServiceRequest(activity, "POST", jsonObject, frag);

                                                getPost.execute(IGCConstants.updateRideRequestURL + "/" + postId);

                                            } else {
                                                ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
                                                dialog.setCancelable(false);
                                                dialog.show();
                                                System.out.println("in error dialog");

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    } else {
                                        destinationLocation = "";
                                        Toast.makeText(activity, "Please set Location.", Toast.LENGTH_SHORT).show();
                                    }

                                }
                                else{
                                    setPhone="";
                                    Toast.makeText(activity, "Please set Phone No.", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                setCarNo = "";
                                Toast.makeText(activity, "Please set Car No.", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            setSeats = "";
                            Toast.makeText(activity, "Please enter Total Vacant seats.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        setFare = "";
                        Toast.makeText(activity, "Please enter fare.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    setTime = "";
                    Toast.makeText(activity, "Please set Departure time of ride.", Toast.LENGTH_SHORT).show();
                }





            }
        });





        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity=(IGCDashboardActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    public void showCarpoolCommentDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();

      //  editComment.setRawInputType(Configuration.KEYBOARD_UNDEFINED);
        editComment = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editComment.setText(textComment.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String str = editComment.getText().toString();


                textComment.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }

    public void showCarpoolPhoneDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();


        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        editPhone = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editPhone.setRawInputType(Configuration.KEYBOARD_QWERTY);
        editPhone.setText(phoneNo.getText().toString());
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String str = editPhone.getText().toString();


                phoneNo.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }
    public void showCarpoolFareDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();

        editFare = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editFare.setRawInputType(Configuration.KEYBOARD_QWERTY);
        editFare.setText(textFare.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String str = editFare.getText().toString();


                textFare.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }

    public void showCarpoolSeatsDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();

        editSeats = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editSeats.setRawInputType(Configuration.KEYBOARD_QWERTY);
        editSeats.setText(textSeats.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String str = editSeats.getText().toString();


                textSeats.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }

    public void showCarpoolCarNoDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();

        editCarNo = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editCarNo.setText(textCarNo.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String str = editCarNo.getText().toString();


                textCarNo.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }


    @Override
    public void fragMultipleResponse(String response, String extra) {

        if (response != null && response.length() > 0) {
            try {

                if (response != null) {
                    editMessage(response);

                  //  Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
//                   Fragment detail=new IGCMyride_detail_fragment();
//                    detail.setArguments(editedData);
//                    FragmentTransaction ft = (activity).getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, detail).addToBackStack(null);
//                    ft.commit();
//                    Fragment detail=new IGCMyride_detail_fragment();
//                    detail.setArguments(editedData);
//                    FragmentTransaction ft = (activity).getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.frame_container, detail).addToBackStack(null);
//                    ft.commit();
                   activity.onBackPressed();


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

        }
   }

    private JSONObject createJsonObjectForService(String carNo,String timeValue,String fareValue,String totalSeats,String rideComment,String phoneNo,String destinationLocation)
    {
        JSONObject jsonObj = new JSONObject();;
        try {
            String actualDepTime;
            String depDateTime;
            String timeObj[] = timeValue.split(":");
            int hoursValue=Integer.parseInt(timeObj[0]);
            String timeObj1[]=timeObj[1].split(" ");
            String minuteValue=timeObj1[0];
            if(hoursValue<12)
            {
                actualDepTime=String.valueOf(hoursValue)+":"+minuteValue + " AM";

            }
            else
            {
                hoursValue=hoursValue-12;
                actualDepTime=String.valueOf(hoursValue)+":"+minuteValue + " PM";
            }

            Date date = new java.util.Date();
            Long currentTimeStamp = date.getTime();
//            Date depDate=new Date();
//            Long depDateLong=depDate.getTime();
//            String depDateString=new SimpleDateFormat("MM/dd/yyyy").format(depDateLong);
//            depDateTime=depDateString+" "+actualDepTime;


            String dateInString=new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(currentTimeStamp);
            jsonObj.put("numberOfSeats", totalSeats);
            jsonObj.put("carNumber", carNo);
            jsonObj.put("fare", fareValue);
            jsonObj.put("status",true);
            jsonObj.put("startingLocation",destinationLocation);
            jsonObj.put("departureTime", actualDepTime);
            jsonObj.put("postedOn", dateInString);
            if (!rideComment.equals("")) {
                jsonObj.put("description", rideComment);
            } else {
                jsonObj.put("description", "");
            }


            JSONObject userObj=new JSONObject();
            if(IGCSharedPreferences.getUserId(getContext())!=null && IGCSharedPreferences.getEmail(getContext())!=null && IGCSharedPreferences.getProfileName(getContext())!=null)
            {
                userObj.put("id", IGCSharedPreferences.getUserId(getContext()));
                userObj.put("name", IGCSharedPreferences.getProfileName(getContext()));
                userObj.put("email", IGCSharedPreferences.getEmail(getContext()));
                userObj.put("phoneNumber",phoneNo);
            }
            jsonObj.put("user", userObj);


            JSONArray arrayDays=new JSONArray();

            int k=0;
            for (int i=0;i<totalDays.size();i++) {

                for (int j = 0; j < rideDays.size(); j++) {
                    if (rideDays.get(j).equalsIgnoreCase(totalDays.get(i))) {
                        arrayDays.put(rideDays.get(j));
                        k++;
                        break;
                    }
                }
            }
            jsonObj.put("days",arrayDays);
            return jsonObj;

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return jsonObj;
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SET_RIDE_REQUEST) {
            Bundle bundle = null;
//            if(data.getExtras()!=null)
//            {
//                bundle  = data.getExtras();
//            }
//            double latitude = (double) bundle.get("latitude");
//            double longitude = (double) bundle.get("longitude");
//            final String location = (String) bundle.get("address");
//            if(location!=null)
//            {
//                destinationLocation=location;
//                setDestinationLocationValue.setText(location);
//            }
//
//            endlatitude = latitude;
//            endlongitude = longitude;

            if(data!=null && data.getExtras()!=null)
            {

                bundle  = data.getExtras();
                double latitude = (double) bundle.get("latitude");
                double longitude = (double) bundle.get("longitude");
                final String location = (String) bundle.get("address");
                if(location!=null)
                {
                    destinationLocation=location;
                    setDestinationLocationValue.setText(location);
                    if(latitude!=0 && longitude!=0)
                    {
                        Marker m1=  gm.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(destinationLocation));
                        m1.showInfoWindow();
                        loc = new LatLng(latitude, longitude);


                        Point mappoint = gm.getProjection().toScreenLocation(new LatLng(latitude, longitude));
                        mappoint.set(mappoint.x, mappoint.y);
                        gm.animateCamera(CameraUpdateFactory.newLatLngZoom(gm.getProjection().fromScreenLocation(mappoint),13));


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                gm.animateCamera(CameraUpdateFactory.scrollBy(0, 500));
                            }

                        }, 2000);
                    }

                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



public void editMessage(String response)
    {    EditMessageClass editMessageObject=new EditMessageClass();
        EditMessageParser editparser=new EditMessageParser();
        editMessageObject= editparser.parseData(response);
        System.out.print(editMessageObject.toString());
        editedData=new Bundle();
        IGCConstants.editeddata=new Bundle();
       ArrayList<String> sortedDays=new ArrayList<>();
        sortedDays= IGCUtils.sortDays(rideDays);
        if(editMessageObject!=null)
        {
            if (editMessageObject.getStatus())
            {
                editedData.putString("location", setDestinationLocationValue.getText().toString());
               editedData.putStringArrayList("Days", sortedDays);
                editedData.putString("time", time.getText().toString());
                editedData.putInt("fare", Integer.parseInt(textFare.getText().toString()));
                editedData.putInt("seats", Integer.parseInt(textSeats.getText().toString()));
                editedData.putString("carNo", textCarNo.getText().toString());
                editedData.putString("phoneNo",phoneNo.getText().toString());
                editedData.putString("comments",textComment.getText().toString());
                editedData.putString("driver",IGCSharedPreferences.getProfileName(getContext()));
                editedData.putString("postid",postId);
                editedData.putParcelableArrayList("occupantsList", occupantList);
                Toast.makeText(getContext(), "Updated Sucessfully", Toast.LENGTH_SHORT).show();



            }
            else
            {
                editedData.putString("location",setDestinationLocationValue.getText().toString());
                editedData.putStringArrayList("Days", sortedDays);
                editedData.putString("time",time.getText().toString());
                editedData.putInt("fare", Integer.parseInt(textFare.getText().toString()));
                editedData.putInt("seats", Integer.parseInt(textSeats.getText().toString()));
                editedData.putString("carNo",textCarNo.getText().toString());
                editedData.putString("phoneNo",phoneNo.getText().toString());
                editedData.putString("comments",textComment.getText().toString());
                editedData.putString("driver",IGCSharedPreferences.getProfileName(getContext()));
                editedData.putParcelableArrayList("occupantsList", occupantList);
                editedData.putString("postid",postId);
                Toast.makeText(getContext(), "Update failed", Toast.LENGTH_SHORT).show();
            }
            IGCConstants.editeddata=editedData;

        }



    }


}
