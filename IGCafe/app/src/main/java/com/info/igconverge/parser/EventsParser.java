package com.info.igconverge.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.info.igconverge.model.IGCEvent;
import com.info.igconverge.model.IGCEventItem;

import com.info.igconverge.utility.IGCUtils;

public class EventsParser {
	ArrayList<IGCEvent> list = new ArrayList<IGCEvent>();

	public EventsParser() {

	}

	public ArrayList<IGCEvent> parseFetchedEvents(String jsonData) {
		if (jsonData != "")
			try {

				JSONArray rootArray = new JSONArray(jsonData);
				for (int index = 0; index < rootArray.length(); index++) {
					JSONObject eventObject = rootArray.getJSONObject(index);
					IGCEventItem igcEventItem = new IGCEventItem();
					IGCEvent events = new IGCEvent();
					JSONObject itemArray = new JSONObject();

					events.setImageId(eventObject.getString("imageId"));
					itemArray = eventObject.getJSONObject("event");
					String startEvent = itemArray.getString("startTime");
					String startDate = startEvent.split(" ")[0];
					String startTime = startEvent.split(" ")[1] + " "
							+ startEvent.split(" ")[2];
					String endEvent = itemArray.getString("endTime");

					String endDate = endEvent.split(" ")[0];
					String endTime = endEvent.split(" ")[1] + " "
							+ endEvent.split(" ")[2];
					igcEventItem.setEventId(itemArray.getString("id"));
					igcEventItem.setEventName(itemArray.getString("name"));
					igcEventItem.setEventDate(itemArray.getString("startTime"));
					igcEventItem
							.setEventEndDate(itemArray.getString("endTime"));

					igcEventItem.setEventTime(IGCUtils
							.getCurrentDateToDisplayDate(startDate)
							+ " - "
							+ IGCUtils.getCurrentDateToDisplayDate(startDate)
							+ " " + startTime + " - " + endTime);

					if (startDate.equals(endDate)) {
						igcEventItem.setEventTimeStamp((IGCUtils
								.getCurrentDateToDisplayDate(startDate)
								+ " "
								+ startTime + " - " + endTime));
					} 
						igcEventItem.setEventTime(IGCUtils
								.getCurrentDateToDisplayDate(startDate)
								+ " - "
								+ IGCUtils.getCurrentDateToDisplayDate(endDate)
								+ " " + startTime + " - " + endTime);
				

					igcEventItem.setEventLocation(itemArray.getString("venue"));
					igcEventItem.setEventDescription(itemArray
							.getString("description"));
					igcEventItem.setEventCategory(itemArray
							.getString("category"));
					igcEventItem.setNotification(itemArray
							.getBoolean("notification"));

					events.setEventItem(igcEventItem);
					list.add(events);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return list;
	}

	public void formatEventDate(String date) {
		String startDate = date.split(" ")[0];
		String startTime = date.split(" ")[1];
		String displayDate = IGCUtils.getCurrentDateToDisplayDate(startDate);

	}
}
