package com.info.igconverge.holidays;

import android.app.Activity;
import android.content.Context;

import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;

import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.ListView;

import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

import com.info.igconverge.R;
import com.info.igconverge.adapter.IGCHolidaysListAdapter;

import com.info.igconverge.dashboard.IGCDashboardActivity;

import com.info.igconverge.model.IGCHolidaysList;



public class IGCShowHolidaysFragment extends Fragment {
	IGCDashboardActivity activity;
	RecyclerView mRecyclerView;
	private RecyclerView.Adapter mAdapter;
	IGCHolidaysListAdapter listAdapter;
	ListView listView;
	List<IGCHolidaysList> holidaysList;
	private RecyclerView.LayoutManager mLayoutManager;
	String[] locationArray;
	int selectedItem = 0;
	View olderView;
	View currentView;
	ImageView thumbnail;
	private String[] locationIconArray;


	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.unlockNavigationDrawerSwipeGesture();
		activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			@Nullable Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_holidays_list,
				container, false);
		mRecyclerView = (RecyclerView) rootView
				.findViewById(R.id.holidays_recycler_view);

		listView = (ListView) rootView.findViewById(R.id.listView);

		locationArray = getResources().getStringArray(R.array.loactions);

		mLayoutManager = new LinearLayoutManager(activity,
				LinearLayoutManager.HORIZONTAL, false);
		mRecyclerView.setLayoutManager(mLayoutManager);

		holidaysList = fetchHolidaysData(0);


		mAdapter = new LocationsAdapter(Arrays.asList(locationArray));

		mRecyclerView.setAdapter(mAdapter);

		return rootView;
	}

	public class LocationsAdapter extends
	RecyclerView.Adapter<LocationsAdapter.LocationViewHolder> {
		View itemView;
		private List<String> locationList;

		public LocationsAdapter(List<String> locationList) {
			this.locationList = locationList;
		}

		@Override
		public int getItemCount() {
			return locationList.size();
		}

		@Override
		public void onBindViewHolder(LocationViewHolder locationViewHolder,
				int position) {
			locationViewHolder.itemView.setSelected(selectedItem == position);

			locationViewHolder.title.setText(locationList.get(position));
			locationIconArray = getResources().getStringArray(
					R.array.location_icon);
			Drawable drawable = getResources().getDrawable(
					getResources().getIdentifier(locationIconArray[position],
							"drawable", activity.getPackageName()));
			locationViewHolder.thumbnail.setImageDrawable(drawable);

			if (position == 0) {
				locationViewHolder.itemView.setAlpha(1f);
				locationViewHolder.currentView.setVisibility(View.VISIBLE);
				olderView = locationViewHolder.itemView;
			} else {
				locationViewHolder.currentView.setVisibility(View.GONE);
				locationViewHolder.itemView.setAlpha(0.3f);
			}
			locationViewHolder.itemView
			.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
			
		
					selectedItem = mRecyclerView.getChildPosition(view);

					if (olderView != null) {
						olderView.setAlpha(0.3f);
						currentView = olderView
								.findViewById(R.id.currentView);
						currentView.setVisibility(View.GONE);
				
					}
					view.setAlpha(1f);
					currentView = view.findViewById(R.id.currentView);
					currentView.setVisibility(View.VISIBLE);
					olderView = view;
					// notifyItemChanged(selectedItem);
					fetchHolidaysData(selectedItem);
					listAdapter.notifyDataSetChanged();
				}
			});
		}

		@Override
		public LocationViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		
			itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
					R.layout.holiday_location_items, viewGroup, false);
			currentView = itemView.findViewById(R.id.currentView);
			thumbnail = (ImageView) itemView.findViewById(R.id.icon);
			return new LocationViewHolder(itemView);
		}

		public class LocationViewHolder extends RecyclerView.ViewHolder
		implements android.view.View.OnClickListener {
			protected ImageView thumbnail;
			protected TextView title;
			protected TextView currentPosition;
			protected View currentView;
			protected LinearLayout layoutRelativeView;

			public LocationViewHolder(View itemView) {
				super(itemView);
	
				this.thumbnail = (ImageView) itemView.findViewById(R.id.icon);
				this.title = (TextView) itemView.findViewById(R.id.title);
			
				this.currentView = (View) itemView
						.findViewById(R.id.currentView);

			}

			@Override
			public void onClick(View view) {
			}

		}
	}

	public List<IGCHolidaysList> fetchHolidaysData(int position) {
	
		List<IGCHolidaysList> holidaysList = new ArrayList<IGCHolidaysList>();
		if (position == 0 ) {

			//january
			holidaysList.add(new IGCHolidaysList("January", "New Year's Day","Friday", "January 1, 2016", true));
			holidaysList.add(new IGCHolidaysList(null, "Republic Day","Tuesday", "January 26, 2016", false));
			//march
			holidaysList.add(new IGCHolidaysList("March", "Mahashivratri", "Monday","March 7, 2016", true));
			holidaysList.add(new IGCHolidaysList(null, "Holi", "Thursday","March 24, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Good Friday", "Friday","March 25, 2016", true));
			//july
			holidaysList.add(new IGCHolidaysList("July", "Id-Ul-Fitr (Ramzan Eid)*", "Wednesday","July 6, 2016", false));
			//august
			holidaysList.add(new IGCHolidaysList("August", "Independence Day", "Monday","August 15, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Raksha Bandhan", "Thursday","August 18, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Janmashtami", "Thursday","August 25, 2016", true));
			//september
			holidaysList.add(new IGCHolidaysList("September", "Ganesh Chathurthi", "Monday","September 5, 2016", true));

			//october
			holidaysList.add(new IGCHolidaysList("October", "Mahatma Gandhi's Birthday", "Sunday","October 2, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Dusshera", "Tuesday","October 11, 2016", false));
			//november
			holidaysList.add(new IGCHolidaysList("November", "Bhai Duj", "Tuesday","November 1, 2016", true));
			holidaysList.add(new IGCHolidaysList(null, "Guru Nanak's Birthday", "Monday","November 14, 2016", true));




		}
		else if(position==1)
		{
			//january
			holidaysList.add(new IGCHolidaysList("January", "New Year's Day","Friday", "January 1, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Republic Day","Tuesday", "January 26, 2016", false));
			//march
			holidaysList.add(new IGCHolidaysList("March", "Mahashivratri", "Monday","March 7, 2016", true));
			holidaysList.add(new IGCHolidaysList(null, "Dhulivandan", "Thursday","March 24, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Good Friday", "Friday","March 25, 2016", true));
			//april
			holidaysList.add(new IGCHolidaysList("April", "Gudi Padwa", "Friday","April 08, 2016", true));
			//may
			holidaysList.add(new IGCHolidaysList("May", "Maharashtra Day/Labour Day","Sunday", "May 1, 2016", false));
			//july
			holidaysList.add(new IGCHolidaysList("July", "Ramzan Eid*","Wednesday", "July 6, 2016", true));
			//August
			holidaysList.add(new IGCHolidaysList("August", "Independence Day","Monday", "August 15,2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Raksha Bandhan","Thursday", "August 18,2016", true));
			//september
			holidaysList.add(new IGCHolidaysList("September", "Ganesh Chaturthi","Monday", "September 5, 2016", false));
			//october
			holidaysList.add(new IGCHolidaysList("October", "Mahatma Gandhi's Birthday", "Sunday","October 2, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Dusshera","Tuesday", "October 11, 2016", true));
			//november
			holidaysList.add(new IGCHolidaysList("November", "Bhai Duj","Tuesday", "November 1, 2016", true));

		}
		else if (position == 2 ) {


			//january
			holidaysList.add(new IGCHolidaysList("January", "New Year's Day","Friday", "January 1, 2016", false));
			//may
			holidaysList.add(new IGCHolidaysList("May", "Leilat al-Meiraj(Ascension of the Prophet)","Thursday", "May 5, 2016", false));
			//july
			holidaysList.add(new IGCHolidaysList("July", "Eid al-Fitr (End of Ramadan)","Thursday", "July 7, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Eid al-Fitr (End of Ramadan)","Friday", "July 8, 2016", false));
			//september
			holidaysList.add(new IGCHolidaysList("September", "Eid al-Adha (Feast of the sacrifice)","Sunday", "September 11, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Eid al-Adha (Feast of the sacrifice)","Monday", "September 12, 2016", false));
			//october
			holidaysList.add(new IGCHolidaysList("October", "Al-Hijra (Islamic New Year)","Sunday", "October 2, 2016", false));
			//december
			holidaysList.add(new IGCHolidaysList("December", "National Day","Friday", "December 02, 2016", false));


		}
		else if( position == 3)
		{
			holidaysList.add(new IGCHolidaysList("January", "New Year's Day","Friday", "January 1, 2016", false));
			holidaysList.add(new IGCHolidaysList("February", "Presidents Day","Monday", "February 15, 2016", false));
			//may
			holidaysList.add(new IGCHolidaysList("May", "Memorial Day","Monday", "May 30, 2016", false));
			//july
			holidaysList.add(new IGCHolidaysList("July", "Independence day","Monday", "July 4, 2016", false));
			//september
			holidaysList.add(new IGCHolidaysList("September", "Labor Day","Monday", "September 5, 2016", false));
            //november
			holidaysList.add(new IGCHolidaysList("November", "Thanksgiving","Thursday", "November 24, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Thanksgiving Weekend","Friday", "November 25, 2016", false));
			//december

			holidaysList.add(new IGCHolidaysList("December", "Christmas Day Observed","Monday", "December 26, 2016", false));

			holidaysList.add(new IGCHolidaysList(null, "Corporate Office Closed PTO Day*","Tuesday", "December 27, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "Corporate Office Closed PTO Day*","Wednesday", "December 28, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "New Year's Eve Observed","Thursday", "December 29, 2016", false));
			holidaysList.add(new IGCHolidaysList(null, "New Year's Eve Observed","Friday", "December 30, 2016", false));
		}
		listAdapter = new IGCHolidaysListAdapter(activity, holidaysList);

		listView.setAdapter(listAdapter);
		return holidaysList;

	}

	public class DividerItemDecoration extends RecyclerView.ItemDecoration {

		private final int[] ATTRS = new int[] { android.R.attr.listDivider };

		public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

		public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

		private Drawable mDivider;

		private int mOrientation;

		public DividerItemDecoration(Context context, int orientation) {
			final TypedArray a = context.obtainStyledAttributes(ATTRS);
			mDivider = a.getDrawable(0);
			a.recycle();
			setOrientation(orientation);
		}

		public void setOrientation(int orientation) {
			if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
				throw new IllegalArgumentException("invalid orientation");
			}
			mOrientation = orientation;
		}

		@Override
		public void onDraw(Canvas c, RecyclerView parent) {
			if (mOrientation == VERTICAL_LIST) {
				drawVertical(c, parent);
			} else {
				drawHorizontal(c, parent);
			}
		}

		public void drawVertical(Canvas c, RecyclerView parent) {
			final int left = parent.getPaddingLeft();
			final int right = parent.getWidth() - parent.getPaddingRight();

			final int childCount = parent.getChildCount();
			for (int i = 0; i < childCount; i++) {
				final View child = parent.getChildAt(i);
				final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
						.getLayoutParams();
				final int top = child.getBottom() + params.bottomMargin;
				final int bottom = top + mDivider.getIntrinsicHeight();
				mDivider.setBounds(left, top, right, bottom);
				mDivider.draw(c);
			}
		}

		public void drawHorizontal(Canvas c, RecyclerView parent) {
			final int top = parent.getPaddingTop();
			final int bottom = parent.getHeight() - parent.getPaddingBottom();

			final int childCount = parent.getChildCount();
			for (int i = 0; i < childCount; i++) {
				final View child = parent.getChildAt(i);
				final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
						.getLayoutParams();
				final int left = child.getRight() + params.rightMargin;
				final int right = left + mDivider.getIntrinsicHeight();
				mDivider.setBounds(left, top, right + 5, bottom);
				mDivider.draw(c);
			}
		}

		@Override
		public void getItemOffsets(Rect outRect, int itemPosition,
				RecyclerView parent) {
			if (mOrientation == VERTICAL_LIST) {
				outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
			} else {
				outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
			}
		}

	}

	public class ItemAnimator extends DefaultItemAnimator {

		private class ChangeInfo {
			public ViewHolder oldHolder, newHolder;
			public int fromX, fromY, toX, toY;

			private ChangeInfo(ViewHolder oldHolder, ViewHolder newHolder) {
				this.oldHolder = oldHolder;
				this.newHolder = newHolder;
			}

			private ChangeInfo(ViewHolder oldHolder, ViewHolder newHolder,
					int fromX, int fromY, int toX, int toY) {
				this(oldHolder, newHolder);
				this.fromX = fromX;
				this.fromY = fromY;
				this.toX = toX;
				this.toY = toY;
			}

			@Override
			public String toString() {
				return "ChangeInfo{" + "oldHolder=" + oldHolder
						+ ", newHolder=" + newHolder + ", fromX=" + fromX
						+ ", fromY=" + fromY + ", toX=" + toX + ", toY=" + toY
						+ '}';
			}
		}

		private ArrayList<ChangeInfo> mPendingChanges = new ArrayList<ChangeInfo>();

		@Override
		public boolean animateAdd(ViewHolder arg0) {

			return false;
		}

		@Override
		public boolean animateChange(ViewHolder oldHolder,
				ViewHolder newHolder, int fromX, int fromY, int toX, int toY) {
			
			final float prevTranslationX = ViewCompat
					.getTranslationX(oldHolder.itemView);
			final float prevTranslationY = ViewCompat
					.getTranslationY(oldHolder.itemView);
			final float prevAlpha = ViewCompat.getAlpha(oldHolder.itemView);
			
			endAnimation(oldHolder);
			int deltaX = (int) (toX - fromX - prevTranslationX);
			int deltaY = (int) (toY - fromY - prevTranslationY);

			ViewCompat.setTranslationX(oldHolder.itemView, prevTranslationX);
			ViewCompat.setTranslationY(oldHolder.itemView, prevTranslationY);
			ViewCompat.setAlpha(oldHolder.itemView, prevAlpha);
			
			if (newHolder != null && newHolder.itemView != null) {
		
				endAnimation(newHolder);
				ViewCompat.setTranslationX(newHolder.itemView, -deltaX);
				ViewCompat.setTranslationY(newHolder.itemView, -deltaY);
				ViewCompat.setAlpha(newHolder.itemView, 1);
				final float nextAlpha = ViewCompat.getAlpha(newHolder.itemView);
				
			}
			mPendingChanges.add(new ChangeInfo(oldHolder, newHolder, fromX,
					fromY, toX, toY));
			return true;
		}

		@Override
		public boolean animateMove(ViewHolder arg0, int arg1, int arg2,
				int arg3, int arg4) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean animateRemove(ViewHolder arg0) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void endAnimation(ViewHolder arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void endAnimations() {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isRunning() {
			// TODO Auto-generated method stub
			return false;
		}

	}
}
