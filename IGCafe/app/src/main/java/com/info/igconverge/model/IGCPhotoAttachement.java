package com.info.igconverge.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class IGCPhotoAttachement implements Parcelable {

	String width;
	String height;
	String photo_reference;

	public IGCPhotoAttachement(String width, String height,
			String photo_reference) {
		super();
		this.width = width;
		this.height = height;
		this.photo_reference = photo_reference;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getPhoto_reference() {
		return photo_reference;
	}

	public void setPhoto_reference(String photo_reference) {
		this.photo_reference = photo_reference;
	}

	@Override
	public String toString() {
		return "IGCPhotoAttachement [width=" + width + ", height=" + height
				+ ", photo_reference=" + photo_reference + "]";
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeStringArray(new String[] { this.width, this.height,
				this.photo_reference });
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public IGCPhotoAttachement createFromParcel(Parcel in) {
			return new IGCPhotoAttachement(in);
		}

		public IGCPhotoAttachement[] newArray(int size) {
			return new IGCPhotoAttachement[size];
		}
	};

	public IGCPhotoAttachement(Parcel in) {
		String[] data = new String[3];

		in.readStringArray(data);
		this.width = data[0];
		this.height = data[1];
		this.photo_reference = data[2];
	}

}
