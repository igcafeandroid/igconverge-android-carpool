package com.info.igconverge.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class IGCDestinationLocationParser
{
	String location = "";
	public IGCDestinationLocationParser()
	{
		// TODO Auto-generated constructor stub
	}
	
	public String locationParser(String result)
	{
		if(result != null)
		{
			try {
				JSONObject jsonObj = new JSONObject(result);
				
				// Getting JSON Array node
				JSONArray results = jsonObj.getJSONArray("results");

				// looping through All Contacts
				for (int i = 0; i < results.length(); i++) {
					JSONObject detailos = results.getJSONObject(i);
					JSONObject geometry = detailos.getJSONObject("geometry");
					JSONObject locationObject = geometry.getJSONObject("location");
					location = location + locationObject.getString("lat")+","+locationObject.getString("lng");
					if(i != results.length() - 1)
					{
						location = location+"%7C";
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			Log.e("ServiceHandler", "Couldn't get any data from the url");
		}
		return location;
	}
}
