package com.info.igconverge.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.info.igconverge.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.telephony.TelephonyManager;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

public class IGCUtils {

	public static int getImageOrientation(File imageFile) {
		int rotate = 0;
		try {

			ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rotate;
	}

	public static Bitmap drawText(String text) {

		Bitmap bitMap = Bitmap.createBitmap(80, 80, Bitmap.Config.ARGB_8888);

		bitMap = bitMap.copy(bitMap.getConfig(), true);
		// Construct a canvas with the specified bitmap to draw into
		Canvas canvas = new Canvas(bitMap);
		// Create a new paint with default settings.
		Paint paint = new Paint();
		// smooths out the edges of what is being drawn
		paint.setAntiAlias(true);
		// set color
		Random rnd = new Random();
		paint.setARGB(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
		// set style
		paint.setStyle(Paint.Style.FILL);
		// set stroke
		paint.setStrokeWidth(4.5f);
		// draw circle with radius 30
		canvas.drawCircle(40, 40, 40, paint);
		paint.setColor(Color.BLACK);
		TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG
				| Paint.LINEAR_TEXT_FLAG);
		textPaint.setStyle(Paint.Style.FILL);
		textPaint.setColor(Color.WHITE);
		textPaint.setTextSize(50);

		canvas.drawText(text.toUpperCase(Locale.ENGLISH), 25, 55, textPaint);

		return bitMap;
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {

			byte[] bytes = new byte[buffer_size];
			for (;;) {
				// Read byte from input stream

				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;

				// Write byte from output stream
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static double distanceBetweenPoints(Location myLocation,
			Location destinationLocation) {
		return myLocation.distanceTo(destinationLocation) / 1000;
	}

	public static int convertDpToPixel(Context context, int dp) {
		float density = context.getResources().getDisplayMetrics().density;
		return Math.round((float) dp * density);
	}

	/*
	 * 
	 * get list of dates between startDate and endDate for events
	 */
	public static List<String> datesBetween(String start, String end) {

		SimpleDateFormat sdf = new SimpleDateFormat(
				IGCConstants.currrentdateFormat);
		Date fromDate, toDate, date;
		List<Date> dates = new ArrayList<Date>(25);
		List<String> datesList = new ArrayList<String>();
		try {
			fromDate = sdf.parse(start);
			toDate = sdf.parse(end);
			Calendar cal = Calendar.getInstance();
			cal.setTime(fromDate);
			while (cal.getTime().before(toDate)) {
				cal.add(Calendar.DATE, 1);
				date = cal.getTime();
				SimpleDateFormat sdf1 = new SimpleDateFormat(
						IGCConstants.currrentdateFormat);

				String dateStr = sdf1.format(date);
				datesList.add(dateStr);

			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return datesList;
	}

	public static List<Date> dates() {
		List<Date> date = new ArrayList<Date>();

//		try {
//			String d = "Sat, 19 Sep 2015 05:27:17 GMT";
//
//			SimpleDateFormat formatter = new SimpleDateFormat(
//					"EEE, dd MMM yyyy HH:mm:ss 'GMT'");
//			IGCConstants.serverDate = formatter.parse(d);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		// Calendar calendar = Calendar.getInstance();
		// calendar.add(Calendar.DATE, -1);
		// IGCConstants.serverDate = calendar.getTime();

		if (IGCConstants.serverDate != null) {

			Date defaultDate = IGCConstants.serverDate;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(defaultDate);
			calendar.set(Calendar.DAY_OF_WEEK, 2);
			Date startDate = calendar.getTime();
			calendar.set(Calendar.DAY_OF_WEEK, 7);
			calendar.setTime(calendar.getTime());
			// calendar.add(Calendar.DATE, 1);
			Date endDate = calendar.getTime();

			date.add(startDate);
			date.add(endDate);
			date.add(defaultDate);
		}

		return date;
	}

	public static Bitmap createBitmapForProfilePic(Uri selectedImageUri,
			File capturedFile, Context context, int requiredSize) {
		Bitmap rotatedBitmap = null;
		Bitmap cameraImageBitmap = null;
		try {
			rotatedBitmap = decodeUri(selectedImageUri, context, requiredSize);

			rotatedBitmap = getRoundedRectBitmap(rotatedBitmap,
					requiredSize / 2, requiredSize, requiredSize);

			Matrix matrix = new Matrix();
			matrix.postRotate(IGCUtils.getImageOrientation(capturedFile));

			rotatedBitmap = Bitmap.createBitmap(rotatedBitmap, 0, 0,
					rotatedBitmap.getWidth(), rotatedBitmap.getHeight(),
					matrix, true);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rotatedBitmap;

	}

	public static Bitmap getRoundedRectBitmap(Bitmap bitmap, int pixels,
			int requiredWidth, int requiredhight) {
		Bitmap result = null;
		Canvas canvas;
		Bitmap sbmp;
		if (bitmap.getWidth() != pixels || bitmap.getHeight() != pixels) {
			float _w_rate = 1.0f * pixels / bitmap.getWidth();
			float _h_rate = 1.0f * pixels / bitmap.getHeight();
			float _rate = _w_rate < _h_rate ? _h_rate : _w_rate;
			sbmp = Bitmap.createScaledBitmap(bitmap,
					(int) (bitmap.getWidth() * _rate),
					(int) (bitmap.getHeight() * _rate), false);
		} else
			sbmp = bitmap;

		try {
			Bitmap resized = Bitmap.createScaledBitmap(bitmap, requiredWidth,
					requiredhight, true);

			result = Bitmap.createBitmap(resized.getWidth(),
					resized.getHeight(), Bitmap.Config.ARGB_8888);
			canvas = new Canvas(result);

			int color = 0xff424242;
			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, resized.getWidth(), resized.getHeight());
			RectF rectF = new RectF(rect);
			int roundPx = pixels;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(resized, rect, rect, paint);

		} catch (NullPointerException e) {

		} catch (OutOfMemoryError o) {

		}
		return result;
	}

	public static Bitmap createBitmapForAuthentication(Uri selectedImageUri,
			File capturedFile, Context context, int width, int height) {
		Bitmap rotatedBitmap = null;
		try {
			rotatedBitmap = decodeUri(selectedImageUri, context, width);
			int radius = width < height ? width : height;
			if (rotatedBitmap.getWidth() != radius
					|| rotatedBitmap.getHeight() != radius) {
				float _w_rate = 1.0f * radius / rotatedBitmap.getWidth();
				float _h_rate = 1.0f * radius / rotatedBitmap.getHeight();
				float _rate = _w_rate < _h_rate ? _h_rate : _w_rate;
				rotatedBitmap = Bitmap.createScaledBitmap(rotatedBitmap,
						(int) (rotatedBitmap.getWidth() * _rate),
						(int) (rotatedBitmap.getHeight() * _rate), false);
			}

			Matrix matrix = new Matrix();
			matrix.postRotate(IGCUtils.getImageOrientation(capturedFile));
			rotatedBitmap = Bitmap.createBitmap(rotatedBitmap, 0, 0,
					rotatedBitmap.getWidth(), rotatedBitmap.getHeight(),
					matrix, true);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rotatedBitmap;

	}

	public static Bitmap decodeUri(Uri selectedImage, Context context,
			int requiredSize) throws FileNotFoundException {

		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(context.getContentResolver()
				.openInputStream(selectedImage), null, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = requiredSize;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(context.getContentResolver()
				.openInputStream(selectedImage), null, o2);

	}

	public static String getCurrentDateToDisplayDate(String currentDate) {
		String displayDate = null;
		try {
			SimpleDateFormat sf1 = new SimpleDateFormat(
					IGCConstants.currrentdateFormat);
			Date date = sf1.parse(currentDate);
			SimpleDateFormat sf2 = new SimpleDateFormat(
					IGCConstants.displayDateFormat);
			displayDate = sf2.format(date);

		} catch (Exception e) {

		}
		return displayDate;
	}

	public static Date getDateFromString(String currentDate) {
		Date date = null;
		try {
			SimpleDateFormat sf1 = new SimpleDateFormat(
					IGCConstants.currrentdateFormat);
			date = sf1.parse(currentDate);

		} catch (Exception e) {

		}
		return date;
	}

	public static Date getDate(String currentDate) {

		try {
			SimpleDateFormat sf1 = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			Date date = sf1.parse(currentDate);
			SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String displayDate = sf2.format(date);

			SimpleDateFormat sf3 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date dateNew = sf3.parse(displayDate);

			return dateNew;
		} catch (Exception e) {

		}
		return null;
	}

	public static String getStringFromDate(Date date) {
		String dateStr = null;
		try {
			SimpleDateFormat sf1 = new SimpleDateFormat(
					IGCConstants.currrentdateFormat);
			dateStr = sf1.format(date);

		} catch (Exception e) {

		}
		return dateStr;
	}

	public static Date currentDate() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}

	public static String getTimeAgo(Date date, Context ctx) {

		if (date == null) {
			return null;
		}

		long time = date.getTime();

		Date curDate = currentDate();
		long now = curDate.getTime();
		if (time > now || time <= 0) {
			return null;
		}

		int dim = getTimeDistanceInMinutes(time);

		String timeAgo = null;

		if (dim == 0) {
			timeAgo = ctx.getResources()
					.getString(R.string.date_util_term_less)
					+ " "
					+ ctx.getResources().getString(R.string.date_util_term_a)
					+ " "
					+ ctx.getResources().getString(
							R.string.date_util_unit_minute);
		} else if (dim == 1) {
			return "1 "
					+ ctx.getResources().getString(
							R.string.date_util_unit_minute);
		} else if (dim >= 2 && dim <= 44) {
			timeAgo = dim
					+ " "
					+ ctx.getResources().getString(
							R.string.date_util_unit_minutes);
		} else if (dim >= 45 && dim < 90) {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_about)
					+ " "
					+ ctx.getResources().getString(R.string.date_util_term_an)
					+ " "
					+ ctx.getResources()
							.getString(R.string.date_util_unit_hour);
		} else if (dim >= 90 && dim <= 1439) {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_about)
					+ " "
					+ (Math.round(dim / 60))
					+ " "
					+ ctx.getResources().getString(
							R.string.date_util_unit_hours);
		} else if (dim >= 1440 && dim <= 2519) {
			timeAgo = "1 "
					+ ctx.getResources().getString(R.string.date_util_unit_day);
		} else if (dim >= 2520 && dim <= 43199) {
			timeAgo = (Math.round(dim / 1440))
					+ " "
					+ ctx.getResources()
							.getString(R.string.date_util_unit_days);
		} else if (dim >= 43200 && dim <= 86399) {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_about)
					+ " "
					+ ctx.getResources().getString(R.string.date_util_term_a)
					+ " "
					+ ctx.getResources().getString(
							R.string.date_util_unit_month);
		} else if (dim >= 86400 && dim <= 525599) {
			timeAgo = (Math.round(dim / 43200))
					+ " "
					+ ctx.getResources().getString(
							R.string.date_util_unit_months);
		} else if (dim >= 525600 && dim <= 655199) {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_about)
					+ " "
					+ ctx.getResources().getString(R.string.date_util_term_a)
					+ " "
					+ ctx.getResources()
							.getString(R.string.date_util_unit_year);
		} else if (dim >= 655200 && dim <= 914399) {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_over)
					+ " "
					+ ctx.getResources().getString(R.string.date_util_term_a)
					+ " "
					+ ctx.getResources()
							.getString(R.string.date_util_unit_year);
		} else if (dim >= 914400 && dim <= 1051199) {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_almost)
					+ " 2 "
					+ ctx.getResources().getString(
							R.string.date_util_unit_years);
		} else {
			timeAgo = ctx.getResources().getString(
					R.string.date_util_prefix_about)
					+ " "
					+ (Math.round(dim / 525600))
					+ " "
					+ ctx.getResources().getString(
							R.string.date_util_unit_years);
		}

		return timeAgo + " "
				+ ctx.getResources().getString(R.string.date_util_suffix);
	}

	private static int getTimeDistanceInMinutes(long time) {
		long timeDistance = currentDate().getTime() - time;
		return Math.round((Math.abs(timeDistance) / 1000) / 60);
	}

	public static String getRealPathFromURI(Context context, Uri contentURI) {
		String result = null;
		Cursor cursor = context.getContentResolver().query(contentURI, null,
				null, null, null);

		if (cursor != null && cursor.getColumnCount() != 0) {
			// cursor.moveToFirst();
			if (cursor.moveToFirst()) {

				try {
					if (!cursor
							.isNull(cursor
									.getColumnIndex(MediaStore.Images.ImageColumns.DATA))) {
						int idx = cursor
								.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
						result = cursor.getString(idx);
					}
				} catch (Exception tr) {
					tr.printStackTrace();
				}

				cursor.close();
			}
		} else {
			result = contentURI.getPath();
		}
		return result;
	}
	
	public static boolean isTelephonyEnabled(Context appcontext){
	    TelephonyManager tm = (TelephonyManager)appcontext.getSystemService(Context.TELEPHONY_SERVICE);
	    return tm != null && tm.getSimState()==TelephonyManager.SIM_STATE_READY;
	}


	public static ArrayList<String> sortDays(ArrayList<String> days)
	{
		ArrayList<String> totalDays=new ArrayList<>();
		ArrayList<String> arrayDays=new ArrayList<>();
		totalDays.add(0, "Monday");
		totalDays.add(1, "Tuesday");
		totalDays.add(2, "Wednesday");
		totalDays.add(3, "Thursday");
		totalDays.add(4, "Friday");
		totalDays.add(5, "Saturday");
		totalDays.add(6, "Sunday");

		int k=0;
	for (int i=0;i<totalDays.size();i++) {

		for (int j = 0; j < days.size(); j++) {
			if (days.get(j).equalsIgnoreCase(totalDays.get(i))) {
				arrayDays.add(days.get(j));
				k++;
				break;
			}


		}
	}
		return arrayDays;
	}
}
