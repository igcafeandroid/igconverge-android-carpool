package com.info.igconverge.menu.datepicker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.R.menu;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.IGCafeApplication.TrackerName;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.adapter.IGCMenuItemsAdapter;
import com.info.igconverge.dashboard.IGCBellRingingActivity;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.model.IGCMenu;
import com.info.igconverge.model.IGCMenuItem;
import com.info.igconverge.parser.MenuParser;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCUtils;

public class IGCShowMenuByDatePickFragment extends Fragment implements
FragmentCallback {
	IGCDashboardActivity activity;
	private DateRecyclerView dateList;
	TextView textMenuPosition, emptyText;
	String mealType;
	String type, mealId;
	String[] itemNames, itemPrice;
	ListView menuListLayout;
	ListView menuPaidListLayout;

	IGCMenuItemsAdapter menuAdapter;
	IGCMenuItemsAdapter menuPaidAdapter;
	View view;
	int currentPosition;
	TextView bellCountView;
	RelativeLayout bellLayout;
	ImageView imageMenuType;
	String feedbackMealDate;

	List<Date> dates = new ArrayList<Date>();
	ProgressCustomDialog progressCustomDialog;
	String startDate, endDate, currentDate;
	private static final int CANCELLED = 4;

	String imgDecodableString;
	List<IGCMenu> menuDetailsList = new ArrayList<IGCMenu>();
	ArrayList<IGCMenuItem> listMenu = new ArrayList<IGCMenuItem>();
	ArrayList<IGCMenuItem> paidListMenu = new ArrayList<IGCMenuItem>();

	ArrayList<NameValuePair> params;
	FragmentCallback fragment;
	String ratingConstant = "rating";
	TextView exclusivesTextView;
	int RESULT_OK = 2;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
		mealType = getArguments().getString("mealType");
		dates = IGCUtils.dates();
		if (dates.size() != 0) {
			startDate = IGCUtils.getStringFromDate(dates.get(0));
			endDate = IGCUtils.getStringFromDate(dates.get(1));
			currentDate = IGCUtils.getStringFromDate(dates.get(2));
		}
		fragment = this;
		menuAdapter = new IGCMenuItemsAdapter(activity, listMenu);
		menuPaidAdapter = new IGCMenuItemsAdapter(activity, paidListMenu);
		//executeTask();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		googleAnalyticsReport();
		if (getArguments() != null) {
			mealType = getArguments().getString("mealType");
			type = getArguments().getString("Type");
		}
		activity.actionBar.setBackgroundDrawable(activity.getResources()
				.getDrawable(R.drawable.header_bg));
		activity.lockNavigationDrawerSwipeGesture();

		setHasOptionsMenu(false);

		// initAd();
		// startAdds();

	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.ring_bell_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	public void googleAnalyticsReport() {
		Tracker t = ((IGCafeApplication) activity.getApplication())
				.getTracker(TrackerName.APP_TRACKER);
		t.setScreenName("IGC Show Menu Fragment Screen");
		t.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.post_feed:

			Intent i = new Intent(activity, IGCBellRingingActivity.class);
			i.putExtra("mealId", mealId);
			if (feedbackMealDate != null) {
				i.putExtra("mealDateFeedback", feedbackMealDate);

			}
			if (mealType != null) {
				i.putExtra("mealType", mealType);

			}

			startActivity(i);
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		/*
		 * Google Analytics stops the service to measure the session/ duration
		 */
		GoogleAnalytics.getInstance(activity).reportActivityStop(activity);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		/*
		 * Google Analytics
		 */
		GoogleAnalytics.getInstance(activity).reportActivityStart(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		System.out.println("on create view");
		activity.actionBar.setTitle(type);
		View view = inflater.inflate(R.layout.fragment_picker_default,
				container, false);
		menuListLayout = (ListView) view.findViewById(R.id.list_showMenu);
		menuPaidListLayout = (ListView) view.findViewById(R.id.list_showPaidMenu);
		exclusivesTextView = (TextView) view.findViewById(R.id.exclusivesText);
		emptyText = (TextView) view.findViewById(R.id.emptyText);
		textMenuPosition = (TextView) view.findViewById(R.id.textMenuPosition);
		dateList = (DateRecyclerView) view.findViewById(R.id.date_list);
		imageMenuType = (ImageView) view.findViewById(R.id.imageMenuType);	
		bellLayout = (RelativeLayout) view.findViewById(R.id.bell_layout);
		bellCountView = (TextView) bellLayout.findViewById(R.id.bell_count);
		emptyText.setVisibility(View.GONE);
		executeTask();
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		IGCConstants.setBellCount(null);
		this.view = view;
		//		menuListLayout = (ListView) view.findViewById(R.id.list_showMenu);
		//		menuPaidListLayout = (ListView) view
		//				.findViewById(R.id.list_showPaidMenu);
		//		exclusivesTextView = (TextView) view.findViewById(R.id.exclusivesText);
		// emptyText = (TextView) view.findViewById(R.id.emptyText);
		//		textMenuPosition = (TextView) view.findViewById(R.id.textMenuPosition);
		//		dateList = (DateRecyclerView) view.findViewById(R.id.date_list);
		//		imageMenuType = (ImageView) view.findViewById(R.id.imageMenuType);
		//		bellLayout = (RelativeLayout) view.findViewById(R.id.bell_layout);
		//		bellCountView = (TextView) bellLayout.findViewById(R.id.bell_count);

		menuListLayout.setAdapter(menuAdapter);
		menuPaidListLayout.setAdapter(menuPaidAdapter);

		if (type.contains("Breakfast")) {
			imageMenuType.setBackgroundResource(R.drawable.breakfast_img);
		} else if (type.contains("Lunch")) {
			imageMenuType.setBackgroundResource(R.drawable.lunch_img);
		} else if (type.contains("Snacks")) {
			imageMenuType.setBackgroundResource(R.drawable.snacks_img);
		} else if (type.contains("Dinner")) {
			imageMenuType.setBackgroundResource(R.drawable.dinner_img);
		}

		dateList.addItemDecoration(new RecyclerViewInsetDecoration(activity,R.dimen.date_card_insets));

		bellLayout.setVisibility(View.GONE);
		System.out.println("dates===" + dates.get(0) + " **" + dates.get(1)
				+ "** " + dates.get(2));
		if (dates != null) {
			if (!dates.isEmpty() && dates.size() != 0) {
				dateList.setAdapter(new DefaultDateAdapter(dates.get(0), dates
						.get(1), dates.get(2)));
				currentPosition = dateList.getCurrentPosition();

				String date = IGCUtils.getStringFromDate(dates.get(0));

			}
		}

		dateList.setDatePickerListener(new DateRecyclerView.DatePickerListener() {
			@Override
			public void onDatePickerItemClick(DateItem dateItem, int position) {

				String date = IGCUtils.getStringFromDate(dateItem.getDate());
				setPositionData(date);
			}

			@Override
			public void onDatePickerPageSelected(int position) {

			}

			@Override
			public void onDatePickerPageStateChanged(int state) {

			}

			@Override
			public void onDatePickerPageScrolled(int position,
					float positionOffset, int positionOffsetPixels) {

			}
		});

	}

	public void setPositionData(String date) {
		int bellCount = 0;
		try {
			setHasOptionsMenu(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
		if(IGCCheckNetworkConnectivity.checkConnection(activity) )
		{
			menuDetailsList = IGCafeApplication.getIgcMenuList();
			feedbackMealDate = date;
			if ( menuDetailsList != null && menuDetailsList.size() > 0) {
				for (int i = 0; i < menuDetailsList.size(); i++) {
					List<IGCMenuItem> list = new ArrayList<IGCMenuItem>();
					List<IGCMenuItem> paidList = new ArrayList<IGCMenuItem>();

					IGCMenu menu = menuDetailsList.get(i);
					bellCount = menu.getBellCount();
					String menuDate = menu.getMenuDate().split(" ")[0];

					if (date.equals(menuDate)) {

						mealId = menu.getMenuId();

						if (mealId != null && mealId.length() > 0) {
							SimpleDateFormat dateFormat = new SimpleDateFormat(
									IGCConstants.currrentdateFormat);
							Date one = IGCUtils.getDateFromString(currentDate);
							Date two = IGCUtils.getDateFromString(feedbackMealDate);
							if (one.before(two)) {
								setHasOptionsMenu(false); // If current date is
								// before position
								// date.
							} else if (one.equals(two)) {
								setHasOptionsMenu(true);
							} else {
								setHasOptionsMenu(true); // If current date is after
								// the
								// position date.
							}
						} else {
							setHasOptionsMenu(false);
						}
						list = (ArrayList<IGCMenuItem>) menu.getMenuItems();
						paidList = (ArrayList<IGCMenuItem>) menu.getMenuPaidItems();

						listMenu.clear();
						paidListMenu.clear();
						for (int k = 0; k < list.size(); k++) {
							listMenu.add(list.get(k));
						}

						for (int k = 0; k < paidList.size(); k++) {
							paidListMenu.add(paidList.get(k));

						}

						setListViewHeightBasedOnChildren(menuListLayout);
						setListViewHeightBasedOnChildren(menuPaidListLayout);

						if (listMenu.size() == 0 && paidListMenu.size() == 0) {
							emptyText.setVisibility(View.VISIBLE);
							bellLayout.setVisibility(View.GONE);
							bellCountView.setText(null);

						} else {
							menuAdapter.notifyDataSetChanged();
							menuPaidAdapter.notifyDataSetChanged();
							menuListLayout.setVisibility(View.VISIBLE);
							menuPaidListLayout.setVisibility(View.VISIBLE);
							if (paidListMenu.size() == 0)
								exclusivesTextView.setVisibility(View.GONE);
							else
								exclusivesTextView.setVisibility(View.VISIBLE);

							emptyText.setVisibility(View.GONE);
							emptyText.setText(activity.getResources().getString(
									R.string.noMenu));
							if (bellCount > 0) {
								bellLayout.setVisibility(View.VISIBLE);

								System.out
								.println("bellCountView=" + bellCountView);
								bellCountView.setVisibility(View.VISIBLE);
								IGCConstants
								.setBellCount(String.valueOf(bellCount));
								bellCountView.setText(String.valueOf(bellCount));

							} else {
								bellLayout.setVisibility(View.GONE);
								bellCountView.setText(null);
							}
						}
						break;
					} else {
						listMenu.clear();
						bellLayout.setVisibility(View.GONE);
						bellCountView.setText(null);

						setHasOptionsMenu(false);
						menuListLayout.setVisibility(View.GONE);
						menuPaidListLayout.setVisibility(View.GONE);
						emptyText.setVisibility(View.VISIBLE);
						menuAdapter.notifyDataSetChanged();
						menuPaidAdapter.notifyDataSetChanged();
						exclusivesTextView.setVisibility(View.GONE);

					}
				}
			} else {
				setHasOptionsMenu(false);

				if(emptyText!=null)
				{
					emptyText.setVisibility(View.VISIBLE);
				}
				if(bellLayout!=null)
				{
					bellLayout.setVisibility(View.GONE);
				}
				if(bellCountView!=null)
				{
					bellCountView.setText(null);
				}
				Toast.makeText(activity, activity.getResources().getString(R.string.no_menu_toast), Toast.LENGTH_SHORT).show();
			}
		}
		else
		{
			listMenu.clear();
			bellLayout.setVisibility(View.GONE);
			bellCountView.setText(null);

			setHasOptionsMenu(false);
			menuListLayout.setVisibility(View.GONE);
			menuPaidListLayout.setVisibility(View.GONE);
			emptyText.setVisibility(View.VISIBLE);
			menuAdapter.notifyDataSetChanged();
			menuPaidAdapter.notifyDataSetChanged();
			exclusivesTextView.setVisibility(View.GONE);
			ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();

		}
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	private ArrayList<NameValuePair> getParams() {
		// define and ArrayList whose elements are of type NameValuePair
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("name", mealType));
		nameValuePairs.add(new BasicNameValuePair("startDate", startDate));
		nameValuePairs.add(new BasicNameValuePair("endDate", endDate));

		return nameValuePairs;
	}

	@Override
	public void fragMultipleResponse(String response, String extra) {

		menuResponse(response);

	}

	private void getMenu(String response) {

		MenuParser menuParser = new MenuParser();

		ArrayList<IGCMenu> igcMenuList = menuParser.parseMealItems(response);
		IGCafeApplication.setIgcMenuList(igcMenuList);

		setPositionData(currentDate);
	}

	public void menuResponse(String response) {
		// TODO Auto-generated method stub

		if (response != null && response.length() > 0) {
			try {

				if (response != null) {
					getMenu(response);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
			dialog.setCancelable(false);
			dialog.show();
			emptyText.setVisibility(View.VISIBLE);
			setHasOptionsMenu(false);
		}
	}

	private void executeTask() {
		if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
			params = getParams();
			IGCafeApplication.setIgcMenuList(null);
			IGCAsyncServiceRequest getPost = new IGCAsyncServiceRequest(activity, "POST", params, fragment);
			getPost.execute(IGCConstants.fetchMealsURL);

		} else {
			IGCafeApplication.setIgcMenuList(null);
			setPositionData(currentDate);
	
		/*	ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();
			System.out.println("in error dialog");*/

		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (IGCConstants.getBellCount() != null) {
			if (Integer.parseInt(IGCConstants.getBellCount()) == 0) {
				bellLayout.setVisibility(View.GONE);
			} else {
				bellLayout.setVisibility(View.VISIBLE);
				bellCountView.setText(IGCConstants.getBellCount());
			}
		}

	}

}
