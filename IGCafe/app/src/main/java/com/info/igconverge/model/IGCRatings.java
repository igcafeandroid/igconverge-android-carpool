package com.info.igconverge.model;

import java.util.Arrays;

public class IGCRatings 
{
	private String comment;
	private int score;
	private byte[] mealImage;
	/**
	 * 
	 */
	public IGCRatings() 
	{
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param comment
	 * @param score
	 * @param mealImage
	 */
	public IGCRatings(String comment, int score, byte[] mealImage) 
	{
		super();
		this.comment = comment;
		this.score = score;
		this.mealImage = mealImage;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * @return the mealImage
	 */
	public byte[] getMealImage() {
		return mealImage;
	}
	/**
	 * @param mealImage the mealImage to set
	 */
	public void setMealImage(byte[] mealImage) {
		this.mealImage = mealImage;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IGCRatings [comment=");
		builder.append(comment);
		builder.append(", score=");
		builder.append(score);
		builder.append(", mealImage=");
		builder.append(Arrays.toString(mealImage));
		builder.append("]");
		return builder.toString();
	}	
}
