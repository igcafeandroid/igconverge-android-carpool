package com.info.igconverge.partyradar;



import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class FeedListRowHolder extends RecyclerView.ViewHolder{

    protected TextView title;
    protected RelativeLayout  main_layout_grid;
    protected TextView distanceShop;
    protected RatingBar ratingBar;
    public FeedListRowHolder(View view,IGCDashboardActivity activity) {
        super(view);
   
        this.title = (TextView) view.findViewById(R.id.title_shop);
     
        this.distanceShop=(TextView)view.findViewById(R.id.distanceShop);

        this.ratingBar=(RatingBar)view.findViewById(R.id.ratingBarList_party_shop);
		this.main_layout_grid = (RelativeLayout) view.findViewById(R.id.main_layout_grid);
		this.main_layout_grid.getLayoutParams().height = activity.screenWidth / 2;
		this.main_layout_grid.getLayoutParams().width = activity.screenWidth / 2;
		
		LayerDrawable stars = (LayerDrawable)this.ratingBar.getProgressDrawable();
		stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
		stars.getDrawable(1).setColorFilter(Color.rgb(221, 107, 66), PorterDuff.Mode.SRC_ATOP);
		stars.getDrawable(2).setColorFilter(Color.rgb(221, 107, 66), PorterDuff.Mode.SRC_ATOP);
		
		Typeface myTypeface1 = Typeface.createFromAsset(activity.getAssets(),"font/Roboto-Regular.ttf");
		this.distanceShop.setTypeface(myTypeface1);

		Typeface myTypeface = Typeface.createFromAsset(activity.getAssets(),"font/Roboto-Medium.ttf");
		this.title.setTypeface(myTypeface);
	
    }



}