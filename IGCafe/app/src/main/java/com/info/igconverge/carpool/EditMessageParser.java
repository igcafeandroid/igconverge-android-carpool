package com.info.igconverge.carpool;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by lab2 on 05/01/16.
 */
public class EditMessageParser {


    public EditMessageParser() {

    }

    public EditMessageClass parseData(String jsonData) {
        EditMessageClass editMessageObject = new EditMessageClass();

        if (jsonData != "") {


            try {


                JSONObject jObject = new JSONObject(jsonData);


                if (!jObject.isNull("message")) {
                    editMessageObject.setMessage(jObject.getString("message"));
                }

                //  carpoolObject.setUser(jObject.getJSONObject("user"));
                if (!jObject.isNull("status")) {
                    editMessageObject.setStatus(jObject.getBoolean("status"));
                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }



        }
        return editMessageObject;

    }

}