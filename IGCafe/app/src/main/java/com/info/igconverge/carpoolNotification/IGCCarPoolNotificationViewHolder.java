package com.info.igconverge.carpoolNotification;

/**
 * Created by nitika on 29/12/15.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;

public class IGCCarPoolNotificationViewHolder extends RecyclerView.ViewHolder {
    protected TextView headingNotify1, headingNotify2, headingNotify3,
            headerNotifyCircle,newTextView;
    IGCDashboardActivity activity;
    TextView idView;
    Typeface myTypefaceMedium, myTypefaceRegular;
    TextView accept,reject;
    Context context;

    public IGCCarPoolNotificationViewHolder(View view, IGCDashboardActivity activity,Context context) {
        super(view);
        this.activity = activity;
        this.context=context;
      //  headerNotifyCircle = (TextView) view.findViewById(R.id.headingNotifyCircle);
        headingNotify1 = (TextView) view.findViewById(R.id.carPoolNotificationHead1);
       headingNotify2 = (TextView) view.findViewById(R.id.carPoolNotificationTitle);
        accept=(TextView)view.findViewById(R.id.acceptButtonCar);
        reject=(TextView)view.findViewById(R.id.rejectButtonCar);



//        headingNotify3 = (TextView) view.findViewById(R.id.headingNotify3);
//        newTextView = (TextView) view.findViewById(R.id.textView1);
//        idView = (TextView) view.findViewById(R.id.notifyId);

        myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
                "font/Roboto-Medium.ttf");
        myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
                "font/Roboto-Regular.ttf");
     //   this.headerNotifyCircle.setTypeface(myTypefaceMedium);
        this.headingNotify1.setTypeface(myTypefaceMedium);
        this.headingNotify2.setTypeface(myTypefaceRegular);
//        this.headingNotify2.setTypeface(myTypefaceRegular);
//        this.headingNotify3.setTypeface(myTypefaceRegular);
    }
}