
package com.info.igconverge.menu.datepicker;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Date;

import com.info.igconverge.R;

public class DefaultDateAdapter extends
		AbsDateAdapter<DefaultDateAdapter.DateItemHolder> {

	public DefaultDateAdapter(Date start, Date end) {
		this(start, end, null);
	}

	public DefaultDateAdapter(Date start, Date end, Date defaultDate) {
		super(start, end, defaultDate);
	}

	@Override
	public DateItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
		View root = inflater.inflate(R.layout.item_view_default_date,
				viewGroup, false);

		return new DateItemHolder(root, this);
	}

	@Override
	public void onBindViewHolder(DateItemHolder dateItemHolder, int i) {
		DateItem dateItem = dateItems.get(i);

		dateItemHolder.setDay(dateItem.getDate());
		dateItemHolder.setMonthName(dateItem.getDate());
		dateItemHolder.setDayName(dateItem.getDate());

		dateItemHolder.itemView.setSelected(true);

		if (isDateSelected(dateItem)) {
			dateItemHolder.updateDateItemView(true);
			selectedDateView = dateItemHolder;
		} else {
			dateItemHolder.updateDateItemView(false);
		}
	}

	@Override
	public void onDateItemHolderClick(DateItemHolder itemHolder) {
		if (onDateItemListener != null) {
			onDateItemListener
					.onDateItemClick(getItem(itemHolder.getPosition()),
							itemHolder.getPosition());
		}

		if (selectedDate != -1 && selectedDateView != null) {
			selectedDateView.changeDateIndicatorColor(false);
			selectedDateView.changeTextColor(false);
		}

		selectedDateView = itemHolder;
		selectedDate = dateItems.get(itemHolder.getPosition()).getDate()
				.getTime();

		selectedDateView.updateDateItemView(true);

	}

	static class DateItemHolder extends AbsDateItemHolder {

		TextView tvDay;
		TextView tvMonth;
		TextView tvDayName;
		View viewDateIndicator;
		RelativeLayout rlDateItem;

		Resources resources;

		public DateItemHolder(View itemView, DefaultDateAdapter adapter) {
			super(itemView, adapter);

			this.resources = itemView.getResources();

			tvDay = (TextView) itemView.findViewById(R.id.tv_date_picker_day);
			tvMonth = (TextView) itemView
					.findViewById(R.id.tv_date_picker_month_name);
			tvDayName = (TextView) itemView
					.findViewById(R.id.tv_date_picker_day_name);
			rlDateItem = (RelativeLayout) itemView
					.findViewById(R.id.rl_date_picker_item);
			viewDateIndicator = itemView.findViewById(R.id.view_date_indicator);
		}

		@Override
		protected void changeTextColor(boolean isSelected) {
			if (isSelected) {
				tvDay.setTextColor(resources
						.getColor(R.color.date_item_selected_color));
				tvDay.setTypeface(Typeface.DEFAULT_BOLD);
				tvMonth.setTypeface(Typeface.DEFAULT_BOLD);
				tvDayName.setTypeface(Typeface.DEFAULT_BOLD);
				tvMonth.setTextColor(resources.getColor(R.color.brown_color));
				tvDayName.setTextColor(resources.getColor(R.color.brown_color));
			} else {
				// tvDay.setTextColor(resources.getColor(R.color.date_item_unselected_color));

				tvDay.setTypeface(Typeface.DEFAULT);
				tvMonth.setTypeface(Typeface.DEFAULT);
				tvDayName.setTypeface(Typeface.DEFAULT);
				tvDay.setTextColor(resources.getColor(R.color.lightblack));
				tvMonth.setTextColor(resources.getColor(R.color.lightblack));
				tvDayName.setTextColor(resources.getColor(R.color.lightblack));
			}
		}

		@Override
		public void setDay(Date date) {
			tvDay.setText(PagerDatePickerDateFormat.DATE_PICKER_DAY_FORMAT
					.format(date));
		}

		@Override
		public void setMonthName(Date date) {
			tvMonth.setText(PagerDatePickerDateFormat.DATE_PICKER_MONTH_NAME_FORMAT
					.format(date));
		}

		@Override
		public void setDayName(Date date) {
			tvDayName
					.setText(PagerDatePickerDateFormat.DATE_PICKER_DAY_NAME_FORMAT
							.format(date));
		}

		@Override
		protected void changeDateIndicatorColor(boolean isSelected) {
			if (isSelected) {
				viewDateIndicator
						.setBackgroundResource(R.color.date_item_selected_indicator);
			} else {
				viewDateIndicator
						.setBackgroundResource(R.color.date_item_unselected_indicator);
			}
		}

		@Override
		protected View getCurrentViewToAnimate() {
			return null;
		}

	}

}
