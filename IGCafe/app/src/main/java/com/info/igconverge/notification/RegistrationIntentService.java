package com.info.igconverge.notification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.info.igconverge.Services.ServiceHandler;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class RegistrationIntentService extends IntentService {

	private static final String TAG = "RegistrationIntentService";
	IGCSharedPreferences sharedPref = new IGCSharedPreferences();

	public RegistrationIntentService() {
		super(TAG);
	}

	/**
	 * GCM
	 * 
	 * @param intent
	 */
	// @SuppressLint("LongLogTag")

	@Override
	protected void onHandleIntent(Intent intent) {
		
		String extra = intent.getStringExtra("recreate");
		if (extra == null) {
			extra = "old";
		}
		String token = null;
		try {
			if (IGCSharedPreferences.getPREF_DEVICEID(this) == null
					|| extra.equalsIgnoreCase("recreate")
					|| !IGCSharedPreferences
							.isPREF_INSTALL_FLAG(getApplicationContext())) {
				InstanceID instanceID = InstanceID.getInstance(this);

				token = instanceID
						.getToken(IGCConstants.sender_id, "GCM", null);

				System.out.println("****DEVICEID"+token);
				IGCSharedPreferences.setPREF_DEVICEID(token, this);
				// .sendToServer(token, getApplicationContext());
				ServiceHandler handler = new ServiceHandler();
				handler.sendToServer(token, getApplicationContext());
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}