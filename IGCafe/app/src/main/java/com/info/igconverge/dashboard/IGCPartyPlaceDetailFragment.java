package com.info.igconverge.dashboard;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.info.igconverge.R;
import com.info.igconverge.Services.IGCCurrentLocation;
import com.info.igconverge.Services.ServiceHandler;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.model.IGCShopDetails;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.SlidingUpPanelLayout;

public class IGCPartyPlaceDetailFragment extends Fragment implements
		SlidingUpPanelLayout.PanelSlideListener, View.OnClickListener {
	IGCDashboardActivity activity;

	private SlidingUpPanelLayout mSlidingUpPanelLayout;
	LatLng shopLatLong;

	MapView mapView;

	IGCShopDetails shopDetails;
	TextView shopName, shopAddress, shopDistance, callTextClick, webTextClick;
	String shopPhoneNumber, shopWebLink;
	ImageView callImageClick, webImageClick, navigationIconMap;
	RelativeLayout ratingLayoutDetailPage;
	private GoogleMap map;
	RatingBar ratingBar;
	ProgressCustomDialog dialog;
	View rootView;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.lockNavigationDrawerSwipeGesture();
		activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.header_bg));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView=inflater.inflate(R.layout.partyplacedetail_fragment,container,false);
		//Shop Contact Details

		shopDetails = getArguments().getParcelable("shopDetails");
		shopPhoneNumber = shopDetails.getContactInfo();
		shopWebLink = shopDetails.getWeblink();
		initViews();
		navigationIconMap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
					String latitude =shopDetails.getShopLatitude();
					String longitude =shopDetails.getShopLongitude();
					Uri gmmIntentUri = Uri.parse("google.navigation:q="+latitude+","+longitude);
					Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
					mapIntent.setPackage("com.google.android.apps.maps");
					startActivity(mapIntent);
				} else {

					ErrorDialog dialog = new ErrorDialog(getActivity(),
							"NO_INTERNET");
					dialog.setCancelable(false);
					dialog.show();
				}

			}});
	
	
		FetchPlaceNearbyDetails fetchPlaceNearbyDetails = new FetchPlaceNearbyDetails();
		fetchPlaceNearbyDetails.execute();

		mSlidingUpPanelLayout.setPanelSlideListener(this);
		expandMap();
		ratingLayoutDetailPage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if(mSlidingUpPanelLayout.isExpanded())
				{


					mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
				}
				else
				{

					mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
				}
			}
		});

		try {

			mapView.onCreate(savedInstanceState);

			// Gets to GoogleMap from the MapView and does initialization stuff
			map = mapView.getMap();
			map.getUiSettings().setMyLocationButtonEnabled(false);
			map.setMyLocationEnabled(true);
			map.getUiSettings().setZoomControlsEnabled(false);
			MapsInitializer.initialize(activity);

			getMapMarkers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootView;
	}

	private void initViews() {
		// TODO Auto-generated method stub
		navigationIconMap=(ImageView)rootView.findViewById(R.id.navigateMapIcon);
		mSlidingUpPanelLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.slidingLayout);
		mSlidingUpPanelLayout.setEnableDragViewTouchEvents(true);
		ratingLayoutDetailPage=(RelativeLayout)rootView.findViewById(R.id.insidelayout);

		ratingBar=(RatingBar)rootView.findViewById(R.id.ratingBarDetail);

		mapView = (MapView) rootView.findViewById(R.id.mapContainer);
		shopName = (TextView) rootView.findViewById(R.id.shopTitle);
		shopAddress = (TextView) rootView.findViewById(R.id.shopDescription);
		shopDistance = (TextView) rootView.findViewById(R.id.shopDistance);
		activity.actionBar.setTitle(shopDetails.getShopName());
		shopDistance.setText(shopDetails.getShopDistance());
		
		shopName.setText(shopDetails.getShopName());
		shopAddress.setText(shopDetails.getShopAddress());
		callImageClick=(ImageView) rootView.findViewById(R.id.callIconDetail);
		callTextClick = (TextView) rootView.findViewById(R.id.callText);
		ratingBar.setNumStars(5);
		webImageClick = (ImageView) rootView.findViewById(R.id.webIconDetail);
		webTextClick=(TextView)rootView.findViewById(R.id.webText);
	}

	@Override
	public void onResume() {

		super.onResume();
		mapView.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

	private void getMapMarkers() {
		// Creating a marker
		MarkerOptions markerOptions = new MarkerOptions();

		shopLatLong = new LatLng(Double.valueOf(shopDetails.getShopLatitude()),
				Double.valueOf((shopDetails.getShopLongitude())));

		// Setting the position for the marker
		markerOptions.position(shopLatLong);

		markerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.location_pin));

		// Setting the title for the marker.
		// This will be displayed on taping the marker
		markerOptions.title(shopDetails.getShopName());

		// Placing a marker on the touched position
		map.addMarker(markerOptions);
		map.moveCamera(CameraUpdateFactory.newLatLng(shopLatLong));
		map.animateCamera(CameraUpdateFactory.zoomTo(12));

	}

	private void collapseMap() {

	}

	private void expandMap() {


	}

	@Override
	public void onPanelSlide(View view, float v) {

	}

	@Override
	public void onPanelCollapsed(View view) {
		expandMap();

		map.animateCamera(CameraUpdateFactory.zoomTo(14f), 1000, null);
		ratingLayoutDetailPage.setBackgroundDrawable(getResources()
				.getDrawable(R.drawable.slide_up));
	}

	@Override
	public void onPanelExpanded(View view) {
		collapseMap();

		map.animateCamera(CameraUpdateFactory.zoomTo(11f), 1000, null);
		ratingLayoutDetailPage.setBackgroundDrawable(getResources()
				.getDrawable(R.drawable.slide_down));

	}

	@Override
	public void onPanelAnchored(View view) {

	}

	public void dialCall() {
		Intent callIntent = new Intent(Intent.ACTION_DIAL);
		callIntent.setData(Uri.parse("tel:" + shopDetails.getContactInfo()));
		activity.startActivity(callIntent);
	}

	public void visitWebsite() {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(shopDetails.getWeblink()));
		startActivity(browserIntent);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.callIconDetail:
			dialCall();
			break;
		case R.id.webIconDetail:
			visitWebsite();
			break;
		case R.id.callText:
			dialCall();
			break;
		case R.id.webText:
			visitWebsite();
			break;
		default:
			break;
		}

	}

	@Override
	public void onPanelHidden(View panel) {
		// TODO Auto-generated method stub

	}

	private class FetchPlaceNearbyDetails extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			dialog = new ProgressCustomDialog(activity);
			dialog.setOnKeyListener(new Dialog.OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface arg0, int keyCode,
						KeyEvent event) {

					// TODO Auto-generated method stub
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						// finish();
						activity.onBackPressed();
						FetchPlaceNearbyDetails.this.cancel(true);

						dialog.dismiss();
					}
					return true;
				}
			});
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {

			if (shopDetails != null) {
				String url="https://maps.googleapis.com/maps/api/place/details/json?reference="
						+ shopDetails.getReferenceString()
						+ "&sensor=true&key="
						+ IGCConstants.GOOGLE_PLACE_API_KEY;
				String placeDetailJsonResult = ServiceHandler
						.makeGetCallService(activity,url);
				if (placeDetailJsonResult != null) {
					try {
						JSONObject detailsJsonResult = new JSONObject(
								placeDetailJsonResult);
						JSONObject detailsResult = detailsJsonResult
								.getJSONObject("result");
						shopDetails.setShopAddress(detailsResult
								.optString("formatted_address"));
						if (!detailsResult.isNull(("website"))) {
							shopDetails.setWeblink(detailsResult
									.optString("website"));
						} else {
							shopDetails.setWeblink(detailsResult.optString(""));
						}
						if (!detailsResult
								.isNull(("international_phone_number"))) {
							shopDetails.setContactInfo(detailsResult
									.optString("international_phone_number"));
						} else {
							shopDetails.setContactInfo(detailsResult
									.optString(""));
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						dialog.dismiss();

					}
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			updateDetailsOnLayout();
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
		}

	}

	public void updateDetailsOnLayout() {
		shopPhoneNumber = shopDetails.getContactInfo();
		shopWebLink = shopDetails.getWeblink();

		activity.actionBar.setTitle(shopDetails.getShopName());
		shopDistance.setText(shopDetails.getShopDistance());
		shopName.setText(shopDetails.getShopName());
		shopAddress.setText(shopDetails.getShopAddress());

		if (shopDetails.getShopRating() != null
				&& shopDetails.getShopRating().length() > 0) {
			ratingBar.setRating(Float.parseFloat(shopDetails.getShopRating()));
		} else {
			ratingBar.setRating(Float.parseFloat("0"));
		}

		if (shopPhoneNumber != null && shopPhoneNumber != ""
				&& shopPhoneNumber.length() > 0) {
			callImageClick.setOnClickListener(this);
			callTextClick.setOnClickListener(this);
			callImageClick.setImageResource(R.drawable.call_icon);
			callTextClick.setTextColor(getResources().getColor(R.color.brown_color));

		}
		else if((shopPhoneNumber!=null && shopPhoneNumber.length()==0) || shopPhoneNumber==null)
		{
			callImageClick.setOnClickListener(null);
			callTextClick.setOnClickListener(null);
			callImageClick.setImageResource(R.drawable.call_icon_disable);
			callTextClick.setTextColor(getResources().getColor(
					R.color.greycolor));

		}
		if (shopWebLink != null && shopWebLink != ""
				&& shopWebLink.length() > 0) {
			webImageClick.setOnClickListener(this);
			webTextClick.setOnClickListener(this);
			webImageClick.setImageResource(R.drawable.website_icon);
			webTextClick.setTextColor(getResources().getColor(
					R.color.brown_color));

		} else if ((shopWebLink != null && shopWebLink.length() == 0)
				|| shopWebLink == null) {
			webImageClick.setOnClickListener(null);
			webTextClick.setOnClickListener(null);
			webImageClick.setImageResource(R.drawable.website_icon_disable);
			webTextClick.setTextColor(getResources().getColor(R.color.greycolor));


		}

	}

}
