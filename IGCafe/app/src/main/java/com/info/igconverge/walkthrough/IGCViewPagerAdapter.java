package com.info.igconverge.walkthrough;

import com.info.igconverge.R;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
 
public class IGCViewPagerAdapter extends PagerAdapter {
	// Declare Variables
	Context context;
	
	int[] backgroundColors;
	String[] title;
	int[] flag;
	LayoutInflater inflater;
 
	public IGCViewPagerAdapter(Context context, int[] backgroundColors,
			String[] title, int[] flag) {
		this.context = context;

		this.backgroundColors = backgroundColors;
		this.title = title;
		this.flag = flag;
	}
 
	@Override
	public int getCount() {
		return title.length;
	}
 
	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}
 
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
 
		// Declare Variables
		//TextView walkThroughTitle;
	//RelativeLayout walkMainLayout;
		ImageView imgflag;
 
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.viewpager_item, container,
				false);
 
	
		imgflag = (ImageView) itemView.findViewById(R.id.walkThroughImage);
 //walkThroughTitle.setText(title[position]);
 imgflag.setImageResource(flag[position]);   

		((ViewPager) container).addView(itemView);
 
		return itemView;
	}
 
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);
 
	}
}