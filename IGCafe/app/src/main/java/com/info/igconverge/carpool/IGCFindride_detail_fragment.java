package com.info.igconverge.carpool;

/**
 * Created by lab2 on 23/12/15.
 */


import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;

public class IGCFindride_detail_fragment extends Fragment
        implements com.info.igconverge.Services.FragmentCallback {
    FragmentCallback frag;

    IGCDashboardActivity activity;
    String postId;
    String callNo;
    TextView emptyText;
    String email;
    Context mContext;
    private Typeface myTypefaceMedium,myTypefaceLight,myTypefaceRegular;
    ArrayList<OccupantsClass> occupantList=new ArrayList<OccupantsClass>();
    ArrayList<String> days_all;
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
        frag = this;
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        initialseActionBar();

    }

    @Override
    public void onResume() {
        super.onResume();
        initialseActionBar();
    }

    private void initialseActionBar() {
        // TODO Auto-generated method stub

        if (activity != null) {

            activity.actionBar.setDisplayHomeAsUpEnabled(true);
            activity.lockNavigationDrawerSwipeGesture();
            activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.header_bg));
            activity.actionBar.setTitle(IGCConstants.CarPoolFragment);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        mContext=container.getContext();
        myTypefaceMedium = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Medium.ttf");
        myTypefaceLight = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Light.ttf");

        View rootView = inflater.inflate(R.layout.carpool_detail,
                container, false);


        TextView fare = (TextView) rootView.findViewById(R.id.cost1);
        TextView time = (TextView) rootView.findViewById(R.id.time1);
        TextView location = (TextView) rootView.findViewById(R.id.loc1);
        TextView seats = (TextView) rootView.findViewById(R.id.seats1);
        TextView carNo = (TextView) rootView.findViewById(R.id.carNo1);
        TextView comments = (TextView) rootView.findViewById(R.id.cmt);
        TextView Driver=(TextView)rootView.findViewById(R.id.Driver);
        TextView fare_label = (TextView) rootView.findViewById(R.id.cost);
        TextView time_label = (TextView) rootView.findViewById(R.id.time);
        TextView location_label = (TextView) rootView.findViewById(R.id.loc);
        TextView seats_label = (TextView) rootView.findViewById(R.id.seats);
        TextView carNo_label = (TextView) rootView.findViewById(R.id.carNo);
        ImageView commentImg=(ImageView)rootView.findViewById(R.id.commentImg);
        fare_label.setTypeface(myTypefaceLight);
        time_label.setTypeface(myTypefaceLight);
        location_label.setTypeface(myTypefaceLight);
        seats_label.setTypeface(myTypefaceLight);
        carNo_label.setTypeface(myTypefaceLight);
        time.setTypeface(myTypefaceLight);
        location.setTypeface(myTypefaceLight);
        seats.setTypeface(myTypefaceLight);
        comments.setTypeface(myTypefaceLight);
        Driver.setTypeface(myTypefaceLight);
        fare.setTypeface(myTypefaceLight);
        carNo.setTypeface(myTypefaceLight);
days_all=new ArrayList<>();


        TextView basic_detail_head = (TextView) rootView.findViewById(R.id.detail);
        TextView occupant_head = (TextView) rootView.findViewById(R.id.occupant);
        TextView comment_head = (TextView) rootView.findViewById(R.id.comments);
        TextView emptyText_comment=(TextView)rootView.findViewById(R.id.emptyText_comment);
        basic_detail_head.setTypeface(myTypefaceLight);
        occupant_head.setTypeface(myTypefaceLight);
        comment_head.setTypeface(myTypefaceLight);




        Bundle bundl = getArguments();

        emptyText = (TextView) rootView.findViewById(R.id.emptyText);
        Driver.setTypeface(myTypefaceMedium);
        fare.setTypeface(myTypefaceLight);
        time.setTypeface(myTypefaceLight);
        location.setTypeface(myTypefaceLight);
        seats.setTypeface(myTypefaceLight);
        carNo.setTypeface(myTypefaceLight);
        comments.setTypeface(myTypefaceLight);

        for (int i=0;i<bundl.getStringArrayList("Days").size();i++)
        {
            days_all.add(bundl.getStringArrayList("Days").get(i).substring(0,3));
        }
        fare.setText(String.valueOf(bundl.getInt("fare")));
        seats.setText(String.valueOf(bundl.getInt("seats")));
        location.setText(bundl.getString("location"));
        time.setText(bundl.getString("time")+" "+days_all);
        carNo.setText(bundl.getString("carNo"));
        String comt= bundl.getString("comments");

        if(comt.equals("")) {
            commentImg.setVisibility(View.GONE);
            emptyText_comment.setVisibility(View.VISIBLE);
        }
        else
        { commentImg.setVisibility(View.VISIBLE);
            comments.setText(comt);

        }
        Driver.setText(bundl.getString("driver"));

        postId = bundl.getString("postid");
        occupantList=bundl.getParcelableArrayList("occupantsList");
        callNo=bundl.getString("phoneNumber");

        email=bundl.getString("email");




        Button btn = (Button) rootView.findViewById(R.id.carpoolButton);
        btn.setText("Join Carpool");

        LinearLayout myLinearLayout = (LinearLayout) rootView.findViewById(R.id.commentsEventsLayout);
        if (occupantList.size()>0 ) {
            //myLinearLayout.setLayoutParams(params)


            final TextView[] myTextViews = new TextView[occupantList.size()];
            for (int i = 0; i < occupantList.size(); i++) {
                if(occupantList.get(i).getStatus().equalsIgnoreCase("accepted")){    //Changes done  ---------------------------------
                  // create a new textview
                    emptyText.setVisibility(View.GONE);
                final TextView rowTextView = new TextView(getActivity());

                // set some properties of rowTextView or something
                rowTextView.setText(occupantList.get(i).getName());
                    rowTextView.setPadding(0,3,0,3);
                rowTextView.setTypeface(myTypefaceMedium);
                    rowTextView.setTextColor(getResources().getColor(R.color.Details_occupant_color));

                // add the textview to the linearlayout
                myLinearLayout.addView(rowTextView);
                rowTextView.setBackgroundResource(R.drawable.carpool_roundcorner);
                rowTextView.setGravity(Gravity.CENTER);



                // save a reference to the textview for later
                myTextViews[i] = rowTextView;
            }

            }
        }
        else
        {
                emptyText.setVisibility(View.VISIBLE);
        }


//        setListViewHeightBasedOnItems(occupant);
        for (int i=0;i<occupantList.size();i++){
            if(occupantList.get(i).getUserId().equalsIgnoreCase(IGCSharedPreferences.getUserId(getContext())) )
            {
              btn.setBackgroundColor(getResources().getColor(R.color.dark_grey));
                btn.setEnabled(false);
            }
            else
            {
                btn.setBackgroundColor(getResources().getColor(R.color.orange));
                btn.setEnabled(true);
            }
        }
        btn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                showConfirmationDeletePicDialog();
            }
        });


        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.findride, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        //Toast.makeText(activity, "Notification Selected", Toast.LENGTH_SHORT).show();


        switch (item.getItemId()) {
            case R.id.carpoolCall:
                //Toast.makeText(activity, "Notification Selected", Toast.LENGTH_SHORT).show();
            {
                carpoolDialCall(callNo);
                break;
            }

            case R.id.carpoolMessage:

                //Toast.makeText(activity, "Selected", Toast.LENGTH_SHORT).show();

                sendMail(email);

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    public void carpoolDialCall(String phoneNo) {
        try {

            if (IGCUtils.isTelephonyEnabled(activity)) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                if (phoneNo != null)
                    callIntent.setData(Uri.parse("tel:" + phoneNo));
                activity.startActivity(callIntent);
            } else {
                Toast.makeText(activity, activity.getResources().getString(R.string.no_call), Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }


    }

    public void sendMail(String emailId) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{emailId});
        i.putExtra(Intent.EXTRA_SUBJECT, "");
        i.putExtra(Intent.EXTRA_TEXT, " ");
        try {

            activity.startActivity(i);
// activity.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
        }
    }

    @Override
    public void fragMultipleResponse(String response, String extra) {
        if (response != null && response.length() > 0) {
            try {

                if (response != null) {
                    //fetchRide(response);
                    //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                    activity.onBackPressed();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

        }}

    public void showConfirmationDeletePicDialog() {

        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(getActivity());
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog")

                .withTitleColor("#FFFFFF")

                .withDividerColor("#11000000")

                .withMessage("This is a modal Dialog.")

                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")

                .withDialogColor(Color.TRANSPARENT)

                .isCancelableOnTouchOutside(false)

                .withDuration(300)

                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.joincarpool_dialog,
                        getContext()).show();

        dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        TextView titleMessage = (TextView) dialogBuilder
                .findViewById(R.id.titleMessage);
        TextView deleteMessage = (TextView) dialogBuilder
                .findViewById(R.id.deleteMessage);
        TextView okDeletePhoto = (TextView) dialogBuilder
                .findViewById(R.id.okDeletePhoto);
        TextView cancelDeletePhoto = (TextView) dialogBuilder
                .findViewById(R.id.cancelDeletePhoto);
        titleMessage.setTypeface(myTypefaceRegular);
        deleteMessage.setText(getResources().getString(R.string.joincarpool));
        titleMessage.setText("Join Carpool!");
        deleteMessage.setVisibility(View.VISIBLE);
        cancelDeletePhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();

            }
        });
        okDeletePhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                IGCSharedPreferences.setProfileImage(getContext(),
//                        null);
//                profileImage.setImageResource(R.drawable.avatar_pic);
                try {
                    if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

                        JSONObject jsonObj = new JSONObject();
                        if(IGCSharedPreferences.getUserId(getContext())!=null && IGCSharedPreferences.getEmail(getContext())!=null && IGCSharedPreferences.getProfileName(getContext())!=null)
                        {
                            jsonObj.put("id", IGCSharedPreferences.getUserId(getContext()));
                            jsonObj.put("name", IGCSharedPreferences.getProfileName(getContext()));
                            jsonObj.put("email", IGCSharedPreferences.getEmail(getContext()));
                        }
                        jsonObj.put("status", "pending");
                        String url = IGCConstants.sendRideRequestURL + "/" + postId;

                        IGCAsyncServiceRequest getPost = new IGCAsyncServiceRequest(activity, "POST", jsonObj, frag);

                        getPost.execute(url);

                    } else {
                        ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
                        dialog.setCancelable(false);
                        dialog.show();
                        System.out.println("in error dialog");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialogBuilder.dismiss();
            }
        });

    }
}


/**
 * Sets ListView height dynamically based on the height of the items.
 *
 * @param listView to be resized
 * @return true if the listView is successfully resized, false otherwise
 */
//    public static boolean setListViewHeightBasedOnItems(ListView listView) {
//
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter != null) {
//
//            int numberOfItems = listAdapter.getCount();
//
//            // Get total height of all items.
//            int totalItemsHeight = 0;
//            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
//                View item = listAdapter.getView(itemPos, null, listView);
//                item.measure(0, 0);
//                totalItemsHeight += item.getMeasuredHeight();
//            }
//
//            // Get total height of all item dividers.
//            int totalDividersHeight = listView.getDividerHeight() *
//                    (numberOfItems - 1);
//
//            // Set list height.
//            ViewGroup.LayoutParams params = listView.getLayoutParams();
//            params.height = totalItemsHeight + totalDividersHeight;
//            listView.setLayoutParams(params);
//            listView.requestLayout();
//
//            return true;
//
//        } else {
//            return false;
//        }
//}
//
//    }






