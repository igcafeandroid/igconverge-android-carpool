package com.info.igconverge.weather;

public interface WeatherConditions {

	String getWeatherCondition(int weatherVal);

}
