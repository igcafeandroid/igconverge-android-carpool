package com.info.igconverge.Services;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class IGCCurrentLocation implements LocationListener {
	Context context;
	LocationManager locationManager;
	Location location;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;

	public IGCCurrentLocation(Context context) {
		this.context = context;
	}



	public Location getLocation()
	{
		locationManager = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
      
        if(!isGPSEnabled && !isNetworkEnabled)
        {

        	return null;
        }
        else
        {
        	if(isGPSEnabled)
        	{
        		
        		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        		if(locationManager != null)
        			location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        	}
        	if(location == null && isNetworkEnabled)
        	{
        	
        		if(locationManager != null)
        			location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        	} 
      
        }
        return location;
	}


	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		


	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
	

	}

}
