package com.info.igconverge.customcontrols.navigationdrawer;


import java.util.ArrayList;

import com.info.igconverge.R;
import com.info.igconverge.model.IGCNavDrawerItem;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<IGCNavDrawerItem> navDrawerItems;
	int selectedPosition;
	public NavDrawerListAdapter(Context context, ArrayList<IGCNavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	public int getSelectedPosition() {
		return selectedPosition;
	}

	public void setSelectedPosition(int selectedPosition) {
		this.selectedPosition = selectedPosition;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
		  
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icondrawerMenu);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.menuTitleDrawer);   
        txtTitle.setText(navDrawerItems.get(position).getTitle());
        imgIcon.setImageResource(navDrawerItems.get(position).getResourceDrawable());   
        Typeface myTypefaceMedium = Typeface.createFromAsset(context.getAssets(),"font/Roboto-Medium.ttf");
        txtTitle.setTypeface(myTypefaceMedium);
        if (position == selectedPosition) {
            txtTitle.setTextColor(context.getResources().getColor(R.color.orange));
                    txtTitle.setTypeface(Typeface.DEFAULT_BOLD);
            } else {
                    txtTitle.setTextColor(context.getResources().getColor(R.color.lightblack));
            }
        
        return convertView;
	}

}
