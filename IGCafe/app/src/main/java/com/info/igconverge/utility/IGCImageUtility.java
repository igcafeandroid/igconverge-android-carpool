package com.info.igconverge.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.media.ExifInterface;
import android.util.DisplayMetrics;
import android.util.Log;

public class IGCImageUtility {
	public static int getImageOrientation(File imageFile) {
		int rotate = 0;
		try {

			ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rotate;
	}

	public static Bitmap result;
	public static Canvas canvas;

	public static Bitmap getRoundedRectBitmap(Bitmap bitmap, int pixels,
			int requiredWidth, int requiredhight) {
		try {
			Bitmap resized = Bitmap.createScaledBitmap(bitmap, requiredWidth,
					requiredhight, true);

			result = Bitmap.createBitmap(resized.getWidth(),
					resized.getHeight(), Bitmap.Config.ARGB_8888);
			canvas = new Canvas(result);

			int color = 0xff424242;
			Paint paint = new Paint();
			Rect rect = new Rect(0, 0, resized.getWidth(), resized.getHeight());
			RectF rectF = new RectF(rect);
			int roundPx = pixels;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(resized, rect, rect, paint);

		} catch (NullPointerException e) {

		} catch (OutOfMemoryError o) {

		}
		return result;
	}

	// Decodes image and scales it to reduce memory consumption
	public static Bitmap decodeFile(File f, int requiredWidth,
			int requiredheight) {

		try {

			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			o.inPreferredConfig = Config.RGB_565;
			o.inDither = true;
			FileInputStream stream1 = new FileInputStream(f);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();

			// Find the correct scale value. It should be the power of 2.

			// Set width/height of recreated image
			// final int REQUIRED_SIZE=85;

			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < requiredWidth
						|| height_tmp / 2 < requiredheight)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// decode with current scale values
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inJustDecodeBounds = false;
			o2.inPreferredConfig = Config.RGB_565;
			o2.inDither = true;
			FileInputStream stream2 = new FileInputStream(f);
			Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;

		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return null;
	}

	public static int checkProfilePicSize(Context context) {
		int density = context.getResources().getDisplayMetrics().densityDpi;
		int profilePicSize = 140;
		switch (density) {
		case DisplayMetrics.DENSITY_LOW:
			
			profilePicSize = 50;

			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			
			profilePicSize = 80;

			break;
		case DisplayMetrics.DENSITY_HIGH:
			profilePicSize = 100;

			break;
		case DisplayMetrics.DENSITY_XHIGH:
		
			profilePicSize = 140;
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
		
			profilePicSize = 220;

			break;
		case DisplayMetrics.DENSITY_XXXHIGH:
			
			profilePicSize = 290;

			break;
		default:
			break;
		}
		return profilePicSize;
	}
}
