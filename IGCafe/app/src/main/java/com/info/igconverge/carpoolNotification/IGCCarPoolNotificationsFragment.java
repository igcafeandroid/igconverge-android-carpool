package com.info.igconverge.carpoolNotification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.carpool.CarpoolClass;

import com.info.igconverge.carpool.OccupantsClass;
import com.info.igconverge.carpool.RideParser;

import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.events.PullToRefreshView;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.MarginDecoration;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nitika on 29/12/15.
 */
public class IGCCarPoolNotificationsFragment extends Fragment implements
        com.info.igconverge.Services.FragmentCallback,
        com.info.igconverge.events.PullToRefreshView.OnRefreshListener, NotificationInterface {

    Typeface myTypefaceRegular, myTypefaceMedium;
    IGCDashboardActivity activity;
    IGCCarPoolNotificationRecyclerAdapter adapter;
    PullToRefreshView mSwipeRefreshLayout;
    TextView emptyText;
    RecyclerView recyclerView;
    FragmentCallback frag;
    View rootView;
Context context;
    ArrayList<CarpoolClass> igAllNotifyList;
    ArrayList<IGCNotificationModel> igMyNotify;
    List<NameValuePair> params;
    ProgressDialog myPd_ring;
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
        frag = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        initialseActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        rootView = inflater.inflate(R.layout.carpool_notification_list_main, container, false);
        emptyText = (TextView) rootView.findViewById(R.id.emptyTextCarPoolNotification);
        igAllNotifyList = new ArrayList<CarpoolClass>();
        igMyNotify = new ArrayList<IGCNotificationModel>();
        params = new ArrayList<>();

        myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
                "font/Roboto-Regular.ttf");

        myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
                "font/Roboto-Medium.ttf");

        init(rootView);

        onRefresh();
        return rootView;
    }

    private void initialseActionBar() {
        // TODO Auto-generated method stub

        if (activity != null) {
            activity.actionBar.setTitle(IGCConstants.CarPoolNotificationFragment);
            activity.actionBar.setDisplayHomeAsUpEnabled(true);

            activity.lockNavigationDrawerSwipeGesture();
            activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.header_bg));
        }
    }

    private void init(View rootView) {

        mSwipeRefreshLayout = (PullToRefreshView) rootView.findViewById(R.id.swiperefreshLayoutCarPoolNotification);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(true);

        recyclerView = (RecyclerView) mSwipeRefreshLayout.findViewById(R.id.recycler_carpool_notification);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new MarginDecoration(activity));
        recyclerView.setHasFixedSize(true);
    }

    public void callCarPoolNotificationService() {
        if (emptyText != null)
            emptyText.setVisibility(View.GONE);
        if(adapter!=null)
        {
            adapter.notifyDataSetChanged();
        }
        checkNetworkStatus();
    }

    private void checkNetworkStatus() {
        if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

            // params = getParams();
//            IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(getActivity(), "GET", frag);
//            getRequest.execute(IGCConstants.fetchAnnouncementsURL);
            IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(
                    getActivity(), "POST", "empId", IGCSharedPreferences.getUserId(getContext()), frag, "fetching");
            getRequest.execute(IGCConstants.fetchRidesNotificationURL);


        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
            dialog.setCancelable(false);
            dialog.show();
            emptyText.setVisibility(View.VISIBLE);
            setAdapter(null);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void fragMultipleResponse(String response, String extra) {
        // TODO Auto-generated method stub

        if (response != null && response.length() > 0) {
            try {
                if (response != null && extra.equalsIgnoreCase("accepted")) {
                 //   activity.onBackPressed();
                    dismissProgressDialog();
                 onRefresh();

                } else if (response != null && extra.equalsIgnoreCase("rejected")) {
                   // activity.onBackPressed();
                    dismissProgressDialog();
                  onRefresh();
                } else if (response != null && extra.equalsIgnoreCase("fetching")) {
                    fetchNotification(response);
                }
            } catch (Exception e) {
                e.printStackTrace();
                emptyText.setVisibility(View.VISIBLE);
                setAdapter(null);
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

            emptyText.setVisibility(View.VISIBLE);
            setAdapter(null);
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    private void fetchNotification(String response) {
        RideParser rideParser = new RideParser();

        igAllNotifyList = rideParser.parseRideData(response);
        IGCafeApplication.setIgcRideList(igAllNotifyList);
        if (igAllNotifyList.size() > 0) {
            igMyNotify = getDataset();
            if (igMyNotify.size() > 0) {
                setAdapter(igMyNotify);
            }
            else
            {
                emptyText.setVisibility(View.VISIBLE);
                setAdapter(null);
            }
            if (mSwipeRefreshLayout != null)
                mSwipeRefreshLayout.setRefreshing(false);



        } else {
            emptyText.setVisibility(View.VISIBLE);
            setAdapter(null);
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }


    private ArrayList<IGCNotificationModel> getDataset() {
        if (IGCSharedPreferences.getUserId(getContext()) != null && IGCSharedPreferences.getUserId(getContext()).length() > 0) {

            Log.d("***************", IGCSharedPreferences.getUserId(getContext()));


        }
        ArrayList result = new ArrayList<IGCNotificationModel>();
        for (int i = 0; i < igAllNotifyList.size(); i++) {
            //if((IGCSharedPreferences.getUserId(getContext())).equalsIgnoreCase(igRideList.get(i).getUser().id))

            if (igAllNotifyList.get(i).getOccupantsList().size() > 0) {
                for (int j = 0; j < igAllNotifyList.get(i).getOccupantsList().size(); j++) {
                    IGCNotificationModel d = new IGCNotificationModel(igAllNotifyList.get(i).getCarNo(), igAllNotifyList.get(i).getOccupantsList().get(j).getName(), igAllNotifyList.get(i).getOccupantsList().get(j).getStatus(), igAllNotifyList.get(i).getPostId(), igAllNotifyList.get(i).getOccupantsList().get(j).getUserId(), igAllNotifyList.get(i).getOccupantsList().get(j).getEmail());
                    result.add(d);
                }

            }
        }

        return result;
        //  return null;

    }


    private void setAdapter(ArrayList<IGCNotificationModel> feedList) {
        adapter = new IGCCarPoolNotificationRecyclerAdapter(feedList, activity, getContext(), this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onRefresh() {
        // TODO Auto-generated method stub
        callCarPoolNotificationService();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialseActionBar();
    }


    @Override
    public void recyclerviewbuttonclicked(View v, String id, int position) {

        if (id.equalsIgnoreCase("1")) {
            checkNetworkStatusAccept(position);
        } else {

            checkNetworkStatusReject(position);
        }


    }
private void showProgressDialog()
{

    myPd_ring=new ProgressDialog(activity);
    myPd_ring.getWindow().setBackgroundDrawable(getResources().getDrawable(R.color.transparent));
    myPd_ring.setTitle(null);
    myPd_ring.setCancelable(false);
    myPd_ring.show();

    myPd_ring.setContentView(R.layout.customdialog);


}

    private void dismissProgressDialog()
    {
        if(myPd_ring!=null)
        {
            if(myPd_ring.isShowing())
            {
                myPd_ring.dismiss();
            }
        }
    }
    private void checkNetworkStatusAccept(int position) {
        if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
            int occupantsCount=0;
            igMyNotify = getDataset();

            String postId = igMyNotify.get(position).getPostId();
            String occupantId = igMyNotify.get(position).getOccupantId();
            params.add(new BasicNameValuePair("rideId", postId));
            params.add(new BasicNameValuePair("empId", occupantId));
           for (int i=0;i<igAllNotifyList.size();i++)
           {
               if (igAllNotifyList.get(i).getPostId().equalsIgnoreCase(igMyNotify.get(position).getPostId()))
               {   ArrayList<OccupantsClass> occupants=new ArrayList<OccupantsClass>();
                   occupants=igAllNotifyList.get(i).getOccupantsList();
                   for (int j=0;j<occupants.size();j++)
                   {
                       if (occupants.get(j).getStatus().equalsIgnoreCase("accepted"))
                       {
                           occupantsCount++;
                       }
                   }

                   if (occupantsCount==igAllNotifyList.get(i).getNoOfSeats())
                   {
                       Toast.makeText(getContext(), "Ride Full", Toast.LENGTH_SHORT).show();
                       Log.d("hello","Rides full");
                   }
                   else {
                       showProgressDialog();
                       IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(
                               getActivity(), "POST", params, frag, "accepted");
                       getRequest.execute(IGCConstants.acceptRidesNotificationURL);
                   }

               }
           }


        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
            dialog.setCancelable(false);
            dialog.show();
            emptyText.setVisibility(View.VISIBLE);
            setAdapter(null);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void checkNetworkStatusReject(int position) {
        try {
            if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
                    showProgressDialog();
                String postId = igMyNotify.get(position).getPostId();
                String occupantId = igMyNotify.get(position).getOccupantId();
                String occupantName = igMyNotify.get(position).getOccupantName();
                String occupanrEmail = igMyNotify.get(position).getOccupantEmail();
                String occupantStatus = igMyNotify.get(position).getOccupantStatus();

                // params = getParams();
//            IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(getActivity(), "GET", frag);
//            getRequest.execute(IGCConstants.fetchAnnouncementsURL);
                JSONObject jsonObj = new JSONObject();

                jsonObj.put("id", occupantId);
                jsonObj.put("name", occupantName);
                jsonObj.put("email", occupanrEmail);
                jsonObj.put("status", occupantStatus);


                String url = IGCConstants.rejectRidesNotificationURL + "/" + postId;

                IGCAsyncServiceRequest getPost = new IGCAsyncServiceRequest(activity, "POST", jsonObj, frag, "rejected");

                getPost.execute(url);


            } else {
                ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
                dialog.setCancelable(false);
                dialog.show();
                emptyText.setVisibility(View.VISIBLE);
                setAdapter(null);
                mSwipeRefreshLayout.setRefreshing(false);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    @Override
//    public void onResume() {
//        IGCafeApplication.NotctivityResumed();
//        super.onResume();
//
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        IGCafeApplication.NotActivityPaused();
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        IGCafeApplication.NotActivityPaused();
//    }

//    @Override
//    public void onStop() {
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
//                mMessageReceiver, new IntentFilter("notify"));
//        super.onStop();
//    }
}
