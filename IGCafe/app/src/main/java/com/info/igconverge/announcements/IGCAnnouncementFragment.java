package com.info.igconverge.announcements;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.LruCache;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.events.IGCEventsListAdapter;
import com.info.igconverge.events.PullToRefreshView;
import com.info.igconverge.model.IGCAnnouncement;
import com.info.igconverge.model.IGCEvent;
import com.info.igconverge.model.IGCEventItem;

import com.info.igconverge.parser.AnnouncementsParser;
import com.info.igconverge.parser.EventsParser;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;
import com.info.igconverge.utility.MarginDecoration;

import static android.widget.Toast.LENGTH_SHORT;

public class IGCAnnouncementFragment extends Fragment implements
		com.info.igconverge.Services.FragmentCallback,
		com.info.igconverge.events.PullToRefreshView.OnRefreshListener {
	private List<IGCAnnouncement> feedItemList = new ArrayList<IGCAnnouncement>();

	private List<IGCAnnouncement> selectedFeedItemList = new ArrayList<IGCAnnouncement>();
	Typeface myTypefaceRegular, myTypefaceMedium;
	IGCDashboardActivity activity;
	IGCAnnouncementRecyclerAdapter adapter;
	PullToRefreshView mSwipeRefreshLayout;
	TextView emptyText;
	RecyclerView recyclerView;
	FragmentCallback frag;
	View rootView;
	List<String> announcementIdList = new ArrayList<String>();

	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			mSwipeRefreshLayout.setRefreshing(true);

			onRefresh();

		}
	};

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;
		frag = this;
		// checkNetworkStatus();
		initialseActionBar();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter("notify"));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		rootView = inflater.inflate(R.layout.announcements_list_main,
				container, false);
		emptyText = (TextView) rootView.findViewById(R.id.emptyText);

		myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");

		myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Medium.ttf");

		init(rootView);
		initialseActionBar();

		onRefresh();

		return rootView;
	}

	private void initialseActionBar() {
		// TODO Auto-generated method stub

		if (activity != null) {
			activity.actionBar.setDisplayHomeAsUpEnabled(true);
			activity.actionBar.setHomeButtonEnabled(true);
			activity.actionBar.setDisplayShowHomeEnabled(true);
			activity.actionBar.setTitle(IGCConstants.AnnouncementsFragment);
			activity.unlockNavigationDrawerSwipeGesture();
			activity.actionBar.setBackgroundDrawable(getResources()
					.getDrawable(R.drawable.header_bg));
		}
	}

	private void init(View rootView) {

		mSwipeRefreshLayout = (PullToRefreshView) rootView
				.findViewById(R.id.swiperefreshLayout);

		mSwipeRefreshLayout.setOnRefreshListener(this);
		mSwipeRefreshLayout.setVisibility(View.VISIBLE);
		mSwipeRefreshLayout.setRefreshing(true);

		recyclerView = (RecyclerView) mSwipeRefreshLayout
				.findViewById(R.id.recycler_view_not);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.addItemDecoration(new MarginDecoration(activity));
		recyclerView.setHasFixedSize(true);

		setAdapter(null);

		final SwipeToDismissTouchListener<RecyclerViewAdapter> touchListener = new SwipeToDismissTouchListener<RecyclerViewAdapter>(
				new RecyclerViewAdapter(recyclerView),
				new SwipeToDismissTouchListener.DismissCallbacks<RecyclerViewAdapter>() {
					@Override
					public boolean canDismiss(int position) {
						return true;
					}

					@Override
					public void onDismiss(RecyclerViewAdapter view, int position) {

						if (feedItemList.size() == 1) {
							adapter.remove(position);
							emptyText.setVisibility(View.VISIBLE);
						} else
							adapter.remove(position);
					}
				});

		recyclerView.setOnTouchListener(touchListener);

		recyclerView
				.setOnScrollListener((RecyclerView.OnScrollListener) touchListener
						.makeScrollListener());
		recyclerView.addOnItemTouchListener(new SwipeableItemClickListener(
				activity, new OnItemClickListener() {
					@Override
					public void onItemClick(View view, int position) {
						/*
						 * if (view.getId() == R.id.txt_delete) {
						 * touchListener.processPendingDismisses(); }
						 */if (view.getId() == R.id.txt_undo) {
							// View parentView = view.getParent();
							adapter.add(position);
							// adapter.add()
							touchListener.undoPendingDismiss();
						} else { // R.id.txt_data
							// Toast.makeText(activity, "Position " + position,
							// LENGTH_SHORT).show();
							showCompleteNotification(position);
						}
					}
				}));
	}

	public void refresh() {
		if (emptyText != null)
			emptyText.setVisibility(View.GONE);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				checkNetworkStatus();
			}

		}, 2000);
	}

	private void checkNetworkStatus() {
		if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

			// params = getParams();
			IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(
					getActivity(), "GET", frag);
			getRequest.execute(IGCConstants.fetchAnnouncementsURL);

		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();
			emptyText.setVisibility(View.VISIBLE);
			setAdapter(null);
			mSwipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void fragMultipleResponse(String response, String extra) {
		// TODO Auto-generated method stub

		if (response != null && response.length() > 0) {
			try {

				if (response != null) {
					fetchAnnouncements(response);

				}
			} catch (Exception e) {
				e.printStackTrace();
				emptyText.setVisibility(View.VISIBLE);
				setAdapter(null);
			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
			dialog.setCancelable(false);
			dialog.show();

			emptyText.setVisibility(View.VISIBLE);
			setAdapter(null);
			mSwipeRefreshLayout.setRefreshing(false);
		}

	}

	private void fetchAnnouncements(String response) {
		if (feedItemList != null)
			feedItemList.clear();
		if (announcementIdList != null)
			announcementIdList.clear();
		AnnouncementsParser parser = new AnnouncementsParser(getActivity());
		feedItemList = parser.parseAnnouncements(response);
		IGCConstants.setAnnouncementsList(feedItemList);
		announcementIdList = IGCSharedPreferences
				.getPREF_ANNOUNCEMENT_ID_LIST(activity);
		if (IGCConstants.getAnnouncementsList().size() == 0) {
			if (emptyText != null) {
				emptyText.setVisibility(View.VISIBLE);
				setAdapter(null);
				mSwipeRefreshLayout.setRefreshing(false);

			}
		}

		else {
			if (announcementIdList != null) {
				if (announcementIdList.size() != 0) {
					for (int i = 0; i < feedItemList.size(); i++) {
						IGCAnnouncement feed = feedItemList.get(i);
						String notificationId = feed.getNotificationId();

						if (!announcementIdList.contains(notificationId)) {
							selectedFeedItemList.add(feed);
						} else {
						}
					}
					if (emptyText != null)
						emptyText.setVisibility(View.GONE);
					feedItemList = selectedFeedItemList;
				}
				// else

			}

			if (feedItemList.size() > 0) {
				setAdapter(feedItemList);
				mSwipeRefreshLayout.setRefreshing(false);
				emptyText.setVisibility(View.GONE);
			} else {
				setAdapter(null);

				mSwipeRefreshLayout.setRefreshing(false);
				emptyText.setVisibility(View.VISIBLE);
			}
		}
	}

	public void showCompleteNotification(int position) {

		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(activity);
		dialogBuilder.setTitleViewGone(true).withTitle("My Dialog")

		.withTitleColor("#FFFFFF")

		.withDividerColor("#11000000")

		.withMessage("This is a modal Dialog.")

		.setMessageViewGone(true).withMessageColor("#FFFFFFFF")

		.withDialogColor(Color.TRANSPARENT)

		.isCancelableOnTouchOutside(false)

		.withDuration(300)

		.withEffect(Effectstype.Slidetop)
				.setCustomView(R.layout.notification_detail, activity).show();
		TextView notifyMessage = (TextView) dialogBuilder
				.findViewById(R.id.notificationDetailText);
		TextView notifyHeading = (TextView) dialogBuilder
				.findViewById(R.id.notificationHeadText);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okNotifyButton);
		notifyMessage.setTypeface(myTypefaceRegular);
		notifyHeading.setTypeface(myTypefaceMedium);
		notifyHeading
				.setText(feedItemList.get(position).getNotificationTitle());
		notifyMessage.setText(feedItemList.get(position)
				.getNotificationMessage());
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialogBuilder.dismiss();

			}
		});

	}

	private void setAdapter(List<IGCAnnouncement> feedList) {
		adapter = new IGCAnnouncementRecyclerAdapter(feedList, activity);
		recyclerView.setAdapter(adapter);
		adapter.notifyDataSetChanged();

	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		refresh();
	}

	@Override
	public void onResume() {
		IGCafeApplication.NotctivityResumed();
		super.onResume();

	}

	@Override
	public void onPause() {
		super.onPause();
		IGCafeApplication.NotActivityPaused();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		IGCafeApplication.NotActivityPaused();
	}

	@Override
	public void onStop() {
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter("notify"));
		super.onStop();
	}
}
