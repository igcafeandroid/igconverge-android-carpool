package com.info.igconverge.carpool;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.info.igconverge.R;

import java.util.HashMap;
import java.util.List;


public class IGCPickLocationAdapter extends BaseAdapter {

    Context context ;
    List<HashMap<String, String>> places;

    public IGCPickLocationAdapter(Context context, List<HashMap<String, String>> places) {
        this.context = context;
        this.places = places;
    }

    @Override
    public int getCount() {
        return places.size();
    }

    @Override
    public Object getItem(int i) {
        return places.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if(view == null) {

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.places_list_item, viewGroup,false);

            viewHolder = new ViewHolder();
            viewHolder.locationImg = (ImageView)view.findViewById(R.id.place_img);
            viewHolder.locationText = (TextView)view.findViewById(R.id.place_text);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        HashMap<String,String> location = places.get(i);
        if(location != null){

            viewHolder.locationImg.setImageResource(R.drawable.places_marker);
            viewHolder.locationText.setText(location.get("description"));
        }

        return view;
    }

    public void updatePlaces(List<HashMap<String, String>> places){
        this.places = places;
    }

    static class ViewHolder {
        ImageView locationImg;
        TextView locationText;
    }
}
