package com.info.igconverge.carpool;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.events.PullToRefreshView;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.MarginDecoration;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;


import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filterable;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by lab2 on 23/12/15.
 */
public class IGCCarpoolFindrideFragment extends Fragment implements

        com.info.igconverge.Services.FragmentCallback,FindRideInterface {

    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter madapter;
    private RecyclerView.LayoutManager mlayout;
    IGCDashboardActivity activity;
    FragmentCallback frag;
    ArrayList<CarpoolClass> igcRideList;
    ArrayList<CarpoolClass> result;
    TextView emptyText;
    ArrayList<CarpoolClass> searchData;
    int count = 0;
    Fragment detail;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
        frag = this;
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.carpool_search, menu);

        SearchManager searchManager = (SearchManager) activity
                .getSystemService(Context.SEARCH_SERVICE);

        MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView search = (SearchView) searchItem.getActionView();
        search.setQueryHint(getResources().getString(R.string.car_search_hint));
        search.setIconifiedByDefault(true);


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override

            public boolean onQueryTextChange(String arg0) {
                // TODO Auto-generated method stub

                if (TextUtils.isEmpty(arg0)) {
                    ((Filterable) madapter).getFilter().filter("");
                } else {

                    ((Filterable) madapter).getFilter().filter(arg0.toString());
                }

                return true;
            }


            @Override
            public boolean onQueryTextSubmit(String arg0) {
                // TODO Auto-generated method stub
                return false;

            }
        });

        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        initializeActionBar();
        setHasOptionsMenu(true);
    }

    private void initializeActionBar() {
        // TODO Auto-generated method stub
        activity.actionBar.setDisplayHomeAsUpEnabled(true);
        activity.actionBar.setHomeButtonEnabled(true);
        activity.actionBar.setDisplayShowHomeEnabled(true);
        activity.actionBar.setTitle(IGCConstants.CarPoolFragment);
        activity.unlockNavigationDrawerSwipeGesture();
        activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View rootView = inflater.inflate(R.layout.findride_search,
                container, false);
        emptyText = (TextView) rootView.findViewById(R.id.emptyText_findRide);
        emptyText.setVisibility(View.GONE);
        igcRideList = new ArrayList<CarpoolClass>();
        result = new ArrayList<CarpoolClass>();
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh_findRide);

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                if(!swipeRefreshLayout.isRefreshing())
                {
                    swipeRefreshLayout.setRefreshing(true);
                }

            }
        });


        recyclerView = (RecyclerView) swipeRefreshLayout.findViewById(R.id.recycle_findRide);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new MarginDecoration(activity));
        recyclerView.setHasFixedSize(true);

        madapter = new RecycleViewClass(getContext(), result,this);
        checkNetworkStatus();

        // callServiceMethod();
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_orange_dark, android.R.color.holo_green_dark,android.R.color.darker_gray,
                android.R.color.holo_orange_light
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                result = new ArrayList<CarpoolClass>();
                callServiceMethod();

            }
        });
        recyclerView.setAdapter(null);
        return rootView;
    }


    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }


    public void callServiceMethod() {

        if (madapter != null) {
            madapter.notifyDataSetChanged();
        }

        checkNetworkStatus();

    }

    private void checkNetworkStatus() {

        if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

            setHasOptionsMenu(true);


            IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(
                    activity, "GET", frag);
            getRequest.execute(IGCConstants.fetchRidesURL);

        } else {

            Log.d("error#####", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

            recyclerView.setAdapter(null);
            emptyText.setVisibility(View.VISIBLE);
            setSwipeRefreshingFalse();
            ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
            dialog.setCancelable(false);
            dialog.show();
            Log.d("error#####", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        }



    }


    @Override
    public void fragMultipleResponse(String response, String extra) {
        // TODO Auto-generated method stub
        setSwipeRefreshingFalse();
        if (response != null && response.length() > 0) {
            try {

                if (response != null) {
                    fetchRide(response);
                    setSwipeRefreshingFalse();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

            emptyText.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(null);

            setSwipeRefreshingFalse();
        }

    }

    public void fetchRide(String response) {
        RideParser carParser = new RideParser();
        // igcRideList = new ArrayList<CarpoolClass>();
        igcRideList.clear();
        igcRideList = carParser.parseRideData(response);

        if (igcRideList.size() > 0) {

            for (int i = 0; i < igcRideList.size(); i++) {
                //if((IGCSharedPreferences.getUserId(getContext())).equalsIgnoreCase(igRideList.get(i).getUser().id))
                CarpoolClass object = new CarpoolClass();


                if (igcRideList.get(i).getUser() != null) {
                    if (!(igcRideList.get(i).getUser().id.equalsIgnoreCase(IGCSharedPreferences.getUserId(getContext())))) {
                        if (igcRideList.get(i).isStatus()) {
                            object.setPostId(igcRideList.get(i).postId);

                            object.setUser(igcRideList.get(i).getUser());
                            object.setStartLocation(igcRideList.get(i).startLocation);
                            object.setNoOfSeats(igcRideList.get(i).noOfSeats);
                            object.setCarNo(igcRideList.get(i).carNo);
                            object.setFare(igcRideList.get(i).fare);
                            object.setStatus(igcRideList.get(i).status);
                            object.setDepTime(igcRideList.get(i).depTime);
                            object.setPostedOn(igcRideList.get(i).postedOn);
                            object.setComments(igcRideList.get(i).comments);
                            object.setOccupantsList(igcRideList.get(i).occupantsList);
                            object.setDays(igcRideList.get(i).days);
                            result.add(object);



                        }
                    }
                }
            }
            if (result.size() > 0) {
                emptyText.setVisibility(View.GONE);

                madapter = new RecycleViewClass(getContext(), result,this);
                // madapter.notifyDataSetChanged();
                recyclerView.setAdapter(madapter);
                if (swipeRefreshLayout != null)
                    setSwipeRefreshingFalse();
            }
            else {
                setHasOptionsMenu(false);
                emptyText.setVisibility(View.VISIBLE);
                setSwipeRefreshingFalse();
            }

        } else {
            setHasOptionsMenu(false);
            emptyText.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(null);
            if (swipeRefreshLayout != null)
                setSwipeRefreshingFalse();
        }

    }
    private void setSwipeRefreshingFalse()
    {
        if(swipeRefreshLayout!=null)
        {
//            if(swipeRefreshLayout.isRefreshing())
//            {
               swipeRefreshLayout.setRefreshing(false);
            //}
        }
    }

    @Override
    public void rideClick(View v, int Position, String postId) {
        for (CarpoolClass g:result){
            if(g.getPostId().equalsIgnoreCase(postId)){

                String postid = g.getPostId();
                String carNo = g.getCarNo();
                int fare = g.getFare();
                String location = g.getStartLocation();
                String time = g.getDepTime();
                int seats = g.getNoOfSeats();
                String comments = g.getComments();
                String driver =g.getUser().getName();
                String email = g.getUser().getEmail();
                ArrayList<OccupantsClass> occupantList = new ArrayList<OccupantsClass>();
                ArrayList<String > days_all=new ArrayList<>();

                String phNo =g.getUser().getPhoneNo();

                System.out.print(phNo);

                for (int j = 0; j < g.getOccupantsList().size(); j++) {
                    occupantList.add(g.getOccupantsList().get(j));

                }

                for (int j = 0; j < g.getDays().size(); j++) {
                    days_all.add(g.getDays().get(j).substring(0,3));

                }
                detail = new IGCFindride_detail_fragment();


                Bundle bundle = new Bundle();
                bundle.putString("postid", postid);
                bundle.putString("driver", driver);
                bundle.putString("carNo", carNo);
                bundle.putInt("fare", fare);
                bundle.putString("location", location);
                bundle.putString("time", time);
                bundle.putInt("seats", seats);
                bundle.putString("comments", comments);
                bundle.putString("email", email);
                bundle.putStringArrayList("Days",days_all);


                bundle.putString("phoneNumber",phNo);
                bundle.putParcelableArrayList("occupantsList", occupantList);

                detail.setArguments(bundle);
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                FragmentTransaction ft = (activity).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, detail).addToBackStack(null);
                ft.commit();
                if ((activity.mDrawerToggle) != null)
                    activity.mDrawerToggle.setDrawerIndicatorEnabled(false);



                //Toast.makeText(getActivity(), "clicked" + igcRideList.get(recyclerView.getChildPosition(child)).getPostId(), Toast.LENGTH_SHORT).show();

                break;


            }
        }
    }
}
