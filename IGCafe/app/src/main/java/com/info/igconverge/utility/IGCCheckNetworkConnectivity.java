package com.info.igconverge.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class IGCCheckNetworkConnectivity 
{
	public static Boolean checkConnection(Context context)
	{
		try
		{
			ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mobileNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			
			if(wifiNetworkInfo.isConnected() || mobileNetworkInfo.isConnected())
			{
				return true;
			}
		}
		catch(Exception e)
		{
          e.printStackTrace();
        }
        return false;
	}
}
