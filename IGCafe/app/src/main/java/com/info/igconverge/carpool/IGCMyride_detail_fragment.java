package com.info.igconverge.carpool;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by lab2 on 24/12/15.
 */
public class IGCMyride_detail_fragment extends Fragment implements FragmentCallback{


    IGCDashboardActivity activity;
    FragmentCallback frag;
    Context mContext;
    private Typeface myTypefaceMedium,myTypefaceLight, myTypefaceRegular;
 String timeString;
    String postId,phoneNo;
    ArrayList<String> totalDays;
    FragmentManager fragmentManager;

    ArrayList<OccupantsClass> occupantList;
    TextView emptyText;
    ArrayList<String> days_all;
    int fareText;
    Bundle bundle = new Bundle();

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
        frag=this;
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        initialseActionBar();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
initialseActionBar();
    }
    private void initialseActionBar() {
        // TODO Auto-generated method stub

        if (activity != null) {
            activity.actionBar.setDisplayHomeAsUpEnabled(true);
            activity.unlockNavigationDrawerSwipeGesture();
            activity.actionBar.setTitle(IGCConstants.CarPoolFragment);
            activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        mContext=container.getContext();
        myTypefaceMedium = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Medium.ttf");
        myTypefaceLight = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Light.ttf");

        View rootView = inflater.inflate(R.layout.carpool_detail,
                container, false);
        occupantList=new ArrayList<>();
        days_all=new ArrayList<>();

        Button btn=(Button)rootView.findViewById(R.id.carpoolButton);
        btn.setText("Delete");


        TextView fare = (TextView) rootView.findViewById(R.id.cost1);
        TextView time = (TextView) rootView.findViewById(R.id.time1);
        TextView location = (TextView) rootView.findViewById(R.id.loc1);
        TextView seats = (TextView) rootView.findViewById(R.id.seats1);
        TextView carNo = (TextView) rootView.findViewById(R.id.carNo1);
        TextView comments = (TextView) rootView.findViewById(R.id.cmt);
        TextView Driver =(TextView)rootView.findViewById(R.id.Driver);
        ImageView commentImg=(ImageView)rootView.findViewById(R.id.commentImg);

        TextView emptyText_comment=(TextView)rootView.findViewById(R.id.emptyText_comment);
        emptyText = (TextView) rootView.findViewById(R.id.emptyText);

        System.out.print(occupantList);

        Driver.setTypeface(myTypefaceMedium);
        fare.setTypeface(myTypefaceLight);
        time.setTypeface(myTypefaceLight);
        location.setTypeface(myTypefaceLight);
        seats.setTypeface(myTypefaceLight);
        carNo.setTypeface(myTypefaceLight);
        comments.setTypeface(myTypefaceLight);
        totalDays=new ArrayList<>();
        if(IGCConstants.editeddata!=null)
        {
            Bundle bundl = IGCConstants.editeddata;
            totalDays=bundl.getStringArrayList("Days");
            occupantList=bundl.getParcelableArrayList("occupantsList");
            fare.setText(String.valueOf(bundl.getInt("fare")));
            seats.setText(String.valueOf(bundl.getInt("seats")));
            location.setText(bundl.getString("location"));
            for (int i=0;i<bundl.getStringArrayList("Days").size();i++)
            {
                days_all.add(bundl.getStringArrayList("Days").get(i).substring(0,3));
            }
            time.setText(bundl.getString("time")+" "+days_all);
            timeString=bundl.getString("time").toString();
            carNo.setText(bundl.getString("carNo"));
            String comt= bundl.getString("comments");
            if(comt.equals("")) {
                commentImg.setVisibility(View.GONE);
                emptyText_comment.setVisibility(View.VISIBLE);
            }
            else
            {
                commentImg.setVisibility(View.VISIBLE);
                comments.setText(comt);
            }
            Driver.setText(bundl.getString("driver"));
            postId = bundl.getString("postid");
            phoneNo=bundl.getString("phoneNo");
            fareText = bundl.getInt("fare");
        }
        else
        {
            Bundle bundl = getArguments();
            totalDays=bundl.getStringArrayList("Days");
            occupantList=bundl.getParcelableArrayList("occupantsList");
            fare.setText(String.valueOf(bundl.getInt("fare")));
            seats.setText(String.valueOf(bundl.getInt("seats")));
            location.setText(bundl.getString("location"));
            for (int i=0;i<bundl.getStringArrayList("Days").size();i++)
            {
                days_all.add(bundl.getStringArrayList("Days").get(i).substring(0,3));
            }
            time.setText(bundl.getString("time")+" "+days_all);
            timeString=bundl.getString("time").toString();
            carNo.setText(bundl.getString("carNo"));
            String comt= bundl.getString("comments");
            if(comt.equals("")) {
                emptyText_comment.setVisibility(View.VISIBLE);
            }
            else
            {
                comments.setText(comt);
            }
            Driver.setText(bundl.getString("driver"));
            postId = bundl.getString("postid");
            phoneNo=bundl.getString("phoneNo");
             fareText = bundl.getInt("fare");
        }


        bundle.putString("postid", postId);
        bundle.putString("carNo", carNo.getText().toString());
        bundle.putInt("fare", fareText);
        bundle.putString("location", location.getText().toString());
        bundle.putString("time",timeString);
        bundle.putInt("seats", Integer.parseInt(seats.getText().toString()));
        bundle.putString("comments", comments.getText().toString());
        bundle.putString("phoneNo", phoneNo);
        bundle.putStringArrayList("days", totalDays);
        bundle.putString("location", location.getText().toString());
        bundle.putParcelableArrayList("occupantsList", occupantList);

        LinearLayout myLinearLayout = (LinearLayout) rootView.findViewById(R.id.commentsEventsLayout);

        if (occupantList.size()>0) {
            //myLinearLayout.setLayoutParams(params)


            final TextView[] myTextViews = new TextView[occupantList.size()];

                for (int i = 0; i < occupantList.size(); i++) {
                    if (occupantList.get(i).getStatus().equalsIgnoreCase("accepted")) {    //Changes done  ---------------------------------
                        // create a new textview
                        emptyText.setVisibility(View.GONE);
                        final TextView rowTextView = new TextView(getActivity());

                        // set some properties of rowTextView or something
                        rowTextView.setText(occupantList.get(i).getName());
                        rowTextView.setTypeface(myTypefaceMedium);

                        // add the textview to the linearlayout
                        myLinearLayout.addView(rowTextView);
                        rowTextView.setTextColor(getResources().getColor(R.color.Details_occupant_color));
                        rowTextView.setBackgroundResource(R.drawable.carpool_roundcorner);
                        rowTextView.setGravity(Gravity.CENTER);


                        // save a reference to the textview for later
                        myTextViews[i] = rowTextView;
                    }

                }
            } else {

                emptyText.setVisibility(View.VISIBLE);


            }

        btn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                showConfirmationDeletePicDialog();
            }
        });





        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.myride, menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.carpoolEdit:

               // Toast.makeText(activity, "Edit ICON", Toast.LENGTH_SHORT).show();
                Fragment fragment=new IGCCarpoolEdit();
                if(fragment!=null)
                {   fragment.setArguments(bundle);
                    fragmentManager=(activity).getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container,fragment).addToBackStack(null).commit();
                    if ((activity.mDrawerToggle) != null)
                        activity.mDrawerToggle.setDrawerIndicatorEnabled(false);
                }

        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void fragMultipleResponse(String response, String extra) {
        if (response != null && response.length() > 0) {
            try {

                if (response != null) {
                    //fetchRide(response);
                    //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                    activity.onBackPressed();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

        }}

    public void showConfirmationDeletePicDialog() {

        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(getActivity());
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog")

                .withTitleColor("#FFFFFF")

                .withDividerColor("#11000000")

                .withMessage("This is a modal Dialog.")

                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")

                .withDialogColor(Color.TRANSPARENT)

                .isCancelableOnTouchOutside(false)

                .withDuration(300)

                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.delete_image_dialog,
                        getContext()).show();
        dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        TextView titleMessage = (TextView) dialogBuilder
                .findViewById(R.id.titleMessage);
        TextView deleteMessage = (TextView) dialogBuilder
                .findViewById(R.id.deleteMessage);
        TextView okDeletePhoto = (TextView) dialogBuilder
                .findViewById(R.id.okDeletePhoto);
        TextView cancelDeletePhoto = (TextView) dialogBuilder
                .findViewById(R.id.cancelDeletePhoto);
        titleMessage.setTypeface(myTypefaceRegular);
        deleteMessage.setText(getResources().getString(R.string.deleteride));
        titleMessage.setText("Delete Ride!");
        deleteMessage.setVisibility(View.VISIBLE);
        cancelDeletePhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();

            }
        });
        okDeletePhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                IGCSharedPreferences.setProfileImage(getContext(),
//                        null);
//                profileImage.setImageResource(R.drawable.avatar_pic);
                try {
                    if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

                        // JSONObject jsonObj = new JSONObject();

                        String url = IGCConstants.RideDeleteRequestURL + "?rideId=" + postId;
                        IGCAsyncServiceRequest getPost = new IGCAsyncServiceRequest(activity, "POST", frag);

                        getPost.execute(url);

                    } else {
                        ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
                        dialog.setCancelable(false);
                        dialog.show();
                        System.out.println("in error dialog");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialogBuilder.dismiss();

            }
        });



    }

}



/**
 * Sets ListView height dynamically based on the height of the items.
 *
 * @param listView to be resized
 * @return true if the listView is successfully resized, false otherwise
 */







