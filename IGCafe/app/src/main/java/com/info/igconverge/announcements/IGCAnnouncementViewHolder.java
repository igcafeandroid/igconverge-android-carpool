package com.info.igconverge.announcements;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;

public class IGCAnnouncementViewHolder extends RecyclerView.ViewHolder {
	protected TextView headingNotify1, headingNotify2, headingNotify3,
			headerNotifyCircle,newTextView;
	IGCDashboardActivity activity;
	TextView idView;
	Typeface myTypefaceMedium, myTypefaceRegular;

	public IGCAnnouncementViewHolder(View view, IGCDashboardActivity activity) {
		super(view);
		this.activity = activity;
		headerNotifyCircle = (TextView) view
				.findViewById(R.id.headingNotifyCircle);
		headingNotify1 = (TextView) view.findViewById(R.id.headingNotify1);
		headingNotify2 = (TextView) view.findViewById(R.id.headingNotify2);
		headingNotify3 = (TextView) view.findViewById(R.id.headingNotify3);
		newTextView = (TextView) view.findViewById(R.id.textView1);
		idView = (TextView) view.findViewById(R.id.notifyId);

		myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Medium.ttf");
		myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");
		this.headerNotifyCircle.setTypeface(myTypefaceMedium);
		this.headingNotify1.setTypeface(myTypefaceMedium);
		this.headingNotify2.setTypeface(myTypefaceRegular);
		this.headingNotify3.setTypeface(myTypefaceRegular);
	}
}