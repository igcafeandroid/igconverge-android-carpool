package com.info.igconverge.events.calendar;



public interface OnDayClickListener {

    public void onClick(String date);
}