package com.info.igconverge.dialogs;

import com.info.igconverge.R;
import com.info.igconverge.utility.TouchImageView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;

import android.widget.ProgressBar;


public class EventImageDialog extends Dialog {
	private AnimationDrawable animation;
	// private Animation animation;
	Context context;
	TouchImageView eventImg;
	int delay = 0;
	ProgressBar mProgressBar;
	Bitmap bitmap;
	Dialog dialog;


	public EventImageDialog(Context context, Bitmap bitmap) {

		super(context);
		this.context = context;
		this.bitmap = bitmap;
	}

	public EventImageDialog(Context context, int theme) {

		super(context, theme);
		this.context = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setCancelable(true);
		dialog = this;

		setContentView(R.layout.event_image_zoom);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		eventImg = (TouchImageView) findViewById(R.id.checkImage);
		eventImg.setImageBitmap(bitmap);
	}

	@Override
	public void show() {

		super.show();
	}

	@Override
	public void dismiss() {

		super.dismiss();

	}

	public static Bitmap decodeSampledBitmapFromResource(byte[] arr,
			int reqWidth, int reqHeight) {

		
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(arr, 0, arr.length, options);

	
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

	
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Config.RGB_565;
		options.inDither = true;
		return BitmapFactory.decodeByteArray(arr, 0, arr.length, options);
	}

	

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
	
		return inSampleSize;
	}
}