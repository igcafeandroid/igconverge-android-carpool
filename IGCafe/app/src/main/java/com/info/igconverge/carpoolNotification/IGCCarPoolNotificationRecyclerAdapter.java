package com.info.igconverge.carpoolNotification;

/**
 * Created by nitika on 29/12/15.
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.info.igconverge.R;

import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.model.IGCAnnouncement;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;

public class IGCCarPoolNotificationRecyclerAdapter extends
        RecyclerView.Adapter<IGCCarPoolNotificationViewHolder> implements FragmentCallback {

    private List<IGCNotificationModel> mDataSet = new ArrayList<IGCNotificationModel>();
    IGCDashboardActivity activity;
    List<String> notificationIdList = new ArrayList<String>();
    int count;
    private static NotificationInterface notify;

    Context context;

    IGCCarPoolNotificationRecyclerAdapter(List<IGCNotificationModel> mDataSet,
                                          IGCDashboardActivity activity, Context context, NotificationInterface notify) {
        this.mDataSet = mDataSet;
        this.activity = activity;
        count = IGCConstants.displayCount;
        this.context = context;
        this.notify = notify;


    }

    @Override
    public IGCCarPoolNotificationViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int position) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.carpool_notification_list_row, null);
        IGCCarPoolNotificationViewHolder mh = new IGCCarPoolNotificationViewHolder(v,
                activity, context);
        return mh;
    }

    @Override
    public void onBindViewHolder(final IGCCarPoolNotificationViewHolder holder, final int position) {
        IGCNotificationModel feedItem = mDataSet.get(position);
        int flag = 0;

        String occupantDetail=null ;
                String titleCircle = "CAR NO: "+feedItem.getCarNo();

        if (feedItem.getOccupantStatus().equalsIgnoreCase("accepted")) {
            occupantDetail = "You have "+feedItem.getOccupantStatus()+" "+feedItem.getOccupantName() + "'s request"  ;
            holder.accept.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);
        }
        else
        {  occupantDetail = feedItem.getOccupantName()+"'s request is "+feedItem.getOccupantStatus();
            holder.reject.setVisibility(View.VISIBLE);
            holder.accept.setVisibility(View.VISIBLE);


        }


        holder.headingNotify1.setText(titleCircle);
        holder.headingNotify2.setText(occupantDetail);
        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notify.recyclerviewbuttonclicked(v, "1",position);
            }
        });
        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              notify.recyclerviewbuttonclicked(v,"0",position);
              //  Toast.makeText(context, "gfghhj", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        if (mDataSet != null) {
            return mDataSet.size();

        } else
            return 0;
    }


    @Override
    public void fragMultipleResponse(String response, String extra) {
        if (response != null && response.length() > 0) {
            try {

                if (response != null) {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

        }
    }


}