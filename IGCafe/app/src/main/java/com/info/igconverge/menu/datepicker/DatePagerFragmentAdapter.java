

package com.info.igconverge.menu.datepicker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public abstract class DatePagerFragmentAdapter<T extends Fragment> extends FragmentStatePagerAdapter {

    private AbsDateAdapter defaultDateAdapter;

    public DatePagerFragmentAdapter(FragmentManager fm, AbsDateAdapter defaultDateAdapter) {
        super(fm);
        this.defaultDateAdapter = defaultDateAdapter;
    }

    @Override
    public Fragment getItem(int position) {
        DateItem dateItem = defaultDateAdapter.getItem(position);
        return getFragment(position, dateItem.getDate().getTime());
    }

    @Override
    public int getCount() {
        return defaultDateAdapter.getItemCount();
    }

    protected abstract T getFragment(int position, long date);

}
