package com.info.igconverge.carpool;

public class HeaderModel implements SectionInterface{
    private String header;

    public HeaderModel(String header) {
        this.header=header;
        // TODO Auto-generated constructor stub
    }

    public String getHeader() {
        return header;
    }
    public void setHeader(String header) {
        this.header = header;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}
