package com.info.igconverge.model;


public class IGCFeedback 
{
	private IGCFeedbackEmployee employee;
	private IGCRatings ratings;
	/**
	 * 
	 */
	public IGCFeedback() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param employee
	 * @param ratings
	 */
	public IGCFeedback(IGCFeedbackEmployee employee, IGCRatings ratings) {
		super();
		this.employee = employee;
		this.ratings = ratings;
	}
	/**
	 * @return the employee
	 */
	public IGCFeedbackEmployee getEmployee() {
		return employee;
	}
	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(IGCFeedbackEmployee employee) {
		this.employee = employee;
	}
	/**
	 * @return the ratings
	 */
	public IGCRatings getRatings() {
		return ratings;
	}
	/**
	 * @param ratings the ratings to set
	 */
	public void setRatings(IGCRatings ratings) {
		this.ratings = ratings;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IGCFeedback [employee=");
		builder.append(employee);
		builder.append(", ratings=");
		builder.append(ratings);
		builder.append("]");
		return builder.toString();
	}
	
	
}
