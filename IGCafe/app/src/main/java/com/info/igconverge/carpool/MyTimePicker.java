package com.info.igconverge.carpool;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

import com.info.igconverge.R;


public class MyTimePicker extends DialogFragment implements OnTimeSetListener{
	TextView setTime;
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub

		setTime=(TextView)getActivity().findViewById(R.id.setTime);
		if(hourOfDay<10 && minute<10) {
			setTime.setText("0"+String.valueOf(hourOfDay) + ":" +"0"+String.valueOf(minute));
		}
		else if (hourOfDay<10)
		{
			setTime.setText("0"+String.valueOf(hourOfDay) + ":" +String.valueOf(minute));

		}
		else if(minute<10)
		{
			setTime.setText(String.valueOf(hourOfDay) + ":" +"0"+String.valueOf(minute));

		}
		else
		{
			setTime.setText(String.valueOf(hourOfDay) + ":" +String.valueOf(minute));

		}
		
		
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		 final Calendar c = Calendar.getInstance();
         int hour = c.get(Calendar.HOUR_OF_DAY);
         int minute = c.get(Calendar.MINUTE);

         return new TimePickerDialog(getActivity(), this, hour, minute,
                 DateFormat.is24HourFormat(getActivity()));
		
		
		//return super.onCreateDialog(savedInstanceState);
	}
	
	

}
