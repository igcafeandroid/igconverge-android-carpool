package com.info.igconverge.login;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.ServiceHandler;
import com.info.igconverge.UIControls.RoundedImageView;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dashboard.IGCMenuFragment;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.model.IGCEmployeeDetails;
import com.info.igconverge.utility.CustomGestureDetector;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCImageUtility;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;
import com.instabug.library.Instabug;
import com.instabug.library.Instabug.IBGInvocationEvent;

public class IGCAuthenticationActivity extends Activity {
	Activity activity;
	String stringStatus, stringMessage;
	int serviceResult = 0;
	String id;
	String username;
	String password;
	String firstName;
	String middleName;
	String lastName;
	String mailId;
	String imageName;
	Button retryButton, loginButton;
	ImageView errorIconAuthenticationScreen;
	TextView authenticationName, messageAuthenticating,
			messageAuthenticatingError;
	ProgressBar loadingIconAuthenticationScreen;
	ImageView profilePicAuthenticating;
	private boolean isUpdateAvailable;
	private String updateTitle;
	private String updateMessage;
	private String updateUrl;
	private Typeface myTypefaceMedium;
	private Typeface myTypefaceRegular;
	private boolean isMandatory;
	LoginTask loginTask;
	NiftyDialogBuilder dialogBuilder;
	private GestureDetector mGestureDetector;

	IGCSharedPreferences sharedPref = new IGCSharedPreferences();
	String type;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// if (IGCafeApplication.isActivityVisible()) {
		// Intent i = new Intent(this, IGCDashboardActivity.class);
		// startActivity(i);
		// } else {
		IGCafeApplication.mSensorManager
				.registerListener(IGCafeApplication.mShakeDetector,
						IGCafeApplication.mAccelerometer,
						SensorManager.SENSOR_DELAY_UI);

		CustomGestureDetector customGestureDetector = new CustomGestureDetector();

		mGestureDetector = new GestureDetector(this, customGestureDetector);

		mGestureDetector.setOnDoubleTapListener(customGestureDetector);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.activity = this;
		Instabug.getInstance().setInvocationEvent(
				IBGInvocationEvent.IBGInvocationEventNone);
		if (getIntent() != null) {

			type = getIntent().getStringExtra("type");
		}
		setContentView(R.layout.authenticating_layout);
		retryButton = (Button) findViewById(R.id.retryButton);
		loginButton = (Button) findViewById(R.id.loginButton);
		errorIconAuthenticationScreen = (ImageView) findViewById(R.id.errorIconAuthenticationScreen);
		loadingIconAuthenticationScreen = (ProgressBar) findViewById(R.id.loadingProgressAuthenticationScreen);
		messageAuthenticating = (TextView) findViewById(R.id.messageAuthenticating);
		messageAuthenticatingError = (TextView) findViewById(R.id.messageAuthenticatingError);
		messageAuthenticatingError.setVisibility(View.INVISIBLE);
		authenticationName = (TextView) findViewById(R.id.userNameAuthentication);
		profilePicAuthenticating = (ImageView) findViewById(R.id.profilePicAuthenticating);
		retryButton.setVisibility(View.INVISIBLE);
		loginButton.setVisibility(View.GONE);
		loadingIconAuthenticationScreen.setVisibility(View.VISIBLE);
		errorIconAuthenticationScreen.setVisibility(View.INVISIBLE);
		myTypefaceMedium = Typeface.createFromAsset(getAssets(),
				"font/Roboto-Medium.ttf");

		myTypefaceRegular = Typeface.createFromAsset(getAssets(),
				"font/Roboto-Regular.ttf");
		loadingIconAuthenticationScreen.getIndeterminateDrawable()
				.setColorFilter(getResources().getColor(R.color.orange),
						android.graphics.PorterDuff.Mode.SRC_IN);

		if (sharedPref.getUserId(getApplicationContext()) != null
				&& IGCSharedPreferences.getProfileName(getApplicationContext()) != null) {
			authenticationName.setText(IGCSharedPreferences
					.getProfileName(getApplicationContext())
					+ " - "
					+ sharedPref.getUserId(getApplicationContext()));
		} else {
			authenticationName.setVisibility(View.INVISIBLE);
		}

		authenticateUserServiceCall();

		retryButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				authenticateUserServiceCall();
			}
		});
		loginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(IGCAuthenticationActivity.this,
						IGCLoginScreenActivity.class);
				startActivity(i);
				finish();

			}
		});
		// }
	}

	@Override
	public void onBackPressed() {

		IGCafeApplication.mSensorManager
				.unregisterListener(IGCafeApplication.mShakeDetector);
		super.onBackPressed();
		if (dialogBuilder != null && dialogBuilder.isShowing()) {
			dialogBuilder.dismiss();
		}
		if (loginTask != null) {
			loginTask.cancel(true);

		}
		finish();
	}

	private void loadProfilePic() {
		if (IGCSharedPreferences.getProfileImage(getApplicationContext()) != null
				&& IGCSharedPreferences
						.getProfileImage(getApplicationContext()).length() > 0) {
			try {

				File imageFile = new File(
						IGCSharedPreferences
								.getProfileImage(getApplicationContext()));
				Uri imageUri = Uri.fromFile(imageFile);
				if (imageUri != null && imageFile != null && imageFile.exists()) {

					settingProfilePic(imageUri, imageFile);
				} else {
					profilePicAuthenticating
							.setImageResource(R.drawable.profile_fill);
				}
			} catch (Exception e) {
				// TODO: handle exception
				if (e != null)
					e.printStackTrace();
			}

		}

	}

	private void authenticateUserServiceCall() {

		if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

			loginTask = new LoginTask();
			loginTask.execute(IGCConstants.loginURL);

		} else {
			messageAuthenticating.setVisibility(View.VISIBLE);

			retryButton.setVisibility(View.VISIBLE);
			loginButton.setVisibility(View.GONE);
			loadingIconAuthenticationScreen.setVisibility(View.INVISIBLE);
			errorIconAuthenticationScreen.setVisibility(View.VISIBLE);

			messageAuthenticatingError.setVisibility(View.VISIBLE);
			messageAuthenticating.setText(R.string.network_connection_error);
			messageAuthenticatingError.setText(R.string.network_message);

		}
	}

	public void settingProfilePic(Uri uri, File imageFile) {

		int sizeRequired = IGCImageUtility.checkProfilePicSize(activity);
		Bitmap newBitmapSize = IGCImageUtility.getRoundedRectBitmap(
				IGCImageUtility.decodeFile(imageFile, sizeRequired,
						sizeRequired), sizeRequired / 2, sizeRequired,
				sizeRequired);

		Matrix matrix = new Matrix();
		matrix.postRotate(IGCImageUtility.getImageOrientation(imageFile));
		Bitmap rotatedBitmap = Bitmap.createBitmap(newBitmapSize, 0, 0,
				newBitmapSize.getWidth(), newBitmapSize.getHeight(), matrix,
				true);

		profilePicAuthenticating.setImageBitmap(rotatedBitmap);
		profilePicAuthenticating.setAdjustViewBounds(true);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		loadProfilePic();
	}

	@Override
	protected void onPause() {

		super.onPause();
		IGCafeApplication.mSensorManager
				.unregisterListener(IGCafeApplication.mShakeDetector);

	}

	private class LoginTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			loadingIconAuthenticationScreen.setVisibility(View.VISIBLE);

			errorIconAuthenticationScreen.setVisibility(View.INVISIBLE);
			retryButton.setVisibility(View.INVISIBLE);
			messageAuthenticating.setVisibility(View.VISIBLE);
			messageAuthenticating.setText(R.string.authenticating);
			messageAuthenticatingError.setVisibility(View.INVISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String loginStatus = null;
			try {
				String version = getPackageManager().getPackageInfo(
						getPackageName(), 0).versionName;

				String deviceType = IGCConstants.deviceType;
				loginStatus = ServiceHandler.loginService(
						getApplicationContext(), urls[0], IGCSharedPreferences
								.getUserName(getApplicationContext()),
						IGCSharedPreferences
								.getPassword(getApplicationContext()),
						deviceType, version);

			} catch (Exception e) {

			}

			return loginStatus;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result != null && result.length() > 0) {
				parseLoginStatus(result);
				if (stringStatus != null) {

					if (firstName != null && firstName.length() > 0
							&& id != null && id.length() > 0) {

						if (isUpdateAvailable) {
							showUpdateDialog();
						} else {
							saveAuthenticationCredentials();
						}

					} else {
						messageAuthenticating.setVisibility(View.INVISIBLE);
						retryButton.setVisibility(View.GONE);
						loadingIconAuthenticationScreen
								.setVisibility(View.GONE);
						loginButton.setVisibility(View.VISIBLE);
						messageAuthenticatingError.setVisibility(View.VISIBLE);
						messageAuthenticatingError.setText(stringMessage);
					}
				} else {
					messageAuthenticating.setVisibility(View.INVISIBLE);
					retryButton.setVisibility(View.VISIBLE);
					loadingIconAuthenticationScreen.setVisibility(View.GONE);
					loginButton.setVisibility(View.GONE);
					messageAuthenticatingError.setVisibility(View.VISIBLE);
					messageAuthenticatingError.setText(R.string.server_error);

				}

			} else {
				messageAuthenticating.setVisibility(View.INVISIBLE);
				retryButton.setVisibility(View.VISIBLE);
				loadingIconAuthenticationScreen.setVisibility(View.GONE);
				loginButton.setVisibility(View.GONE);
				messageAuthenticatingError.setVisibility(View.VISIBLE);
				messageAuthenticatingError.setText(R.string.server_error);

			}

		}

	}

	public void showUpdateDialog() {
		dialogBuilder = NiftyDialogBuilder.getInstance(this);
		dialogBuilder.setCancelable(false);
		dialogBuilder
				.setTitleViewGone(true)
				.withTitle("My Dialog")

				.withTitleColor("#FFFFFF")

				.withDividerColor("#11000000")

				.withMessage("This is a modal Dialog.")

				.setMessageViewGone(true)
				.withMessageColor("#FFFFFFFF")

				.withDialogColor(Color.TRANSPARENT)

				.isCancelableOnTouchOutside(false)

				.withDuration(300)

				.withEffect(Effectstype.Slidetop)
				.setCustomView(R.layout.delete_image_dialog,
						getApplicationContext()).show();

		TextView deleteMessage = (TextView) dialogBuilder
				.findViewById(R.id.deleteMessage);
		TextView titleMessage = (TextView) dialogBuilder
				.findViewById(R.id.titleMessage);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okDeletePhoto);
		TextView cancelDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.cancelDeletePhoto);
		titleMessage.setTypeface(myTypefaceMedium);
		deleteMessage.setTypeface(myTypefaceRegular);
		okDeletePhoto.setText("Update");
		titleMessage.setVisibility(View.VISIBLE);
		titleMessage.setText(updateTitle);
		deleteMessage.setText(updateMessage);
		cancelDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isMandatory) {
					dialogBuilder.dismiss();
					finish();
				} else {
					saveAuthenticationCredentials();
				}
				dialogBuilder.dismiss();
			}
		});
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (updateUrl != null && updateUrl.length() > 0) {
					Uri uri;
					if (updateUrl.contains("http")) {
						uri = Uri.parse(updateUrl);
					} else {
						updateUrl = "http://" + updateUrl;
						uri = Uri.parse(updateUrl);
					}

					Update installUpdate = new Update();
					installUpdate.execute(uri.toString());
				}
				dialogBuilder.dismiss();

			}
		});

	}

	private void saveAuthenticationCredentials() {
		Intent intent = new Intent(IGCAuthenticationActivity.this,
				IGCDashboardActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		overridePendingTransition(0, 0);
		intent.putExtra("type", type);
		startActivity(intent);
		finish();
	}

	ProgressBar updateProgress;

	private class Update extends AsyncTask<String, String, String> {

		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pDialog = new ProgressDialog(activity);

			pDialog.show();
			pDialog.setContentView(R.layout.progress_dialog_update);

			pDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

			pDialog.setCancelable(false);
			updateProgress = (ProgressBar) pDialog
					.findViewById(R.id.progressBarUpdate);
			updateProgress.setProgress(0);
			updateProgress.setMax(100);

		}

		@Override
		protected String doInBackground(String... urls) {
			String mediaFile = updateDownloadService(getApplicationContext(),
					urls[0].toString());
			return mediaFile;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
			}
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(Environment
					.getExternalStorageDirectory() + "/download/" + result)),
					"application/vnd.android.package-archive");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();

		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values[0]);

			updateProgress.setProgress(Integer.parseInt(values[0]));

		}

		public String updateDownloadService(Context context,
				String urlserviceUpdate) {
			String mediaFile = "igConverge.apk";
			try {

				String PATH = Environment.getExternalStorageDirectory()
						+ "/download/";
				File file = new File(PATH);
				file.mkdirs();

				File outputFile = new File(file, mediaFile);
				FileOutputStream fos = new FileOutputStream(outputFile);

				HttpGet m_httpGet = null;
				HttpResponse m_httpResponse = null;

				HttpClient m_httpClient = new DefaultHttpClient();
				String result = null;

				try {

					m_httpGet = new HttpGet(urlserviceUpdate);

					// Execute the html request
					m_httpResponse = m_httpClient.execute(m_httpGet);
					HttpEntity entity = m_httpResponse.getEntity();

					// See if we get a response
					if (entity != null) {

						InputStream instream = entity.getContent();
						long lengthOfFile = entity.getContentLength();
						byte[] buffer = new byte[1024];

						// Write out the file
						int len1 = 0;

						long total = 0;
						while ((len1 = instream.read(buffer)) != -1) {
							total += len1;
							publishProgress(""
									+ (int) ((total * 100) / lengthOfFile));

							fos.write(buffer, 0, len1);
						}
						fos.close();
						instream.close();// till here, it works fine - .apk is
						// download to my sdcard in download
						// file

					}

				} catch (ConnectTimeoutException cte) {
					Toast.makeText(getApplicationContext(), "Update error!",
							Toast.LENGTH_LONG).show();

				} catch (Exception e) {

				} finally {
					m_httpClient.getConnectionManager()
							.closeExpiredConnections();
				}

			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Update error!",
						Toast.LENGTH_LONG).show();
			}

			return mediaFile;
		}

	}

	private boolean parseLoginStatus(String result) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(result);

			if (jsonObject != null) {
				if (!jsonObject.isNull("status")) {
					stringStatus = jsonObject.getString("status");
				}
				if (!jsonObject.isNull("message")) {
					stringMessage = jsonObject.getString("message");
				}
				if (!jsonObject.isNull("updateResponse")) {

					JSONObject updateResponseJSONObject = jsonObject
							.getJSONObject("updateResponse");
					if (!updateResponseJSONObject.isNull("isUpdate")) {
						isUpdateAvailable = updateResponseJSONObject
								.getBoolean("isUpdate");
					}
					if (!updateResponseJSONObject.isNull("title")) {
						updateTitle = updateResponseJSONObject
								.getString("title");
					}
					if (!updateResponseJSONObject.isNull("message")) {
						updateMessage = updateResponseJSONObject
								.getString("message");
					}
					if (!updateResponseJSONObject.isNull("url")) {
						updateUrl = updateResponseJSONObject.getString("url");
					}
					if (isUpdateAvailable) {
						if (!updateResponseJSONObject.isNull("isMandatory")) {
							isMandatory = updateResponseJSONObject
									.getBoolean("isMandatory");
						}
					}
				}
				if (!jsonObject.isNull("aspireEmployee")) {

					JSONObject employeeJSONObject = jsonObject
							.getJSONObject("aspireEmployee");
					if (!employeeJSONObject.isNull("id")) {
						id = employeeJSONObject.getString("id");
					}
					if (!employeeJSONObject.isNull("firstName")) {
						firstName = employeeJSONObject.getString("firstName");
					}
					if (!employeeJSONObject.isNull("middleName")) {
						middleName = employeeJSONObject.getString("middleName");
					}
					if (!employeeJSONObject.isNull("lastName")) {
						lastName = employeeJSONObject.getString("lastName");
					}
					if (!employeeJSONObject.isNull("mailId")) {
						mailId = employeeJSONObject.getString("mailId");
					}
					if (!employeeJSONObject.isNull("imageName")) {
						imageName = employeeJSONObject.getString("imageName");
					}

					IGCConstants.employeeDetailObject = new IGCEmployeeDetails(
							id, username, password, firstName, middleName,
							lastName, mailId, imageName);
					return true;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		mGestureDetector.onTouchEvent(event);

		return super.dispatchTouchEvent(event);
	}

}