package com.info.igconverge.utility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class IGCSharedPreferences {

	public static String PREFS_NAME = "igfile";
	public static String PREFS_NAME_PROFILE = "igfile2";

	public static boolean PREF_INSTALL_FLAG;
	public static String PREF_USERNAME = "uNameS";
	public static String PREF_FIRSTNAME = "fNameS";
	public static String PREF_MIDDLENAME = "mNameS";
	public static String PREF_LASTNAME = "lNameS";
	public static String PREF_PASSWORD = "myPwdS";
	public static String PREF_EMAIL = "myEmail";
	public static String PREF_PROFILENAME = "pNameS";
	public static String PREF_USERID = "myUserId";
	public static String PREF_USERPROFILEIMAGEPATH = "myUserProfileImage";
	public static String PREF_DEVICEID = null;
	private static String PREF_ANNOUNCEMENT_ID = "announcementId";
	private static List<String> PREF_ANNOUNCEMENT_ID_LIST = new ArrayList<String>();

	

	public static void setUserName(Context context, String username) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_USERNAME, username);
		editor1.commit();
	}

	public static String getUserName(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_USERNAME, null);
	}

	public static void setPassword(Context context, String password) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_PASSWORD, password);
		editor1.commit();
	}

	public static String getPassword(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_PASSWORD, null);
	}

	public static void setProfileImage(Context context, String profileImgaeName) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_USERPROFILEIMAGEPATH, profileImgaeName);
		editor1.commit();
	}

	public static String getProfileImage(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_USERPROFILEIMAGEPATH, null);
	}

	public static void setFirstName(Context context, String firstName) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_FIRSTNAME, firstName);
		editor1.commit();
	}

	public static String getFirstName(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_FIRSTNAME, null);
	}

	public static void setMiddleName(Context context, String middleName) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_MIDDLENAME, middleName);
		editor1.commit();
	}

	public static String getMiddleName(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_MIDDLENAME, null);
	}

	public static void setLastName(Context context, String lastName) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_LASTNAME, lastName);
		editor1.commit();
	}

	public static String getLastName(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_LASTNAME, null);
	}

	public static void setEmail(Context context, String email) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();

		editor1.putString(PREF_EMAIL, email);

		editor1.commit();
	}

	public static String getEmail(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_EMAIL, null);
	}

	public static void setUserId(Context context, String userid) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_USERID, userid);
		editor1.commit();


	}

	public static String getUserId(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		String s = mPreferences.getString(PREF_USERID, null);
		return mPreferences.getString(PREF_USERID, null);
	}


	public static void setProfileName(Context context, String profileName) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_PROFILENAME, profileName);
		editor1.commit();
	}

	public static String getProfileName(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_PROFILENAME, null);
	}

	public static List<String> getPREF_ANNOUNCEMENT_ID_LIST(Context context) {
		SharedPreferences mPreferences = context.getSharedPreferences(
				PREFS_NAME_PROFILE, 0);
		Set<String> set = new HashSet<String>();
		set = mPreferences.getStringSet(PREF_ANNOUNCEMENT_ID, null);
	
		if (set != null) {
			Iterator its = set.iterator();
			for (String id : set) {
				PREF_ANNOUNCEMENT_ID_LIST.add(id);
			}
			return PREF_ANNOUNCEMENT_ID_LIST;
		} else
			return null;
	
	}

	public static void setPREF_ANNOUNCEMENT_ID_LIST(
			List<String> pREF_ANNOUNCEMENT_ID_LIST, Context context) {
		Set<String> set = new HashSet<String>();
		if (pREF_ANNOUNCEMENT_ID_LIST != null) {

			set.addAll(pREF_ANNOUNCEMENT_ID_LIST);
		} else
			set = null;
		SharedPreferences mPreferences = context.getSharedPreferences(
				PREFS_NAME_PROFILE, 0);
		SharedPreferences.Editor editor1 = mPreferences.edit();
		editor1.putStringSet(PREF_ANNOUNCEMENT_ID, set);

		editor1.commit();

	}

	public static boolean isPREF_INSTALL_FLAG(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		
		return mPreferences.getBoolean("GOOGLE_SEARCH_TYPE", false);

	}

	public static void setPREF_INSTALL_FLAG(Context context,
			boolean pREF_INSTALL_FLAG) {
		PREF_INSTALL_FLAG = pREF_INSTALL_FLAG;

		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME_PROFILE, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putBoolean("GOOGLE_SEARCH_TYPE", PREF_INSTALL_FLAG);
		editor1.commit();
	}

	public static String getPREF_DEVICEID(Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		return mPreferences.getString(PREF_DEVICEID, null);
	}

	public static void setPREF_DEVICEID(String pREF_DEVICEID, Context context) {
		SharedPreferences mPreferences = context.getApplicationContext()
				.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor1 = mPreferences.edit();
		editor1.putString(PREF_DEVICEID, pREF_DEVICEID);
		editor1.commit();
	}

}
