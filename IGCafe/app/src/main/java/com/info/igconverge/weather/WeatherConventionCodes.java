package com.info.igconverge.weather;

import java.util.HashMap;
import java.util.Map;

public class WeatherConventionCodes {
	public static enum WeatherCodes {
		tornado(1), tropical_storm(2), hurricane(3), severe_thunderstorms(4), thunderstorms(
				5), mixed_rain_snow(6), mixed_rain_sleet(7), mixed_snow_sleet(8), freezing_drizzle(
				9), drizzle(10), freezing_rain(11), showers(12), snow_flurries(
				13), light_snow_showers(14), blowing_snow(15), snow(16), hail(
				17), sleet(18), dust(19), foggy(20), haze(21), smoky(22), blustery(
				23), windy(24), cold(25), cloudy(26), mostly_cloudy_night(27), mostly_cloudy_day(
				28), partly_cloudy_night(29),partly_cloudy_day(30),
				  clear_night(31), sunny(32), fair_night(33), fair_day(34), mixed_rain_hail(
				35), hot(36), isolated_thunderstorms(37), scattered_thunderstorms(
				38),scattered_thunderstorms_dup(39),
				  scattered_showers(40), heavy_snow(41),scattered_snow_showers(42),heavy_snow_dup(43),
				  partly_cloudy(44), thundershowers(
				45), snow_showers(46), isolated_thundershowers(47), not_available(
				3200);

		private int value;
		private static Map<Integer, WeatherCodes> map = new HashMap<Integer, WeatherCodes>();

		static {
			for (WeatherCodes weatherEnum : WeatherCodes.values()) {
				map.put(weatherEnum.value, weatherEnum);
			}
		}

		private WeatherCodes(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		public static WeatherCodes valueOf(int weatherCode) {
			return map.get(weatherCode);
		}
	}

}