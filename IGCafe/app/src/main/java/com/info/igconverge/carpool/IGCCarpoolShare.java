package com.info.igconverge.carpool;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.carpoolNotification.IGCCarPoolNotificationsFragment;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;

import java.util.ArrayList;
import java.util.List;


public class IGCCarpoolShare extends Fragment implements FragmentCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SwipeRefreshLayout swipeRefreshLayout;
    IGCDashboardActivity activity;
    TextView emptyText, statusText;
    ArrayList<ListModel> list, list2;
    ArrayList<CarpoolClass> igRideList;
    ListView listView;
    FragmentManager fragmentManager;
    Fragment bookRideDetail, myRideDetail;


    FragmentCallback frag;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EntryAdapter adapter = null;
    ArrayList items = null;

    public IGCCarpoolShare() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
        frag = this;
    }

    // TODO: Rename and change types and number of parameters
    public static IGCCarpoolShare newInstance(String param1, String param2) {
        IGCCarpoolShare fragment = new IGCCarpoolShare();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActionBar();

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.carpool_ride_plus, menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActionBar();
    }

    private void initializeActionBar() {
        // TODO Auto-generated method stub
        activity.actionBar.setDisplayHomeAsUpEnabled(true);
        activity.actionBar.setHomeButtonEnabled(true);
        activity.actionBar.setDisplayShowHomeEnabled(true);
        activity.actionBar.setTitle(IGCConstants.CarPoolFragment);
        activity.unlockNavigationDrawerSwipeGesture();
        activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.ride_notify:

                fragment = new IGCCarPoolNotificationsFragment();
                if (fragment != null) {
                    fragmentManager = (activity).getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                    if ((activity.mDrawerToggle) != null)
                        activity.mDrawerToggle.setDrawerIndicatorEnabled(false);
                }
                break;
            case R.id.ride_plus:
                fragment = new IGCCarPost();
                if (fragment != null) {
                    fragmentManager = (activity).getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                    if ((activity.mDrawerToggle) != null)
                        activity.mDrawerToggle.setDrawerIndicatorEnabled(false);
                }
                break;
            //Toast.makeText(activity, "Selected", Toast.LENGTH_SHORT).show();


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_igccarpool_share, container, false);
        list = new ArrayList<ListModel>();
        list2 = new ArrayList<ListModel>();
        igRideList = new ArrayList<CarpoolClass>();
        activity.actionBar.setDisplayHomeAsUpEnabled(true);
        activity.actionBar.setHomeButtonEnabled(true);
        activity.actionBar.setDisplayShowHomeEnabled(true);
        activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
                R.drawable.header_bg));
        emptyText = (TextView) view.findViewById(R.id.emptyText);
        statusText = (TextView) view.findViewById(R.id.status);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh_shareRide);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if(!swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(true);
                }

            }
        });
        listView = (ListView) swipeRefreshLayout.findViewById(R.id.list);
        items = new ArrayList();
        adapter = new EntryAdapter(getContext(), items);
        checkNetworkStatus();
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_orange_dark, android.R.color.holo_green_dark, android.R.color.darker_gray,
                android.R.color.holo_orange_light
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                items.clear();
                checkNetworkStatus();

            }
        });
        adapter.notifyDataSetChanged();
        listView.setAdapter(null);
        emptyText.setVisibility(View.GONE);

        return view;
    }


    private ArrayList<ListModel> getDataset() {

        ArrayList result = new ArrayList<ListModel>();
        for (int i = 0; i < igRideList.size(); i++) {
            //if((IGCSharedPreferences.getUserId(getContext())).equalsIgnoreCase(igRideList.get(i).getUser().id))
            if (igRideList.get(i).getUser() != null) {
                if (igRideList.get(i).getUser().id.equalsIgnoreCase(IGCSharedPreferences.getUserId(getContext())) && igRideList.get(i).isStatus()) {
                    for (int j = 0; j < igRideList.get(i).getDays().size(); j++)
                        System.out.println("%%%%%%%%%%%%%% " + i + igRideList.get(i).getDays().get(j));
                    ListModel d = new ListModel(true, igRideList.get(i).getDepTime(), igRideList.get(i).user.name, igRideList.get(i).fare, igRideList.get(i).getCarNo(), igRideList.get(i).getStartLocation(), igRideList.get(i).getPostId(), "Active", igRideList.get(i).getDays());
                    result.add(d);
                }
            }
        }
        return result;
        //  return null;

    }

    private ArrayList<ListModel> getBook() {

        ArrayList result = new ArrayList<ListModel>();
        for (int i = 0; i < igRideList.size(); i++) {
            if (igRideList.get(i).getOccupantsList().size() > 0 && igRideList.get(i).isStatus()) {
                for (int j = 0; j < igRideList.get(i).getOccupantsList().size(); j++) {


                    if (igRideList.get(i).getOccupantsList().get(j).getUserId().equalsIgnoreCase(IGCSharedPreferences.getUserId(getContext()))) {

                        String status = igRideList.get(i).occupantsList.get(j).getStatus().substring(0, 1).toUpperCase() + igRideList.get(i).occupantsList.get(j).getStatus().substring(1);

                        ListModel d = new ListModel(false, igRideList.get(i).getDepTime(), igRideList.get(i).user.name, igRideList.get(i).fare, igRideList.get(i).getCarNo(), igRideList.get(i).getStartLocation(), igRideList.get(i).getPostId(), status, igRideList.get(i).getDays());
                        result.add(d);
                    }
                }
            }

        }
        return result;
        //return null;

    }

    private void checkNetworkStatus() {
        if(adapter!=null)
        {
            adapter.notifyDataSetChanged();
        }
        if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
            setHasOptionsMenu(true);

            // params = getParams();
            IGCAsyncServiceRequest getRequest = new IGCAsyncServiceRequest(getActivity(), "GET", frag);
            getRequest.execute(IGCConstants.fetchRidesURL);

        } else {
            emptyText.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
            listView.setAdapter(null);
            setSwipeRefreshingFalse();
            ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
            dialog.setCancelable(false);
            dialog.show();
        }
    }


    @Override
    public void fragMultipleResponse(String response, String extra) {
        list.clear();
        list2.clear();
        items.clear();
        if (response != null && response.length() > 0) {
            try {

                if (response != null) {

                    fetchRides(response);

                    setSwipeRefreshingFalse();
                }
            } catch (Exception e) {
                e.printStackTrace();

                setSwipeRefreshingFalse();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

            emptyText.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
            listView.setAdapter(null);
           setSwipeRefreshingFalse();
        }
    }

    private void setSwipeRefreshingFalse()
    {
        if(swipeRefreshLayout!=null)
        {
            if(swipeRefreshLayout.isRefreshing())
            {

                swipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private void fetchRides(String response) {
        list.clear();
        list2.clear();
        items.clear();
        RideParser rideParser = new RideParser();
        igRideList.clear();
        igRideList = rideParser.parseRideData(response);
        IGCafeApplication.setIgcRideList(igRideList);
        listView.setAdapter(null);
        if (igRideList.size() > 0) {
            emptyText.setVisibility(View.GONE);


            list = getDataset();
            list2 = getBook();

            if (list != null || list2 != null) {
                if (list.size() == 0 && list2.size() == 0) {
                    emptyText.setVisibility(View.VISIBLE);
                    adapter.notifyDataSetChanged();
                    listView.setAdapter(null);
                } else if (list.size() != 0 && list2.size() == 0) {
                    emptyText.setVisibility(View.GONE);
                    items.add(new HeaderModel("My Shared Rides"));
                    for (int i = 0; i < list.size(); i++) {
                        items.add(new ListModel(true, list.get(i).getTime(), list.get(i).getName(), list.get(i).getCost(), list.get(i).getCarNo(), list.get(i).Location, list.get(i).getPostId(), list.get(i).getStatus(), list.get(i).getDays()));
                    }
                    adapter.notifyDataSetChanged();
                    listView.setAdapter(adapter);
                    // statusText.setVisibility(View.VISIBLE);
                } else if (list2.size() != 0 && list.size() == 0) {
                    emptyText.setVisibility(View.GONE);

                    items.add(new HeaderModel("My Bookings "));
                    for (int i = 0; i < list2.size(); i++) {
                        items.add(new ListModel(false, list2.get(i).getTime(), list2.get(i).getName(), list2.get(i).getCost(), list2.get(i).getCarNo(), list2.get(i).Location, list2.get(i).getPostId(), list2.get(i).getStatus(), list2.get(i).getDays()));
                    }
                    adapter.notifyDataSetChanged();
                    listView.setAdapter(adapter);


                    //statusText.setVisibility(View.VISIBLE);
                } else {
                    emptyText.setVisibility(View.GONE);

                    items.add(new HeaderModel("My Shared Rides"));
                    for (int i = 0; i < list.size(); i++) {
                        items.add(new ListModel(true, list.get(i).getTime(), list.get(i).getName(), list.get(i).getCost(), list.get(i).getCarNo(), list.get(i).Location, list.get(i).getPostId(), list.get(i).getStatus(), list.get(i).getDays()));
                    }

                    items.add(new HeaderModel("My Bookings "));

                    for (int i = 0; i < list2.size(); i++) {
                        items.add(new ListModel(false, list2.get(i).getTime(), list2.get(i).getName(), list2.get(i).getCost(), list2.get(i).getCarNo(), list2.get(i).Location, list2.get(i).getPostId(), list2.get(i).getStatus(), list2.get(i).getDays()));
                    }

                    adapter.notifyDataSetChanged();
                    listView.setAdapter(adapter);
                }
            } else {
                emptyText.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                listView.setAdapter(null);
            }

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long duration) {
                    ListModel val = (ListModel) items.get(position);
                    ListModel item = (ListModel) items.get(position);
                    Bundle bundle = new Bundle();
                    ArrayList<OccupantsClass> occupantList = new ArrayList<OccupantsClass>();


                    for (int i = 0; i < igRideList.size(); i++) {
                        if (((ListModel) items.get(position)).getPostId().equalsIgnoreCase(igRideList.get(i).getPostId())) {

                            bundle.putString("postid", igRideList.get(i).getPostId());
                            bundle.putString("carNo", igRideList.get(i).getCarNo());
                            bundle.putInt("fare", igRideList.get(i).getFare());
                            bundle.putString("location", igRideList.get(i).getStartLocation());
                            bundle.putString("time", igRideList.get(i).getDepTime());
                            bundle.putInt("seats", igRideList.get(i).getNoOfSeats());
                            bundle.putString("comments", igRideList.get(i).getComments());
                            bundle.putString("phoneNo", igRideList.get(i).getUser().getPhoneNo());
                            bundle.putStringArrayList("Days", igRideList.get(i).getDays());
                            bundle.putString("email", igRideList.get(i).getUser().getEmail());

                            bundle.putString("phoneNumber", igRideList.get(i).getUser().getPhoneNo());

                            bundle.putString("driver", igRideList.get(i).getUser().getName());
                            for (int j = 0; j < igRideList.get(i).getOccupantsList().size(); j++) {
                                occupantList.add(igRideList.get(i).getOccupantsList().get(j));

                            }
                            bundle.putParcelableArrayList("occupantsList", occupantList);


                            break;

                        }
                    }

                    if (val.value) {
                        myRideDetail = new IGCMyride_detail_fragment();
                        myRideDetail.setArguments(bundle);
                        //Toast.makeText(getContext(), "You clicked " + ((ListModel) items.get(position)).getPostId(), Toast.LENGTH_SHORT).show();
                        FragmentTransaction ft = (activity).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_container, myRideDetail).addToBackStack(null);
                        ft.commit();
                        if ((activity.mDrawerToggle) != null)
                            activity.mDrawerToggle.setDrawerIndicatorEnabled(false);

                    } else {
                        bookRideDetail = new IGCBookedride_detail_fragment();
                        bookRideDetail.setArguments(bundle);
                        //Toast.makeText(getContext(), "You clicked " +((ListModel) items.get(position)).getPostId(), Toast.LENGTH_SHORT).show();
                        FragmentTransaction ft = (activity).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame_container, bookRideDetail).addToBackStack(null);
                        ft.commit();
                        if ((activity.mDrawerToggle) != null)
                            activity.mDrawerToggle.setDrawerIndicatorEnabled(false);

                    }

                }
            });
        } else {
            emptyText.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
            listView.setAdapter(null);
        }

    }
}


