package com.info.igconverge.announcements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.model.IGCAnnouncement;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;

public class IGCAnnouncementRecyclerAdapter extends
		RecyclerView.Adapter<IGCAnnouncementViewHolder> {

	private List<IGCAnnouncement> mDataSet = new ArrayList<IGCAnnouncement>();
	IGCDashboardActivity activity;
	List<String> notificationIdList = new ArrayList<String>();
	int count;

	IGCAnnouncementRecyclerAdapter(List<IGCAnnouncement> mDataSet,
			IGCDashboardActivity activity) {
		this.mDataSet = mDataSet;
		this.activity = activity;
		count = IGCConstants.displayCount;
		
	}

	@Override
	public IGCAnnouncementViewHolder onCreateViewHolder(ViewGroup parent,
			int position) {
		View v = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.announcements_list_row, null);
		IGCAnnouncementViewHolder mh = new IGCAnnouncementViewHolder(v,
				activity);
		return mh;
	}

	@Override
	public void onBindViewHolder(IGCAnnouncementViewHolder holder, int position) {
		IGCAnnouncement feedItem = mDataSet.get(position);
		int flag=0;


		String titleCircle=feedItem.getNotificationTitle();
		for(int i=0;i<=titleCircle.length();i++)
		{
			char character = titleCircle.charAt(i);
			int ascii = (int) character; 
			if((ascii>=65 && ascii<=90 )|| (ascii>=97 && ascii<=122))
			{
				flag=1;
				holder.headerNotifyCircle.setText(String.valueOf(character));
				break;
			}
		}
		if(flag==0)
		{
			holder.headerNotifyCircle.setText("I");
		}
		Date date;
		try {
			String notificationId;
			notificationId = feedItem.getNotificationId();
			notificationIdList = IGCSharedPreferences
					.getPREF_ANNOUNCEMENT_ID_LIST(activity);
			holder.idView.setText(notificationId);
			holder.headingNotify1.setText(feedItem.getNotificationTitle());
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
			date = format.parse(feedItem.getNotificationDate());
			
			String agoDate = DateUtils.getRelativeTimeSpanString(
					date.getTime(), System.currentTimeMillis(),
					DateUtils.SECOND_IN_MILLIS).toString();

			holder.headingNotify2.setText(agoDate);
			holder.headingNotify3.setText(feedItem.getNotificationMessage());
			holder.newTextView.setVisibility(View.INVISIBLE);

			
			if (position < count) {
				holder.newTextView.setVisibility(View.VISIBLE);
				
			}

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public int getItemCount() {
		if (mDataSet != null) {
			return mDataSet.size();

		} else
			return 0;
	}

	public void remove(int position) {
		if (mDataSet.size() > position) {
			IGCAnnouncement feedItem = mDataSet.get(position);
			String notificationId = feedItem.getNotificationId();
			if (notificationIdList != null) {
				if (!notificationIdList.contains(notificationId))
					notificationIdList.add(notificationId);

			} else {
				notificationIdList = new ArrayList<String>();
				notificationIdList.add(notificationId);
			}
			count = count - 1;
			IGCConstants.displayCount = count;
		
			IGCSharedPreferences.setPREF_ANNOUNCEMENT_ID_LIST(
					notificationIdList, activity);
			mDataSet.remove(position);
			notifyItemRemoved(position);

		} else {
	
		}
	}

	public void add(int position) {
		if (mDataSet.size() > position) {
			IGCAnnouncement feedItem = mDataSet.get(position);
			String notificationId = feedItem.getNotificationId();
			if (notificationIdList != null) {
				if (notificationIdList.contains(notificationId))
					notificationIdList.remove(notificationId);
			}
			IGCSharedPreferences.setPREF_ANNOUNCEMENT_ID_LIST(
					notificationIdList, activity);
		}
	}
}