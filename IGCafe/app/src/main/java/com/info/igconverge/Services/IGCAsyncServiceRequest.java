package com.info.igconverge.Services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.json.JSONObject;

import com.info.igconverge.R;
import com.info.igconverge.announcements.IGCAnnouncementFragment;
import com.info.igconverge.carpool.IGCCarpoolFindrideFragment;
import com.info.igconverge.carpool.IGCCarpoolShare;
import com.info.igconverge.carpoolNotification.IGCCarPoolNotificationsFragment;
import com.info.igconverge.dashboard.IGCBellRingingActivity;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.events.IGCEventsFragment;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.KeyEvent;

public class IGCAsyncServiceRequest extends AsyncTask<String, Integer, String> {

	OnAsyncRequestComplete activityCaller;
	// Context context;
	Activity activity;
	String method = "GET";
	List<NameValuePair> parameters = null;
	MultipartEntity reqEntity = null;
	FragmentCallback fragmentCaller;
	// ProgressDialog pDialog = null;
	ProgressCustomDialog pDialog = null;
	JSONObject jsonObj;
	String extra;
	AsyncTask<String, Integer, String> task;
	String key=null,value=null;

	public IGCAsyncServiceRequest(Activity activity, String m, List<NameValuePair> p, FragmentCallback fragmentCallback) {
		this.activity = activity;
		method = m;
		parameters = p;
		this.fragmentCaller = fragmentCallback;
	}
	public IGCAsyncServiceRequest(Activity activity, String m, JSONObject jsonObj, FragmentCallback fragmentCallback) {
		this.activity = activity;
		method = m;
		this.jsonObj=jsonObj;
		this.fragmentCaller = fragmentCallback;
	}
	public IGCAsyncServiceRequest(Activity activity, String m, JSONObject jsonObj, FragmentCallback fragmentCallback,String  extra) {
		this.activity = activity;
		method = m;
		this.jsonObj=jsonObj;
		this.extra=extra;
		this.fragmentCaller = fragmentCallback;
	}
	public IGCAsyncServiceRequest(Activity activity, String m, String key,String value, FragmentCallback fragmentCallback,String extra) {
		this.activity = activity;
		method = m;
		this.key=key;
		this.value=value;
this.extra=extra;
		this.fragmentCaller = fragmentCallback;
	}
	public IGCAsyncServiceRequest(Activity activity, String m, List<NameValuePair> p, FragmentCallback fragmentCallback, String extra) {
		// this.context = context;
		this.activity = activity;
		method = m;
		parameters = p;
		this.extra = extra;
		this.fragmentCaller = fragmentCallback;
	}

	public IGCAsyncServiceRequest(Activity activity, String m, FragmentCallback fragmentCallback) {
		// caller = (OnAsyncRequestComplete) a;
		method = m;
		this.activity = activity;
		this.fragmentCaller = fragmentCallback;
	}

	// Three Constructors
	public IGCAsyncServiceRequest(Activity activity, String m,
			List<NameValuePair> p) {
		activityCaller = (OnAsyncRequestComplete) activity;
		this.activity = activity;
		method = m;
		parameters = p;
	}

	public IGCAsyncServiceRequest(Activity activity, String m,
			MultipartEntity reqEntity) {
		activityCaller = (OnAsyncRequestComplete) activity;
		this.activity = activity;
		method = m;
		this.reqEntity = reqEntity;
	}

	public IGCAsyncServiceRequest(Activity activity, String m) {
		activityCaller = (OnAsyncRequestComplete) activity;
		this.activity = activity;
		method = m;
	}

	public IGCAsyncServiceRequest(Activity activity) {
		activityCaller = (OnAsyncRequestComplete) activity;
		this.activity = activity;
	}

	// Interface to be implemented by calling activity
	public interface OnAsyncRequestComplete {
		public void asyncResponse(String response);
	}

	public String doInBackground(String... urls) {
		// get url pointing to entry point of API
		String address = urls[0].toString();
		try {
			if (method == "POST") {
				if (jsonObj != null) {
					return makePostJSONRequest(address, jsonObj);
				}
				else if(key!=null && value!=null  ){

					return makePostKeyvaluResponse(address,key,value);
				}
					else
				 {
					return post(address);
				}
			}

			if (method == "GET") {
				return get(address);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void onPreExecute() {

		if (!(fragmentCaller instanceof IGCEventsFragment)
				&& !(fragmentCaller instanceof IGCAnnouncementFragment) && !(fragmentCaller instanceof IGCCarPoolNotificationsFragment) &&
				!(fragmentCaller instanceof IGCCarpoolFindrideFragment) && !(fragmentCaller instanceof IGCCarpoolShare)) {

		
			pDialog = new ProgressCustomDialog(activity);
			
			task = this;

			pDialog.setOnKeyListener(new Dialog.OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface arg0, int keyCode,
						KeyEvent event) {

					// TODO Auto-generated method stub
					if (keyCode == KeyEvent.KEYCODE_BACK) {
				
						if (activity instanceof IGCDashboardActivity) {
							activity.onBackPressed();
							
							task.cancel(true);
							if (pDialog != null && pDialog.isShowing()) {
								pDialog.dismiss();
							}

						}
						

					}
					return true;
				}
			});
			if(!(activity instanceof IGCBellRingingActivity))
			{
				pDialog.show();
			}
		
		}

	}

	public void onProgressUpdate(Integer... progress) {
		
	}

	public void onPostExecute(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}
		if (activityCaller != null)
			activityCaller.asyncResponse(response);
		else {
			
			fragmentCaller.fragMultipleResponse(response, extra);
		}

	}

	@SuppressWarnings("deprecation")
	private String get(String address) {
		try {

			if (parameters != null) {
				String query = "";
				String EQ = "=";
				String AMP = "&";
				for (NameValuePair param : parameters) {
					query += param.getName() + EQ
							+ URLEncoder.encode(param.getValue()) + AMP;
				}

				if (query != "") {
					address += "?" + query;
				}
			}

			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(address);

			HttpResponse response = client.execute(get);
			return stringifyResponse(response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

		return null;
	}

	private String post(String address) {
		try {

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(address);
			client.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
			client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
					30000);
			if (parameters != null) {
				post.setEntity(new UrlEncodedFormEntity(parameters));
				
			}
			if (reqEntity != null) {
				post.setEntity(reqEntity);
			
			}
			HttpResponse response = client.execute(post);

			Header[] headers = response.getAllHeaders();
			if(headers!=null) {
				for (Header header : headers) {
					if (header.getName().equalsIgnoreCase("Date")) {
						SimpleDateFormat formatter = new SimpleDateFormat(
								"EEE, dd MMM yyyy HH:mm:ss 'GMT'");
						IGCConstants.serverDate = formatter
								.parse(header.getValue());
						System.err.println(IGCConstants.serverDate);
					}

				}
			}
			
			return stringifyResponse(response);

		} catch (Exception bug) {
			bug.printStackTrace();
			if (pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
			}
			return "";

		}
	}
	private String makePostJSONRequest(String path, JSONObject jsonObject) throws Exception
	{
		try {

				HttpResponse response;
				//instantiates httpclient to make request
				DefaultHttpClient httpclient = new DefaultHttpClient();

				//url with the post data
				HttpPost httpost = new HttpPost(path);

				//convert parameters into JSON object
				JSONObject holder = jsonObject;

				//passes the results to a string builder/entity
				StringEntity se = new StringEntity(holder.toString());

				//sets the post request as the resulting string
				httpost.setEntity(se);
				//sets a request header so the page receving the request
				//will know what to do with it
				httpost.setHeader("Accept", "application/json");
				httpost.setHeader("Content-type", "application/json");

				response = httpclient.execute(httpost);

				return stringifyResponse(response);


		}
		catch (Exception e)
		{
			e.printStackTrace();
			if (pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
			}
			return "";
		}

	}

	private String makePostKeyvaluResponse(String path, final String key, final String value)
	{

		try {

			HttpResponse response;
			//instantiates httpclient to make request
			DefaultHttpClient httpclient = new DefaultHttpClient();

			//url with the post data
			HttpPost httpost = new HttpPost(path);

			//convert parameters into JSON object


			//passes the results to a string builder/entity

			//sets a request header so the page receving the request
			//will know what to do with it

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(key, value));

			if (params != null) {
				httpost.setEntity(new UrlEncodedFormEntity(params));

			}
			response = httpclient.execute(httpost);

			return stringifyResponse(response);


		}
		catch (Exception e)
		{
			e.printStackTrace();
			if (pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
			}
			return "";
		}

	}


	private String stringifyResponse(HttpResponse response) {
		BufferedReader in;
		try {
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			return sb.toString();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}