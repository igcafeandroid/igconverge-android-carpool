package com.info.igconverge.partyradar;

public class FeedItem {
    private String title;
    private String thumbnail;

    public FeedItem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FeedItem(String title, String thumbnail) {
		super();
		this.title = title;
		this.thumbnail = thumbnail;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}