
package com.info.igconverge.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.info.igconverge.model.IGCEventItem;
import com.info.igconverge.model.IGCRatingMenu;
import com.info.igconverge.model.IGCShopDetails;


public class RatingParser {
	ArrayList<IGCRatingMenu> list = new ArrayList<IGCRatingMenu>();

	public RatingParser() {

	}

	public ArrayList<IGCRatingMenu> parseFetchedRating(String jsonData) {
		
		if (jsonData!=null && jsonData.length()>0 )
			try {

				JSONArray rootArray = new JSONArray(jsonData);
				for (int index = 0; index < rootArray.length(); index++) {
					JSONObject eventObject = rootArray.getJSONObject(index);
					IGCRatingMenu igcRating = new IGCRatingMenu();
					igcRating.setMenuDate(eventObject.getString("_id"));
					igcRating.setMenuFeedback(eventObject
							.getString("averageFeedback"));

					list.add(igcRating);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return list;
			}
		return list;
	}
}
