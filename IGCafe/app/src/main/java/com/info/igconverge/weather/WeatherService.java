package com.info.igconverge.weather;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.info.igconverge.R;
import com.info.igconverge.model.IGCWeatherDetails;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.weatherlibrary.WeatherInfo;
import com.info.igconverge.weatherlibrary.YahooWeather;
import com.info.igconverge.weatherlibrary.YahooWeather.SEARCH_MODE;
import com.info.igconverge.weatherlibrary.YahooWeather.UNIT;
import com.info.igconverge.weatherlibrary.YahooWeatherExceptionListener;
import com.info.igconverge.weatherlibrary.YahooWeatherInfoListener;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class WeatherService extends IntentService implements
		YahooWeatherInfoListener, YahooWeatherExceptionListener,
		WeatherConditions {

	public WeatherService() {
		super(WeatherService.class.getName());
		// TODO Auto-generated constructor stub
	}

	boolean isNoidaLocation = false;
	public static IGCWeatherDetails weather = new IGCWeatherDetails();
	private YahooWeather mYahooWeather = YahooWeather.getInstance(5000, 5000,
			true);
	String currentCity;
	String currentDate, currentTemp, climateCondition;
	String weatherImageText;
	WeatherInfo mWeatherInfo;
	List<String> weatherImgList = new ArrayList<String>();
	String tempLowValue;
	String tempHighValue;
	List<String> weekTempList = new ArrayList<String>();

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		mYahooWeather.setExceptionListener(this);
		searchByGPS(getApplicationContext());
	}

	private void speedExceedMessageToActivity() {

	
		Intent intent = new Intent("weatherFinder");
		sendLocationBroadcast(intent);
	}

	private void sendLocationBroadcast(Intent intent) {
		
		Bundle bundle = new Bundle();
		bundle.putBoolean("isNoidaLocation", isNoidaLocation);
		bundle.putParcelable("weather", weather);
		intent.putExtra("weather", bundle);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	public void searchByGPS(Context context) {
	
		mYahooWeather.setNeedDownloadIcons(true);
		mYahooWeather.setUnit(UNIT.CELSIUS);
		mYahooWeather.setSearchMode(SEARCH_MODE.GPS);

		String[] noidaLatLong = getResources().getStringArray(
				R.array.noida_lat_long);

		if (!mYahooWeather.queryYahooWeatherByGPS(context, this)) {
			
			mYahooWeather.queryYahooWeatherByLatLon(getApplicationContext(),
					noidaLatLong[0], noidaLatLong[1], this);
			isNoidaLocation = true;
		}

	}

	// TODO Auto-generated method stub

	@Override
	public void gotWeatherInfo(WeatherInfo weatherInfo) {
		try {
		
			if (weatherInfo != null) {

				StringBuilder address = new StringBuilder();
				
				if (weatherInfo.getWOEIDCounty() != null)
					address.append(weatherInfo.getWOEIDCounty() + ", ");

				if (weatherInfo.getWOEIDState() != null)
					address.append(weatherInfo.getWOEIDState() + ", ");

				if (weatherInfo.getWOEIDCountry() != null)
					address.append(weatherInfo.getWOEIDCountry());
				currentCity = address.toString();
			
				
				setDayOfWeek();
				currentDate = weatherInfo.getCurrentConditionDate();
			
				currentTemp = weatherInfo.getCurrentTemp() + "ºC";
				;
			
				int tempLow = weatherInfo.getForecastInfoList().get(0)
						.getForecastTempLow();
				tempLowValue = tempLow + "ºC";
				
				tempHighValue = weatherInfo.getForecastInfoList().get(0)
						.getForecastTempHigh()
						+ "ºC";
				
				climateCondition = weatherInfo.getCurrentText();
			
				for (int i = 0; i < 5; i++) {
					weekTempList.add(weatherInfo.getForecastInfoList().get(i)
							.getForecastTempLow()
							+ "ºC"
							+ "/"
							+ weatherInfo.getForecastInfoList().get(i)
									.getForecastTempHigh() + "ºC");

				}
			
				weatherImageText = getWeatherCondition(weatherInfo
						.getCurrentCode());
			
				mWeatherInfo = weatherInfo;
			
				forecastWeatherImages(weatherInfo);
				weather = new IGCWeatherDetails(currentDate, currentCity,
						currentTemp, climateCondition, weatherImageText,
						tempLowValue, tempHighValue, weatherImgList,
						weekTempList);
				speedExceedMessageToActivity();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void forecastWeatherImages(WeatherInfo weatherInfo) {
		try {
			
			for (int i = 0; i < 5; i++) {
				
				weatherImgList.add(getWeatherCondition(weatherInfo
						.getForecastInfoList().get(i).getForecastCode()));
			
			}
			;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFailConnection(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailParsing(Exception e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailFindLocation(Exception e) {
		// TODO Auto-generated method stub
	

	}

	
	public String getWeatherCondition(int weatherVal) {
		
		char customText;
		switch (WeatherConventionCodes.WeatherCodes.valueOf(weatherVal)) {
		case tornado:
		case hurricane: {
			// set tornado/hurricane text
			customText = WeatherConstants.ClimaconTornado;
			break;
		}

		case tropical_storm:
		case severe_thunderstorms:
		case thunderstorms:
		case isolated_thunderstorms:
		case scattered_thunderstorms:
		case scattered_thunderstorms_dup:
		case thundershowers:
		case isolated_thundershowers: {
			// set thunderstorms text
			customText = WeatherConstants.ClimaconLightning;
			break;
		}

		case mixed_rain_snow:
		case snow:
		case blowing_snow:
		case heavy_snow:
		case heavy_snow_dup:
		case snow_showers: {
			// set mixed_rain_snow text
			customText = WeatherConstants.ClimaconSnow;
			break;
		}

		case mixed_rain_sleet:
		case mixed_snow_sleet:
		case sleet: {
			// set sleet text
			customText = WeatherConstants.ClimaconSleet;
			break;
		}

		case freezing_drizzle:
		case drizzle: {
			// set drizzle text
			customText = WeatherConstants.ClimaconDrizzle;
			break;
		}

		case freezing_rain: {
			// set rain text
			customText = WeatherConstants.ClimaconRain;
			break;
		}

		case showers:
		case light_snow_showers:
		case scattered_snow_showers:
		case scattered_showers: {
			// set showers text
			customText = WeatherConstants.ClimaconRainAlt;
			break;
		}

		case snow_flurries: {
			// set snow_flurries text
			customText = WeatherConstants.ClimaconFlurries;
			break;
		}

		case hail:
		case mixed_rain_hail: {
			// set hail text
			customText = WeatherConstants.ClimaconHail;
			break;
		}

		case dust:
		case windy:
		case blustery: {
			// set dust text
			customText = WeatherConstants.ClimaconWind;
			break;
		}

		case foggy:
		case smoky: {
			// set foggy text
			customText = WeatherConstants.ClimaconFog;
			break;
		}

		case haze: {
			// set haze text
			customText = WeatherConstants.ClimaconHaze;
			break;
		}

		case cold: {
			// set cold text
			customText = WeatherConstants.ClimaconThermometerLow;
			break;
		}

		case cloudy:
		case mostly_cloudy_day:
		case partly_cloudy_day: {
			// set cloudy text
			customText = WeatherConstants.ClimaconCloud;
			break;
		}

		case hot: {
			// set hot text
			customText = WeatherConstants.ClimaconThermometerHigh;
			break;
		}
		case partly_cloudy_night:
		case mostly_cloudy_night: {
			// set mostly_cloudy_night text
			customText = WeatherConstants.ClimaconCloudMoon;
			break;
		}

		case clear_night:
		case fair_night: {
			// set clear_night text
			customText = WeatherConstants.ClimaconMoon;
			break;
		}

		case sunny:
		case fair_day: {
			// set fair_day text
			customText = WeatherConstants.ClimaconSun;
			break;
		}

		default: {
			customText = '-';
			break;
		}
		}
		return String.valueOf(customText);
	}

	public void setDayOfWeek() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		

	}

	public String returnDayString(int day) {
		
		String dayOfWeek = "";
		switch (day) {
		case 1:
			dayOfWeek = "SUN";
			break;

		case 2:
			dayOfWeek = "MON";
			break;
		case 3:
			dayOfWeek = "TUE";
			break;

		case 4:
			dayOfWeek = "WED";
			break;

		case 5:
			dayOfWeek = "THU";
			break;

		case 6:
			dayOfWeek = "FRI";
			break;

		case 7:
			dayOfWeek = "SAT";
			break;
		}
		
		return dayOfWeek;
	}

}
