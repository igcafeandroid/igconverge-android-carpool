package com.info.igconverge.carpool;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.info.igconverge.R;

import java.util.ArrayList;

/**
 * Created by lab1 on 24/12/15.
 */
public class EntryAdapter extends ArrayAdapter {
    private Context mContext;
    private ArrayList items;
    private LayoutInflater vi;

    private Typeface myTypefaceMedium,myTypefaceLight, myTypefaceLightItaliat;

    public EntryAdapter(Context context,ArrayList items) {
        super(context,0, items);
        this.mContext = context;
        this.items = items;
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myTypefaceMedium = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Medium.ttf");
        myTypefaceLight = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Light.ttf");
        myTypefaceLightItaliat=Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-LightItalic.ttf");
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        final SectionInterface i = (SectionInterface) items.get(position);
        if (i != null) {
            if(i.isSection()){
                HeaderModel si = (HeaderModel)i;
                v = vi.inflate(R.layout.car_section, null);
                v.setOnClickListener(null);
                v.setOnLongClickListener(null);
                v.setLongClickable(false);
                final TextView sectionView =
                        (TextView) v.findViewById(R.id.section_text);
                sectionView.setTypeface(myTypefaceLightItaliat);
                sectionView.setText(si.getHeader());
            } else {
                ListModel ei = (ListModel)i;
                v = vi.inflate(R.layout.car_list, null);

                final TextView location = (TextView)v.findViewById(R.id.location1);
                final TextView time1=(TextView)v.findViewById(R.id.time1);
                final TextView name1=(TextView)v.findViewById(R.id.Driver);
                final TextView cost1=(TextView)v.findViewById(R.id.cost_item);
                final TextView carNo1=(TextView)v.findViewById(R.id.carNo1);
                final TextView status =(TextView)v.findViewById(R.id.status);
                final TextView postId=(TextView)v.findViewById(R.id.postId);
                ArrayList<String> days_all=new ArrayList<>();
                name1.setTypeface(myTypefaceMedium);
                time1.setTypeface(myTypefaceLight);
status.setTypeface(myTypefaceMedium);
                cost1.setTypeface(myTypefaceMedium);
                carNo1.setTypeface(myTypefaceLight);
                location.setTypeface(myTypefaceLight);
                if (location!= null) location.setText(ei.Location);
                System.out.print("________"+ei.days);
                if(time1 != null)
                {for(int j=0;j<ei.days.size();j++){
                   days_all.add(ei.days.get(j).substring(0,1));

                }
                    time1.setText(ei.time+" " +days_all);
                  }
                if(name1 != null) name1.setText(ei.name);
                cost1.setText(String.valueOf(ei.cost));
                if(carNo1 != null) carNo1.setText(ei.carNo);
                if(status != null) status.setText(ei.status);
                if(postId != null) postId.setText(ei.postId);



            }
        }
        return v;
    }

}