package com.info.igconverge.menu.datepicker;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class DateUtils {

	public static List<DateItem> getDaysBetweenStartAndEnd(Date startDate,
			Date endDate) {
		List<DateItem> dates = new ArrayList<DateItem>();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		// while (calendar.getTime().) {
		while (calendar.getTime().before(endDate)) {
			Date result = calendar.getTime();
			dates.add(new DateItem(result));
			calendar.add(Calendar.DATE, 1);
		}
		dates.add(new DateItem(calendar.getTime()));

		return dates;
	}

}
