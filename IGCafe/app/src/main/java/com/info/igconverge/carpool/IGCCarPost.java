package com.info.igconverge.carpool;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.emergency.GPSTracker;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class IGCCarPost extends Fragment implements com.info.igconverge.Services.FragmentCallback   {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    IGCDashboardActivity activity;
    TextView userNameText;
    LinearLayout locationLayout, monLayout, tueLayout, wedLayout, thuLayout, friLayout, satLayout, sunLayout;
    TextView monday, tuesday, wednesday, thursday, friday, saturday, sunday,addTime,carNumber,noOfSeats,fareAmt,popup_text;
    ImageView imageMon, imageTue, imageWed, imageThu, imageFri, imageSat, imageSun;
    int countMon = 0, countTue = 0, countWed = 0, countThu = 0, countFri = 0, countSat = 0, countSun = 0;
    LinearLayout timeLayout, commentLayout, fareLayout, seatsLayout, carNoLayout, phoneLayout;
    String rideDescription;
    public boolean isRetry = false;
    public int retryCount = 0;
    boolean mapshowPlacePending=false;
    GPSTracker gps;
    private GoogleMap gm;
    Location l;
    protected LocationManager locManager;
    LatLng loc;
    String msg = "demo";
    String setTime, setFare, setSeats, setCarNo, setPhone;
    FragmentCallback frag;

    TextView newfare, newseats, newcarno, newtime,newphone,setDestinationLocationValue,newcomment;
double endlatitude,endlongitude;
    ArrayList<String> days=new ArrayList<>();
    ArrayList<String> totalDays=new ArrayList<>();

    Button addPostButton;
    Context mContext;
    private Typeface myTypefaceLight,myTypefaceMedium,myTypefaceRegular;

    String destinationLocation;
    EditText editPhone,editComment,editFare,editSeats,editCarNo;

    int SET_RIDE_REQUEST = 111;


    private OnFragmentInteractionListener mListener;


    // TODO: Rename and change types and number of parameters


    public IGCCarPost() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        frag = this;
        setHasOptionsMenu(true);
        initialseActionBar();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialseActionBar();
        if(retryCount==1)
        {
            retryCount = 0;
            isRetry = false;
            gps=new GPSTracker(activity);
            if (gps.canGetLocation()) {

                    setMapMarker(gps.getLatitude(),gps.getLongitude());



            } else {
                isRetry = true;
                gps.showSettingsAlert();
            }
        }
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(gm!=null)
//                gm.animateCamera(CameraUpdateFactory.scrollBy(0, 350));
//            }
//
//        }, 3000);

    }

    private void initialseActionBar() {
        // TODO Auto-generated method stub

        if (activity != null) {

            activity.actionBar.setDisplayHomeAsUpEnabled(true);
            activity.lockNavigationDrawerSwipeGesture();
            activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
            activity.actionBar.setTitle(IGCConstants.ShareRide);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext=container.getContext();
        days.add(0,"Monday");
        days.add(1, "Tuesday");
        days.add(2, "Wednesday");
        days.add(3, "Thursday");
        days.add(4, "Friday");


        totalDays.add(0, "Monday");
        totalDays.add(1, "Tuesday");
        totalDays.add(2, "Wednesday");
        totalDays.add(3, "Thursday");
        totalDays.add(4, "Friday");
        totalDays.add(5, "Saturday");
        totalDays.add(6, "Sunday");




        View v = inflater.inflate(R.layout.fragment_carpool_new_post, container, false);

        monLayout = (LinearLayout) v.findViewById(R.id.carpoolmon);
        tueLayout = (LinearLayout) v.findViewById(R.id.carpooltues);
        tuesday = (TextView) v.findViewById(R.id.text_tuesday);
        monday = (TextView) v.findViewById(R.id.text_monday);
        imageTue = (ImageView) v.findViewById(R.id.image_tuesday);
        imageMon = (ImageView) v.findViewById(R.id.image_monday);
        wedLayout = (LinearLayout) v.findViewById(R.id.carpoolwed);
        wednesday = (TextView) v.findViewById(R.id.text_wednesday);
        imageWed = (ImageView) v.findViewById(R.id.image_wednesday);
        thuLayout = (LinearLayout) v.findViewById(R.id.carpoolthu);
        thursday = (TextView) v.findViewById(R.id.text_thursday);
        imageThu = (ImageView) v.findViewById(R.id.image_thursday);
        friLayout = (LinearLayout) v.findViewById(R.id.carpoolfri);
        friday = (TextView) v.findViewById(R.id.text_friday);
        imageFri = (ImageView) v.findViewById(R.id.image_friday);


        monLayout.setBackgroundColor(getResources().getColor(R.color.green));
        tueLayout.setBackgroundColor(getResources().getColor(R.color.green));
        wedLayout.setBackgroundColor(getResources().getColor(R.color.green));
        thuLayout.setBackgroundColor(getResources().getColor(R.color.green));
        friLayout.setBackgroundColor(getResources().getColor(R.color.green));

        monday.setTextColor(Color.WHITE);
        imageMon.setVisibility(View.VISIBLE);
        tuesday.setTextColor(Color.WHITE);
        imageTue.setVisibility(View.VISIBLE);
        wednesday.setTextColor(Color.WHITE);
        imageWed.setVisibility(View.VISIBLE);
        thursday.setTextColor(Color.WHITE);
        imageThu.setVisibility(View.VISIBLE);
        friday.setTextColor(Color.WHITE);
        imageFri.setVisibility(View.VISIBLE);


        userNameText = (TextView) v.findViewById(R.id.text_user);
        setDestinationLocationValue = (TextView) v.findViewById(R.id.setLoctionValue);
        String userName = IGCSharedPreferences.getProfileName(getContext());
        userNameText.setText(userName);


        try {
            if (gm == null) {
                gm = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
                Log.d(msg, "Before map");
            }
            gm.setMapType(gm.MAP_TYPE_TERRAIN);
            gm.getUiSettings().setMyLocationButtonEnabled(true);
            gm.setOnMarkerClickListener(
                    new GoogleMap.OnMarkerClickListener() {
                        boolean doNotMoveCameraToCenterMarker = true;

                        public boolean onMarkerClick(Marker marker) {
                            //Do whatever you need to do here ....
                            return doNotMoveCameraToCenterMarker;
                        }
                    });
            gps = new GPSTracker(getActivity());
            if (gps.canGetLocation()) {

                setMapMarker(gps.getLatitude(), gps.getLongitude());

            }
            else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                isRetry=true;
                mapshowPlacePending=true;
                gps.showSettingsAlert();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        locationLayout = (LinearLayout) v.findViewById(R.id.locationlayout);
        locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, IGCPickLocationActivity.class);
                i.putExtra("requestCode", SET_RIDE_REQUEST);
                startActivityForResult(i, SET_RIDE_REQUEST);

            }
        });
        setDestinationLocationValue=(TextView)v.findViewById(R.id.setLoctionValue);
        setDestinationLocationValue.setTypeface(myTypefaceLight);
        newfare = (TextView) v.findViewById(R.id.text_fare);
        newfare.setTypeface(myTypefaceMedium);
        newseats = (TextView) v.findViewById(R.id.text_seats);
        newseats.setTypeface(myTypefaceMedium);
        newcarno = (TextView) v.findViewById(R.id.text_car_no);
        newcarno.setTypeface(myTypefaceMedium);
        newtime = (TextView) v.findViewById(R.id.setTime);
        newphone=(TextView)v.findViewById(R.id.textPhone);
        newcomment=(TextView)v.findViewById(R.id.text_comment);
        newtime.setTypeface(myTypefaceMedium);
        newphone = (TextView) v.findViewById(R.id.textPhone);
        newphone.setTypeface(myTypefaceMedium);
        addTime = (TextView) v.findViewById(R.id.text_fare);
        noOfSeats = (TextView) v.findViewById(R.id.text_fare);
        fareAmt = (TextView) v.findViewById(R.id.text_fare);
        carNumber = (TextView) v.findViewById(R.id.text_fare);
        addTime.setTypeface(myTypefaceLight);
        noOfSeats.setTypeface(myTypefaceLight);
        fareAmt.setTypeface(myTypefaceLight);
        carNumber.setTypeface(myTypefaceLight);




        monday.setTypeface(myTypefaceMedium);

        monLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                countMon++;
                if (countMon % 2 == 0) {
                    days.add("Monday");
                    monday.setTextColor(Color.WHITE);
                    imageMon.setVisibility(View.VISIBLE);
                    monLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Monday")) {
                            days.remove(i);
                        }
                    }
                    monday.setTextColor(Color.BLACK);
                    imageMon.setVisibility(View.INVISIBLE);
                    monLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));

                }
            }
        });

        tuesday.setTypeface(myTypefaceMedium);

        tueLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countTue++;
                if (countTue % 2 == 0) {
                    days.add("Tuesday");
                    tuesday.setTextColor(Color.WHITE);
                    imageTue.setVisibility(View.VISIBLE);
                    tueLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Tuesday")) {
                            days.remove(i);
                        }
                    }
                    tuesday.setTextColor(Color.BLACK);
                    imageTue.setVisibility(View.INVISIBLE);
                    tueLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));

                }
            }
        });


        wednesday.setTypeface(myTypefaceMedium);;

        wedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countWed++;
                if (countWed % 2 == 0) {
                    days.add("Wednesday");
                    wednesday.setTextColor(Color.WHITE);
                    imageWed.setVisibility(View.VISIBLE);
                    wedLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Wednesday")) {
                            days.remove(i);
                        }
                    }
                    wednesday.setTextColor(Color.BLACK);
                    imageWed.setVisibility(View.INVISIBLE);
                    wedLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });


        thursday.setTypeface(myTypefaceMedium);

        thuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countThu++;
                if (countThu % 2 == 0) {
                    days.add("Thursday");
                    thursday.setTextColor(Color.WHITE);
                    imageThu.setVisibility(View.VISIBLE);
                    thuLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Thursday")) {
                            days.remove(i);
                        }
                    }
                    thursday.setTextColor(Color.BLACK);
                    imageThu.setVisibility(View.INVISIBLE);
                    thuLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });


        friday.setTypeface(myTypefaceMedium);
        friLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countFri++;
                if (countFri % 2 == 0) {
                    days.add("Friday");
                    friday.setTextColor(Color.WHITE);
                    imageFri.setVisibility(View.VISIBLE);
                    friLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Friday")) {
                            days.remove(i);
                        }
                    }
                    friday.setTextColor(Color.BLACK);
                    imageFri.setVisibility(View.INVISIBLE);
                    friLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        satLayout = (LinearLayout) v.findViewById(R.id.carpoolsat);
        saturday = (TextView) v.findViewById(R.id.text_saturday);
        saturday.setTypeface(myTypefaceMedium);
        imageSat = (ImageView) v.findViewById(R.id.image_saturday);
        satLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countSat++;
                if (countSat % 2 != 0) {
                    days.add("Saturday");
                    saturday.setTextColor(Color.WHITE);
                    imageSat.setVisibility(View.VISIBLE);
                    satLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Saturday")) {
                            days.remove(i);
                        }
                    }
                    saturday.setTextColor(Color.BLACK);
                    imageSat.setVisibility(View.INVISIBLE);
                    satLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        sunLayout = (LinearLayout) v.findViewById(R.id.carpoolsun);
        sunday = (TextView) v.findViewById(R.id.text_sunday);
        sunday.setTypeface(myTypefaceMedium);

        imageSun = (ImageView) v.findViewById(R.id.image_sunday);
        sunLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countSun++;
                if (countSun % 2 != 0) {
                    days.add("Sunday");
                    sunday.setTextColor(Color.WHITE);
                    imageSun.setVisibility(View.VISIBLE);
                    sunLayout.setBackgroundColor(getResources().getColor(R.color.green));
                } else {
                    for (int i = 0; i < days.size(); i++) {
                        if (days.get(i).equalsIgnoreCase("Sunday")) {
                            days.remove(i);
                        }
                    }
                    sunday.setTextColor(Color.BLACK);
                    imageSun.setVisibility(View.INVISIBLE);
                    sunLayout.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        timeLayout = (LinearLayout) v.findViewById(R.id.timelayout);
        timeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new MyTimePicker();
                newFragment.show(getActivity().getFragmentManager(), "timePicker");


            }
        });

        commentLayout = (LinearLayout) v.findViewById(R.id.commentlayout);
        commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolCommentDialog();

            }
        });

        fareLayout = (LinearLayout) v.findViewById(R.id.farelayout);
        fareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolFareDialog();
            }
        });
        seatsLayout = (LinearLayout) v.findViewById(R.id.seatslayout);
        seatsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolSeatsDialog();
            }
        });
        carNoLayout = (LinearLayout) v.findViewById(R.id.carnolayout);
        carNoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCarpoolCarNoDialog();
            }
        });
        phoneLayout = (LinearLayout) v.findViewById(R.id.phoneLayout);
        phoneLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCarpoolPhoneDialog();
            }
        });


        addPostButton = (Button) v.findViewById(R.id.addpostButton);
        addPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!newtime.getText().toString().equals("")) {
                    setTime = newtime.getText().toString();

                    if (!newfare.getText().toString().equals("")) {
                        setFare = newfare.getText().toString();


                        if (!newseats.getText().toString().equals("")) {
                            setSeats = newseats.getText().toString();

                            if (!newcarno.getText().toString().equals("")) {
                                setCarNo = newcarno.getText().toString();
                                if (!newphone.getText().toString().equals("")) {
                                    setPhone = newphone.getText().toString();
                                    if (destinationLocation!=null && destinationLocation.length()>0) {

                                        try {
                                            if (IGCCheckNetworkConnectivity.checkConnection(activity)) {


                                                JSONObject jsonObject = createJsonObjectForService(setCarNo, setTime, setFare, setSeats, rideDescription, setPhone,destinationLocation);


                                                IGCAsyncServiceRequest getPost = new IGCAsyncServiceRequest(activity, "POST", jsonObject, frag);

                                                getPost.execute(IGCConstants.saveRideRequestURL);

                                            } else {
                                                ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
                                                dialog.setCancelable(false);
                                                dialog.show();
                                                System.out.println("in error dialog");

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    else {
                                        destinationLocation="";
                                        Toast.makeText(activity, "Please set Location.", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    setPhone = "";
                                    Toast.makeText(activity, "Please set Phone No.", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                setCarNo = "";
                                Toast.makeText(activity, "Please set Car No.", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            setSeats = "";
                            Toast.makeText(activity, "Please enter Total Vacant seats.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        setFare = "";
                        Toast.makeText(activity, "Please enter fare.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    setTime = "";
                    Toast.makeText(activity, "Please set Departure time of ride.", Toast.LENGTH_SHORT).show();
                }


//                if (setTime.length()>0 && setFare.length()>0 && setSeats.length()>0 && setCarNo.length()>0) {
//
//                    Toast.makeText(activity, "DDDDDDDDDDDDD.", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    Toast.makeText(activity, "Kindly fill all the Car Ride details.", Toast.LENGTH_SHORT).show();
//                }


            }
        });


        return v;
    }
private void setMapMarker(double latitude,double longitude)
{
    if(gps.getLatitude()!=0 && gps.getLongitude()!=0) {
        loc = new LatLng(latitude, longitude);
        Marker m1 = gm.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
        gm.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (gm != null)
                    gm.animateCamera(CameraUpdateFactory.scrollBy(0, 500));
            }

        }, 3000);
    }

}
    private JSONObject createJsonObjectForService(String carNo, String timeValue, String fareValue, String totalSeats, String rideComment, String phoneNo,String location) {
        JSONObject jsonObj = new JSONObject();
        ;
        try {
            String actualDepTime;
            String depDateTime;
            String timeObj[] = timeValue.split(":");
            int hoursValue = Integer.parseInt(timeObj[0]);
            String timeObj1[]=timeObj[1].split(" ");
            String minuteValue=timeObj1[0];
            if (hoursValue < 12) {
                actualDepTime = String.valueOf(hoursValue) + ":" + String.valueOf(minuteValue) + " AM";

            } else {
                hoursValue = hoursValue - 12;
                actualDepTime = String.valueOf(hoursValue) + ":" + String.valueOf(minuteValue) + " PM";
            }

            Date date = new java.util.Date();
           Long currentTimeStamp = date.getTime();
//            Date depDate=new Date();
//            Long depDateLong=depDate.getTime();
//            String depDateString=new SimpleDateFormat("MM/dd/yyyy").format(depDateLong);
//             depDateTime=depDateString+" "+actualDepTime;
//
//
//
            String dateInString=new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(currentTimeStamp);
            jsonObj.put("numberOfSeats", totalSeats);
            jsonObj.put("carNumber", carNo);
            jsonObj.put("fare", fareValue);
            jsonObj.put("status",true);
            jsonObj.put("startingLocation",location);
            jsonObj.put("departureTime", actualDepTime);

            jsonObj.put("postedOn", dateInString);
            if (rideComment != null) {
                jsonObj.put("description", rideComment);
            } else {
                jsonObj.put("description", "");
            }


            JSONObject userObj = new JSONObject();
            if (IGCSharedPreferences.getUserId(getContext()) != null && IGCSharedPreferences.getEmail(getContext()) != null && IGCSharedPreferences.getProfileName(getContext()) != null) {
                userObj.put("id", IGCSharedPreferences.getUserId(getContext()));
                userObj.put("name", IGCSharedPreferences.getProfileName(getContext()));
                userObj.put("email", IGCSharedPreferences.getEmail(getContext()));
                userObj.put("phoneNumber", phoneNo);
            }
            jsonObj.put("user", userObj);


            JSONArray arrayDays = new JSONArray();
            int k=0;
            for (int i=0;i<totalDays.size();i++) {

                for (int j = 0; j < days.size(); j++) {
                    if (days.get(j).equalsIgnoreCase(totalDays.get(i)))
                    {
                        arrayDays.put(days.get(j));
                        k++;
                        break;
                    }


                }
            }
            jsonObj.put("days", arrayDays);
            return jsonObj;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isRetry) {
            isRetry = true;
            retryCount = 1;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void fragMultipleResponse(String response, String extra) {

        if (response != null && response.length() > 0) {
            try {

                if (response != null) {
                    //fetchRide(response);
                    Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                    activity.onBackPressed();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }


    public void showCarpoolCommentDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialogBuilder.show();
        popup_text=(TextView)dialogBuilder.findViewById(R.id.text_heading);
         popup_text.setText("Comments");
         editComment = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
         //editComment.requestFocus();
        //dialogBuilder.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        //editComment.setRawInputType(Configuration.KEYBOARD_UNDEFINED);
        editComment.setText(newcomment.getText().toString());
        if (editComment.getText().toString().equals(""))
        {
            editComment.setHint("Enter Comments");
        }




        //editComment.setText(newcomment.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                rideDescription = editComment.getText().toString();


                TextView textComment = (TextView) getActivity().findViewById(R.id.text_comment);
                textComment.setTypeface(myTypefaceMedium);
                textComment.setText(rideDescription);

                //TextView textComment = (TextView) getActivity().findViewById(R.id.text_comment);
                newcomment.setText(rideDescription);

                dialogBuilder.dismiss();
            }
        });

    }

    public void showCarpoolFareDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();
        popup_text=(TextView)dialogBuilder.findViewById(R.id.text_heading);
        popup_text.setText("Fare");
         editFare = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
         editFare.setRawInputType(Configuration.KEYBOARD_QWERTY);

        editFare.setText(newfare.getText().toString());
        if (editFare.getText().toString().equals(""))
        {
            editFare.setHint("Enter Fare");
        }


        //editFare.setText(newfare.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                String str = editFare.getText().toString();

                //TextView textFare = (TextView) getActivity().findViewById(R.id.text_fare);
                newfare.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }


    public void showCarpoolPhoneDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();
        popup_text=(TextView)dialogBuilder.findViewById(R.id.text_heading);
        popup_text.setText("Contact Number");
         editPhone = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editPhone.setRawInputType(Configuration.KEYBOARD_QWERTY);
        editPhone.setText(newphone.getText().toString());
        if (editPhone.getText().toString().equals(""))
        {
            editPhone.setHint("Enter the Contact no.");
        }

        //editPhone.setText(newphone.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String str = editPhone.getText().toString();


                TextView textPhone = (TextView) getActivity().findViewById(R.id.textPhone);
                textPhone.setTypeface(myTypefaceMedium);
                textPhone.setText(str);

                //TextView textPhone = (TextView) getActivity().findViewById(R.id.textPhone);
                newphone.setText(str);

                dialogBuilder.dismiss();
            }
        });
    }

    public void showCarpoolSeatsDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();
        popup_text=(TextView)dialogBuilder.findViewById(R.id.text_heading);
        popup_text.setText("Number of Seats");
         editSeats = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editSeats.setText(newseats.getText().toString());
        editSeats.setRawInputType(Configuration.KEYBOARD_QWERTY);
        if (editSeats.getText().toString().equals(""))
        {
            editSeats.setHint("Enter the No. of Seats");
        }
         //editSeats.setText(newseats.getText().toString());
        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        //addButton.setHint("Enter No. of Vacant Seats");
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String str = editSeats.getText().toString();

                //TextView textSeats = (TextView) getActivity().findViewById(R.id.text_seats);
                newseats.setText(str);
                dialogBuilder.dismiss();
            }
        });

    }

    public void showCarpoolCarNoDialog() {
        final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
                .getInstance(activity);
        dialogBuilder
                .setTitleViewGone(true)
                .withTitle("My Dialog1")
                        // .withTitle(null) no title
                .withTitleColor("#FFFFFF")
                        // def
                .withDividerColor("#11000000")
                        // def
                .withMessage("This is a modal Dialog.")
                        // .withMessage(null) no Msg
                .setMessageViewGone(true)
                .withMessageColor("#FFFFFFFF")
                        // def | withMessageColor(int resid)
                .withDialogColor(Color.TRANSPARENT)
                        // def | withDialogColor(int resid) //def
                .isCancelableOnTouchOutside(false)
                        // def | isCancelable(true)
                .withDuration(300)
                        // def
                .withEffect(Effectstype.Slidetop)
                .setCustomView(R.layout.carpool_custom_dialog, activity).setCustomView(R.layout.carpool_custom_dialog, activity).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialogBuilder.show();
        popup_text=(TextView)dialogBuilder.findViewById(R.id.text_heading);
        popup_text.setText("Car Number");
         editCarNo = (EditText) dialogBuilder.findViewById(R.id.carpoolCustomDialog);
        editCarNo.setText(newcarno.getText().toString());

        if (editCarNo.getText().toString().equals(""))
          {
         editCarNo.setHint("Enter Car No.");
          }

        Button addButton = (Button) dialogBuilder.findViewById(R.id.carpoolCustomExtensionDialog);
        //addButton.setHint("Enter Car No.");
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String str = editCarNo.getText().toString();

                //TextView textCarNo = (TextView) getActivity().findViewById(R.id.text_car_no);
                newcarno.setText(str);

                dialogBuilder.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SET_RIDE_REQUEST) {
            Bundle bundle = null;
            if(data!=null && data.getExtras()!=null)
            {

               bundle  = data.getExtras();
                double latitude = (double) bundle.get("latitude");
                double longitude = (double) bundle.get("longitude");
                final String location = (String) bundle.get("address");
                if(location!=null)
                {
                    destinationLocation=location;
                    setDestinationLocationValue.setText(location);
                    if(latitude!=0 && longitude!=0)
                    {
                      Marker m1=  gm.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(destinationLocation));
                            m1.showInfoWindow();
                        loc = new LatLng(latitude, longitude);


                        Point mappoint = gm.getProjection().toScreenLocation(new LatLng(latitude, longitude));
                        mappoint.set(mappoint.x, mappoint.y);
                        gm.animateCamera(CameraUpdateFactory.newLatLngZoom(gm.getProjection().fromScreenLocation(mappoint),13));


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                gm.animateCamera(CameraUpdateFactory.scrollBy(0, 500));
                            }

                        }, 2000);
                    }

                }

            }





        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}


