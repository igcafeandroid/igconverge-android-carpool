package com.info.igconverge.model;

import java.util.ArrayList;
import java.util.Comparator;

import android.os.Parcel;
import android.os.Parcelable;

public class IGCShopDetails implements Parcelable {

	String shopName;
	String shopAddress;
	String shopDistance;
	String shopLatitude;
	String shopLongitude;
	String shopRating;
	String referenceString;
	ArrayList<IGCPhotoAttachement> photoAttachements = new ArrayList<IGCPhotoAttachement>();

	/**
	 * @return the referenceString
	 */

	public String getReferenceString() {
		return referenceString;
	}

	public ArrayList<IGCPhotoAttachement> getPhotoAttachements() {
		return photoAttachements;
	}

	public void setPhotoAttachements(
			ArrayList<IGCPhotoAttachement> photoAttachements) {
		this.photoAttachements = photoAttachements;
	}

	/**
	 * @return the weblink
	 */
	public String getWeblink() {
		return weblink;
	}

	/**
	 * @return the contactInfo
	 */
	public String getContactInfo() {
		return contactInfo;
	}

	/**
	 * @param referenceString
	 *            the referenceString to set
	 */
	public void setReferenceString(String referenceString) {
		this.referenceString = referenceString;
	}

	/**
	 * @param weblink
	 *            the weblink to set
	 */
	public void setWeblink(String weblink) {
		this.weblink = weblink;
	}

	/**
	 * @param contactInfo
	 *            the contactInfo to set
	 */
	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	String weblink;
	String contactInfo;

	public IGCShopDetails() {

	}

	public IGCShopDetails(String shopName, String shopAddress,
			String shopDistance, String shopLatitude, String shopLongitude,
			String shopRating, String referenceString, String weblink,
			String contactInfo, ArrayList<IGCPhotoAttachement> photoAttachements) {
		super();
		this.shopName = shopName;
		this.shopAddress = shopAddress;
		this.shopDistance = shopDistance;
		this.shopLatitude = shopLatitude;
		this.shopLongitude = shopLongitude;
		this.shopRating = shopRating;
		this.referenceString = referenceString;
		this.weblink = weblink;
		this.contactInfo = contactInfo;
		this.photoAttachements = photoAttachements;
	}

	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}

	/**
	 * @return the shopAddress
	 */
	public String getShopAddress() {
		return shopAddress;
	}

	/**
	 * @return the shopDistance
	 */
	public String getShopDistance() {
		return shopDistance;
	}

	/**
	 * @return the shopLatitude
	 */
	public String getShopLatitude() {
		return shopLatitude;
	}

	/**
	 * @return the shopLongitude
	 */
	public String getShopLongitude() {
		return shopLongitude;
	}

	/**
	 * @return the shopRating
	 */
	public String getShopRating() {
		return shopRating;
	}

	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeStringArray(new String[] { this.shopName, this.shopAddress,
				this.shopDistance, this.shopLatitude, this.shopLongitude,
				this.shopRating, this.referenceString, this.weblink,
				this.contactInfo });
		dest.writeList(this.photoAttachements);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public IGCShopDetails createFromParcel(Parcel in) {
			return new IGCShopDetails(in);
		}

		public IGCShopDetails[] newArray(int size) {
			return new IGCShopDetails[size];
		}
	};

	public IGCShopDetails(Parcel in) {
		String[] data = new String[9];

		in.readStringArray(data);
		this.shopName = data[0];
		this.shopAddress = data[1];
		this.shopDistance = data[2];
		this.shopLatitude = data[3];
		this.shopLongitude = data[4];
		this.shopRating = data[5];
		this.referenceString = data[6];
		this.weblink = data[7];
		this.contactInfo = data[8];
		this.photoAttachements = in.readArrayList(null);
	}

	/**
	 * @param shopName
	 *            the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	/**
	 * @param shopAddress
	 *            the shopAddress to set
	 */
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	/**
	 * @param shopDistance
	 *            the shopDistance to set
	 */
	public void setShopDistance(String shopDistance) {
		this.shopDistance = shopDistance;
	}

	/**
	 * @param shopLatitude
	 *            the shopLatitude to set
	 */
	public void setShopLatitude(String shopLatitude) {
		this.shopLatitude = shopLatitude;
	}

	/**
	 * @param shopLongitude
	 *            the shopLongitude to set
	 */
	public void setShopLongitude(String shopLongitude) {
		this.shopLongitude = shopLongitude;
	}

	/**
	 * @param shopRating
	 *            the shopRating to set
	 */
	public void setShopRating(String shopRating) {
		this.shopRating = shopRating;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IGCShopDetails [shopName=" + shopName + ", shopAddress="
				+ shopAddress + ", shopDistance=" + shopDistance
				+ ", shopLatitude=" + shopLatitude + ", shopLongitude="
				+ shopLongitude + ", shopRating=" + shopRating
				+ ", referenceString=" + referenceString + ", weblink="
				+ weblink + ", contactInfo=" + contactInfo + "]";
	}

	public static Comparator<IGCShopDetails> distanceComparator = new Comparator<IGCShopDetails>() {

		@Override
		public int compare(IGCShopDetails fisrtObject,
				IGCShopDetails secondObject) {
			String f1[] = fisrtObject.getShopDistance().split(" ");
			String f2[] = secondObject.getShopDistance().split(" ");

			double f1_d = Double.parseDouble(f1[0].trim());
			double f2_d = Double.parseDouble(f2[0].trim());

			if (f1_d > f2_d)
				return 1;
			else if (f2_d > f1_d)
				return -1;
			else
				return 0;
		}

	};
}
