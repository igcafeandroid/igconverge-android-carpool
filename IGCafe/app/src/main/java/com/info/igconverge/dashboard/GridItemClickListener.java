package com.info.igconverge.dashboard;

import android.view.View;

public interface GridItemClickListener {

	void onGridItemClicked(View v, int position, long itemId);

}