package com.info.igconverge.menu.datepicker;



import android.support.v7.widget.RecyclerView;
import android.view.animation.Animation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbsDateAdapter<T extends AbsDateItemHolder> extends
		RecyclerView.Adapter<T> {

	public static interface DateItemListener {
		void onDateItemClick(DateItem dateItem, int position);
	}

	protected List<DateItem> dateItems;
	protected DefaultDateAdapter.DateItemListener onDateItemListener;

	protected long selectedDate = -1;
	protected T selectedDateView = null;

	private Animation currentViewAnimation;

	public AbsDateAdapter(Date start, Date end) {
		this(start, end, null);
	}

	public AbsDateAdapter(Date start, Date end, Date defaultDate) {
		if (start.getTime() > end.getTime()) {
			throw new IllegalArgumentException(
					"Wrong dates : StartDate > EndDate");
		}

		this.dateItems = new ArrayList<DateItem>();
		this.dateItems.addAll(DateUtils.getDaysBetweenStartAndEnd(start, end));

		if (defaultDate != null
				&& ((defaultDate.getTime() <= end.getTime() && defaultDate
						.getTime() >= start.getTime()))) {
			setSelectedDate(getPosition(defaultDate.getTime()));
		}
	}

	protected abstract void onDateItemHolderClick(T itemHolder);

	protected boolean isDateSelected(DateItem dateItem) {
		return selectedDate == dateItem.getDate().getTime();
	}

	@Override
	public int getItemCount() {
		return dateItems != null ? dateItems.size() : 0;
	}

	public void setOnDateItemClickClistener(DateItemListener onDateItemListener) {
		this.onDateItemListener = onDateItemListener;
	}

	public DateItem getItem(int position) {
		return dateItems.get(position);
	}

	public void setSelectedDate(int position) {
		notifyItemChanged(getPosition(selectedDate));
		selectedDate = dateItems.get(position).getDate().getTime();
		notifyItemChanged(position);
	}

	public int getPosition(long dateInMiliseconds) {
		DateItem dateItem = new DateItem(new Date(dateInMiliseconds));
		return dateItems.indexOf(dateItem);
	}

	public int getCurrentPosition() {
		return getPosition(selectedDate);
	}

	public Animation getCurrentViewAnimation() {
		return currentViewAnimation;
	}

	public void setCurrentViewAnimation(Animation currentViewAnimation) {
		this.currentViewAnimation = currentViewAnimation;
	}

	public boolean hasCurrentViewAnimation() {
		return currentViewAnimation != null;
	}
}
