package com.info.igconverge.contact;

import java.util.List;

import com.info.igconverge.R;
import com.info.igconverge.contact.interfaces.OnItemClickListener;
import com.info.igconverge.contact.interfaces.OnItemLongClickListener;
import com.info.igconverge.contact.interfaces.OnMenuItemClickListener;
import com.info.igconverge.contact.interfaces.OnMenuItemLongClickListener;
import com.info.igconverge.dashboard.IGCDashboardActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class ContextMenuDialogFragment extends DialogFragment {

	public static final String TAG = ContextMenuDialogFragment.class
			.getSimpleName();
	private static final String BUNDLE_MENU_PARAMS = "BUNDLE_MENU_PARAMS";

	private LinearLayout mWrapperButtons;
	private LinearLayout mWrapperText;
	private MenuAdapter mDropDownMenuAdapter;
	private OnMenuItemClickListener mItemClickListener;
	private OnMenuItemLongClickListener mItemLongClickListener;
	private MenuParams mMenuParams;
	IGCDashboardActivity activity;

	@Deprecated
	public static ContextMenuDialogFragment newInstance(int actionBarSize,
			int actionBarColor, List<MenuObject> menuObjects, Activity activity) {
		MenuParams params = new MenuParams();
		params.setActionBarSize(actionBarSize);
		params.setmActionBarColor(actionBarColor);
		params.setMenuObjects(menuObjects);
		activity = (IGCDashboardActivity) activity;
		return newInstance(params, activity);
	}

	@Deprecated
	public static ContextMenuDialogFragment newInstance(int actionBarSize,int actionBarColor,
			List<MenuObject> menuObjects, int animationDelay, Activity activity) {
		MenuParams params = new MenuParams();
		params.setActionBarSize(actionBarSize);
		params.setmActionBarColor(actionBarColor);
		params.setMenuObjects(menuObjects);
		params.setAnimationDelay(animationDelay);
		activity = (IGCDashboardActivity) activity;
		return newInstance(params, activity);
	}

	@Deprecated
	public static ContextMenuDialogFragment newInstance(int actionBarSize,int actionBarColor,
			List<MenuObject> menuObjects, int animationDelay,
			int animationDuration, Activity activity) {
		MenuParams params = new MenuParams();
		params.setActionBarSize(actionBarSize);
		params.setmActionBarColor(actionBarColor);
		params.setMenuObjects(menuObjects);
		params.setAnimationDelay(animationDelay);
		params.setAnimationDuration(animationDuration);
		activity = (IGCDashboardActivity) activity;
		return newInstance(params, activity);
	}

	@Deprecated
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static ContextMenuDialogFragment newInstance(int actionBarSize,int actionBarColor,
			List<MenuObject> menuObjects, int animationDelay,
			int animationDuration, boolean fitsSystemWindow,
			boolean clipToPadding, Activity activity) {
		MenuParams params = new MenuParams();
		params.setActionBarSize(actionBarSize);
		params.setMenuObjects(menuObjects);
		params.setmActionBarColor(actionBarColor);
		params.setAnimationDelay(animationDelay);
		params.setAnimationDuration(animationDuration);
		activity = (IGCDashboardActivity) activity;
		params.setFitsSystemWindow(fitsSystemWindow);
		params.setClipToPadding(clipToPadding);
		return newInstance(params, activity);
	}

	public static ContextMenuDialogFragment newInstance(MenuParams menuParams,
			Activity activity) {
		ContextMenuDialogFragment fragment = new ContextMenuDialogFragment();
		activity = (IGCDashboardActivity) activity;
		Bundle args = new Bundle();
		args.putParcelable(BUNDLE_MENU_PARAMS, menuParams);
		fragment.setArguments(args);
		return fragment;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setStyle(STYLE_NO_FRAME, R.style.MenuFragmentStyle);
		if (getArguments() != null) {

			mMenuParams = getArguments().getParcelable(BUNDLE_MENU_PARAMS);
		
		}
	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_context_menu,
				container, false);
		rootView.setBackgroundColor(mMenuParams.getmActionBarColor());
		rootView.setFitsSystemWindows(mMenuParams.isFitsSystemWindow());
		((ViewGroup) rootView).setClipToPadding(mMenuParams.isClipToPadding());

		initViews(rootView);
		getDialog().getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		initDropDownMenuAdapter();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				mDropDownMenuAdapter.menuToggle();
			}
		}, mMenuParams.getAnimationDelay());

		if (mMenuParams.isClosableOutside()) {
			rootView.findViewById(R.id.root).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							activity.onBackPressed();
						}
					});
		}
		return rootView;
	}

	private void initViews(View view) {
		mWrapperButtons = (LinearLayout) view
				.findViewById(R.id.wrapper_buttons);
		mWrapperText = (LinearLayout) view.findViewById(R.id.wrapper_text);
	}

	private void initDropDownMenuAdapter() {
		mDropDownMenuAdapter = new MenuAdapter(getActivity(), mWrapperButtons,
				mWrapperText, mMenuParams.getMenuObjects(),
				mMenuParams.getActionBarSize());
		mDropDownMenuAdapter.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onClick(View v) {
				if (mItemClickListener != null) {
					mItemClickListener.onMenuItemClick(v,
							mWrapperButtons.indexOfChild(v));
				}
				close();

			}
		});
		mDropDownMenuAdapter
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public void onLongClick(View v) {
						if (mItemClickListener != null) {
							mItemClickListener.onMenuItemClick(v,
									mWrapperButtons.indexOfChild(v));
						}
						close();
					}
				});
		mDropDownMenuAdapter.setAnimationDuration(mMenuParams
				.getAnimationDuration());
	}

	private void close() {
	
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				dismiss();
			}
		}, mMenuParams.getAnimationDelay());
	}

	/**
	 * Menu item click method
	 */

}