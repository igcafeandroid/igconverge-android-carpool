package com.info.igconverge;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.login.IGCAuthenticationActivity;
import com.info.igconverge.login.IGCLoginScreenActivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;

import android.view.Window;

/*
 * Class to show SplashScreen
 *
 */
public class IGCSplashActivity extends Activity {
	Activity activity;
	IGCSharedPreferences sharedPref = new IGCSharedPreferences();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		IGCafeApplication.mSensorManager
				.unregisterListener(IGCafeApplication.mShakeDetector);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splashscreen);
		this.activity = this;
		new Handler().postDelayed(new Runnable() {
			// Using handler with postDelayed called runnable run method
			@Override
			public void run() {
				// Intent i = new
				// Intent(IGCSplashActivity.this,IGCDashboardActivity.class);
				// startActivity(i);
				// finish();

				if (IGCSharedPreferences.getUserName(getApplicationContext()) != null
						&& IGCSharedPreferences
								.getPassword(getApplicationContext()) != null
						&& IGCSharedPreferences.getUserName(
								getApplicationContext()).length() > 0
						&& IGCSharedPreferences.getPassword(

						getApplicationContext()).length() > 0) {
					if (!IGCSharedPreferences
							.isPREF_INSTALL_FLAG(getApplicationContext())) {
					
						Intent i = new Intent(IGCSplashActivity.this,
								IGCLoginScreenActivity.class);
						startActivity(i);
						finish();
					} else {
						
						Intent i = new Intent(IGCSplashActivity.this,
								IGCAuthenticationActivity.class);
						startActivity(i);
						finish();

					}

				}

				else {
				
					Intent i = new Intent(IGCSplashActivity.this,
							IGCLoginScreenActivity.class);
					startActivity(i);
					finish();
				}

			}
		}, 1 * 1000);

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	@Override
	protected void onPause() {
		super.onPause();
		IGCafeApplication.mSensorManager
				.registerListener(IGCafeApplication.mShakeDetector,
						IGCafeApplication.mAccelerometer,
						SensorManager.SENSOR_DELAY_UI);

	}

}