package com.info.igconverge.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.info.igconverge.model.IGCMenu;
import com.info.igconverge.model.IGCMenuItem;

public class MenuParser {
	ArrayList<IGCMenu> list = new ArrayList<IGCMenu>();
	ArrayList<IGCMenuItem> itemList = new ArrayList<IGCMenuItem>();
	ArrayList<IGCMenuItem> paidItemList = new ArrayList<IGCMenuItem>();

	public MenuParser() {

	}

	public ArrayList<IGCMenu> parseMealItems(String jsonData) {

		if (jsonData != "")
			try {

				JSONArray rootArray = new JSONArray(jsonData);

				for (int index = 0; index < rootArray.length(); index++) {
					JSONObject mealObject = rootArray.getJSONObject(index);
					IGCMenu igcMenu = new IGCMenu();
					IGCMenuItem igcMenuItem = new IGCMenuItem();
					igcMenu.setMenuId(mealObject.getString("id"));
					igcMenu.setMenuName(mealObject.getString("name"));
					igcMenu.setMenuDate(mealObject.getString("date"));
					igcMenu.setBellCount(mealObject.getInt("bellCount"));
					JSONArray itemArray = new JSONArray();
					itemArray = mealObject.getJSONArray("items");

					itemList = new ArrayList<IGCMenuItem>();
					paidItemList = new ArrayList<IGCMenuItem>();
					for (int k = 0; k < itemArray.length(); k++) {
						igcMenuItem = new IGCMenuItem();
						JSONObject itemObject = itemArray.getJSONObject(k);
						igcMenuItem.setItem(itemObject.getString("itemName"));
						String itemPrice = itemObject.getString("itemPrice");
						igcMenuItem.setPrice(itemPrice);

						// igcMenuItem.setPrice(itemObject.getString("itemPrice"));
						if (itemPrice.equalsIgnoreCase("0")) {
							itemList.add(igcMenuItem);
						} else {
							paidItemList.add(igcMenuItem);
							// itemList = paidItemList;
						}
					}

					igcMenu.setMenuItems(itemList);
					igcMenu.setMenuPaidItems(paidItemList);

					list.add(igcMenu);
				}

				for (int i = 0; i < list.size(); i++) {

					IGCMenu menu = list.get(i);

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return list;
	}
}
