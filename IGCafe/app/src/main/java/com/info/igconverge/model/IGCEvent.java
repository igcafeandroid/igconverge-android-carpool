package com.info.igconverge.model;

public class IGCEvent {
	String imageId;
	IGCEventItem eventItem;

	/**
	 * 
	 */
	public IGCEvent() {
	}

	/**
	 * @param eventName
	 * @param eventLocation
	 * @param eventTime
	 * @param eventDescription
	 * @param eventCategory
	 */
	public IGCEvent(String imageId, IGCEventItem eventItem) {
		super();
		this.eventItem = eventItem;
		this.imageId = imageId;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public IGCEventItem getEventItem() {
		return eventItem;
	}

	public void setEventItem(IGCEventItem eventItem) {
		this.eventItem = eventItem;
	}


}
