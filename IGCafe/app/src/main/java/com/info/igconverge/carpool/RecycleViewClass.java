package com.info.igconverge.carpool;

import java.util.ArrayList;
import java.util.List;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;


import android.R.integer;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


/**
 * Created by lab2 on 23/12/15.
 */
public class RecycleViewClass extends RecyclerView.Adapter<RecycleViewClass.DBHolder> implements Filterable,com.info.igconverge.Services.FragmentCallback  {


    IGCDashboardActivity activity;
    private ArrayList<CarpoolClass> data;
    private List<CarpoolClass> orig;
    Context context;
    FindRideInterface findListener;
    ArrayList<String> days;
    // private static MyClickListener myclick;
    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public DBHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // TODO Auto-generated method stub
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.find_ride_item,parent,false);
        DBHolder datahold = new DBHolder(context,view);
        return datahold;
    }
    @Override
    public void onBindViewHolder(DBHolder holder, final int position) {
        // TODO Auto-generated method stub
        days=new ArrayList<>();
for (int i=0;i<data.get(position).getDays().size();i++){
    days.add(data.get(position).getDays().get(i).substring(0,1));
}
        holder.time1.setText(data.get(position).getDepTime()+" "+days);
        holder.name1.setText(data.get(position).getUser().getName());
        holder.cost1.setText(String.valueOf(data.get(position).getFare()));
        holder.carNo1.setText(data.get(position).getCarNo());
        holder.location1.setText(data.get(position).getStartLocation());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findListener.rideClick(v,position,data.get(position).postId);
            }
        });

    }

    @Override
    public void fragMultipleResponse(String response, String extra) {
        if (response != null && response.length() > 0) {
            try {

                if (response != null) {

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
            dialog.setCancelable(false);
            dialog.show();

        }
    }


    public static class DBHolder extends RecyclerView.ViewHolder {

        //implements View.OnClickListener{
        TextView time1;
        TextView name1;
        TextView cost1;
        TextView carNo1;
        TextView location1;

        private Typeface myTypefaceMedium,myTypefaceLight;

        private Context mContext;
        public DBHolder(Context context,View itemView) {
            super(itemView);
            this.mContext=context;
            // TODO Auto-generated constructor stub
            myTypefaceMedium = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Medium.ttf");
            myTypefaceLight = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Light.ttf");
            time1=(TextView)itemView.findViewById(R.id.time1);
            name1=(TextView)itemView.findViewById(R.id.Driver);
            cost1=(TextView)itemView.findViewById(R.id.fare_ride_item);
            carNo1=(TextView)itemView.findViewById(R.id.carNo1);
            location1=(TextView)itemView.findViewById(R.id.location1);

            name1.setTypeface(myTypefaceMedium);
            time1.setTypeface(myTypefaceLight);

            cost1.setTypeface(myTypefaceMedium);
            carNo1.setTypeface(myTypefaceLight);
            location1.setTypeface(myTypefaceLight);
            //itemView.setOnClickListener(this);
        }

//		@Override
//		public void onClick(View v) {
//			// TODO Auto-generated method stub
//			myclick.onItemClick(getPosition(),v);
//
//		}

    }
    //	public void setOnItemClickListener(MyClickListener myclickli){
//    this.myclick=myclickli;
//}
    public RecycleViewClass(Context context,ArrayList<CarpoolClass> data,FindRideInterface findListener) {
        this.data = data;
        this.context=context;
        this.findListener=findListener;
    }

    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<CarpoolClass> results = new ArrayList<CarpoolClass>();
                if (orig == null)
                    orig  = data;
                if (constraint != null){

                    if(orig !=null & orig.size()>0 ){
                        for ( final CarpoolClass g :orig) {
                            if (g.getStartLocation().toLowerCase().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;


                }
                return oReturn;
            }


            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                data = (ArrayList<CarpoolClass>)results.values;

                notifyDataSetChanged();


            }
        };


    }	}

