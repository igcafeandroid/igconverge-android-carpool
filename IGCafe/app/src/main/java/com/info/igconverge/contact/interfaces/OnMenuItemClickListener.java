package com.info.igconverge.contact.interfaces;


import android.view.View;

/**
 * Menu item click listener
 */
public interface OnMenuItemClickListener {

    public void onMenuItemClick(View clickedView, int position);
}