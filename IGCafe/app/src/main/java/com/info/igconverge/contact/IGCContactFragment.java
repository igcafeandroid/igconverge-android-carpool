package com.info.igconverge.contact;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.IGCafeApplication.TrackerName;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;
import com.info.igconverge.walkthrough.IGCLinePageIndicator;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;

import com.nineoldandroids.animation.AnimatorSet;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.Toolbar;

import android.util.DisplayMetrics;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;

import android.view.animation.Animation;

import android.view.animation.AnimationUtils;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.Toast;

public class IGCContactFragment extends Fragment implements
		OnPageChangeListener, OnClickListener, OnLongClickListener {
	GoogleMap map;
	private int zoomLevel = 12;
	com.info.igconverge.contact.WrappingViewPager viewPager;
	MapView mapView;
	IGCDashboardActivity activity;
	IGCLinePageIndicator mIndicator;
	private LatLng defaultLatLng;
	private SupportMapFragment fragment;
	ContactPageAdapter mPagerAdapter;
	String[] contactResources;
	String[] contactLocationResources;
	String[] contactHeaderResources;
	String[] contactPhoneResources;
	String[] contactFaxResources;
	LatLng latLong;
	String phoneNo;
	private static View rootView;
	LinearLayout socialNetLayout;
	ImageView twImg, fbImg, mailImg, inImg;
	Animation animFadein;
	FragmentManager fragmentManager;
	private static DialogFragment mMenuDialogFragment;
	private int mAnimationDurationMilis = 100;
	private boolean mIsAnimationRun = false;
	View mainView;
	FragmentManager childFm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.unlockNavigationDrawerSwipeGesture();
		setHasOptionsMenu(true);
		googleAnalyticsReport();

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(activity).reportActivityStart(activity);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;

	}

	@Override
	public void onDetach() {
		super.onDetach();
		socialNetLayout.setVisibility(View.GONE);
		AnimatorSet mAnimatorSetShowMenu = setOpenCloseAnimation(true);
		mAnimatorSetShowMenu.start();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		activity.actionBar.setTitle(IGCConstants.ContactFragmentName);
		activity.actionBar.setBackgroundDrawable(activity.getResources()
				.getDrawable(R.drawable.header_bg));
		fragmentManager = activity.getSupportFragmentManager();

		rootView = inflater.inflate(R.layout.contact_us_fragment, container,
				false);

		initialiseViews(rootView);
		initialiseArrays();
		setViews();
		mapView.onCreate(savedInstanceState);
		map = mapView.getMap();
		map.getUiSettings().setMyLocationButtonEnabled(false);
		map.setMyLocationEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(false);
		MapsInitializer.initialize(activity);
		initMenuFragment();
		setLocation(0);

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.contact_menu_view, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu:
			if (socialNetLayout != null) {
				if (socialNetLayout.getVisibility() == View.GONE) {
					socialNetLayout.setVisibility(View.VISIBLE);
					AnimatorSet mAnimatorSetShowMenu = setOpenCloseAnimation(false);
					mAnimatorSetShowMenu.start();
				} else {
					socialNetLayout.setVisibility(View.GONE);
					AnimatorSet mAnimatorSetShowMenu = setOpenCloseAnimation(true);
					mAnimatorSetShowMenu.start();
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}

	public int getItemCount() {
		return 4;
	}

	private AnimatorSet setOpenCloseAnimation(boolean isCloseAnimation) {
		List<Animator> textAnimations = new ArrayList<Animator>();
		List<Animator> imageAnimations = new ArrayList<Animator>();

		if (isCloseAnimation) {
			for (int i = getItemCount() - 1; i >= 0; i--) {
				fillOpenClosingAnimations(true, imageAnimations, i);
			}
		} else {
			for (int i = 0; i < getItemCount(); i++) {
				fillOpenClosingAnimations(false, imageAnimations, i);
			}
		}

		AnimatorSet textCloseAnimatorSet = new AnimatorSet();
		textCloseAnimatorSet.playSequentially(textAnimations);

		AnimatorSet imageCloseAnimatorSet = new AnimatorSet();
		imageCloseAnimatorSet.playSequentially(imageAnimations);

		AnimatorSet animatorFullSet = new AnimatorSet();
		animatorFullSet.playTogether(imageCloseAnimatorSet,
				textCloseAnimatorSet);
		animatorFullSet.setDuration(mAnimationDurationMilis);
		animatorFullSet.addListener(mCloseOpenAnimatorListener);
		animatorFullSet.setStartDelay(0);
		animatorFullSet.setInterpolator(new HesitateInterpolator());
		return animatorFullSet;
	}

	private void fillOpenClosingAnimations(boolean isCloseAnimation,
			List<Animator> imageAnimations, int wrapperPosition) {
		Animator imageRotation = isCloseAnimation ? wrapperPosition == 0 ? AnimatorUtils
				.rotationCloseToRight(socialNetLayout
						.getChildAt(wrapperPosition)) : AnimatorUtils
				.rotationCloseVertical(socialNetLayout
						.getChildAt(wrapperPosition))
				: wrapperPosition == 0 ? AnimatorUtils
						.rotationOpenFromRight(socialNetLayout
								.getChildAt(wrapperPosition)) : AnimatorUtils
						.rotationOpenVertical(socialNetLayout
								.getChildAt(wrapperPosition));
		imageAnimations.add(imageRotation);
	}

	@Override
	public void onResume() {
		super.onResume();
		socialNetLayout = (LinearLayout) rootView
				.findViewById(R.id.socialNetLayout);
		mainView = (RelativeLayout) rootView.findViewById(R.id.container);
		mapView.onResume();
		
		initialiseArrays();
		

	}

	private Animator.AnimatorListener mCloseOpenAnimatorListener = new Animator.AnimatorListener() {
		@Override
		public void onAnimationStart(Animator animation) {
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			toggleIsAnimationRun();
		}

		@Override
		public void onAnimationCancel(Animator animation) {
		}

		@Override
		public void onAnimationRepeat(Animator animation) {
		}
	};

	class ContactPageAdapter extends PagerAdapter {

		Context mContext;
		LayoutInflater mLayoutInflater;

		public ContactPageAdapter(Context context) {
			mContext = context;
			mLayoutInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return contactResources.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((RelativeLayout) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View itemView = mLayoutInflater.inflate(
					R.layout.contact_view_pager_item, container, false);
			addContactViews(itemView, position);
			container.addView(itemView);
			return itemView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((RelativeLayout) object);
		}
	}

	private void toggleIsAnimationRun() {
		mIsAnimationRun = !mIsAnimationRun;
	}

	public void addContactViews(View itemView, int position) {
		LinearLayout contactLayout = (LinearLayout) itemView
				.findViewById(R.id.contactLayout);
		TextView locationView = (TextView) itemView.findViewById(R.id.location);
		TextView phoneNoView = (TextView) itemView.findViewById(R.id.phoneNo);
		TextView headerView = (TextView) itemView.findViewById(R.id.header);
		RelativeLayout locationLayout = (RelativeLayout) itemView
				.findViewById(R.id.locationLayout);
		locationLayout.setTag(contactLocationResources[position]);
		LinearLayout contactView = (LinearLayout) itemView
				.findViewById(R.id.contactLayout);
		if (contactPhoneResources[position] == null
				|| contactPhoneResources[position].isEmpty()) {
			contactLayout.setVisibility(View.INVISIBLE);
		} else {

			contactLayout.setVisibility(View.VISIBLE);
			phoneNoView.setText(contactPhoneResources[position]);
			contactView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					phoneNo = (String) ((TextView) view
							.findViewById(R.id.phoneNo)).getText();
					dialCall(phoneNo);
				}

			});

		}
		locationView.setText(contactResources[position]);
		headerView.setText(contactHeaderResources[position]);

		TextView phoneNoText = (TextView) contactView
				.findViewById(R.id.phoneNo);
		phoneNo = (String) phoneNoText.getText();

		locationLayout.setOnLongClickListener(this);

	}

	private void initialiseViews(View rootView) {
		mainView = (RelativeLayout) rootView.findViewById(R.id.container);

		LinearLayout pagerLayout = (LinearLayout) mainView
				.findViewById(R.id.pagerlayout);

		viewPager = (com.info.igconverge.contact.WrappingViewPager) pagerLayout
				.findViewById(R.id.pager);
		FrameLayout frame = (FrameLayout) mainView
				.findViewById(R.id.fragment_contactmap);
		mapView = (MapView) frame.findViewById(R.id.map);
		socialNetLayout = (LinearLayout) rootView
				.findViewById(R.id.socialNetLayout);

		fbImg = (ImageView) socialNetLayout.findViewById(R.id.fbImage);
		twImg = (ImageView) socialNetLayout.findViewById(R.id.twiiterImage);
		inImg = (ImageView) socialNetLayout.findViewById(R.id.inImage);
		mailImg = (ImageView) socialNetLayout.findViewById(R.id.mailImage);
		fbImg.setOnClickListener(this);
		twImg.setOnClickListener(this);
		inImg.setOnClickListener(this);
		mailImg.setOnClickListener(this);
	}

	private void setViews() {
		mPagerAdapter = new ContactPageAdapter(activity);
		viewPager.setAdapter(mPagerAdapter);
		viewPager.setPageMargin(IGCUtils.convertDpToPixel(activity, 20));

		mIndicator = (IGCLinePageIndicator) rootView
				.findViewById(R.id.indicator);
		mIndicator.setOnPageChangeListener(this);
		mIndicator.setBackgroundColor(activity.getResources().getColor(
				R.color.default_line_indicator_width_color));

		mIndicator.setViewPager(viewPager);

	}

	public void initialiseArrays() {
		contactResources = getResources().getStringArray(
				R.array.contact_us_list);
		contactHeaderResources = getResources().getStringArray(
				R.array.contact_us_header_list);
		contactLocationResources = getResources().getStringArray(
				R.array.contact_us_location_list);
		contactFaxResources = getResources().getStringArray(
				R.array.contact_us_fax_list);
		contactPhoneResources = getResources().getStringArray(
				R.array.contact_us_phoneno_list);
	}

	public void dialCall(String phoneNo) {
		Intent callIntent = new Intent(Intent.ACTION_DIAL);
		if (phoneNo != null)
			callIntent.setData(Uri.parse("tel:" + phoneNo));
		activity.startActivity(callIntent);
	}

	public int dpToPx(int dp) {
		DisplayMetrics displayMetrics = activity.getResources()
				.getDisplayMetrics();
		int px = Math.round(dp
				* (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
		return px;
	}

	@Override
	public boolean onLongClick(View view) {
		TextView address = (TextView) view.findViewById(R.id.location);
		ClipboardManager clipboard = (ClipboardManager) activity
				.getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("label", address.getText());
		clipboard.setPrimaryClip(clip);
		Toast.makeText(activity, R.string.copied, Toast.LENGTH_LONG).show();
		return false;
	}

	@Override
	public void onPageScrollStateChanged(int state) {
		// Called when the scroll state changes.
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		// This method will be invoked when a new page becomes selected.
		
		setLocation(position);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private void initMenuFragment() {
		MenuParams menuParams = new MenuParams();
		menuParams.setActionBarSize((int) getResources().getDimension(
				R.dimen.tool_bar_height));
		menuParams.setmActionBarColor(getResources().getColor(
				R.color.transparent));
		menuParams.setMenuObjects(getMenuObjects());
		menuParams.setClosableOutside(false);
		mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams,
				activity);
	}

	private List<MenuObject> getMenuObjects() {

		List<MenuObject> menuObjects = new ArrayList();

		MenuObject send = new MenuObject("");
		send.setResource(R.drawable.twitter_icon);
		send.setBgColor(getResources().getColor(R.color.transparent));
		send.setDividerColor(0);

		MenuObject like = new MenuObject("");
		Bitmap b = BitmapFactory.decodeResource(getResources(),
				R.drawable.facebook_icon);
		like.setDividerColor(0);
		like.setBitmap(b);
		like.setBgColor(getResources().getColor(R.color.transparent));

		MenuObject addFr = new MenuObject("");
		BitmapDrawable bd = new BitmapDrawable(getResources(),
				BitmapFactory
						.decodeResource(getResources(), R.drawable.in_icon));
		addFr.setDrawable(bd);
		addFr.setBgColor(getResources().getColor(R.color.transparent));
		addFr.setDividerColor(0);

		menuObjects.add(send);
		menuObjects.add(like);
		menuObjects.add(addFr);

		return menuObjects;
	}

	protected void addFragment(Fragment fragment, boolean addToBackStack,
			int containerId) {

		// invalidateOptionsMenu();
		String backStackName = fragment.getClass().getName();
		boolean fragmentPopped = fragmentManager.popBackStackImmediate(
				backStackName, 0);
		if (!fragmentPopped) {
			FragmentTransaction transaction = fragmentManager
					.beginTransaction();
			transaction.add(containerId, fragment, backStackName)
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			if (addToBackStack)
				transaction.addToBackStack(backStackName);
			transaction.commit();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String webLink = null;
		switch (v.getId()) {
		case R.id.twiiterImage:
			webLink = activity.getResources().getString(R.string.twitterLink);
			visitWebsite(webLink);
			break;
		case R.id.inImage:
			webLink = activity.getResources().getString(R.string.linkedinLink);
			visitWebsite(webLink);
			break;
		case R.id.fbImage:
			webLink = activity.getResources().getString(R.string.fbLink);
			visitWebsite(webLink);
			break;

		case R.id.mailImage:
			webLink = activity.getResources().getString(R.string.mailId);
			sendMail(webLink);
		}
	}

	public void visitWebsite(String webLink) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(webLink));
		activity.startActivity(browserIntent);
	}

	public void sendMail(String emailId) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { emailId });
		i.putExtra(Intent.EXTRA_SUBJECT, "");
		i.putExtra(Intent.EXTRA_TEXT, " ");
		try {
			activity.startActivity(i);
			// activity.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		}
	}

	private void setLocation(int position) {

		try {

			String location = contactLocationResources[position];
			defaultLatLng = new LatLng(Double.valueOf(location.split(",")[0]),
					Double.valueOf(location.split(",")[1]));

		

			map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLatLng,
					zoomLevel));
			map.getUiSettings().setZoomControlsEnabled(true);

			map.addMarker(new MarkerOptions()
					.position(defaultLatLng)
					.title(contactHeaderResources[position])
					.snippet(contactResources[position])
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.location_pin)));
		
			map.getUiSettings().setMapToolbarEnabled(false);
			map.moveCamera(CameraUpdateFactory.newLatLng(defaultLatLng));
			map.animateCamera(CameraUpdateFactory.zoomTo(10), 500, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

	public void googleAnalyticsReport() {
		Tracker t = ((IGCafeApplication) getActivity().getApplication())
				.getTracker(TrackerName.APP_TRACKER);
		t.setScreenName("IGC Contact Screen");
		t.send(new HitBuilders.AppViewBuilder().build());
	}
}
