package com.info.igconverge.events;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;

import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Instances;

import android.support.v4.util.LruCache;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.info.igconverge.R;
import com.info.igconverge.Services.FragmentCallback;
import com.info.igconverge.dashboard.IGCBellRingingActivity;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.EventImageDialog;
import com.info.igconverge.model.IGCEvent;
import com.info.igconverge.model.IGCEventItem;
import com.info.igconverge.parser.ImageParser;
import com.info.igconverge.utility.IGCConstants;

public class IGCEventsListAdapter extends BaseAdapter implements
		com.info.igconverge.Services.FragmentCallback {
	IGCDashboardActivity activity;
	int layoutResourceId;
	ViewholderItem holder;
	ArrayList<IGCEvent> eventItemsList = new ArrayList<IGCEvent>();
	private LruCache<String, Bitmap> mMemoryCache;
	FragmentCallback frag;

	Typeface myTypefaceMedium, myTypefaceRegular;
	public static final String[] EVENT_PROJECTION = new String[] {
			Calendars._ID, // 0
			Calendars.ACCOUNT_NAME, // 1
			Calendars.CALENDAR_DISPLAY_NAME, // 2
			Calendars.OWNER_ACCOUNT // 3
	};

	String eventId;

	static class ViewholderItem {
		TextView headerText, eventText2Location, eventText3Time,
				eventText4Description;
		RelativeLayout headerLayout;
		String imageId;
		int position;
		ImageView imageCircle, addCalendar, eventImage, progressImage;
		// ProgressBar progressBar;
	}

	public IGCEventsListAdapter(IGCDashboardActivity activity) {
		this.activity = activity;
	}

	public IGCEventsListAdapter(IGCDashboardActivity activity,
			ArrayList<IGCEvent> eventItemsList,
			LruCache<String, Bitmap> mMemoryCache) {
		this.activity = activity;
		this.eventItemsList = eventItemsList;
		this.mMemoryCache = mMemoryCache;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			row = inflater.inflate(R.layout.list_row_event, parent, false);
			holder = new ViewholderItem();
			holder.headerText = (TextView) row.findViewById(R.id.headerText);
			holder.addCalendar = (ImageView) row
					.findViewById(R.id.addCalendarReminder);
			holder.eventText2Location = (TextView) row
					.findViewById(R.id.eventText2Location);
			holder.eventText3Time = (TextView) row
					.findViewById(R.id.eventText3Time);
			holder.eventText4Description = (TextView) row
					.findViewById(R.id.eventText4Description);
			holder.progressImage = (ImageView) row
					.findViewById(R.id.progressImageView);

			holder.imageCircle = (ImageView) row.findViewById(R.id.imageCircle);
			holder.headerLayout = (RelativeLayout) row
					.findViewById(R.id.headerLayout);
			holder.eventImage = (ImageView) row.findViewById(R.id.eventImage);
			holder.eventImage.setVisibility(View.GONE);

			myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
					"font/Roboto-Medium.ttf");
			myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
					"font/Roboto-Regular.ttf");
			holder.headerText.setTypeface(myTypefaceMedium);

			holder.eventText2Location.setTypeface(myTypefaceRegular);
			holder.eventText3Time.setTypeface(myTypefaceRegular);
			holder.eventText4Description.setTypeface(myTypefaceRegular);
			holder.position = position;
			row.setTag(holder);

		} else {
			holder = (ViewholderItem) row.getTag();
		}

		Typeface myTypeface = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");

		IGCEvent event = eventItemsList.get(position);
		IGCEventItem item = event.getEventItem();

		holder.imageId = event.getImageId();

		holder.headerText.setText(item.getEventName());
		holder.headerText.setTypeface(myTypeface);
		holder.eventText2Location.setText(item.getEventLocation());
		holder.eventText2Location.setTypeface(myTypeface);
		// try {
		// String eventTime = item.getEventTime();
		// String date = eventTime.split("-", 2)[0];
		// String time = eventTime.split("-", 2)[1];
		// time = time.replaceAll("^\\s+", "");
		// String dateTime = time.split(" ")[0];
		// if (date.equalsIgnoreCase(dateTime)) {
		// holder.eventText3Time.setText(date + dateTime.split(" ")[1]);
		//
		// } else {
		// holder.eventText3Time.setText(item.getEventTime());
		//
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		if (item.getEventTimeStamp() != null) {
			holder.eventText3Time.setText(item.getEventTimeStamp());
		} else {
			holder.eventText3Time.setText(item.getEventTime());
		}
		holder.eventText3Time.setTypeface(myTypeface);
		holder.eventText4Description.setText(item.getEventDescription());
		holder.eventText4Description.setTypeface(myTypeface);
		final String eventTimeStamp = item.getEventTimeStamp();
		final String eventTimeText = item.getEventTime();

		if (item.getEventCategory().equalsIgnoreCase("Doctor")) {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_doctor);

			holder.imageCircle.setImageResource(R.drawable.doctor_icon);

		} else if (item.getEventCategory().equalsIgnoreCase("Dance")) {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_dance);

			holder.imageCircle.setImageResource(R.drawable.dance_icon);
		} else if (item.getEventCategory().equalsIgnoreCase("Yoga")) {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_yoga);

			holder.imageCircle.setImageResource(R.drawable.yoga_icon);

		}

		else if (item.getEventCategory().equalsIgnoreCase("Camp")) {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_camp);

			holder.imageCircle.setImageResource(R.drawable.camp_icon);
		} else if (item.getEventCategory().equalsIgnoreCase("Sports")) {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_sports);

			holder.imageCircle.setImageResource(R.drawable.sports_icon);
		}

		else if (item.getEventCategory().equalsIgnoreCase("Music")) {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_music);

			holder.imageCircle.setImageResource(R.drawable.music_icon);
		}

		else {
			holder.headerLayout
					.setBackgroundResource(R.drawable.head_round_corner_default);

			holder.imageCircle.setImageResource(R.drawable.default_event_icon);
		}

		eventId = item.getEventId();
		holder.progressImage.setVisibility(View.VISIBLE);
		holder.eventImage.setVisibility(View.GONE);

		if (holder.imageId != null && holder.imageId.length() != 0
				&& !holder.imageId.equals("null")) {

			holder.progressImage.setVisibility(View.VISIBLE);

			final Bitmap bitmap = getBitmapFromMemCache(holder.imageId);
			if (bitmap != null) {
				int screenWidth = activity.getResources().getDisplayMetrics().widthPixels;

				holder.progressImage.setVisibility(View.GONE);

				holder.eventImage.setVisibility(View.VISIBLE);
				holder.eventImage.setAdjustViewBounds(true);
				holder.eventImage.setImageBitmap(bitmap);
			} else {
				holder.progressImage.setVisibility(View.VISIBLE);

				GetImageTask task = new GetImageTask(holder.eventImage, holder,
						position);
				task.execute(IGCConstants.fetchEventsImageURL, holder.imageId);
			}
		} else {
			holder.eventImage.setVisibility(View.GONE);
			holder.progressImage.setVisibility(View.GONE);

		}

		holder.addCalendar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				System.out.println("position--" + eventTimeStamp);
				String startDate, endDate, eventDate, eventTime, startTime, endTime, startEventTime, endEventTime;
				LinearLayout parentView = (LinearLayout) view.getParent()
						.getParent();
				view = parentView;
				TextView eventName = (TextView) view
						.findViewById(R.id.headerText);
				// TextView eventTimeText = (TextView) view
				// .findViewById(R.id.eventText3Time);
				// if(eventTimeStamp!=null){
				// eventTimeStamp=;
				// }
				// else
				// {
				//
				// }
				startDate = eventTimeText.split("-", 2)[0];
				eventDate = eventTimeText.split("-", 2)[1];
				// if (eventDate != null) {
				String[] str = eventDate.split(" ", 5);
				// for (int i = 0; i < str.length; i++) {
				//
				// }
				endDate = str[0] + str[1] + " " + str[2] + " " + str[3];
				eventTime = str[4];

				endTime = eventTime.split("-")[1];
				startTime = eventTime.split("-")[0];

				startEventTime = startDate + startTime;
				endEventTime = endDate + endTime;

				if (!isEventInCal((String) eventName.getText(), startEventTime,
						endEventTime)) {

					addEventToCalendar((String) eventName.getText(),
							startEventTime, endEventTime);
				} else
					Toast.makeText(
							activity,
							activity.getResources().getString(
									R.string.eventAlreadyAdded),
							Toast.LENGTH_SHORT).show();

			}
		});

		holder.eventImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				try {
					Bitmap bmp = ((BitmapDrawable) ((ImageView) view)
							.getDrawable()).getBitmap();
					EventImageDialog dialog = new EventImageDialog(activity,
							bmp);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.show();

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}
		});

		// }
		return row;

	}

	@Override
	public int getCount() {
		if (eventItemsList != null) {
			return eventItemsList.size();

		}
		return 0;
	}

	@Override
	public Object getItem(int position) {
		return eventItemsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void addEventToCalendar(String title, String startDate,
			String endDate) {
		try {
			ContentValues values = new ContentValues();
			values.put(CalendarContract.Events.CALENDAR_ID, 1);
			values.put(CalendarContract.Events.TITLE, title);
			values.put(CalendarContract.Events.DTSTART,
					convertDateToLong(startDate));
			values.put(CalendarContract.Events.DTEND,
					convertDateToLong(endDate));
			values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone
					.getDefault().getID());

			values.put(CalendarContract.Events.CUSTOM_APP_PACKAGE,
					activity.getPackageName());
			values.put(CalendarContract.Events.CUSTOM_APP_URI,
					"myAppointment://1");

			activity.getContentResolver().insert(
					CalendarContract.Events.CONTENT_URI, values);
			Toast.makeText(activity,
					activity.getResources().getString(R.string.eventAdded),
					Toast.LENGTH_SHORT).show();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean isEventInCal(String title, String startDate, String endDate) {
		boolean flag = false;
		long begin = convertDateToLong(startDate);
		// starting time in milliseconds
		long end = convertDateToLong(endDate);

		try {
			String[] mSelectionArgs = new String[3];
			mSelectionArgs[0] = title;
			mSelectionArgs[1] = Long.toString(begin);
			mSelectionArgs[2] = Long.toString(end);
			String selection = "((" + CalendarContract.Events.TITLE
					+ "= ?) AND (" + CalendarContract.Events.DTSTART
					+ "= ?) AND (" + CalendarContract.Events.DTEND + "=?))";

			Cursor cursor = activity.getContentResolver().query(
					CalendarContract.Events.CONTENT_URI,
					new String[] { "title", "dtstart", "dtend" }, selection,
					mSelectionArgs, null);

			if (cursor.getCount() > 0) {

				try {
					while (cursor.moveToNext()) {
						String eventTitle = cursor.getString(0);
						if (eventTitle.equalsIgnoreCase(title)) {
							flag = true;
							return flag;
						}

						else
							flag = false;
					}
				} finally {
					cursor.close();
				}

			} else
				flag = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;

	}

	public long convertDateToLong(String conversionDate) {

		long timeInMilli = 0;

		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy hh:mm a");
		Date date;
		try {
			date = sdf.parse(conversionDate);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);

			timeInMilli = calendar.getTimeInMillis();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return timeInMilli;

	}

	public class GetImageTask extends AsyncTask<String, Void, String> {
		List<String> imageDetails = new ArrayList<String>();

		ViewholderItem holder;

		ProgressBar dialog;
		int position;

		public GetImageTask(ImageView image, ViewholderItem holder, int position) {

			this.holder = holder;
			this.position = position;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {

			String jsonData = POSTForMenuItem(params[0], holder.imageId);

			ImageParser imageParser = new ImageParser();
			imageDetails = imageParser.parseImageDetails(jsonData);
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			boolean imageSet = false;
			super.onPostExecute(result);
			if (imageDetails.size() != 0) {
				try {
					if (imageDetails.get(1).equalsIgnoreCase("success")
							&& imageDetails.get(2).length() != 0) {
						String imageStr = imageDetails.get(2);
						// if (holder.position == position) {
						holder.eventImage.setVisibility(View.VISIBLE);
						byte[] imageAsBytes = Base64.decode(
								imageStr.getBytes(), Base64.DEFAULT);

						int screenWidth = activity.getResources()
								.getDisplayMetrics().widthPixels;
						holder.eventImage.setAdjustViewBounds(true);
						final Bitmap bitmap = decodeSampledBitmapFromResource(
								imageAsBytes, screenWidth - 100, 50);

						holder.progressImage.setVisibility(View.GONE);

						holder.eventImage.setImageBitmap(bitmap);
						addBitmapToMemoryCache(holder.imageId, bitmap);
						imageSet = true;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}

	public static Bitmap decodeSampledBitmapFromResource(byte[] arr,
			int reqWidth, int reqHeight) {

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(arr, 0, arr.length, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Config.RGB_565;
		options.inDither = true;
		return BitmapFactory.decodeByteArray(arr, 0, arr.length, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight || width > reqWidth) {
			int halfHeight = (int) (height / 2);
			int halfWidth = (int) (width / 2);
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		return inSampleSize;
	}

	private String POSTForMenuItem(String url, String eventId) {
		InputStream inputStream = null;
		String result = "";
		try {
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
			httpclient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 30000);
			HttpPost httpPost = new HttpPost(url.replaceAll(" ", ""));
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("imageId", eventId));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse httpResponse = httpclient.execute(httpPost);
			inputStream = httpResponse.getEntity().getContent();
			if (inputStream != null) {
				result = convertInputStreamToStringDeleteAlarm(inputStream);

			} else
				result = "Did not work!";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private String convertInputStreamToStringDeleteAlarm(InputStream inputStream)
			throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			stringBuilder.append(line);
		}
		inputStream.close();
		return stringBuilder.toString();
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return mMemoryCache.get(key);
	}

	@Override
	public void fragMultipleResponse(String response, String extra) {
		// TODO Auto-generated method stub
		if (response != null && response.length() > 0) {
			try {

				if (response != null) {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
			dialog.setCancelable(false);
			dialog.show();

		}

	}
}
