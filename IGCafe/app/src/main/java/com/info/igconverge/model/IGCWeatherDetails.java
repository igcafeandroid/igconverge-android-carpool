package com.info.igconverge.model;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class IGCWeatherDetails implements Parcelable {
	String currentDate;
	String currentCity;
	String currentTemp;
	String minTemp;
	String maxTemp;

	String climateCondition;
	List<String> weekTempList = new ArrayList<String>();
	String weatherImageText;
	List<String> weatherImgList = new ArrayList<String>();

	public String getCurrentDate() {
		return currentDate;
	}

	public String getCurrentCity() {
		return currentCity;
	}

	public String getCurrentTemp() {
		return currentTemp;
	}

	public String getClimateCondition() {
		return climateCondition;
	}

	public String getWeatherImageText() {
		return weatherImageText;
	}

	public List<String> getWeatherImgList() {
		return weatherImgList;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}

	public void setCurrentTemp(String currentTemp) {
		this.currentTemp = currentTemp;
	}

	public void setClimateCondition(String climateCondition) {
		this.climateCondition = climateCondition;
	}

	public void setWeatherImageText(String weatherImageText) {
		this.weatherImageText = weatherImageText;
	}

	public void setWeatherImgList(List<String> weatherImgList) {
		this.weatherImgList = weatherImgList;
	}

	public String getMinTemp() {
		return minTemp;
	}

	public String getMaxTemp() {
		return maxTemp;
	}

	public void setMinTemp(String minTemp) {
		this.minTemp = minTemp;
	}

	public void setMaxTemp(String maxTemp) {
		this.maxTemp = maxTemp;
	}

	public List<String> getWeekTempList() {
		return weekTempList;
	}

	public void setWeekTempList(List<String> weekTempList) {
		this.weekTempList = weekTempList;
	}

	public IGCWeatherDetails() {
		super();
	}

	public IGCWeatherDetails(String currentdate, String currentCity,
			String currentTemp, String climateCondition, String weatherImage,
			String minTemp, String maxTemp, List<String> weatherImgList,
			List<String> weekTempList) {
		super();
		this.currentDate = currentdate;
		this.currentCity = currentCity;
		this.currentTemp = currentTemp;
		this.climateCondition = climateCondition;
		this.weatherImageText = weatherImage;
		this.weatherImgList = weatherImgList;
		this.minTemp = minTemp;
		this.maxTemp = maxTemp;
		this.weekTempList = weekTempList;

	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
	
		dest.writeStringArray(new String[] { this.currentDate,
				this.currentCity, this.currentTemp, this.climateCondition,
				this.minTemp, this.maxTemp, this.weatherImageText });
		dest.writeList(this.weatherImgList);
		dest.writeList(this.weekTempList);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public IGCWeatherDetails createFromParcel(Parcel in) {
			return new IGCWeatherDetails(in);
		}

		public IGCWeatherDetails[] newArray(int size) {
			return new IGCWeatherDetails[size];
		}
	};

	public IGCWeatherDetails(Parcel in) {
		String[] data = new String[7];

		in.readStringArray(data);
		this.currentDate = data[0];
		this.currentCity = data[1];
		this.currentTemp = data[2];
		this.climateCondition = data[3];
		this.minTemp = data[4];
		this.maxTemp = data[5];
		this.weatherImageText = data[6];
		this.weatherImgList = in.readArrayList(String.class.getClassLoader());
		this.weekTempList = in.readArrayList(String.class.getClassLoader());
	}
}
