package com.info.igconverge.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

import com.info.igconverge.model.IGCAnnouncement;
import com.info.igconverge.model.IGCEvent;
import com.info.igconverge.model.IGCEventItem;

import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;

public class AnnouncementsParser {
	List<IGCAnnouncement> list = new ArrayList<IGCAnnouncement>();
	IGCAnnouncement announcement = new IGCAnnouncement();
	Context context;

	public AnnouncementsParser(Context context) {
		this.context = context;
	}

	@SuppressLint("NewApi")
	public List<IGCAnnouncement> parseAnnouncements(String jsonData) {
		if (jsonData != "") {
			try {

				JSONObject rootObj = new JSONObject(jsonData);
				String status = (String) rootObj.get("status");
				
				JSONArray notificationArr = (JSONArray) rootObj
						.get("notification");
				if (notificationArr != null
						&& status.equalsIgnoreCase("success")) {

					for (int index = 0; index < notificationArr.length(); index++) {
						announcement = new IGCAnnouncement();
						JSONObject notObject = notificationArr
								.getJSONObject(index);
						String notificationId = (String) notObject.get("id");
						announcement.setNotificationTitle((String) notObject
								.get("title"));
						announcement.setNotificationMessage((String) notObject
								.get("message"));
						announcement.setNotificationDate((String) notObject
								.get("date"));
						announcement.setNotificationId(notificationId);
						list.add(announcement);

					}
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return list;
	}
}
