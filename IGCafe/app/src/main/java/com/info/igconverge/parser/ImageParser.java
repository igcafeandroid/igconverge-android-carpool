
package com.info.igconverge.parser;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;



public class ImageParser {

	List<String> imageDetails = new ArrayList<String>();

	public ImageParser() {

	}

	public List<String> parseImageDetails(String jsonData) {
		if (jsonData != "")
			try {

				// JSONArray rootArray = new JSONArray(jsonData);
				// for (int index = 0; index < rootArray.length(); index++) {
				JSONObject eventObject = new JSONObject(jsonData);
				imageDetails.add(eventObject.getString("id"));
				imageDetails.add(eventObject.getString("status"));
				if (eventObject.has("image"))
					imageDetails.add(eventObject.getString("image"));
				// }
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return imageDetails;
	}
}
