package com.info.igconverge.Services;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class IGCDestinationDistanceParser 
{
	ArrayList<String> destinationDistance = new ArrayList<String>();
	
	public IGCDestinationDistanceParser() 
	{
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<String> distanceParser(String distanceJsonResult)
	{
		if(distanceJsonResult != null)
		{
			try 
			{
				JSONObject distanceJsonObject = new JSONObject(distanceJsonResult);
				JSONArray distanceJsonArray = distanceJsonObject.getJSONArray("rows");
				for(int index = 0; index < distanceJsonArray.length(); index++)
				{
					JSONObject distanceDetailObject = distanceJsonArray.getJSONObject(index);
					JSONArray distanceDetailArray = distanceDetailObject.getJSONArray("elements");
					for(int j_index = 0; j_index < distanceDetailArray.length(); j_index++)
					{
						JSONObject elementDetailObject = distanceDetailArray.getJSONObject(j_index);
						JSONObject distanceObject = elementDetailObject.getJSONObject("distance");
						destinationDistance.add(distanceObject.optString("text"));
					}
				}
					
			} catch (JSONException e) 
			{
				e.printStackTrace();
			}
		} else 
		{
			Log.e("ServiceHandler", "Couldn't get any data from the url");
		}
		return destinationDistance;
	}
}
