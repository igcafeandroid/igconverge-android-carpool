package com.info.igconverge.carpool;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.utility.IGCConstants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IGCCarpoolMainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IGCCarpoolMainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IGCCarpoolMainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    IGCDashboardActivity activity;
    private OnFragmentInteractionListener mListener;

    public IGCCarpoolMainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IGCCarpoolMainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IGCCarpoolMainFragment newInstance(String param1, String param2) {
        IGCCarpoolMainFragment fragment = new IGCCarpoolMainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActionBar();
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//        activity.actionBar.setDisplayHomeAsUpEnabled(true);
//        activity.lockNavigationDrawerSwipeGesture();
//        activity.actionBar.setBackgroundDrawable(getResources().getDrawable(
//                R.drawable.header_bg));
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActionBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_igccarpool_main, container, false);
        ViewPager viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new TabPagerAdapter(getContext(),getChildFragmentManager()));
        TabLayout tabLayout = (TabLayout)view. findViewById(R.id.sliding_tabs);

       tabLayout.setupWithViewPager(viewPager);
       // tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    private void initializeActionBar() {
        // TODO Auto-generated method stub
        activity.actionBar.setDisplayHomeAsUpEnabled(true);
        activity.actionBar.setHomeButtonEnabled(true);
        activity.actionBar.setDisplayShowHomeEnabled(true);
        activity.actionBar.setTitle(IGCConstants.CarPoolFragment);
        activity.unlockNavigationDrawerSwipeGesture();
        activity.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header_bg));
    }
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        this.activity = (IGCDashboardActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
