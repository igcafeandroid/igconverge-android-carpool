package com.info.igconverge.announcements;

public class FeedItem {
    private String headingNotify1;
    private String headingNotify2;
    private String headingNotify3;
	public FeedItem(String headingNotify1, String headingNotify2,
			String headingNotify3) {
		super();
		this.headingNotify1 = headingNotify1;
		this.headingNotify2 = headingNotify2;
		this.headingNotify3 = headingNotify3;
	}
	public FeedItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getHeadingNotify1() {
		return headingNotify1;
	}
	public void setHeadingNotify1(String headingNotify1) {
		this.headingNotify1 = headingNotify1;
	}
	public String getHeadingNotify2() {
		return headingNotify2;
	}
	public void setHeadingNotify2(String headingNotify2) {
		this.headingNotify2 = headingNotify2;
	}
	public String getHeadingNotify3() {
		return headingNotify3;
	}
	public void setHeadingNotify3(String headingNotify3) {
		this.headingNotify3 = headingNotify3;
	}
    
    
    
}