package com.info.igconverge.adapter;


import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.info.igconverge.R;
import com.info.igconverge.model.IGCHolidaysList;


public class IGCHolidaysListAdapter extends BaseAdapter {
	Context context;
	List<IGCHolidaysList> holidaysList;

	static class ViewholderItem {
		TextView headerTextView;
		TextView dayTextView;
		TextView dateTextView;
		TextView occasionTextView;
		TextView optionalTextView;
	}

	public IGCHolidaysListAdapter(Activity context,
			List<IGCHolidaysList> holidaysList) {
		this.context = context;
		this.holidaysList = holidaysList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewholderItem holder;
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(R.layout.holiday_list_items, parent, false);
			holder = new ViewholderItem();
			holder.headerTextView = (TextView) row.findViewById(R.id.header);
			holder.occasionTextView = (TextView) row
					.findViewById(R.id.occasion);
			holder.dayTextView = (TextView) row.findViewById(R.id.day);
			holder.dateTextView = (TextView) row.findViewById(R.id.date);
			holder.optionalTextView = (TextView) row
					.findViewById(R.id.optional);
			row.setTag(holder);
		} else {
			holder = (ViewholderItem) row.getTag();
		}
		

		Typeface myTypeface = Typeface.createFromAsset(context.getAssets(),
				"font/Roboto-Medium.ttf");
		holder.occasionTextView.setTypeface(myTypeface);

		Typeface myTypefaceLight = Typeface.createFromAsset(
				context.getAssets(), "font/Roboto-Light.ttf");
		holder.headerTextView.setTypeface(myTypefaceLight);
		holder.dayTextView.setTypeface(myTypefaceLight);
		holder.dateTextView.setTypeface(myTypefaceLight);

		if (holidaysList.get(position).getMonth() != null) {
			
			holder.headerTextView.setVisibility(View.VISIBLE);

			holder.headerTextView
					.setText(holidaysList.get(position).getMonth());
		} else
			holder.headerTextView.setVisibility(View.GONE);

		holder.occasionTextView.setText(holidaysList.get(position)
				.getOccasion());

		if (holidaysList.get(position).isOptional()) {
	
			holder.optionalTextView.setVisibility(View.VISIBLE);
			holder.optionalTextView.setText(R.string.optional);
			
		} else {
			holder.optionalTextView.setVisibility(View.INVISIBLE);
		}
		holder.dayTextView.setText(holidaysList.get(position).getDay() + " ");

		holder.dateTextView.setText(holidaysList.get(position).getDate());
		

		return row;

	}

	@Override
	public int getCount() {

		// TODO Auto-generated method stub
		return holidaysList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return holidaysList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
