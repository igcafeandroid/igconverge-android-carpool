package com.info.igconverge.dialogs;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCBellRingingActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ErrorDialog extends Dialog implements
android.view.View.OnClickListener {

	private Button relOkButton;
	private String dialogName;
	private String message;
	private String title;
	private Context context;
	private TextView dialogBoldText, dialogText1, dialogText2;
	private boolean finishActivity;

	public ErrorDialog(Context context, String dialogName) {
		super(context);
		this.context = context;
		this.dialogName = dialogName;

	}
	public ErrorDialog(Context context, String dialogName,String dialogTitle) {
		super(context);
		this.context = context;
		this.dialogName = dialogName;
		this.title=dialogTitle;
	}
	public ErrorDialog(Context context, String dialogName,
			boolean finishActivity) {
		super(context);
		this.context = context;
		this.dialogName = dialogName;
		this.finishActivity = finishActivity;
	}

	public ErrorDialog(Context context, String dialogName, String message,
			String title) {
		super(context);
		this.context = context;
		this.message = message;
		this.title = title;
		this.dialogName = dialogName;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		getWindow().getAttributes().windowAnimations = R.style.dialog_animation1;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_error);

		initializeControls();

	}

	private void initializeControls() {
		relOkButton = (Button) findViewById(R.id.relOkButton);
		relOkButton.setOnClickListener(this);
		dialogBoldText = (TextView) findViewById(R.id.dialogBoldText);
		dialogText1 = (TextView) findViewById(R.id.dialogText1);
		if (dialogName.equals("NO_INTERNET")) {
		//	dialogBoldText.setVisibility(View.GONE);
//			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//					new LayoutParams(LayoutParams.WRAP_CONTENT,
//							LayoutParams.WRAP_CONTENT));
//			params.setMargins(25, 120,25, 0);
//			dialogText1.setLayoutParams(params);
			dialogBoldText.setVisibility(View.VISIBLE);
			dialogBoldText.setText(R.string.no_internet_head);
			dialogText1.setText(R.string.no_internet_body);
		} else if (dialogName.equals("LIMIT_EXCEEDED")) {
			dialogBoldText.setText(R.string.limit_exceeded_header);
			dialogText1.setText(R.string.limit_exceeded_body);

		} else if (dialogName.equals("TIME_OUT")) {
			dialogBoldText.setText(R.string.time_out);
			dialogText1.setText(R.string.time_out_body);

		} 
		else if(dialogName.equals("SERVER_ERROR"))
			{
			dialogBoldText.setText(R.string.connection_error);
			dialogText1.setText(R.string.server_error);
			
			}else {
			if(title!=null)
			{
				dialogBoldText.setText(title);
				dialogText1.setText(dialogName);
			}
			else
			{
				dialogBoldText.setVisibility(View.GONE);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						new LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
				params.setMargins(25, 130,25, 0);
				dialogText1.setLayoutParams(params);
				// dialogText1.setMa
				// LayoutParams param=(LayoutParams) dialogText1.getLayoutParams();
				// param.setMargins(0, 20,0, 0);
				dialogText1.setText(dialogName);
			}
			
		}

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.relOkButton) {
			dismiss();
		
		}

	}
}
