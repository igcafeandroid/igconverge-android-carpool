package com.info.igconverge.Services;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.ByteArrayBuffer;

import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class ServiceHandler {

	public static String makeGetCallService(Context context, String url) {
		// string buffers the url
		StringBuffer buffer_string = new StringBuffer(url);
		String replyString = "";
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(buffer_string.toString());
		httpclient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
		httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
				30000);
		try {
			HttpResponse response = httpclient.execute(httpget);
			InputStream is = response.getEntity().getContent();
			BufferedInputStream bis = new BufferedInputStream(is);
			ByteArrayBuffer baf = new ByteArrayBuffer(20);
			int current = 0;
			while ((current = bis.read()) != -1) {
				baf.append((byte) current);
			}
			replyString = new String(baf.toByteArray());
		} catch (SocketTimeoutException bug) {
			
			ErrorDialog dialog = new ErrorDialog(context, "TIME_OUT");
			dialog.setCancelable(false);
			dialog.show();
		} catch (ConnectTimeoutException bug) {
			
			ErrorDialog dialog = new ErrorDialog(context, "TIME_OUT");
			dialog.setCancelable(false);
			dialog.show();

		} catch (Exception bug) {
			bug.printStackTrace();
		}

		// trim the whitespaces
		return replyString.trim();
	}

	public static String loginService(Context context, String url,
			String username, String password, String deviceType, String version) {
		InputStream inputStream = null;
		String result = "";

		// 1. create HttpClient
		HttpClient httpclient = new DefaultHttpClient();

		// 2. make POST request to the given URL
		HttpPost httpPost = new HttpPost(url.replaceAll(" ", ""));
		httpclient.getParams().setParameter(
				CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
		httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
				30000);
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("userName", username));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs
					.add(new BasicNameValuePair("deviceType", deviceType));
			nameValuePairs.add(new BasicNameValuePair("version", version));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// httpclient.execute(httpPost);
			HttpResponse httpResponse = httpclient.execute(httpPost);
			inputStream = httpResponse.getEntity().getContent();
			Header[] headers = httpResponse.getAllHeaders();
			for (Header header : headers) {
				if (header.getName().equalsIgnoreCase("Date")) {
					SimpleDateFormat formatter = new SimpleDateFormat(
							"EEE, dd MMM yyyy HH:mm:ss 'GMT'");
					IGCConstants.serverDate = formatter
							.parse(header.getValue());
					System.err.println(IGCConstants.serverDate);
				}

			}
			// 9. convert inputstream to string
			if (inputStream != null) {
				result = convertInputStreamToString(inputStream);
			} else {
				result = "";
			}
		} catch (Exception e) {
			result = "";
			e.printStackTrace();
		}
		return result;

	}

	

	public static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		// TODO Auto-generated method stub
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			stringBuilder.append(line);
		}
		inputStream.close();
		return stringBuilder.toString();
	}

	public void sendToServer(String token, Context context) {
	
		String empId = IGCSharedPreferences.getUserId(context);
	
		postToServer(IGCConstants.registerDevice, token, empId);

	}

	private String postToServer(String url, String token, String empId) {
		InputStream inputStream = null;
		String result = "";
		try {

			HttpClient httpclient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url.replaceAll(" ", ""));
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

			nameValuePairs.add(new BasicNameValuePair("deviceToken", token));
			nameValuePairs.add(new BasicNameValuePair("deviceType",
					IGCConstants.deviceType));
			nameValuePairs.add(new BasicNameValuePair("id", empId));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse httpResponse = httpclient.execute(httpPost);
			inputStream = httpResponse.getEntity().getContent();
			if (inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
