package com.info.igconverge.notification;

import com.info.igconverge.utility.IGCConstants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		String action = intent.getAction();
		if (action.equals("notification_cancelled")) {
			IGCConstants.notificationCount = 0;
			IGCConstants.notificationMessageList.clear();
			IGCConstants.eventCount = 0;
			IGCConstants.eventMessageList.clear();

		}
	}
}
