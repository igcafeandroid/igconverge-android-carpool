package com.info.igconverge.carpool;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by lab1 on 23/12/15.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    Context mContext;
    private Typeface myTypefaceLight;
    private String tabTitles[] = new String[] { "Find Ride", "My Rides"};
    public TabPagerAdapter(Context context,FragmentManager fm) {
        super(fm);
        mContext=context;
        myTypefaceLight = Typeface.createFromAsset(mContext.getAssets(), "font/Roboto-Light.ttf");

        // TODO Auto-generated constructor stub
    }
    @Override
    public Fragment getItem(int position) {
        // TODO Auto-generated method stub
        Fragment fragment = null;
        switch(position){
            case 0:   fragment = new IGCCarpoolFindrideFragment();
                break;
            case 1:
                fragment = new IGCCarpoolShare();
                break;


        }

        return fragment;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return PAGE_COUNT;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }


}

