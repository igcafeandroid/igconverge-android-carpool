package com.info.igconverge.dashboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.ApplicationErrorReport.CrashInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.Drive;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.IGCafeApplication.TrackerName;
import com.info.igconverge.announcements.IGCAnnouncementFragment;
import com.info.igconverge.bottomsheet.BottomSheet;

import com.info.igconverge.carpool.IGCCarpoolMainFragment;
import com.info.igconverge.carpool.IGCCarpoolShare;

import com.info.igconverge.contact.IGCContactFragment;
import com.info.igconverge.customcontrols.navigationdrawer.NavDrawerListAdapter;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.emergency.IGCEmergencyActivity;
import com.info.igconverge.events.IGCEventsFragment;
import com.info.igconverge.holidays.IGCShowHolidaysFragment;
import com.info.igconverge.info.IGCTermsConditionsActivity;
import com.info.igconverge.login.IGCLoginScreenActivity;
import com.info.igconverge.model.IGCEmployeeDetails;
import com.info.igconverge.model.IGCNavDrawerItem;
import com.info.igconverge.model.IGCWeatherDetails;
import com.info.igconverge.notification.RegistrationIntentService;
import com.info.igconverge.utility.CustomGestureDetector;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCImageUtility;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;
import com.info.igconverge.utility.SimpleTwoFingerDoubleTapDetector;
import com.info.igconverge.weather.IGCWeather;
import com.info.igconverge.weather.WeatherService;
import com.instabug.library.Instabug;
import com.instabug.library.Instabug.IBGInvocationEvent;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.Services.ParseServiceRequest;
import com.info.igconverge.Services.ServiceHandler;

public class IGCDashboardActivity extends ActionBarActivity implements
		View.OnClickListener, IGCAsyncServiceRequest.OnAsyncRequestComplete {

	private static final int UPLOAD_FROM_GALLARY = 1;
	private InterstitialAd mInterstitialAd;
	ProgressBar updateProgressBarinfo;
	String mediaFile = null;
	public static final int MEDIA_TYPE_IMAGE = 1;
	private Toolbar toolbar;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private LinearLayout weatherLayout;

	String[] contactResources;
	public ActionBarDrawerToggle mDrawerToggle;
	FragmentManager fragmentManager;
	private CharSequence mDrawerTitle;

	private CharSequence mTitle;
	View header;

	View footerView;
	private String stringUpdateMessage;
	private String stringUpdateTitle;
	private String stringUpdateUrl;
	private Boolean isUpdateAvailable;
	Typeface myTypefaceMedium, myTypefaceRegular;

	private String[] navMenuTitles;
	private TypedArray navMenuTitlesIcons;
	EditText userNameInput, passwordInput;
	private ArrayList<IGCNavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	TextView userNameDrawerText, logout, info, signInClick, welcomeMessage,
			weatherText;
	TextView check;
	ArrayList<NameValuePair> params;

	ImageView profileImage;

	private static final int REQUEST_RESOLVE_ERROR = 1001;

	private static final String DIALOG_ERROR = "dialog_error";
	BottomSheet bottomSheetDialog;
	Activity activity;
	ProgressBar progressBar, progressBar2;
	View weatherView;
	IGCCheckNetworkConnectivity conn;
	private PopupWindow pwindo;
	Button btnClosePopup;
	public int screenWidth, screenHeight;
	public ActionBar actionBar;
	IGCWeatherDetails weatherDetails;
	TextView weatherCondition, tempValue;
	private GestureDetector mGestureDetector;

	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

	com.info.igconverge.UIControls.TextViewClimaconsRegular weatherLogo;
	Fragment currentFragment;
	boolean isServiceRunning = true;
	File capturedImageFile;
	boolean isNoidaLocation = false;
	Bitmap galleryImageBitmap, cameraImageBitmap;
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private BroadcastReceiver mRegistrationBroadcastReceiver;
	int count = 0;
	private GestureDetector gd;

	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			Bundle bundle = intent.getBundleExtra("weather");
			isNoidaLocation = bundle.getBoolean("isNoidaLocation");

			weatherDetails = bundle.getParcelable("weather");
			String noidaText = getResources().getString(R.string.weather_noida);
			if (weatherDetails != null) {
				progressBar.setVisibility(View.GONE);
				weatherLayout.setVisibility(View.VISIBLE);
				tempValue.setText(weatherDetails.getCurrentTemp());

				weatherLogo.setText(weatherDetails.getWeatherImageText());
				weatherCondition.setText(weatherDetails.getClimateCondition());
				weatherView.setVisibility(View.VISIBLE);
				weatherText.setVisibility(View.VISIBLE);
				isServiceRunning = false;
				if (isNoidaLocation) {
					weatherText.setText(noidaText);
				}

			} else {
				progressBar.setVisibility(View.GONE);
				weatherText.setVisibility(View.INVISIBLE);
				weatherView.setVisibility(View.INVISIBLE);
			}
		}
	};

	@Override
	protected void onStart() {
		getInstanceIdToken();

		IGCafeApplication.mSensorManager
				.registerListener(IGCafeApplication.mShakeDetector,
						IGCafeApplication.mAccelerometer,
						SensorManager.SENSOR_DELAY_UI);

		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver, new IntentFilter("weatherFinder"));
		String email = IGCSharedPreferences.getEmail(this);

		Instabug.getInstance().setDefaultEmail(email)
				.setInvocationEvent(IBGInvocationEvent.IBGInvocationEventNone);

		super.onStart();

		/*
		 * Google Analytics
		 */
		GoogleAnalytics.getInstance(IGCDashboardActivity.this)
				.reportActivityStart(this);

		CustomGestureDetector customGestureDetector = new CustomGestureDetector();

		mGestureDetector = new GestureDetector(this, customGestureDetector);

		mGestureDetector.setOnDoubleTapListener(customGestureDetector);

	}

	private void loadProfilePic() {
		if (IGCSharedPreferences.getProfileImage(getApplicationContext()) != null
				&& IGCSharedPreferences
						.getProfileImage(getApplicationContext()).length() > 0) {
			try {

				File imageFile = new File(
						IGCSharedPreferences
								.getProfileImage(getApplicationContext()));
				Uri imageUri = Uri.fromFile(imageFile);
				if (imageUri != null && imageFile != null && imageFile.exists()) {

					settingProfilePic(imageUri, imageFile);

				} else {
					profileImage.setImageResource(R.drawable.avatar_pic);
				}

			} catch (Exception e) {

				e.printStackTrace();
			}

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		activity = this;
		if (IGCEmergencyActivity.activity != null)
			IGCEmergencyActivity.activity.finish();
		setContentView(R.layout.drawer_list_mainactivity);
		googleAnalyticsReport();

		Intent intent = new Intent(this, WeatherService.class);
		startService(intent);

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		initialiseViews();
		initialiseResources();

		screenWidth = this.getResources().getDisplayMetrics().widthPixels;
		screenHeight = this.getResources().getDisplayMetrics().heightPixels;
		mTitle = mDrawerTitle = getTitle();
		addActionBar();

		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						displayView(position - 1);

					}
				});
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
				R.string.app_name, R.string.app_name) {
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				supportInvalidateOptionsMenu();
				if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {

					bottomSheetDialog.dismiss();

				}
			}

			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(mDrawerTitle);
				supportInvalidateOptionsMenu();
				hideKeyboard();
			}

			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				super.onDrawerSlide(drawerView, slideOffset);
				mDrawerLayout.bringChildToFront(drawerView);
				mDrawerLayout.requestLayout();
				if (bottomSheetDialog != null && bottomSheetDialog.isShowing()) {
					bottomSheetDialog.dismiss();

				}

			}
		};
		if (savedInstanceState == null) {
			mDrawerToggle.setDrawerIndicatorEnabled(false);
			if (getIntent() != null) {
				String type = getIntent().getStringExtra("type");
				if (type != null) {
					if (type.equalsIgnoreCase("0")) {
						displayView(2);
						count = IGCConstants.notificationCount;
						IGCConstants.notificationCount = 0;
						IGCConstants.displayCount = count;

						IGCConstants.notificationMessageList.clear();
					}
					else if (type.equalsIgnoreCase("2")) {
						displayView(4);
						count = IGCConstants.carPoolCount;
						IGCConstants.carPoolCount = 0;
						IGCConstants.displayCount = count;

						IGCConstants.carPoolNotificationMessageList.clear();
					}else {
						displayView(1);
						count = IGCConstants.eventCount;
						IGCConstants.eventCount = 0;
						IGCConstants.eventMessageList.clear();

					}
				} else {
					if (!IGCSharedPreferences.isPREF_INSTALL_FLAG(getApplicationContext())) {

						displayView(7);

					} else {
						displayView(0);

					}
					mDrawerLayout.openDrawer(Gravity.LEFT);

				}
			} else {
				if (!IGCSharedPreferences.isPREF_INSTALL_FLAG(getApplicationContext())) {

					displayView(7);
				} else {
					displayView(0);

				}
				mDrawerLayout.openDrawer(Gravity.LEFT);

			}
		}
		mDrawerToggle.syncState();
		mDrawerToggle.setDrawerIndicatorEnabled(true);
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle

		.setToolbarNavigationClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (getSupportFragmentManager().getBackStackEntryCount() > 1) {

					getSupportFragmentManager().popBackStack();
					mDrawerToggle.setDrawerIndicatorEnabled(false);

				} else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {

					getSupportFragmentManager().popBackStack();

					mDrawerToggle.setDrawerIndicatorEnabled(true);

				} else {

					mDrawerToggle.setDrawerIndicatorEnabled(true);

				}

				mDrawerLayout
						.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

			}
		});

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				if (isServiceRunning) {
					try {
						Intent intent = new Intent(activity,
								WeatherService.class);
						stopService(intent);

						if (weatherView != null) {
							weatherView.setVisibility(View.INVISIBLE);
						}
						if (progressBar.getVisibility() == View.VISIBLE)
							progressBar.setVisibility(View.INVISIBLE);

						if (weatherText != null)
							weatherText.setVisibility(View.INVISIBLE);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}, 120000);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.weatherLayout:
			Fragment fragment = new IGCWeather();
			Bundle bundle = new Bundle();
			bundle.putParcelable("weather", weatherDetails);
			fragment.setArguments(bundle);

			fragmentManager = getSupportFragmentManager();
			fragmentManager
					.beginTransaction()
					.replace(R.id.frame_container, fragment,
							IGCConstants.WeatherFragmentName).commit();
			fragmentManager.executePendingTransactions();

			mDrawerLayout.closeDrawer(mDrawerList);
			setTitle("Weather");
			currentFragment = fragment;
			break;
		}
	}

	@Override
	protected void onResume() {

		super.onResume();
		IGCafeApplication.mSensorManager
				.registerListener(IGCafeApplication.mShakeDetector,
						IGCafeApplication.mAccelerometer,
						SensorManager.SENSOR_DELAY_UI);

		loadProfilePic();
		IGCafeApplication.activityResumed();
	}

	@Override
	protected void onStop() {
		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver, new IntentFilter("speedExceeded"));
		super.onStop();

		GoogleAnalytics.getInstance(IGCDashboardActivity.this)

		.reportActivityStop(this);

	}

	private void openLoginWindow() {

		if (!IGCConstants.userLoggedIn) {

			mDrawerLayout.closeDrawer(mDrawerList);

		}
	}

	private void initialiseViews() {
		conn = new IGCCheckNetworkConnectivity();

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		mDrawerLayout.setDrawerShadow(R.color.transparent, GravityCompat.START);

		header = View.inflate(IGCDashboardActivity.this,
				R.layout.custom_drawer_header, null);

		mDrawerList.addHeaderView(header, null, false);

		footerView = View.inflate(IGCDashboardActivity.this,
				R.layout.custom_drawer_footer, null);
		mDrawerList.addFooterView(footerView, null, false);
		userNameDrawerText = (TextView) header.findViewById(R.id.usernameText);
		logout = (TextView) footerView.findViewById(R.id.drawerLogout);
		info = (TextView) footerView.findViewById(R.id.drawerInfo);

		profileImage = (ImageView) header.findViewById(R.id.employeeLoginImage);
		weatherLayout = (LinearLayout) header.findViewById(R.id.weatherLayout);
		tempValue = (TextView) header.findViewById(R.id.temperatureValue);
		if (IGCSharedPreferences.getProfileName(getApplicationContext()) != null
				&& IGCSharedPreferences.getProfileName(getApplicationContext())
						.length() > 0) {
			userNameDrawerText.setText(IGCSharedPreferences
					.getProfileName(getApplicationContext()));
		}

		weatherLogo = (com.info.igconverge.UIControls.TextViewClimaconsRegular) header
				.findViewById(R.id.weatherLogo);
		weatherCondition = (TextView) header
				.findViewById(R.id.weatherCondition);
		weatherText = (TextView) header.findViewById(R.id.weatherText);
		weatherView = header.findViewById(R.id.drawerheader_line);
		progressBar = (ProgressBar) header.findViewById(R.id.progressBar);
		progressBar.getIndeterminateDrawable().setColorFilter(
				getResources().getColor(R.color.orange),
				android.graphics.PorterDuff.Mode.SRC_IN);

		weatherLayout.setOnClickListener(this);

		myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Medium.ttf");
		userNameDrawerText.setTypeface(myTypefaceMedium);
		myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");
		weatherText.setTypeface(myTypefaceRegular);

		tempValue.setTypeface(myTypefaceMedium);
		weatherCondition.setTypeface(myTypefaceMedium);
		logout.setTypeface(myTypefaceMedium);
		info.setTypeface(myTypefaceMedium);
		profileImage.setImageResource(R.drawable.avatar_pic);
		profileImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				bottomSheetDialog = new BottomSheet.Builder(
						IGCDashboardActivity.this).title("Profile Pic")
						.sheet(R.menu.bottom_sheet_grid)
						.listener(new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								switch (which) {
								case R.id.snapPhoto:
									try {

										Intent intent = new Intent(
												MediaStore.ACTION_IMAGE_CAPTURE);
										String timeStamp = new SimpleDateFormat(
												"yyyyMMdd_HHmmss", Locale
														.getDefault())
												.format(new Date());
										capturedImageFile = new File(
												android.os.Environment
														.getExternalStorageDirectory(),
												timeStamp + ".jpg");
										intent.putExtra(
												MediaStore.EXTRA_OUTPUT,
												Uri.fromFile(capturedImageFile));

										startActivityForResult(intent,
												CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

									} catch (Exception exception) {
										exception.printStackTrace();
									}
									break;
								case R.id.uploadPhoto:
									try {

										Intent intent = new Intent(
												Intent.ACTION_PICK,
												android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
										intent.setType("image/*");

										startActivityForResult(Intent
												.createChooser(intent,
														"Select File"),
												UPLOAD_FROM_GALLARY);

									} catch (Exception exception) {
										exception.printStackTrace();
									}
									break;
								case R.id.deletePhoto:
									try {
										if (IGCSharedPreferences
												.getProfileImage(getApplicationContext()) != null
												&& IGCSharedPreferences
														.getProfileImage(
																getApplicationContext())
														.length() != 0) {
											showConfirmationDeletePicDialog();

										}

									} catch (Exception exception) {
										exception.printStackTrace();
									}

									break;
								}
							}
						}).show();

			}
		});
		logout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

					String deviceToken = "";
					if (IGCSharedPreferences.getPREF_DEVICEID(activity) != null) {
						deviceToken = IGCSharedPreferences
								.getPREF_DEVICEID(activity);
						Log.d("***TOKEN", "" + deviceToken);
					}

					String deviceType = "android";
					IGCAsyncServiceRequest getPosts = new IGCAsyncServiceRequest(
							activity, "POST", ParseServiceRequest
									.getLogoutParams(deviceToken, deviceType));
					getPosts.execute(IGCConstants.logoutURL);
				} else {
					ErrorDialog dialog = new ErrorDialog(activity,
							"NO_INTERNET");
					dialog.setCancelable(false);
					dialog.show();
				}

			}
		});

		info.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showAppInfoDialog();

			}
		});

	}

	public void showConfirmationDeletePicDialog() {

		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(this);
		dialogBuilder
				.setTitleViewGone(true)
				.withTitle("My Dialog")

				.withTitleColor("#FFFFFF")

				.withDividerColor("#11000000")

				.withMessage("This is a modal Dialog.")

				.setMessageViewGone(true)
				.withMessageColor("#FFFFFFFF")

				.withDialogColor(Color.TRANSPARENT)

				.isCancelableOnTouchOutside(false)

				.withDuration(300)

				.withEffect(Effectstype.Slidetop)
				.setCustomView(R.layout.delete_image_dialog,
						getApplicationContext()).show();

		TextView titleMessage = (TextView) dialogBuilder
				.findViewById(R.id.titleMessage);
		TextView deleteMessage = (TextView) dialogBuilder
				.findViewById(R.id.deleteMessage);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okDeletePhoto);
		TextView cancelDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.cancelDeletePhoto);
		titleMessage.setTypeface(myTypefaceRegular);
		deleteMessage.setText(getResources().getString(R.string.deleteprofile));
		titleMessage.setText("Remove Photo!");
		deleteMessage.setVisibility(View.VISIBLE);
		cancelDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();

			}
		});
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				IGCSharedPreferences.setProfileImage(getApplicationContext(),
						null);
				profileImage.setImageResource(R.drawable.avatar_pic);
				dialogBuilder.dismiss();

			}
		});

	}

	private void initialiseResources() {
		screenWidth = this.getResources().getDisplayMetrics().widthPixels;
		screenHeight = this.getResources().getDisplayMetrics().heightPixels;
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuTitlesIcons = getResources().obtainTypedArray(
				R.array.nav_drawer_items_icons);
		navDrawerItems = new ArrayList<IGCNavDrawerItem>();

		for (int i = 0; i < navMenuTitles.length; i++) {
			navDrawerItems.add(new IGCNavDrawerItem(navMenuTitles[i],
					navMenuTitlesIcons.getResourceId(i, -1)));
		}
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

	}

	private void addActionBar() {
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(
				R.color.greycolor));
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.header_bg));

	}

	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;

		switch (position) {
		case 0:
			fragment = new IGCMenuFragment();
			if (currentFragment instanceof IGCMenuFragment) {
				// mDrawerLayout.closeDrawer(mDrawerPane);
				mDrawerLayout.closeDrawer(mDrawerList);

			} else {
				replaceFragment(position, fragment,
						IGCConstants.MenuFragmentName);

			}
			break;
		case 1:
			fragment = new IGCEventsFragment();
			replaceFragment(position, fragment, IGCConstants.EventsFragmentName);
			break;
		case 2:
			fragment = new IGCAnnouncementFragment();
			replaceFragment(position, fragment,
					IGCConstants.AnnouncementsFragment);
			break;

		case 3:
			fragment = new IGCPartyRadarFragment();
			replaceFragment(position, fragment,
					IGCConstants.PartyRadarFragmentName);
			break;
		case 4:
				fragment = new IGCCarpoolMainFragment();
				replaceFragment(position, fragment,
						IGCConstants.CarpoolFragmentName);
			break;
		case 5:
			fragment = new IGCShowHolidaysFragment();
			replaceFragment(position, fragment,
					IGCConstants.HolidaysFragmentName);
			break;

		case 6:
			fragment = new IGCContactFragment();
			replaceFragment(position, fragment,
					IGCConstants.ContactFragmentName);
			break;

		case 7:
			fragment = new IGCWalkthroughFragment();
			replaceFragment(position, fragment,
					IGCConstants.WalkthroughFragmentName);
			break;
		case 8:
			mDrawerLayout.closeDrawer(mDrawerList);
			Intent i = new Intent(getApplicationContext(), IGCEmergencyActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(i);
			adapter.setSelectedPosition(position);
			break;
		case 9:
			mDrawerLayout.closeDrawer(mDrawerList);
			adapter.setSelectedPosition(position);
			if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
				Instabug.getInstance().invoke();

			} else {
				ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
				dialog.setCancelable(false);
				dialog.show();
			}

			break;

		default:
			break;
		}

	}

	private void replaceFragment(int position, Fragment fragment,
			String fragmentName) {

		if (fragment != null) {

			fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment, fragmentName)
					.commit();
			fragmentManager.executePendingTransactions();

			mDrawerList.setItemChecked(position, true);
			adapter.setSelectedPosition(position);

			if (!IGCSharedPreferences
					.isPREF_INSTALL_FLAG(getApplicationContext())) {
				IGCSharedPreferences.setPREF_INSTALL_FLAG(
						getApplicationContext(), true);
				mDrawerList.setSelection(0);

			} else
				mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);

			mDrawerLayout.closeDrawer(mDrawerList);

			// }
		} else {

			Log.e("MainActivity", "Error in creating fragment");
		}
		currentFragment = fragment;
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {
		if (getSupportFragmentManager() != null) {
			if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
				getSupportFragmentManager().popBackStack();
				mDrawerToggle.setDrawerIndicatorEnabled(false);

			} else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {

				super.onBackPressed();
				mDrawerToggle.setDrawerIndicatorEnabled(true);

			} else {

				if (currentFragment instanceof IGCMenuFragment) {
					// super.onBackPressed();
					finish();
				} else {
					displayView(0);
				}

				mDrawerToggle.setDrawerIndicatorEnabled(true);

			}
		}

	}

	public void lockNavigationDrawerSwipeGesture() {
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	}

	public void unlockNavigationDrawerSwipeGesture() {
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
	}

//	public void dialCall(View v) {
//		Intent callIntent = new Intent(Intent.ACTION_CALL);
//		callIntent.setData(Uri.parse("tel:" + 123456));
//		activity.startActivity(callIntent);
//
//	}

	@Override
	public boolean onSupportNavigateUp() {

		getSupportFragmentManager().popBackStack();
		mDrawerToggle.setDrawerIndicatorEnabled(false);
		return true;
	}

	public void showAppInfoDialog() {
		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(this);
		dialogBuilder
				.setTitleViewGone(true)
				.withTitle("My Dialog")

				.withTitleColor("#FFFFFF")

				.withDividerColor("#11000000")

				.withMessage("This is a modal Dialog.")

				.setMessageViewGone(true)
				.withMessageColor("#FFFFFFFF")

				.withDialogColor(Color.TRANSPARENT)

				.isCancelableOnTouchOutside(false)

				.withDuration(300)

				.withEffect(Effectstype.Slidetop)
				.setCustomView(R.layout.infodialog_custom_view,
						getApplicationContext()).show();
		final FrameLayout updateButton = (FrameLayout) dialogBuilder
				.findViewById(R.id.update);
		check = (TextView) dialogBuilder.findViewById(R.id.checkButton);
		TextView versionAppInfo = (TextView) dialogBuilder
				.findViewById(R.id.versionAppInfo);
		try {
			versionAppInfo
					.setText(getResources().getString(R.string.version)
							+ ": "
							+ getPackageManager().getPackageInfo(
									getPackageName(), 0).versionName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		updateProgressBarinfo = (ProgressBar) dialogBuilder
				.findViewById(R.id.progressBar);
		updateProgressBarinfo.getIndeterminateDrawable().setColorFilter(
				getResources().getColor(R.color.orange),
				android.graphics.PorterDuff.Mode.SRC_IN);
		updateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (IGCCheckNetworkConnectivity.checkConnection(activity)) {

					check.setText("Checking ...");
					updateProgressBarinfo.setVisibility(View.VISIBLE);
					UpdateTask updateTask = new UpdateTask();
					updateTask.execute(IGCConstants.checkupdate);

				} else {

					ErrorDialog dialog = new ErrorDialog(activity,
							"NO_INTERNET");
					dialog.setCancelable(false);
					dialog.show();

				}

			}
		});

		Button termsButton = (Button) dialogBuilder
				.findViewById(R.id.termsInfoButton);
		termsButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity,
						IGCTermsConditionsActivity.class);
				intent.putExtra("showButton", "false");
				startActivity(intent);

			}
		});
		ImageButton closeButton = (ImageButton) dialogBuilder
				.findViewById(R.id.closebutton);
		closeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
			}
		});
	}

	public void googleAnalyticsReport() {
		Tracker t = ((IGCafeApplication) getApplication())
				.getTracker(TrackerName.APP_TRACKER);
		t.setScreenName("IGC Dashboard Screen");

		if (IGCSharedPreferences.getUserName(this) != null) {

			t.send(new HitBuilders.EventBuilder()
					.setCategory(IGCSharedPreferences.getUserName(this))
					.setAction("LoggedIn").setLabel("UserName").build());
		} else
			t.send(new HitBuilders.AppViewBuilder().build());
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		IGCafeApplication.mSensorManager
				.unregisterListener(IGCafeApplication.mShakeDetector);
		IGCafeApplication.activityPaused();

	}

	@SuppressLint("NewApi")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:

			if (resultCode == RESULT_OK) {
				try {
					if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {

						try {
							if (capturedImageFile != null) {
								Uri selectedImageUri = Uri
										.fromFile(capturedImageFile);

								IGCSharedPreferences.setProfileImage(
										getApplicationContext(),
										capturedImageFile.toString());

								settingProfilePic(selectedImageUri,
										capturedImageFile);

							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			} else if (resultCode == RESULT_CANCELED) {

			} else {
				Toast.makeText(this, "Failed to snap a photo",
						Toast.LENGTH_LONG).show();
			}
			break;

		case UPLOAD_FROM_GALLARY:
			if (resultCode == RESULT_OK) {
				if (requestCode == UPLOAD_FROM_GALLARY) {
					if (data != null) {
						Uri uri = data.getData();

						String selectedImagePath = IGCUtils.getRealPathFromURI(
								activity, uri);

						if (selectedImagePath != null) {
							File imageFile = new File(selectedImagePath);
							if (imageFile != null) {
								// previewCapturedImage(uri, imageFile);
								IGCSharedPreferences.setProfileImage(
										getApplicationContext(),
										imageFile.toString());

								settingProfilePic(uri, imageFile);

							}
						}
					}
				}
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void settingProfilePic(Uri uri, File imageFile) {

		int sizeRequired = IGCImageUtility.checkProfilePicSize(activity);
		Bitmap newBitmapSize = IGCImageUtility.getRoundedRectBitmap(
				IGCImageUtility.decodeFile(imageFile, sizeRequired,
						sizeRequired), sizeRequired / 2, sizeRequired,
				sizeRequired);

		Matrix matrix = new Matrix();
		matrix.postRotate(IGCImageUtility.getImageOrientation(imageFile));
		Bitmap rotatedBitmap = Bitmap.createBitmap(newBitmapSize, 0, 0,
				newBitmapSize.getWidth(), newBitmapSize.getHeight(), matrix,
				true);

		profileImage.setImageBitmap(rotatedBitmap);
		profileImage.setAdjustViewBounds(true);
	}

	private String getPathFromUri(Uri uri, Activity activity) {

		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = activity.getContentResolver().query(uri, proj, null, null,
					null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

	}

	private class UpdateTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {
			String updateStatus = checkUpdateService(params[0]);
			return updateStatus;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (parseUpdateServiceResult(result)) {
				if (isUpdateAvailable) {
					showUpdateDialog();
				} else {
					Toast.makeText(getApplicationContext(),
							"No Update Available", Toast.LENGTH_LONG).show();
				}
			} else {
				ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
				dialog.setCancelable(false);
				dialog.show();

			}

			check.setText("Check For Update");
			updateProgressBarinfo.setVisibility(View.GONE);

		}
	}

	private boolean parseUpdateServiceResult(String result) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(result);

			if (jsonObject != null) {
				if (!jsonObject.isNull("isUpdate")) {
					isUpdateAvailable = jsonObject.getBoolean("isUpdate");
				}
				if (!jsonObject.isNull("title")) {
					stringUpdateTitle = jsonObject.getString("title");
				}
				if (!jsonObject.isNull("message")) {
					stringUpdateMessage = jsonObject.getString("message");
				}
				if (!jsonObject.isNull("url")) {
					stringUpdateUrl = jsonObject.getString("url");
				}
				return true;

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}

	private String checkUpdateService(String url) {
		InputStream inputStream = null;
		String result = "";
		try {

			HttpClient httpclient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url.replaceAll(" ", ""));
			try {

				String version = getPackageManager().getPackageInfo(
						getPackageName(), 0).versionName;
				try {
					version = getPackageManager().getPackageInfo(
							getPackageName(), 0).versionName;
				} catch (NameNotFoundException e) {
					Log.e("tag", e.getMessage());
				}

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("version", version));
				nameValuePairs.add(new BasicNameValuePair("deviceType",
						"android"));
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				HttpResponse httpResponse = httpclient.execute(httpPost);
				inputStream = httpResponse.getEntity().getContent();

				if (inputStream != null) {
					result = convertInputStreamToStringDeleteAlarm(inputStream);
				}

				else {
					result = "Did not work!";
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				result = "";
			}

		} catch (Exception e) {
			result = "";
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	private String convertInputStreamToStringDeleteAlarm(InputStream inputStream)
			throws IOException {
		// TODO Auto-generated method stub
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			stringBuilder.append(line);
		}
		inputStream.close();
		return stringBuilder.toString();
	}

	public void getInstanceIdToken() {
		if (checkPlayServices()) {
			Intent intent = new Intent(this, RegistrationIntentService.class);
			startService(intent);
		}
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {

				finish();
			}
			return false;
		}
		return true;
	}

	public void showUpdateDialog() {
		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(this);
		dialogBuilder.setTitleViewGone(true).withTitle("My Dialog")
				// .withTitle(null) no title
				.withTitleColor("#FFFFFF")
				// def
				.withDividerColor("#11000000")
				// def
				.withMessage("This is a modal Dialog.")
				// .withMessage(null) no Msg
				.setMessageViewGone(true).withMessageColor("#FFFFFFFF")
				// def | withMessageColor(int resid)
				.withDialogColor(Color.TRANSPARENT)
				// def | withDialogColor(int resid) //def
				.isCancelableOnTouchOutside(false)
				// def | isCancelable(true)
				.withDuration(300)
				// def
				.withEffect(Effectstype.Slidetop)
				.setCustomView(R.layout.delete_image_dialog,
						getApplicationContext()).show();
		TextView deleteMessage = (TextView) dialogBuilder
				.findViewById(R.id.deleteMessage);
		TextView titleMessage = (TextView) dialogBuilder
				.findViewById(R.id.titleMessage);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okDeletePhoto);
		TextView cancelDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.cancelDeletePhoto);
		titleMessage.setTypeface(myTypefaceMedium);
		deleteMessage.setTypeface(myTypefaceRegular);
		okDeletePhoto.setText("Update");
		titleMessage.setVisibility(View.VISIBLE);
		titleMessage.setText(stringUpdateTitle);
		deleteMessage.setText(stringUpdateMessage);
		cancelDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
			}
		});
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (stringUpdateUrl != null && stringUpdateUrl.length() > 0) {
					Uri uri;
					if (stringUpdateUrl.contains("http")) {
						uri = Uri.parse(stringUpdateUrl);
					} else {
						stringUpdateUrl = "http://" + stringUpdateUrl;
						uri = Uri.parse(stringUpdateUrl);
					}

					Update installUpdate = new Update();
					installUpdate.execute(uri.toString());
				}
				dialogBuilder.dismiss();

			}
		});

	}

	@Override
	public void asyncResponse(String response) {

		if (response != null && response.length() > 0) {
			try {
				JSONObject objects = new JSONObject(response);

				String logoutStatus = null, logoutMessgae;
				if (!objects.isNull("status")) {
					logoutStatus = objects.getString("status");

				}
				if (!objects.isNull("message")) {
					logoutMessgae = objects.getString("message");

				}
				if (logoutStatus != null && !logoutStatus.equals("failure")) {
					performlogoutAction();

				}
			} catch (JSONException e) {

				e.printStackTrace();
				ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
				dialog.setCancelable(false);
				dialog.show();

			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
			dialog.setCancelable(false);
			dialog.show();

		}

	}

	private void performlogoutAction() {
		// TODO Auto-generated method stub
		IGCConstants.employeeDetailObject = new IGCEmployeeDetails();
		if (IGCSharedPreferences.getUserName(getApplicationContext()) != null
				&& IGCSharedPreferences.getUserName(getApplicationContext())
						.length() > 0) {
			IGCSharedPreferences sharedPref = new IGCSharedPreferences();

			IGCSharedPreferences.setUserName(getApplicationContext(), null);
			IGCSharedPreferences.setPassword(getApplicationContext(), null);
			IGCSharedPreferences.setUserId(getApplicationContext(), null);
			IGCSharedPreferences.setPREF_INSTALL_FLAG(getApplicationContext(),
					false);
			IGCSharedPreferences.setFirstName(getApplicationContext(), null);
			IGCSharedPreferences.setLastName(getApplicationContext(), null);
			IGCSharedPreferences.setMiddleName(getApplicationContext(), null);
			IGCSharedPreferences.setProfileName(getApplicationContext(), null);
			IGCSharedPreferences.setProfileImage(getApplicationContext(), null);
			IGCSharedPreferences.setPREF_ANNOUNCEMENT_ID_LIST(null, activity);
			Intent intent = new Intent(activity, IGCLoginScreenActivity.class);
			startActivity(intent);
			finish();

		}

	}

	private void hideKeyboard() {

		View view = activity.getCurrentFocus();
		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);

		imm.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private class Update extends AsyncTask<String, String, String> {

		private ProgressDialog pDialogUpdate;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialogUpdate = new ProgressDialog(activity);
			pDialogUpdate.setMessage("Downloading update..");

			pDialogUpdate.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

			pDialogUpdate.setProgress(0);

			pDialogUpdate.show();

		}

		@Override
		protected String doInBackground(String... urls) {
			String mediaFile = updateDownloadService(getApplicationContext(),
					urls[0].toString());
			return mediaFile;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pDialogUpdate != null && pDialogUpdate.isShowing()) {
				pDialogUpdate.dismiss();
			}
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(Environment
					.getExternalStorageDirectory() + "/download/" + result)),
					"application/vnd.android.package-archive");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();

		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values[0]);

			pDialogUpdate.setProgress(Integer.parseInt(values[0]));
		}

		public String updateDownloadService(Context context,
				String urlserviceUpdate) {
			String mediaFile = "igConverge.apk";
			try {

				String PATH = Environment.getExternalStorageDirectory()
						+ "/download/";
				File file = new File(PATH);
				file.mkdirs();
				// Create a file on the external storage under download
				File outputFile = new File(file, mediaFile);
				FileOutputStream fos = new FileOutputStream(outputFile);

				HttpGet m_httpGet = null;
				HttpResponse m_httpResponse = null;

				// Create a http client with the parameters
				HttpClient m_httpClient = new DefaultHttpClient();
				String result = null;

				try {

					// Create a get object
					m_httpGet = new HttpGet(urlserviceUpdate);

					// Execute the html request
					m_httpResponse = m_httpClient.execute(m_httpGet);
					HttpEntity entity = m_httpResponse.getEntity();

					// See if we get a response
					if (entity != null) {

						InputStream instream = entity.getContent();
						long lengthOfFile = entity.getContentLength();
						byte[] buffer = new byte[1024];

						// Write out the file
						int len1 = 0;

						long total = 0;
						while ((len1 = instream.read(buffer)) != -1) {
							total += len1;
							publishProgress(""
									+ (int) ((total * 100) / lengthOfFile));

							fos.write(buffer, 0, len1);
						}
						fos.close();
						instream.close();
					}

				} catch (ConnectTimeoutException cte) {
					Toast.makeText(getApplicationContext(), "Update error!",
							Toast.LENGTH_LONG).show();

				} catch (Exception e) {

				} finally {
					m_httpClient.getConnectionManager()
							.closeExpiredConnections();
				}

			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Update error!",
						Toast.LENGTH_LONG).show();
			}

			return mediaFile;
		}

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		mGestureDetector.onTouchEvent(event);

		return super.dispatchTouchEvent(event);
	}

}
