package com.info.igconverge.carpool;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by naina on 26/10/15.
 */
public class IGCPlaceDetailsJsonParser {

    /** Receives a JSONObject and returns a list */
    public List<HashMap<String,String>> parse(JSONObject jObject){

        Double lat = Double.valueOf(0);
        Double lng = Double.valueOf(0);
String name=null;
        HashMap<String, String> hm = new HashMap<String, String>();
        List<HashMap<String, String>> list = new ArrayList<HashMap<String,String>>();

        try {

            lat = (Double)jObject.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").get("lat");
            lng = (Double)jObject.getJSONObject("result").getJSONObject("geometry").getJSONObject("location").get("lng");
            if(jObject.getJSONObject("result").getString("formatted_address")!=null)
            {
                name = (String)jObject.getJSONObject("result").getString("formatted_address");
                if(name.contains(", India") || name.contains("India") || name.contains("india") || name.contains(", india" )|| name.contains(",India")|| name.contains(",india"))
                {
                    name=name.replace(",India","");
                }
            }
            else
            {
                name = (String)jObject.getJSONObject("result").getString("name");
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }

        hm.put("lat", Double.toString(lat));
        hm.put("lng", Double.toString(lng));
        hm.put("name", name);

        list.add(hm);
        return list;
    }
    }
