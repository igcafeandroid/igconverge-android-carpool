package com.info.igconverge.carpool;

import java.util.ArrayList;
import java.util.List;


/**

 * Created by lab2 on 24/12/15.
 */
public class CarpoolClass {
    String postId;
    User user;
    String startLocation;
    int noOfSeats;
    String carNo;
    int fare;
    boolean status;
    String depTime;
    String postedOn;
    String comments;

 ArrayList<String> days=new ArrayList<>();

    public CarpoolClass(String postId, User user, int noOfSeats, String startLocation, String carNo, int fare, boolean status, String depTime, String postedOn, String comments, ArrayList<String> days, ArrayList<OccupantsClass> occupantsList) {
        this.postId = postId;
        this.user = user;
        this.noOfSeats = noOfSeats;
        this.startLocation = startLocation;
        this.carNo = carNo;
        this.fare = fare;
        this.status = status;
        this.depTime = depTime;
        this.postedOn = postedOn;
        this.comments = comments;
        this.days = days;
        this.occupantsList = occupantsList;
    }

    public ArrayList<String> getDays() {
        return days;
    }

    public void setDays(ArrayList<String> days) {
        this.days = days;
    }

    ArrayList<OccupantsClass> occupantsList=new ArrayList<OccupantsClass>();
    public CarpoolClass(){

    }

    public ArrayList<OccupantsClass> getOccupantsList() {
        return occupantsList;
    }

    public void setOccupantsList(ArrayList<OccupantsClass> occupantsList) {
        this.occupantsList = occupantsList;

    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;

    }



    public int getFare() {
        return fare;
    }

    public void setFare(int fare) {
        this.fare = fare;
    }


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDepTime() {
        return depTime;
    }

    public void setDepTime(String depTime) {
        this.depTime = depTime;

    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }


    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }




}
