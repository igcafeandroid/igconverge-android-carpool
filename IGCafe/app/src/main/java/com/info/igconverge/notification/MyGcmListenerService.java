package com.info.igconverge.notification;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GcmListenerService;
import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;

import com.info.igconverge.login.IGCAuthenticationActivity;
import com.info.igconverge.utility.IGCConstants;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

public class MyGcmListenerService extends GcmListenerService {

	private static final String TAG = "MyGcmListenerService";
	int eventNotificationId = 001;
	int notificationId = 1001;
	int carPoolNotificationId = 10001;
	List<String> notificationMessageList = new ArrayList<String>();
	List<String> carPoolNotMsgList = new ArrayList<String>();

	List<String> eventMessageList = new ArrayList<String>();
	int notificationCount = 0, eventCount = 0,carPoolCount=0;

	@Override
	public void onMessageReceived(String from, Bundle data) {
		IGCafeApplication.mSensorManager
				.unregisterListener(IGCafeApplication.mShakeDetector);

		notificationMessageList = IGCConstants.notificationMessageList;
		eventMessageList = IGCConstants.eventMessageList;
		carPoolNotMsgList=IGCConstants.carPoolNotificationMessageList;
		String title = null, message = null, type = null;
		try {
			String notificationData = data.getString("notification");
			JSONObject jsonObj = new JSONObject(notificationData);
			type = jsonObj.getString("category");
			title = jsonObj.getString("title");
			message = jsonObj.getString("message");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sendNotification(title, type, message);
	}

	private void sendLocationBroadcast(Intent intent) {

		Bundle bundle = new Bundle();
		// bundle.putBoolean("isNoidaLocation", isNoidaLocation);
		// bundle.putParcelable("weather", weather);
		// intent.putExtra("weather", bundle);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	private void sendNotification(String title, String type, String message) {

		notificationCount = IGCConstants.notificationCount;
		eventCount = IGCConstants.eventCount;
		if (IGCafeApplication.isActivityVisible()) {
			if (type.equalsIgnoreCase("0") && IGCafeApplication.isNotVisible()) {
				Intent in = new Intent("notify");
				notificationCount = notificationCount + 1;
				in.putExtra("notificationCount", notificationCount);

				sendLocationBroadcast(in);
			} else if (type.equalsIgnoreCase("1")
					&& IGCafeApplication.isEventActivityVisible()) {
				Intent in = new Intent("notifyEvent");
				// notificationCount = notificationCount + 1;
				// in.putExtra("notificationCount", notificationCount);

				sendLocationBroadcast(in);
			} else {
				createNotification(title, type, message);

			}
		} else {
			createNotification(title, type, message);
		}

	}

	void createNotification(String title, String type, String message) {
		notificationCount = IGCConstants.notificationCount;
		carPoolCount=IGCConstants.carPoolCount;
		eventCount = IGCConstants.eventCount;
		String countMessage = null;
		PendingIntent pendingIntent;
		Intent intent = new Intent();

		intent = new Intent(this, IGCAuthenticationActivity.class);

		intent.putExtra("notificationCount", notificationCount);
		intent.putExtra("eventCount", eventCount);
		intent.putExtra("carPoolCount", carPoolCount);
		if (type.equalsIgnoreCase("0")) {
			notificationCount = notificationCount + 1;
			notificationMessageList.add(title);
			IGCConstants.notificationCount = notificationCount;
			if (notificationCount > 1)
				countMessage = notificationCount + " new Announcements";
			else if (notificationCount == 1)
				countMessage = notificationCount + " new Announcement";
			intent.setAction("com.info.igConverge" + notificationId);

			intent.putExtra("type", type);
			pendingIntent = PendingIntent.getActivity(this, notificationId,
					intent, PendingIntent.FLAG_ONE_SHOT);

		} else if (type.equalsIgnoreCase("2")) {

			carPoolCount = carPoolCount + 1;

			carPoolNotMsgList.add(title);


			IGCConstants.carPoolCount = carPoolCount;
			notificationMessageList=carPoolNotMsgList;

			if (carPoolCount > 1)
				countMessage = carPoolCount + " new Car Pool Updates";
			else if (eventCount == 1)
				countMessage = carPoolCount + " new Car Pool Update";
			intent.setAction("com.info.igConverge" + carPoolNotificationId);

			intent.putExtra("type", type);
			pendingIntent = PendingIntent.getActivity(this,
					carPoolNotificationId, intent, PendingIntent.FLAG_ONE_SHOT);

		}
		else  {

			eventCount = eventCount + 1;

			eventMessageList.add(title);
			IGCConstants.eventCount = eventCount;
			notificationMessageList = eventMessageList;
			if (eventCount > 1)
				countMessage = eventCount + " new Events";
			else if (eventCount == 1)
				countMessage = eventCount + " new Event";
			intent.setAction("com.info.igConverge" + eventNotificationId);

			intent.putExtra("type", type);
			pendingIntent = PendingIntent.getActivity(this,
					eventNotificationId, intent, PendingIntent.FLAG_ONE_SHOT);

		}

		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
		// | Intent.FLAG_ACTIVITY_CLEAR_TASK);

		// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Uri defaultSoundUri = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

		inboxStyle.setBigContentTitle(countMessage);

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
				getApplicationContext());
		notificationBuilder.setSmallIcon(R.drawable.app_icon)
				.setContentTitle(getResources().getString(R.string.app_name))
				.setStyle(inboxStyle)

				.setAutoCancel(true).setSound(defaultSoundUri)
				.setVibrate(new long[] { 100, 250, 100, 250, 100, 250 })

				.setContentIntent(pendingIntent);

		for (String name : notificationMessageList) {
			inboxStyle.addLine(name);
			notificationBuilder.setContentText(name);
		}

		notificationBuilder.setContentInfo(countMessage);

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (type.equalsIgnoreCase("0")) {
			notificationManager.notify(notificationId,
					notificationBuilder.build());

		}
		if (type.equalsIgnoreCase("2")) {
			notificationManager.notify(carPoolNotificationId,
					notificationBuilder.build());

		}else {
			notificationManager.notify(eventNotificationId,
					notificationBuilder.build());

		}

	}
}