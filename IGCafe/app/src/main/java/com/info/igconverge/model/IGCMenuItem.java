package com.info.igconverge.model;

public class IGCMenuItem {

	String item;
	String price;
	
	public IGCMenuItem() {
		super();
		
	}
	
	public IGCMenuItem(String item, String price) {
		super();
		this.item = item;
		this.price = price;
	}

	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
