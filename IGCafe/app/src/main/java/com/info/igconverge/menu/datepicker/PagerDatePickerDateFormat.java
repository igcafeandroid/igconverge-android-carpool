
package com.info.igconverge.menu.datepicker;


import java.text.SimpleDateFormat;

public interface PagerDatePickerDateFormat {

    SimpleDateFormat DATE_PICKER_DAY_FORMAT = new SimpleDateFormat("dd");
    SimpleDateFormat DATE_PICKER_MONTH_NAME_FORMAT = new SimpleDateFormat("MMM");
    SimpleDateFormat DATE_PICKER_DAY_NAME_FORMAT = new SimpleDateFormat("EEE");

    SimpleDateFormat DATE_PICKER_DD_MM_YYYY_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
}
