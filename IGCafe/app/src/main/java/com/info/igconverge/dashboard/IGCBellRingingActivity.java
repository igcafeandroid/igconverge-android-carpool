package com.info.igconverge.dashboard;

import org.json.JSONException;
import org.json.JSONObject;

import com.info.igconverge.R;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.Services.ParseServiceRequest;

import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class IGCBellRingingActivity extends Activity implements
		OnClickListener, IGCAsyncServiceRequest.OnAsyncRequestComplete {

	String stringStatus, stringMessage, bellCount;
	private ImageView mPendulum;
	MediaPlayer mp;
	int ratingValue = 0;
	private Animation mAnimation;
	Button ringButton, cancelBellButton;
	public static IGCBellRingingActivity activity;
	private Typeface myTypefaceMedium, myTypefaceRegular,
			myTypefaceMediumItalic;
	TextView ringBellText, likeFoodText;
	String mealId, mealType, feedbackDate;
	int RESULT_OK = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		setContentView(R.layout.activity_bell_ringing);
		mPendulum = (ImageView) findViewById(R.id.bellImage);
		mp = MediaPlayer.create(this, R.raw.bell);
		myTypefaceMedium = Typeface.createFromAsset(getAssets(),
				"font/Roboto-Medium.ttf");
		myTypefaceMediumItalic = Typeface.createFromAsset(getAssets(),
				"font/Roboto-MediumItalic.ttf");
		myTypefaceRegular = Typeface.createFromAsset(getAssets(),
				"font/Roboto-Regular.ttf");
		mAnimation = AnimationUtils.loadAnimation(this, R.anim.pendulum);
		mAnimation.reset();
		ringButton = (Button) findViewById(R.id.ringOkButton);
		ringButton.setOnClickListener(this);

		cancelBellButton = (Button) findViewById(R.id.ringCancelButton);
		cancelBellButton.setOnClickListener(this);
		ringButton.setTypeface(myTypefaceRegular);
		cancelBellButton.setTypeface(myTypefaceRegular);
		ringBellText = (TextView) findViewById(R.id.ringBellText);
		likeFoodText = (TextView) findViewById(R.id.likeFoodText);
		likeFoodText.setTypeface(myTypefaceRegular);
		likeFoodText.setText("\""
				+ getResources().getString(R.string.ring_bell_likefood) + "\"");
		ringBellText.setTypeface(myTypefaceMediumItalic);

		if (getIntent().getExtras() != null) {

			mealId = getIntent().getExtras().getString("mealId");
			mealType = getIntent().getExtras().getString("mealType");
			feedbackDate = getIntent().getExtras()
					.getString("mealDateFeedback");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		// mPendulum.startAnimation(mAnimation);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ringOkButton:

			submitFeedback();
			break;
		case R.id.ringCancelButton:
			finish();
			break;
		default:
			break;
		}

		// asound.startsound();

	}

	private void submitFeedback() {
		// TODO Auto-generated method stub

		if (IGCCheckNetworkConnectivity.checkConnection(this)) {
			mPendulum.startAnimation(mAnimation);
			// mp.start();
			ringButton.setOnClickListener(null);
			String employeeId = "";
			if (IGCSharedPreferences.getUserId(getApplicationContext()) != null) {
				employeeId = IGCSharedPreferences
						.getUserId(getApplicationContext());
			}

			IGCAsyncServiceRequest getPosts = new IGCAsyncServiceRequest(this,
					"POST", ParseServiceRequest.sendRingBellparams(mealId,
							employeeId));
			getPosts.execute(IGCConstants.ringBellURL);

		} else {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.no_internet_body),
					Toast.LENGTH_SHORT).show();
			ringButton.setOnClickListener(this);
		}

	}

	@Override
	public void asyncResponse(String response) {
		// TODO Auto-generated method stub
		Handler handler = new Handler();
		Runnable r = new Runnable() {
			public void run() {
				mPendulum.clearAnimation();
				Intent returnIntent = new Intent();
				returnIntent.putExtra("result", bellCount);
				setResult(RESULT_OK, returnIntent);
				// finish();
				finish();

			}
		};
		handler.postDelayed(r, 1000);

		if (response != null && response.length() > 0) {
			try {

				parseRingBellStatus(response);

				if (stringStatus != null && stringStatus.length() > 0) {
					if (stringStatus.contains("success")) {
						mp.start();
					}

					Toast.makeText(getApplicationContext(), stringMessage,
							Toast.LENGTH_SHORT).show();

				} else {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.server_error),
							Toast.LENGTH_SHORT).show();
					ringButton.setOnClickListener(this);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			ringButton.setOnClickListener(this);
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.server_error),
					Toast.LENGTH_SHORT).show();

		}
	}

	private boolean parseRingBellStatus(String result) {
		try {
	
			JSONObject jsonObject = new JSONObject(result);
			if (jsonObject != null) {
				if (!jsonObject.isNull("status")) {
					stringStatus = jsonObject.getString("status");
				}
				if (!jsonObject.isNull("message")) {
					stringMessage = jsonObject.getString("message");
				}

				if (!jsonObject.isNull("bellCount")) {
					bellCount = jsonObject.getString("bellCount");
					IGCConstants.bellCount = bellCount;
			
				}
			}
			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
