package com.info.igconverge.emergency;


import org.json.JSONException;
import org.json.JSONObject;
import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.Services.ParseServiceRequest;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.utility.CustomGestureDetector;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class IGCEmergencyActivity extends Activity implements
IGCAsyncServiceRequest.OnAsyncRequestComplete, View.OnClickListener {
	GPSTracker gps;
	RelativeLayout distressLayout, reachHomeSafelyLayout,
	breakdownAssistanceLayout;
	ImageView distressImage, reachHomeSafelyImage, breakdownAssistanceImage;

	String stringStatus, stringMessage;
	public boolean isRetry = false;
	public int retryCount = 0;
	double latitude = 0, longitude = 0;
	public static IGCEmergencyActivity activity;
	RelativeLayout reLayoutcallAdmin;
	private GestureDetector mGestureDetector;

	int emergencyType;
	ImageButton sosImageButton;
	Typeface myTypefaceMedium, myTypefaceRegular;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.emergency_screen);
		CustomGestureDetector customGestureDetector = new CustomGestureDetector();

		mGestureDetector = new GestureDetector(this, customGestureDetector);

		mGestureDetector.setOnDoubleTapListener(customGestureDetector);
		activity = this;
		distressImage = (ImageView) findViewById(R.id.imageEmergencyTypeDistress);
		reachHomeSafelyImage = (ImageView) findViewById(R.id.imageEmergencyTypeReachHome);
		breakdownAssistanceImage = (ImageView) findViewById(R.id.imageEmergencyTypeBreakDown);

		distressLayout = (RelativeLayout) findViewById(R.id.relativeEmergencyDistress);
		reachHomeSafelyLayout = (RelativeLayout) findViewById(R.id.relativeEmergencyReachHome);
		breakdownAssistanceLayout = (RelativeLayout) findViewById(R.id.relativeEmergencyBreakDown);

		distressLayout.setOnClickListener(this);
		reachHomeSafelyLayout.setOnClickListener(this);
		breakdownAssistanceLayout.setOnClickListener(this);
		myTypefaceMedium = Typeface.createFromAsset(this.getAssets(),
				"font/Roboto-Medium.ttf");

		myTypefaceRegular = Typeface.createFromAsset(this.getAssets(),
				"font/Roboto-Regular.ttf");
		sosImageButton = (ImageButton) findViewById(R.id.sos_imageButton);
		sosImageButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {

					if(IGCUtils.isTelephonyEnabled(activity))
					{
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:"+ IGCConstants.adminContactNumber));
						startActivity(callIntent);
					}
					else
					{
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_call), Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				sosClicked();

			}
		});

		reLayoutcallAdmin = (RelativeLayout) findViewById(R.id.relativeCallButton);

		reLayoutcallAdmin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				/*Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:"
						+ IGCConstants.adminContactNumber));
				startActivity(callIntent);*/
				try {

					if(IGCUtils.isTelephonyEnabled(activity))
					{
						Intent callIntent = new Intent(Intent.ACTION_CALL);
						callIntent.setData(Uri.parse("tel:"+ IGCConstants.adminContactNumber));
						startActivity(callIntent);
					}
					else
					{
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_call), Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		int screenHeight = this.getResources().getDisplayMetrics().heightPixels;
		int height = IGCUtils.convertDpToPixel(this, 200);
		distressImage.getLayoutParams().height = (screenHeight - height) / 3;
		reachHomeSafelyImage.getLayoutParams().height = (screenHeight - height) / 3;
		breakdownAssistanceImage.getLayoutParams().height = (screenHeight - height) / 3;
	}

	@Override
	protected void onResume() {

		super.onResume();
		if (retryCount == 1) {
			retryCount = 0;
			isRetry = false;
			gps = new GPSTracker(this);

			if (gps.canGetLocation()) {

				confirmDialogEmergencyCall();
			} else {
				isRetry = true;
				gps.showSettingsAlert();
			}
		}
	}

	@Override
	protected void onPause() {

		super.onPause();

		if (isRetry) {
			isRetry = true;
			retryCount = 1;
		}

		IGCafeApplication.mSensorManager
		.registerListener(IGCafeApplication.mShakeDetector,
				IGCafeApplication.mAccelerometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	private void checkLocation() {
		gps = new GPSTracker(this);
		if (gps.canGetLocation()) {

			latitude = gps.getLatitude();
			longitude = gps.getLongitude();
			confirmDialogEmergencyCall();
		} else {

			isRetry = true;
			gps.showSettingsAlert();
		}

	}

	@Override
	public void asyncResponse(String response) {
		// TODO Auto-generated method stub
		if (response != null && response.length() > 0) {
			try {

				parseEmergencyStatus(response);
				if (stringStatus != null && stringStatus.length() > 0) {
					if (emergencyType == 2) {
						Toast.makeText(getApplicationContext(), stringMessage,
								Toast.LENGTH_LONG).show();
					} else {
						ErrorDialog dialog = new ErrorDialog(this,
								stringMessage, true);
						dialog.setCancelable(false);
						dialog.show();
					}

				} else {
					ErrorDialog dialog = new ErrorDialog(activity,
							"SERVER_ERROR");
					dialog.setCancelable(false);
					dialog.show();

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "SERVER_ERROR");
			dialog.setCancelable(false);
			dialog.show();

		}
	}

	private boolean parseEmergencyStatus(String result) {
		try {
			JSONObject jsonObject = new JSONObject(result);
			if (jsonObject != null) {
				if (!jsonObject.isNull("status")) {
					stringStatus = jsonObject.getString("status");
				}
				if (!jsonObject.isNull("message")) {
					stringMessage = jsonObject.getString("message");
				}

			}
			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void confirmDialogEmergencyCall() {
		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(this);
		dialogBuilder.setTitleViewGone(true).withTitle("My Dialog")

		.withTitleColor("#FFFFFF")

		.withDividerColor("#11000000")

		.withMessage("This is a modal Dialog.")

		.setMessageViewGone(true).withMessageColor("#FFFFFFFF")

		.withDialogColor(Color.TRANSPARENT)

		.isCancelableOnTouchOutside(false)

		.withDuration(300)

		.withEffect(Effectstype.Slidetop)
		.setCustomView(R.layout.delete_image_dialog,
				getApplicationContext()).show();
		TextView deleteMessage = (TextView) dialogBuilder
				.findViewById(R.id.deleteMessage);
		TextView titleMessage = (TextView) dialogBuilder
				.findViewById(R.id.titleMessage);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okDeletePhoto);
		TextView cancelDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.cancelDeletePhoto);
		titleMessage.setTypeface(myTypefaceMedium);
		deleteMessage.setTypeface(myTypefaceRegular);
		okDeletePhoto.setText(this.getResources().getString(R.string.yes));
		deleteMessage.setVisibility(View.VISIBLE);
		titleMessage.setVisibility(View.VISIBLE);
		deleteMessage.setTextColor(this.getResources().getColor(
				R.color.blackcolor));
		if (emergencyType == 0) {
			titleMessage.setText(this.getResources().getString(
					R.string.confirm_title_reach_home));
			deleteMessage.setText(this.getResources().getString(
					R.string.confirm_message_reach_home));
		} else if (emergencyType == 1) {

			titleMessage.setText(this.getResources().getString(
					R.string.confirm_title_breakdown));
			deleteMessage.setText(this.getResources().getString(
					R.string.confirm_message_breakdown));
		} else if (emergencyType == 2) {
			titleMessage.setText(this.getResources().getString(
					R.string.confirm_title_distress));
			deleteMessage.setText(this.getResources().getString(
					R.string.confirm_message_distress));
		}

		cancelDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
			}
		});
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
				serviceEmergencyCall("");

			}
		});

	}

	public void serviceEmergencyCall(String comment) {
		if (IGCCheckNetworkConnectivity.checkConnection(this)) {

			String employeeName = "";
			String employeeId = "";
			if (IGCSharedPreferences.getProfileName(this) != null) {
				employeeName = IGCSharedPreferences.getProfileName(this);
			}
			if (IGCSharedPreferences.getUserId(this) != null) {
				employeeId = IGCSharedPreferences.getUserId(this);
			}
			IGCAsyncServiceRequest getPosts = new IGCAsyncServiceRequest(this,
					"POST", ParseServiceRequest.emergencyRequestParams(
							employeeId, emergencyType, latitude, longitude,
							employeeName, comment));
			getPosts.execute(IGCConstants.emergencyServiceURL);

		} else {
			ErrorDialog dialog = new ErrorDialog(this, "NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.relativeEmergencyReachHome:
			reachHomeClicked();
			break;
		case R.id.relativeEmergencyBreakDown:
			assistanceRequired();

			break;
		case R.id.relativeEmergencyDistress:
			distressClicked();
			break;
		default:
			break;
		}

	}

	private void assistanceRequired() {

		emergencyType = 1;

		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(this);
		dialogBuilder.setTitleViewGone(true).withTitle("My Dialog")

		.withTitleColor("#FFFFFF")

		.withDividerColor("#11000000")

		.withMessage("This is a modal Dialog.")

		.setMessageViewGone(true).withMessageColor("#FFFFFFFF")

		.withDialogColor(Color.TRANSPARENT)

		.isCancelableOnTouchOutside(false)

		.withDuration(300)

		.withEffect(Effectstype.Slidetop)
		.setCustomView(R.layout.assistance_dialog_layout,
				getApplicationContext()).show();
		TextView shareComment = (TextView) dialogBuilder
				.findViewById(R.id.shareCommnet);
		TextView cancelComment = (TextView) dialogBuilder
				.findViewById(R.id.cancelComment);
		final EditText assistanceComment = (EditText) dialogBuilder
				.findViewById(R.id.assisstanceComment);

		cancelComment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogBuilder.dismiss();
			}
		});
		shareComment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String comment = assistanceComment.getText().toString();
				if (comment != null && comment.length() > 0) {

					dialogBuilder.dismiss();

					serviceEmergencyCall(comment);

				} else {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.emergency_empty),
							Toast.LENGTH_LONG).show();
				}

			}
		});

	}



	public void reachHomeClicked() {
		emergencyType = 0;
		confirmDialogEmergencyCall();
	}

	public void distressClicked() {
		emergencyType = 2;
		gps = new GPSTracker(IGCEmergencyActivity.this);
		if (gps.canGetLocation()) {

			latitude = gps.getLatitude();
			longitude = gps.getLongitude();
		}
		confirmDialogEmergencyCall();
	}

	public void sosClicked() {
		emergencyType = 2;
		gps = new GPSTracker(IGCEmergencyActivity.this);
		if (gps.canGetLocation()) {

			latitude = gps.getLatitude();
			longitude = gps.getLongitude();
		}
		serviceEmergencyCall("");
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		mGestureDetector.onTouchEvent(event);
		return super.dispatchTouchEvent(event);
	}

}
