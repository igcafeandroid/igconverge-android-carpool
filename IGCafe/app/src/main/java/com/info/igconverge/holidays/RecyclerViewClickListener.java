package com.info.igconverge.holidays;

import android.view.View;

public interface RecyclerViewClickListener
{

     public void recyclerViewListClicked(View v, int position);
}
