package com.info.igconverge.dialogs;

import com.info.igconverge.R;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.AvoidXfermode.Mode;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressCustomDialog extends ProgressDialog {
	private AnimationDrawable animation;
	// private Animation animation;
	Context context;
	ImageView animateImg1, animateImg2, animateImg3;
	int delay = 0;
	ProgressBar mProgressBar;

	public static ProgressDialog ctor(Context context) {

		ProgressCustomDialog dialog = new ProgressCustomDialog(context);

		dialog.setCancelable(true);
		dialog.setIndeterminate(true);
		return dialog;
	}

	public ProgressCustomDialog(Context context) {

		super(context);
		this.context = context;
	}

	public ProgressCustomDialog(Context context, int theme) {

		super(context, theme);
		this.context = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setCancelable(false);

		setContentView(R.layout.view_custom_progress_dialog);
		mProgressBar = (ProgressBar) findViewById(R.id.google_progress);
		Rect bounds = mProgressBar.getIndeterminateDrawable().getBounds();
		getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
	
		Drawable progressDrawable = new ChromeFloatingCirclesDrawable.Builder(
				context).colors(getProgressDrawableColors()).build();
		mProgressBar.setIndeterminateDrawable(progressDrawable);
		mProgressBar.getIndeterminateDrawable().setBounds(bounds);
	}

	private int[] getProgressDrawableColors() {
		int[] colors = new int[4];
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		colors[0] = prefs.getInt(context
				.getString(R.string.firstcolor_pref_key), context
				.getResources().getColor(R.color.red));
		colors[1] = prefs.getInt(context
				.getString(R.string.secondcolor_pref_key), context
				.getResources().getColor(R.color.blue));
		colors[2] = prefs.getInt(context
				.getString(R.string.thirdcolor_pref_key), context
				.getResources().getColor(R.color.yellow));
		colors[3] = prefs.getInt(context
				.getString(R.string.fourthcolor_pref_key), context
				.getResources().getColor(R.color.green));
		return colors;
	}

	@Override
	public void show() {

		super.show();
	}

	@Override
	public void dismiss() {

		super.dismiss();
		// animateImg.setAnimation(null);
		// animation.stop();

	}

	public void onBackPressed() {
	

	}

}