package com.info.igconverge.Services;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

import com.info.igconverge.R;
import com.info.igconverge.utility.IGCConstants;

public class ParseServiceRequest {

	public static ArrayList<NameValuePair> getLoginParams(String username,String password,String deviceType,String version) {
		// define and ArrayList whose elements are of type NameValuePair
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("userName", username));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("deviceType", deviceType));
		params.add(new BasicNameValuePair("version", version));
	
	
		return params;
	}

	public static ArrayList<NameValuePair> getLogoutParams(String deviceToken, String deviceType) {

		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("deviceToken", deviceToken));
		params.add(new BasicNameValuePair("deviceType", deviceType));

		return params;
	}
	public static ArrayList<NameValuePair> sendRingBellparams(String mealId, String employeeId)
	{
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("mealId", mealId));
		params.add(new BasicNameValuePair("employeeId", employeeId));

		return params;
	}
	public static ArrayList<NameValuePair> emergencyRequestParams(String id, int  type,double latitude,double longitude,String name,String comment)
	{
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", id));
		params.add(new BasicNameValuePair("type", String.valueOf(type)));
		params.add(new BasicNameValuePair("latitude", String.valueOf(latitude)));
		params.add(new BasicNameValuePair("longitude", String.valueOf(longitude)));
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("info", comment));
		
		return params;
	}
	public static MultipartEntity getPostFeedBackParams(String mealId, String employeeName,String feedbackComment,String ratingValue,String mediaFile) {
		MultipartEntity reqEntity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);
		try {
			if(mealId!=null)
			{
			reqEntity.addPart("mealId", new StringBody(mealId));
			}
			if(IGCConstants.employeeDetailObject.getId()!=null)
			{
			reqEntity.addPart("employeeId", new StringBody(IGCConstants.employeeDetailObject.getId()));
			}
			if(employeeName!=null)
			{
			reqEntity.addPart("employeeName", new StringBody(employeeName));
			}
			if(feedbackComment!=null)
			{
			reqEntity.addPart("feedbackComment", new StringBody(feedbackComment));
			}
			if(ratingValue!=null)
			{
			reqEntity.addPart("feedbackRating",new StringBody(ratingValue));
			}
			if (mediaFile != null)
				reqEntity.addPart("mealImage",new FileBody(new File(mediaFile)));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reqEntity;
	}

}
