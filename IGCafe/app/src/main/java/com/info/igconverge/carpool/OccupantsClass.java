package com.info.igconverge.carpool;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by lab1 on 28/12/15.
 */

public class OccupantsClass implements Parcelable{
    String userId;

    String name;
    String email;
String status;
    public OccupantsClass() {

    }

    public OccupantsClass(Parcel in) {

       userId= in.readString();
        name=in.readString();
        email=in.readString();
        status=in.readString();
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public OccupantsClass(String userId, String name, String email, String status) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(userId);
        out.writeString(name);
        out.writeString(email);
        out.writeString(status);
    }


    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<OccupantsClass> CREATOR = new Parcelable.Creator<OccupantsClass>() {
        public OccupantsClass createFromParcel(Parcel in) {
            return new OccupantsClass(in);
        }

        public OccupantsClass[] newArray(int size) {
            return new OccupantsClass[size];
        }
    };

}
