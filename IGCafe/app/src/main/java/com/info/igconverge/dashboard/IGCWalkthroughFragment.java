package com.info.igconverge.dashboard;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.info.igconverge.R;
import com.info.igconverge.walkthrough.IGCCirclePageIndicator;
import com.info.igconverge.walkthrough.IGCViewPagerAdapter;

public class IGCWalkthroughFragment extends Fragment {
	IGCDashboardActivity activity;
	ViewPager viewPager;
	IGCCirclePageIndicator mIndicator;
	PagerAdapter adapter;

	String[] title;
	int[] walkImages;
	int[] backgroundColorsWalkthrough;
	RelativeLayout mainPagerLayout;
	TextView walkThroughTitle;
	View rootView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = (IGCDashboardActivity) activity;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.viewpager_main, container,
				false);
		

		initActionBar();
		initViews();
	

		mIndicator.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int page) {
				// page changed

				mainPagerLayout.setBackgroundColor(backgroundColorsWalkthrough[page]);
				activity.actionBar.setBackgroundDrawable(new ColorDrawable(backgroundColorsWalkthrough[page]));
				walkThroughTitle.setText(title[page]);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		mIndicator.setViewPager(viewPager);
		return rootView;
	}


	private void initViews() {
		// TODO Auto-generated method stub
		backgroundColorsWalkthrough = activity.getResources().getIntArray(
				R.array.backgroundColorsWalkthrough);
		mainPagerLayout = (RelativeLayout) rootView.findViewById(R.id.mainPagerLayout);
		mIndicator = (IGCCirclePageIndicator) rootView.findViewById(R.id.indicatorWalkThrough);
		viewPager = (ViewPager) rootView.findViewById(R.id.pagerWalkThrough);

		walkThroughTitle = (TextView) rootView.findViewById(R.id.walkThroughTitle);
		title = getResources().getStringArray(R.array.titlesWalkthrough);
		walkImages = new int[] { R.drawable.walk_splash,R.drawable.walk_cafeteria,R.drawable.walk_ring_bell,R.drawable.walk_events,
				R.drawable.walk_party, R.drawable.walk_holidays,
				R.drawable.walk_contact ,R.drawable.walk_weather,R.drawable.walk_emergency,R.drawable.walk_reportbug};

		final float density = getResources().getDisplayMetrics().density;
		mIndicator.setRadius(4* density);
		mIndicator.setPageColor(getResources().getColor(R.color.greycolor));
		mIndicator.setFillColor(getResources().getColor(R.color.whitecolor));
	

		// Pass results to ViewPagerAdapter Class
		adapter = new IGCViewPagerAdapter(activity,backgroundColorsWalkthrough, title, walkImages);
		// Binds the Adapter to the ViewPager
		viewPager.setAdapter(adapter);

		Typeface myTypefaceRegular = Typeface.createFromAsset(
				activity.getAssets(), "font/Roboto-Regular.ttf");
		walkThroughTitle.setTypeface(myTypefaceRegular);

		mainPagerLayout.setBackgroundColor(backgroundColorsWalkthrough[0]);
		activity.actionBar.setBackgroundDrawable(new ColorDrawable(backgroundColorsWalkthrough[0]));
		walkThroughTitle.setText(title[0]);

	}

	private void initActionBar() {
		// TODO Auto-generated method stub
		activity.actionBar.setDisplayHomeAsUpEnabled(true);
		activity.actionBar.setHomeButtonEnabled(true);
		activity.actionBar.setDisplayShowHomeEnabled(true);
		activity.actionBar.setBackgroundDrawable(null);
		activity.unlockNavigationDrawerSwipeGesture();

	}
}
