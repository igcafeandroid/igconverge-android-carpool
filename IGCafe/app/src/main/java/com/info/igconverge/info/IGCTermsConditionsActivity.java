package com.info.igconverge.info;

import com.info.igconverge.R;
import com.info.igconverge.dashboard.IGCDashboardActivity;
import com.info.igconverge.login.IGCLoginScreenActivity;
import com.info.igconverge.utility.CustomGestureDetector;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.instabug.library.Instabug;
import com.instabug.library.Instabug.IBGInvocationEvent;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;

public class IGCTermsConditionsActivity extends Activity implements
		OnClickListener {

	Activity activity;
	String flag = null;
	IGCLoginScreenActivity activity1;
	WebView webView;
	IGCSharedPreferences sharedPref = new IGCSharedPreferences();
	String type = null;
	private GestureDetector mGestureDetector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.terms);
		CustomGestureDetector customGestureDetector = new CustomGestureDetector();

		mGestureDetector = new GestureDetector(this, customGestureDetector);
	
		mGestureDetector.setOnDoubleTapListener(customGestureDetector);
		if (getIntent() != null) {
			type = getIntent().getStringExtra("type");
		}
		activity = this;
		Instabug.getInstance().setInvocationEvent(
				IBGInvocationEvent.IBGInvocationEventNone);

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			flag = (String) bundle.get("showButton");
		}

		Button agreeBtn = (Button) findViewById(R.id.agreeBtn);
		Button disagreeBtn = (Button) findViewById(R.id.disAgreeBtn);
		ImageButton cancelTermsScreen = (ImageButton) findViewById(R.id.cancelTermsScreen);
		if ("false".equalsIgnoreCase(flag)) {
			agreeBtn.setVisibility(View.GONE);
			disagreeBtn.setVisibility(View.GONE);
			cancelTermsScreen.setOnClickListener(this);
			cancelTermsScreen.setVisibility(View.VISIBLE);
		} else {
			agreeBtn.setOnClickListener(this);
			disagreeBtn.setOnClickListener(this);
			cancelTermsScreen.setVisibility(View.GONE);
		}
		webView = (WebView) findViewById(R.id.eulaWebViewFragment);
		webView.setVerticalScrollBarEnabled(false);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setBackgroundColor(Color.TRANSPARENT);
		webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webView.getSettings()
				.setRenderPriority(WebSettings.RenderPriority.HIGH);
		webView.loadUrl(getResources().getString(R.string.terms_html));

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.agreeBtn:
			
			IGCSharedPreferences.setUserName(activity,
					IGCConstants.employeeDetailObject.getUsername());
			IGCSharedPreferences.setPassword(activity,
					IGCConstants.employeeDetailObject.getPassword());
			IGCSharedPreferences.setUserId(activity,
					IGCConstants.employeeDetailObject.getId());
			if (IGCConstants.employeeDetailObject.getFirstName() != null) {
				IGCSharedPreferences.setFirstName(activity,
						IGCConstants.employeeDetailObject.getFirstName());
			}
			if (IGCConstants.employeeDetailObject.getMiddleName() != null) {
				IGCSharedPreferences.setMiddleName(activity,
						IGCConstants.employeeDetailObject.getMiddleName());
			}
			if (IGCConstants.employeeDetailObject.getLastName() != null) {
				IGCSharedPreferences.setLastName(activity,
						IGCConstants.employeeDetailObject.getLastName());
			}
			if (IGCConstants.employeeDetailObject.getMailId() != null) {
				IGCSharedPreferences.setEmail(activity,
						IGCConstants.employeeDetailObject.getMailId());
			}
			if (IGCConstants.employeeDetailObject.getFirstName() != null) {
				IGCConstants.profileName = IGCConstants.employeeDetailObject
						.getFirstName();
				if (IGCConstants.employeeDetailObject.getMiddleName() != null) {
					IGCConstants.profileName = IGCConstants.profileName + " "
							+ IGCConstants.employeeDetailObject.getMiddleName();
				}

				if (IGCConstants.employeeDetailObject.getLastName() != null) {
					IGCConstants.profileName = IGCConstants.profileName + " "
							+ IGCConstants.employeeDetailObject.getLastName();
				}

				IGCSharedPreferences.setProfileName(getApplicationContext(),
						IGCConstants.profileName);
			}
			Intent intent = new Intent(activity, IGCDashboardActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.putExtra("type", type);

			overridePendingTransition(0, 0);
			startActivity(intent);

			int bgColor = getResources().getColor(R.color.whitecolor);
			int orangeColor = getResources().getColor(R.color.orange);
			int transparentColor = getResources().getColor(R.color.transparent);
			Drawable headerBg = getResources()
					.getDrawable(R.drawable.header_bg);

			finish();
			if (IGCLoginScreenActivity.activity != null)
				IGCLoginScreenActivity.activity.finish();

			break;
		case R.id.cancelTermsScreen:

			finish();
			break;
		default:
			if (IGCLoginScreenActivity.activity != null)
				IGCLoginScreenActivity.activity.finish();
			finish();
		}

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
	
		mGestureDetector.onTouchEvent(event);
		
		return super.dispatchTouchEvent(event);
	}
}
