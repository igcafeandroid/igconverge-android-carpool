package com.info.igconverge.model;

public class IGCHolidaysList {
	String month;
	String date;
	String day;
	String occasion;
	boolean isOptional;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getOccasion() {
		return occasion;
	}
	public void setOccasion(String occasion) {
		this.occasion = occasion;
	}
	
	public boolean isOptional() {
		return isOptional;
	}
	public void setOptional(boolean isOptional) {
		this.isOptional = isOptional;
	}
	public IGCHolidaysList(String month, String occasion,String day,String date,boolean optional) {
		super();
		this.month = month;
		this.date = date;
		this.day = day;
		this.isOptional=optional;
		this.occasion = occasion;
	}
	
}
