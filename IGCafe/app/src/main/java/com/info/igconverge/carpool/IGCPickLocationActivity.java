package com.info.igconverge.carpool;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.info.igconverge.R;
import com.info.igconverge.utility.IGCConstants;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class IGCPickLocationActivity extends AppCompatActivity {

    EditText atvPlaces;
    ListView placesListView;
    ProgressBar searchProgress;
    ImageView googleLogo;
    Button clearBtn;
    DownloadTask placesDownloadTask;
    DownloadTask placeDetailsDownloadTask;
    ParserTask placesParserTask;
    ParserTask placeDetailsParserTask;
    final int PLACES = 0;
    final int PLACES_DETAILS = 1;
    int requestCode;
    static final int UPDATE_MAP_REQUEST = 1;
    static final int FARE_ESTIMATE_REQUEST = 2;
    static final int SET_DESTINATION = 3;
    static final int SET_RIDE_REQUEST = 111;
    static final int NO_PLACE_SELECTED = 10;

    IGCPickLocationAdapter locationAdapter;
    String location, destLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chpick_location);


        atvPlaces = (EditText) findViewById(R.id.atv_places);
        placesListView = (ListView) findViewById(R.id.places_listview);
        searchProgress = (ProgressBar) findViewById(R.id.searching);
        clearBtn = (Button) findViewById(R.id.clear_btn);
        clearBtn.setVisibility(View.INVISIBLE);
        googleLogo = (ImageView) findViewById(R.id.google_logo);
        // atvPlaces.setThreshold(1);
        if(getIntent().getExtras()!=null) {
            location = ((String) (getIntent().getExtras().get("location")));
            destLocation = ((String) (getIntent().getExtras().get("destLocation")));

            requestCode = ((int) (getIntent().getExtras().get("requestCode")));
        }
        if (location != null) {
            atvPlaces.setText(location);
            clearBtn.setVisibility(View.VISIBLE);
        } else if (destLocation != null) {
            atvPlaces.setText(destLocation);
            clearBtn.setVisibility(View.VISIBLE);
        }
        atvPlaces.setSelection(atvPlaces.length());
        if (requestCode == FARE_ESTIMATE_REQUEST || requestCode == SET_DESTINATION || requestCode == SET_RIDE_REQUEST) {
            atvPlaces.setHint(getResources().getString(R.string.str_destination_place));
        } else
            atvPlaces.setHint(getResources().getString(R.string.str_atv_places));

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                atvPlaces.setText("");
                clearBtn.setVisibility(View.INVISIBLE);
                placesListView.setVisibility(View.GONE);
                googleLogo.setVisibility(View.GONE);

            }
        });
      atvPlaces.setOnEditorActionListener(new EditText.OnEditorActionListener() {
          @Override
          public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

              if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                      actionId == EditorInfo.IME_ACTION_DONE ||
                      event.getAction() == KeyEvent.ACTION_DOWN &&
                              event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                 // if (!event.isShiftPressed()) {
                      // the user is done typing.

                      String value=atvPlaces.getText().toString();

                      if (value!=null)
                      {
                          Intent locationIntent = getIntent();
                          Bundle bundle = new Bundle();
                          bundle.putDouble("latitude", Double.parseDouble("0"));
                          bundle.putDouble("longitude", Double.parseDouble("0"));
                          bundle.putString("address", value);
                          locationIntent.putExtras(bundle);

                              setResult(RESULT_OK, locationIntent);

                          finish();
                          return true;
                      }
                      else {
                          setResult(NO_PLACE_SELECTED, null);
                          finish();
                          return true;
                      }
                  }
             // }
              return false; // pass on to other listeners.
          }
      });
        // Adding textchange listener
        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Creating a DownloadTask to download Google Places matching "s"

                if (s.toString().length() > 0) {
                    clearBtn.setVisibility(View.VISIBLE);

                    placesDownloadTask = new DownloadTask(PLACES);
                    Log.d("*******sss", s.toString());
                    // Getting url to the Google Places Autocomplete api
                    String url = getAutoCompleteUrl(s.toString());

                    // Start downloading Google Places
                    // This causes to execute doInBackground() of DownloadTask class
                    placesDownloadTask.execute(url);
                    searchProgress.setVisibility(View.VISIBLE);
                    clearBtn.setVisibility(View.GONE);
                    placesListView.setVisibility(View.GONE);
//                    googleLogo.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        // Setting an item click listener for the AutoCompleteTextView dropdown list
        placesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int index,
                                    long id) {
//
                Log.d("** on item click", "");

                ListView lv = (ListView) arg0;
                IGCPickLocationAdapter adapter = (IGCPickLocationAdapter) arg0.getAdapter();

                HashMap<String, String> hm = (HashMap<String, String>) adapter.getItem(index);

                // Creating a DownloadTask to download Places details of the selected place
                placeDetailsDownloadTask = new DownloadTask(PLACES_DETAILS);

                // Getting url to the Google Places details api
                String url = getPlaceDetailsUrl(hm.get("reference"));

                // Start downloading Google Place Details
                // This causes to execute doInBackground() of DownloadTask class
                placeDetailsDownloadTask.execute(url);

            }
        });
    }

    private String getAutoCompleteUrl(String place) {

        String key = "key=" + IGCConstants.GOOGLE_PLACE_API_KEY;

        String input = "input=" + place;

        String parameters = input + "&" + key;

        String output = "json";

/*
        String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
*/
        String url = IGCConstants.PLACES_URL + output + "?" + parameters;

        return url;
    }

    private String getPlaceDetailsUrl(String ref) {

        String key = "key=" +IGCConstants.GOOGLE_PLACE_API_KEY;
        Log.d("****ref", ref);
        String reference = "reference=" + ref;
        String sensor = "sensor=false";
        String parameters = reference + "&" + sensor + "&" + key;

        String output = "json";

        String url = IGCConstants.PLACES_DETAIL_URL + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        private int downloadType = 0;

        // Constructor
        public DownloadTask(int type) {
            this.downloadType = type;
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                Log.d("***url", url[0]);
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (downloadType) {
                case PLACES:
                    // Creating ParserTask for parsing Google Places
                    placesParserTask = new ParserTask(PLACES);

                    // Start parsing google places json data
                    // This causes to execute doInBackground() of ParserTask class
                    placesParserTask.execute(result);

                    break;

                case PLACES_DETAILS:
                    // Creating ParserTask for parsing Google Places
                    placeDetailsParserTask = new ParserTask(PLACES_DETAILS);

                    // Starting Parsing the JSON string
                    // This causes to execute doInBackground() of ParserTask class
                    placeDetailsParserTask.execute(result);
            }
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        int parserType = 0;

        public ParserTask(int type) {
            this.parserType = type;
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<HashMap<String, String>> list = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                switch (parserType) {
                    case PLACES:
                        IGCPlacesJsonParser placeJsonParser = new IGCPlacesJsonParser();
                        // Getting the parsed data as a List construct
                        list = placeJsonParser.parse(jObject);
                        break;
                    case PLACES_DETAILS:
                        IGCPlaceDetailsJsonParser placeDetailsJsonParser = new IGCPlaceDetailsJsonParser();
                        // Getting the parsed data as a List construct
                        list = placeDetailsJsonParser.parse(jObject);
                }

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return list;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            switch (parserType) {
                case PLACES:
                    String[] from = new String[]{"description"};
                    int[] to = new int[]{android.R.id.text1};
                    try {
                        // Creating a SimpleAdapter for the AutoCompleteTextView
                        searchProgress.setVisibility(View.GONE);
                        clearBtn.setVisibility(View.VISIBLE);
                        if (result != null && result.size() > 0) {
                            if (locationAdapter == null) {
                                locationAdapter = new IGCPickLocationAdapter(getApplicationContext(), result);
                                placesListView.setAdapter(locationAdapter);
                            } else {
                                locationAdapter.updatePlaces(result);
                                locationAdapter.notifyDataSetChanged();
                            }

                            placesListView.setVisibility(View.VISIBLE);
                            googleLogo.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case PLACES_DETAILS:
                    try {
                        HashMap<String, String> hm = result.get(0);

                        // Getting latitude from the parsed data
                        double latitude = Double.parseDouble(hm.get("lat"));
                        String location = hm.get("name");
                        // Getting longitude from the parsed data
                        double longitude = Double.parseDouble(hm.get("lng"));

                        Intent locationIntent = getIntent();
                        Bundle bundle = new Bundle();
                        bundle.putDouble("latitude", latitude);
                        bundle.putDouble("longitude", longitude);
                        bundle.putString("address", location);
                        locationIntent.putExtras(bundle);

                        Log.d("** on post latitude", String.valueOf(latitude));
                        if (requestCode == UPDATE_MAP_REQUEST) {
                            setResult(RESULT_OK, locationIntent);
                        } else if (requestCode == FARE_ESTIMATE_REQUEST) {

                            setResult(RESULT_OK, locationIntent);

                        } else if (requestCode == SET_DESTINATION) {

                            setResult(RESULT_OK, locationIntent);

                        } else if (requestCode == SET_RIDE_REQUEST) {

                            setResult(RESULT_OK, locationIntent);

                        }
                        finish();
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chpick_location, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Log.d("onBack", "presssed");
        String value=atvPlaces.getText().toString();

        if (value!=null)
        {

        }
        else {
            setResult(NO_PLACE_SELECTED, null);
            finish();
        }
        super.onBackPressed();
    }

}
