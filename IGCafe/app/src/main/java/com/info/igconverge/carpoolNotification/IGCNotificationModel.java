package com.info.igconverge.carpoolNotification;

/**
 * Created by nitika on 29/12/15.
 */
public class IGCNotificationModel {
    String carNo;
    String OccupantName;
    String OccupantStatus;
   String postId;
    String OccupantId;
    String OccupantEmail;
    public IGCNotificationModel(String carNo, String occupantName, String occupantStatus,String postId,String OccupantId,String OccupantEmail) {
        this.carNo = carNo;
        OccupantName = occupantName;
        OccupantStatus = occupantStatus;
        this.postId=postId;
        this.OccupantId=OccupantId;
        this.OccupantEmail=OccupantEmail;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public String getOccupantName() {
        return OccupantName;
    }

    public void setOccupantName(String occupantName) {
        OccupantName = occupantName;
    }

    public String getOccupantStatus() {
        return OccupantStatus;
    }

    public void setOccupantStatus(String occupantStatus) {
        OccupantStatus = occupantStatus;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getOccupantId() {
        return OccupantId;
    }

    public void setOccupantId(String occupantId) {
        OccupantId = occupantId;
    }

    public String getOccupantEmail() {
        return OccupantEmail;
    }

    public void setOccupantEmail(String occupantEmail) {
        OccupantEmail = occupantEmail;
    }
}
