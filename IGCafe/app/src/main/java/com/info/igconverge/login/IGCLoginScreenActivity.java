package com.info.igconverge.login;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PublicKey;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.info.igconverge.IGCafeApplication;
import com.info.igconverge.R;
import com.info.igconverge.Services.IGCAsyncServiceRequest;
import com.info.igconverge.Services.ParseServiceRequest;
import com.info.igconverge.Services.ServiceHandler;
import com.info.igconverge.customtoast.Configuration;
import com.info.igconverge.customtoast.Crouton;
import com.info.igconverge.customtoast.Style;
import com.info.igconverge.dialogeffects.Effectstype;
import com.info.igconverge.dialogs.ErrorDialog;
import com.info.igconverge.dialogs.NiftyDialogBuilder;
import com.info.igconverge.dialogs.ProgressCustomDialog;
import com.info.igconverge.info.IGCTermsConditionsActivity;
import com.info.igconverge.model.IGCEmployeeDetails;
import com.info.igconverge.notification.RegistrationIntentService;
import com.info.igconverge.utility.IGCCheckNetworkConnectivity;
import com.info.igconverge.utility.IGCConstants;
import com.info.igconverge.utility.IGCSharedPreferences;
import com.info.igconverge.utility.IGCUtils;
import com.instabug.library.Instabug;
import com.instabug.library.Instabug.IBGInvocationEvent;

@SuppressLint("SimpleDateFormat")
public class IGCLoginScreenActivity extends Activity implements
		OnClickListener, IGCAsyncServiceRequest.OnAsyncRequestComplete {

	EditText userNameInput;
	EditText passwordInput;
	TextView signInClick;
	ProgressCustomDialog progressCustomDialog;
	public static IGCLoginScreenActivity activity;
	ImageButton clearUsername, clearPassword;
	String stringStatus, stringMessage;
	int serviceResult = 0;
	String id;
	String username;
	String password;
	String firstName;
	String middleName;
	String lastName;
	String mailId;
	String imageName;
	private boolean isUpdateAvailable;
	private String updateTitle;
	private String updateMessage;
	private String updateUrl;

	private Typeface myTypefaceMedium;
	private Typeface myTypefaceRegular;
	private Boolean isMandatory;
	String type = null;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		IGCafeApplication.mSensorManager
				.unregisterListener(IGCafeApplication.mShakeDetector);
//		if (getIntent() != null) {
//
//			type = getIntent().getStringExtra("type");
//			if(type!=null)
//			{
//				if (IGCSharedPreferences.getUserName(getApplicationContext()) != null
//						&& IGCSharedPreferences
//								.getPassword(getApplicationContext()) != null
//						&& IGCSharedPreferences
//								.getUserName(getApplicationContext()).length() > 0
//						&& IGCSharedPreferences.getPassword(
//
//						getApplicationContext()).length() > 0) {
//					Intent intent = new Intent(this,
//							IGCAuthenticationActivity.class);
//					intent.putExtra("type", type);
//					startActivity(intent);
//					overridePendingTransition(0, 0);
//
//				}
//			}
//			
//		}
		activity = this;
		Instabug.getInstance().setInvocationEvent(
				IBGInvocationEvent.IBGInvocationEventNone);
		// Sets the View Layer
		setContentView(R.layout.activity_login_layout);
		checkNetworkStatus("load");

		userNameInput = (EditText) findViewById(R.id.userNameInput);
		passwordInput = (EditText) findViewById(R.id.passwordInput);
		clearPassword = (ImageButton) findViewById(R.id.clearPassword);
		clearUsername = (ImageButton) findViewById(R.id.clearUsername);
		clearPassword.setOnClickListener(this);
		clearUsername.setOnClickListener(this);
		clearUsername.setVisibility(View.INVISIBLE);
		clearPassword.setVisibility(View.INVISIBLE);
		signInClick = (TextView) findViewById(R.id.signInClick);
		signInClick.setOnClickListener(this);

		myTypefaceMedium = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Medium.ttf");

		myTypefaceRegular = Typeface.createFromAsset(activity.getAssets(),
				"font/Roboto-Regular.ttf");

		userNameInput.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() > 0) {
					clearUsername.setVisibility(View.VISIBLE);
				}
				if (s.length() == 0) {
					clearUsername.setVisibility(View.INVISIBLE);
				}
			}
		});
		passwordInput.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() > 0) {
					clearPassword.setVisibility(View.VISIBLE);
				}
				if (s.length() == 0) {
					clearPassword.setVisibility(View.INVISIBLE);
				}
			}
		});
		(passwordInput)
				.setOnEditorActionListener(new EditText.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub
						if (actionId == EditorInfo.IME_ACTION_DONE
								|| event.getAction() == KeyEvent.ACTION_DOWN
								&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

							// the user is done typing.
							hideKeyboard();
							username = userNameInput.getText().toString();
							password = passwordInput.getText().toString();

							checkNetworkStatus("signin");
							return true; // consume.

						}
						return false;
					}
				});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.signInClick:
			hideKeyboard();
			username = userNameInput.getText().toString();
			password = passwordInput.getText().toString();
			checkNetworkStatus("signin");
			break;
		case R.id.clearUsername:
			userNameInput.setText("");

			break;
		case R.id.clearPassword:
			passwordInput.setText("");
			break;
		default:
			break;
		}

	}

	private void saveLoginCredentials() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(activity, IGCTermsConditionsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.putExtra("type", type);

		startActivity(intent);
		overridePendingTransition(0, 0);

	}

	private void hideKeyboard() {

		View view = this.getCurrentFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
	
		imm.hideSoftInputFromWindow(view.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private void checkNetworkStatus(String callingView) {
		if (IGCCheckNetworkConnectivity.checkConnection(activity)) {
			if (callingView.equalsIgnoreCase("signin")) {
				username = userNameInput.getText().toString();
				password = passwordInput.getText().toString();
				if (username.contains("@infogain.com")) {
					username = username.replace("@infogain.com", "");
				}

				username = username.replaceAll("\\s+$", "");
				if (username != null && password != null
						&& username.length() > 0 && password.length() > 0) {
					try {
						String version = getPackageManager().getPackageInfo(
								getPackageName(), 0).versionName;
					
						String deviceType = IGCConstants.deviceType;
						IGCAsyncServiceRequest getPosts = new IGCAsyncServiceRequest(
								activity, "POST",
								ParseServiceRequest.getLoginParams(username,
										password, deviceType, version));
						getPosts.execute(IGCConstants.loginURL);

						if (IGCSharedPreferences.getPREF_DEVICEID(activity) != null) {
							
							Intent intent = new Intent(this,
									RegistrationIntentService.class);
							startService(intent);

						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ErrorDialog dialog = new ErrorDialog(activity,
							"Please enter valid username and password.",
							"Invalid Credentials!");
					dialog.setCancelable(false);
					dialog.show();

					passwordInput.setText("");
				}
			
			}
		} else {
			ErrorDialog dialog = new ErrorDialog(activity, "NO_INTERNET");
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	@SuppressLint("ResourceAsColor")
	private void showCustomCrouton(String message) {
		Configuration croutonConfiguration = new Configuration.Builder()
				.setDuration(2500).build();
		Style CUSTOM_STYLE = new Style.Builder()
				.setTextColor(R.color.whitecolor)
				.setBackgroundColorValue(
						getResources().getColor(R.color.orange)).setHeight(110)
				.setGravity(Gravity.CENTER_HORIZONTAL)
				.setConfiguration(croutonConfiguration).build();

		Crouton.makeText(IGCLoginScreenActivity.this, message, CUSTOM_STYLE)
				.show();
	}

	@SuppressLint("DefaultLocale")
	private void showCrouton(Style style, String stringToast) {

		Crouton.makeText(IGCLoginScreenActivity.this, stringToast, style)
				.show();
	}

	private boolean parseLoginStatus(String result) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObject = new JSONObject(result);
		
			if (jsonObject != null) {
				if (!jsonObject.isNull("status")) {
					stringStatus = jsonObject.getString("status");
				}
				if (!jsonObject.isNull("message")) {
					stringMessage = jsonObject.getString("message");
				}
				if (!jsonObject.isNull("updateResponse")) {

					JSONObject updateResponseJSONObject = jsonObject
							.getJSONObject("updateResponse");
					if (!updateResponseJSONObject.isNull("isUpdate")) {
						isUpdateAvailable = updateResponseJSONObject
								.getBoolean("isUpdate");
					}
					if (!updateResponseJSONObject.isNull("title")) {
						updateTitle = updateResponseJSONObject
								.getString("title");
					}
					if (!updateResponseJSONObject.isNull("message")) {
						updateMessage = updateResponseJSONObject
								.getString("message");
					}
					if (!updateResponseJSONObject.isNull("url")) {
						updateUrl = updateResponseJSONObject.getString("url");
					}
					if (isUpdateAvailable) {
						if (!updateResponseJSONObject.isNull("isMandatory")) {
							isMandatory = updateResponseJSONObject
									.getBoolean("isMandatory");
						}
					}
				}
				if (!jsonObject.isNull("aspireEmployee")) {

					JSONObject employeeJSONObject = jsonObject
							.getJSONObject("aspireEmployee");
					if (!employeeJSONObject.isNull("id")) {
						id = employeeJSONObject.getString("id");
					}
					if (!employeeJSONObject.isNull("firstName")) {
						firstName = employeeJSONObject.getString("firstName");
					}
					if (!employeeJSONObject.isNull("middleName")) {
						middleName = employeeJSONObject.getString("middleName");
					}
					if (!employeeJSONObject.isNull("lastName")) {
						lastName = employeeJSONObject.getString("lastName");
					}
					if (!employeeJSONObject.isNull("mailId")) {
						mailId = employeeJSONObject.getString("mailId");
					}
					if (!employeeJSONObject.isNull("imageName")) {
						imageName = employeeJSONObject.getString("imageName");
					}
					IGCConstants.employeeDetailObject = new IGCEmployeeDetails(
							id, username, password, firstName, middleName,
							lastName, mailId, imageName);
					return true;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public void asyncResponse(String response) {
		// TODO Auto-generated method stub
		if (response != null && response.length() > 0) {
			try {
				parseLoginStatus(response);
				if (stringStatus != null) {
					// isUpdateAvailable=false;
					if (firstName != null && firstName.length() > 0
							&& id != null && id.length() > 0) {

						if (isUpdateAvailable) {
							showUpdateDialog();
						} else {
							saveLoginCredentials();
						}
					} else if (stringMessage != null
							&& stringMessage.length() > 0) {
						showCustomCrouton(stringMessage);

					}
				} else {

					showCustomCrouton(getResources().getString(
							R.string.server_error));

					passwordInput.setText("");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			showCustomCrouton(getResources().getString(R.string.server_error));
			userNameInput.setText("");
			passwordInput.setText("");
		}
	}

	public void showUpdateDialog() {
		final NiftyDialogBuilder dialogBuilder = NiftyDialogBuilder
				.getInstance(this);
		dialogBuilder.setCancelable(false);
		dialogBuilder.setTitleViewGone(true).withTitle("My Dialog")
				// .withTitle(null) no title
				.withTitleColor("#FFFFFF")
				// def
				.withDividerColor("#11000000")
				// def
				.withMessage("This is a modal Dialog.")
				// .withMessage(null) no Msg
				.setMessageViewGone(true).withMessageColor("#FFFFFFFF")
				// def | withMessageColor(int resid)
				.withDialogColor(Color.TRANSPARENT)
				// def | withDialogColor(int resid) //def
				.isCancelableOnTouchOutside(false)
				// def | isCancelable(true)
				.withDuration(300)
				// def
				.withEffect(Effectstype.Slidetop)
				.setCustomView(R.layout.delete_image_dialog,
						getApplicationContext()).show();
		TextView deleteMessage = (TextView) dialogBuilder
				.findViewById(R.id.deleteMessage);
		TextView titleMessage = (TextView) dialogBuilder
				.findViewById(R.id.titleMessage);
		TextView okDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.okDeletePhoto);
		TextView cancelDeletePhoto = (TextView) dialogBuilder
				.findViewById(R.id.cancelDeletePhoto);
		titleMessage.setTypeface(myTypefaceMedium);
		deleteMessage.setTypeface(myTypefaceRegular);
		okDeletePhoto.setText("Update");
		titleMessage.setVisibility(View.VISIBLE);
		titleMessage.setText(updateTitle);
		deleteMessage.setText(updateMessage);
		cancelDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isMandatory) {
					dialogBuilder.dismiss();
					finish();
				} else {
					saveLoginCredentials();
				}
				dialogBuilder.dismiss();
			}
		});
		okDeletePhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (updateUrl != null && updateUrl.length() > 0) {
					Uri uri;
					if (updateUrl.contains("http")) {
						uri = Uri.parse(updateUrl);
					} else {
						updateUrl = "http://" + updateUrl;
						uri = Uri.parse(updateUrl);
					}
					
					Update installUpdate = new Update();
					installUpdate.execute(uri.toString());
				}
				dialogBuilder.dismiss();

			}
		});

	}

	private class Update extends AsyncTask<String, String, String> {
		ProgressBar updateProgress;

		private ProgressDialog pDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(activity);
		
			pDialog.show();
			pDialog.setContentView(R.layout.progress_dialog_update);
			pDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));

			pDialog.setCancelable(false);
			updateProgress = (ProgressBar) pDialog
					.findViewById(R.id.progressBarUpdate);
			
			updateProgress.setProgress(0);
			updateProgress.setMax(100);
			

		}

		@Override
		protected String doInBackground(String... urls) {

			String mediaFile = updateDownloadService(getApplicationContext(),
					urls[0].toString());

			return mediaFile;

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (pDialog != null && pDialog.isShowing()) {
				pDialog.dismiss();
			}
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(Environment
					.getExternalStorageDirectory() + "/download/" + result)),
					"application/vnd.android.package-archive");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);

			finish();
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values[0]);
			updateProgress.setProgress(Integer.parseInt(values[0]));
			
		}

		public String updateDownloadService(Context context,
				String urlserviceUpdate) {
			String mediaFile = "igConverge.apk";
			try {

				String PATH = Environment.getExternalStorageDirectory()
						+ "/download/";
				File file = new File(PATH);
				file.mkdirs();
				// Create a file on the external storage under download
				File outputFile = new File(file, mediaFile);
				FileOutputStream fos = new FileOutputStream(outputFile);

				HttpGet m_httpGet = null;
				HttpResponse m_httpResponse = null;

				// Create a http client with the parameters
				HttpClient m_httpClient = new DefaultHttpClient();
				String result = null;

				try {

					// Create a get object
					m_httpGet = new HttpGet(urlserviceUpdate);

					// Execute the html request
					m_httpResponse = m_httpClient.execute(m_httpGet);
					HttpEntity entity = m_httpResponse.getEntity();

					// See if we get a response
					if (entity != null) {

						InputStream instream = entity.getContent();
						long lengthOfFile = entity.getContentLength();
						byte[] buffer = new byte[1024];

						// Write out the file
						int len1 = 0;

						long total = 0;
						while ((len1 = instream.read(buffer)) != -1) {
							total += len1;
							publishProgress(""
									+ (int) ((total * 100) / lengthOfFile));

							fos.write(buffer, 0, len1);
						}
						fos.close();
						instream.close();// till here, it works fine - .apk is
											// download to my sdcard in download
											// file

					}

				} catch (ConnectTimeoutException cte) {
					Toast.makeText(getApplicationContext(), "Update error!",
							Toast.LENGTH_LONG).show();

				} catch (Exception e) {

				} finally {
					m_httpClient.getConnectionManager()
							.closeExpiredConnections();
				}

			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Update error!",
						Toast.LENGTH_LONG).show();
			}

			return mediaFile;
		}

	}

	@Override
	protected void onStop() {
		super.onStop();

	}

}